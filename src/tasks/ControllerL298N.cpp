/************************************************************************
 NestoRPi Engine
 Copyright (C) 2015-2025 Franck Talbart

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

/* Author: Franck Talbart */

/* This task is responsible for the L298N module only */

#include "src/tasks/ControllerL298N.hpp"

#include "src/core/util.hpp"

// Static variables
const std::string ControllerL298N::JSON_TASKS_L298N_ID = "actuator_L298N_id";
const std::string ControllerL298N::MSG_TASKS_MOTOR = "motor";
const std::string ControllerL298N::MSG_TASKS_RIGHT = "right";
const std::string ControllerL298N::MSG_TASKS_LEFT = "left";

//------------------------------------------------------------------------------
/**
 * @brief Constructor
 *
 * @param name: name of the task
 * @param cfg_task: map of json values (task config file)
 *
 */
ControllerL298N::ControllerL298N(const std::string &name, val_json_t &cfg_task)
    : Controller(name, cfg_task)
{
    try
    {
        L298N_id = cfg_task.at(JSON_TASKS_L298N_ID).toString();
    }
    catch (const std::out_of_range &oor)
    {
        NRPI_LOG(ERROR) << "L298N Controller: Some parameters are missing!";
        throw;
    }
}

//------------------------------------------------------------------------------
/**
 * @brief Init the task
 *
 * @param data_channel: the data channel where we send the data
 *
 */
void ControllerL298N::init(DataChannelObserver *data_channel)
{
    try
    {
        float val = dynamic_cast<ActuatorL298N *>(actuators.at(L298N_id).get())
                        ->get_action(ActuatorL298N::LEFT);
        this->send_msg_client({MSG_TASKS_MOTOR, MSG_TASKS_LEFT, util::float_to_str(val)},
                              data_channel);
        val = dynamic_cast<ActuatorL298N *>(actuators.at(L298N_id).get())
                  ->get_action(ActuatorL298N::RIGHT);
        this->send_msg_client({MSG_TASKS_MOTOR, MSG_TASKS_RIGHT, util::float_to_str(val)},
                              data_channel);
    }
    catch (const std::out_of_range &oor)
    {
        NRPI_LOG(ERROR) << "actuator not found";
    }
}

//------------------------------------------------------------------------------
/**
 * @brief Set the speed of a motor
 *
 * @param motor_loc: 0 => motor 1 => left / right 2 => -100 to 100
 * @return true if executed succesfully, false otherwhise
 */
bool ControllerL298N::run(const std::vector<std::string> &motor_loc)
{
    if (motor_loc.size() != 3)
        return false;

    float val = std::atof(motor_loc[2].c_str());
    val = (val > 100) ? 100 : val;
    val = (val < -100) ? -100 : val;

    if (motor_loc[0] == MSG_TASKS_MOTOR)
    {
        ActuatorL298N::motor motor_id;
        if (motor_loc[1] == MSG_TASKS_LEFT)
        {
            motor_id = ActuatorL298N::LEFT;
        }
        else if (motor_loc[1] == MSG_TASKS_RIGHT)
        {
            motor_id = ActuatorL298N::RIGHT;
        }
        else
        {
            return false;
        }

        try
        {
            dynamic_cast<ActuatorL298N *>(actuators.at(L298N_id).get())->set_action(motor_id, val);
            val = dynamic_cast<ActuatorL298N *>(actuators.at(L298N_id).get())->get_action(motor_id);
            this->send_msg_client({MSG_TASKS_MOTOR, motor_loc[1], util::float_to_str(val)});
        }
        catch (const std::out_of_range &oor)
        {
            NRPI_LOG(ERROR) << "actuator not found";
            return false;
        }
        return true;
    }
    return false;
}

//------------------------------------------------------------------------------
/**
 * @brief Reset every motors
 *
 *
 */
void ControllerL298N::reset(void)
{
    Controller::reset();
    try
    {
        dynamic_cast<ActuatorL298N *>(actuators.at(L298N_id).get())->reset();
        float val = dynamic_cast<ActuatorL298N *>(actuators.at(L298N_id).get())
                        ->get_action(ActuatorL298N::LEFT);
        this->send_msg_client({MSG_TASKS_MOTOR, MSG_TASKS_LEFT, util::float_to_str(val)});
        val = dynamic_cast<ActuatorL298N *>(actuators.at(L298N_id).get())
                  ->get_action(ActuatorL298N::RIGHT);
        this->send_msg_client({MSG_TASKS_MOTOR, MSG_TASKS_RIGHT, util::float_to_str(val)});
    }
    catch (const std::out_of_range &oor)
    {
        NRPI_LOG(ERROR) << "actuator not found";
    }
}
