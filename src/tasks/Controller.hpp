/************************************************************************
 NestoRPi Engine
 Copyright (C) 2015-2025 Franck Talbart

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

/* Author: Franck Talbart */

#ifndef DEF_CONTROLLER
#define DEF_CONTROLLER

#include "src/tasks/Task.hpp"

class Controller : public Task
{
  public:
    explicit Controller(const std::string &name, val_json_t &cfg_task);
    static void set_tasks(
        const std::map<std::string, std::shared_ptr<Controller>> &tasks_controller_l);
    bool set_action(const role_type role, const std::vector<std::string> &actions);
    virtual bool run(const std::vector<std::string> &actions) = 0;
    virtual ~Controller(void){};

    static const std::string JSON_TASKS_ROLE_MINIMAL;
    static const std::string JSON_TASKS_TYPE_VALUE;
    static const std::string MSG_TASKS_FORBIDDEN;

  protected:
    std::string get_type(void) const override { return JSON_TASKS_TYPE_VALUE; }
    static std::map<std::string, std::weak_ptr<Controller>> tasks_controller;

  private:
    role_type role_minimal;
};

typedef std::map<std::string, std::shared_ptr<Controller>> tasks_controller_t;
#endif // DEF_CONTROLLER
