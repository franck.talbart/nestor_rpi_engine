/************************************************************************
 NestoRPi Engine
 Copyright (C) 2015-2025 Franck Talbart

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

/* Author: Franck Talbart */

#ifndef DEF_AUTOMATION_PARKING
#define DEF_AUTOMATION_PARKING

#include "src/tasks/Automation.hpp"

class AutomationParking : public Automation
{
  public:
    explicit AutomationParking(const std::string &name, const val_json_t &cfg_task);
    void start_check_parking(const Device *s);
    void stop_check_parking(const Device *s);

  private:
    bool run(void) override;

    short cpt;
    std::atomic<short> sem;
    float voltage_station;
    int wait_before_park;
    gason::JsonValue on_stop;
    std::string parking_id, voltage_id, relay_id;

    static const std::string JSON_TASKS_ON_STOP;
    static const std::string JSON_TASKS_VOLTAGE_STATION;
    static const std::string JSON_TASKS_WAIT_BEFORE_PARK;

    static const std::string JSON_TASKS_PARKING_ID;
    static const std::string JSON_TASKS_LIGHT_ID;
    static const std::string JSON_TASKS_VOLTAGE_ID;
    static const std::string JSON_TASKS_RELAY_ID;
};
#endif // DEF_AUTOMATION_PARKING
