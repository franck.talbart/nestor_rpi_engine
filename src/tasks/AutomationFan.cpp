/************************************************************************
 NestoRPi Engine
 Copyright (C) 2015-2025 Franck Talbart

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

/* Author: Franck Talbart */

/* This task is responsible for the fan */

#include "src/tasks/AutomationFan.hpp"

#include <unistd.h>

#include "src/devices/ActuatorRelay.hpp"
#include "src/tasks/Task.hpp"

// Static variables
const std::string AutomationFan::JSON_TASKS_MAX_TEMP = "max_temp";
const std::string AutomationFan::JSON_TASKS_MIN_TEMP = "min_temp";

const std::string AutomationFan::JSON_TASKS_FAN_ID = "actuator_fan_id";
const std::string AutomationFan::JSON_TASKS_RELAY_ID = "task_relay_id";
const std::string AutomationFan::JSON_TASKS_TEMP_ID = "sensor_temp_id";

//------------------------------------------------------------------------------
/**
 * @brief Constructor
 *
 * @param name: name of the task
 * @param cfg_task: map of json values (task config file)
 *
 */
AutomationFan::AutomationFan(const std::string &name, const val_json_t &cfg_task)
    : Automation(name, cfg_task), temp(-1)
{
    try
    {
        max_temp = cfg_task.at(JSON_TASKS_MAX_TEMP).toNumber();
        min_temp = cfg_task.at(JSON_TASKS_MIN_TEMP).toNumber();
        fan_id = cfg_task.at(JSON_TASKS_FAN_ID).toString();
        relay_id = cfg_task.at(JSON_TASKS_RELAY_ID).toString();
        temp_id = cfg_task.at(JSON_TASKS_TEMP_ID).toString();
    }
    catch (const std::out_of_range &oor)
    {
        NRPI_LOG(ERROR) << "Some parameters are missing!";
        throw;
    }
}

//------------------------------------------------------------------------------
/**
 * @brief Run the Task
 *
 * @return true if executed successfully, false otherwhise
 */
bool AutomationFan::run(void)
{
    bool val = false;
    bool old_val = false;
    while (true)
    {
        std::unique_lock<std::mutex> lk(mtx_running);
        if (!running)
            break;
        try
        {
            temp = stof(sensors.at(temp_id)->get_data());
        }
        catch (const std::out_of_range &oor)
        {
            NRPI_LOG(ERROR) << "sensor not found";
        }

        if (temp >= max_temp && !val)
        {
            NRPI_LOG(INFO) << "Enabling the fan";
            val = true;
        }
        else if (temp <= min_temp && val)
        {
            NRPI_LOG(INFO) << "Disabling the fan";
            val = false;
        }

        try
        {
            if (old_val != val)
            {
                tasks_controller.at(relay_id).lock()->run({fan_id, "on", (val) ? "1" : "0"});
                old_val = val;
            }
        }
        catch (const std::out_of_range &oor)
        {
            NRPI_LOG(ERROR) << "Actuator or controller not found";
        }
        cv.wait_for(lk, std::chrono::seconds(10), [&] { return !running; });
        if (!running)
            break;
    }
    try
    {
        tasks_controller.at(relay_id).lock()->run({fan_id, "on", "0"});
    }
    catch (const std::out_of_range &oor)
    {
        NRPI_LOG(ERROR) << "Actuator or controller not found";
    }

    return true;
}
