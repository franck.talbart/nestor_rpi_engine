/************************************************************************
 NestoRPi Engine
 Copyright (C) 2015-2025 Franck Talbart

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

/* Author: Franck Talbart */

/* This task is responsible for the LCD screen */

#include "src/tasks/AutomationLcd.hpp"

#include <unistd.h>

#include <iomanip>

#include "src/devices/ActuatorLcdI2c.hpp"
#include "src/devices/ActuatorMbrola.hpp"
#include "src/devices/ActuatorRelay.hpp"
#include "src/tasks/Task.hpp"

// Static variables
const std::string AutomationLcd::JSON_TASKS_ACTUATOR_LCD_ID = "actuator_lcd_id";
const std::string AutomationLcd::JSON_TASKS_ACTUATOR_VOICE_ID = "actuator_voice_id";
const std::string AutomationLcd::JSON_TASKS_ECO = "eco";
const std::string AutomationLcd::JSON_TASKS_LOW_PRIORITY_TIMEOUT = "low_priority_timeout";
const std::string AutomationLcd::JSON_TASKS_HIGH_PRIORITY_TIMEOUT = "high_priority_timeout";
const std::string AutomationLcd::JSON_TASKS_SENSORS = "sensors";

//------------------------------------------------------------------------------
/**
 * @brief Constructor
 *
 * @param name: name of the task
 * @param cfg_task: map of json values (task config file)
 *
 */
AutomationLcd::AutomationLcd(const std::string &name, const val_json_t &cfg_task)
    : Automation(name, cfg_task)
{
    std::string robot_name;
    try
    {
        actuator_lcd_id = cfg_task.at(JSON_TASKS_ACTUATOR_LCD_ID).toString();
        actuator_voice_id = cfg_task.at(JSON_TASKS_ACTUATOR_VOICE_ID).toString();
        low_priority_timeout = cfg_task.at(JSON_TASKS_LOW_PRIORITY_TIMEOUT).toNumber();
        high_priority_timeout = cfg_task.at(JSON_TASKS_HIGH_PRIORITY_TIMEOUT).toNumber();

        for (const auto &s : cfg_task.at(JSON_TASKS_SENSORS))
            list_sensors[s->key] = s->value.toString();

        robot_name = util::get_cfg_value(CONFIG_ROBOT_NAME).toString();
    }
    catch (const std::out_of_range &oor)
    {
        NRPI_LOG(ERROR) << "Some parameters are missing!";
        throw;
    }

    robot_name[0] = toupper(robot_name[0]);
    push_msg("NestoRPi Engine Talbarts");
    push_msg("Hey! Nice to see you!");
    push_msg("My name is " + robot_name + ".");

    // sem is an int because turn_on / turn_off can be called multiple times (multiple users)
    sem = 0;

    try
    {
        ADD_TRIGGER(Device::ON, actuators.at(actuator_voice_id), &AutomationLcd::send_msg);
        for (const auto &e : cfg_task.at(JSON_TASKS_ECO))
        {
            ADD_TRIGGER(Device::ON, actuators.at(e->value.toString()), &AutomationLcd::turn_off);
            ADD_TRIGGER(Device::OFF, actuators.at(e->value.toString()), &AutomationLcd::turn_on);
        }
    }
    catch (const std::out_of_range &oor)
    {
        NRPI_LOG(ERROR) << "Actuator not found ";
        throw;
    }
}

//------------------------------------------------------------------------------
/**
 * @brief Task init
 *
 * @param data_channel: the data channel where we send the data
 *
 */
void AutomationLcd::init(DataChannelObserver *data_channel)
{
    UNUSED(data_channel);
    const std::string msg_welcome = "A new client is connected!";
    push_msg(msg_welcome);
}

//------------------------------------------------------------------------------
/**
 * @brief Push a message in the queue of messages
 *
 * @param txt: the message
 *
 */
void AutomationLcd::push_msg(const std::string &txt)
{
    // a vector is not thread safe
    lock_vec.lock();
    messages.push(txt);
    lock_vec.unlock();
}

//------------------------------------------------------------------------------
/**
 * @brief Send a message (wrapper for the trigger)
 *
 * @param s: the triggereing device
 *
 */
void AutomationLcd::send_msg(const Device *s)
{
    push_msg(dynamic_cast<const ActuatorMbrola *>(s)->get_text());
}

//------------------------------------------------------------------------------
/**
 * @brief Switch on the LCD screen (wrapper for the trigger)
 *
 * @param s: useless
 *
 */
void AutomationLcd::turn_on(const Device *s)
{
    UNUSED(s);
    try
    {
        dynamic_cast<ActuatorLcdI2c *>(actuators.at(actuator_lcd_id).get())->turn(true);
    }
    catch (const std::out_of_range &oor)
    {
        NRPI_LOG(ERROR) << "actuator not found ";
    }
    ++sem;
    cv.notify_all();
}

//------------------------------------------------------------------------------
/**
 * @brief Swith off the LCD screen (wrapper for the trigger)
 *
 * @param s: useless
 *
 */
void AutomationLcd::turn_off(const Device *s)
{
    UNUSED(s);

    if (sem > 0)
        --sem;

    if (sem == 0)
    {
        try
        {
            dynamic_cast<ActuatorLcdI2c *>(actuators.at(actuator_lcd_id).get())->turn(false);
        }
        catch (const std::out_of_range &oor)
        {
            NRPI_LOG(ERROR) << "actuator not found ";
        }
    }
    cv.notify_all();
}

//------------------------------------------------------------------------------
/**
 * @brief Run the Task
 *
 * @return true if executed successfully, false otherwhise
 */
bool AutomationLcd::run(void)
{
    try
    {
        while (true)
        {
            std::unique_lock<std::mutex> lk(mtx_running);
            cv.wait(lk, [this] { return (sem > 0) || !running; });
            if (!running)
                break;
            if (messages.size() != 0)
            { // High priority messages
                lock_vec.lock();
                std::string message = messages.front();
                messages.pop();
                lock_vec.unlock();
                dynamic_cast<ActuatorLcdI2c *>(actuators.at(actuator_lcd_id).get())->clear();
                dynamic_cast<ActuatorLcdI2c *>(actuators.at(actuator_lcd_id).get())
                    ->set_action(ActuatorLcdI2c::AUTOMATIC, message);
                short cpt = 0;
                while (cpt < high_priority_timeout && running)
                {
                    cv.wait_for(lk, std::chrono::seconds(1), [&] { return !running; });
                    ++cpt;
                }
                if (!running)
                    break;
            }
            else
            { // Low priority
                for (const auto &s : list_sensors)
                {
                    std::string out;
                    try
                    {
                        out = dynamic_cast<Sensor *>(sensors.at(s.second).get())->get_data();
                    }
                    catch (const std::out_of_range &oor)
                    {
                        NRPI_LOG(ERROR) << "sensor not found";
                        return false;
                    }

                    dynamic_cast<ActuatorLcdI2c *>(actuators.at(actuator_lcd_id).get())->clear();
                    dynamic_cast<ActuatorLcdI2c *>(actuators.at(actuator_lcd_id).get())
                        ->set_action(ActuatorLcdI2c::LINE1, s.first + ": ");
                    dynamic_cast<ActuatorLcdI2c *>(actuators.at(actuator_lcd_id).get())
                        ->set_action(ActuatorLcdI2c::LINE2, out);
                    short cpt = 0;
                    while (cpt < low_priority_timeout && messages.size() == 0 && running)
                    {
                        cv.wait_for(lk, std::chrono::seconds(1), [&] { return !running; });
                        ++cpt;
                    }
                    if (messages.size() != 0 || !running)
                        break;
                }
            }
        }
        dynamic_cast<ActuatorLcdI2c *>(actuators.at(actuator_lcd_id).get())->turn(true);
    }
    catch (const std::out_of_range &oor)
    {
        NRPI_LOG(ERROR) << "actuator not found ";
        return false;
    }
    return true;
}
