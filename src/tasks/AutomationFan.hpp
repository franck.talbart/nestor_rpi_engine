/************************************************************************
 NestoRPi Engine
 Copyright (C) 2015-2025 Franck Talbart

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

/* Author: Franck Talbart */

#ifndef DEF_AUTOMATION_FAN
#define DEF_AUTOMATION_FAN

#include "src/tasks/Automation.hpp"

class AutomationFan : public Automation
{
  public:
    explicit AutomationFan(const std::string& name, const val_json_t& cfg_task);

  private:
    bool run(void) override;

    float temp;
    std::string fan_id, relay_id, temp_id;
    short max_temp, min_temp;

    static const std::string JSON_TASKS_MAX_TEMP;
    static const std::string JSON_TASKS_MIN_TEMP;

    static const std::string JSON_TASKS_FAN_ID;
    static const std::string JSON_TASKS_RELAY_ID;
    static const std::string JSON_TASKS_TEMP_ID;
};
#endif // DEF_AUTOMATION_FAN
