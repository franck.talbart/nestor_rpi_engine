/************************************************************************
 NestoRPi Engine
 Copyright (C) 2015-2025 Franck Talbart

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

/* Author: Franck Talbart */

/* This task is used to run commands at a client's connection */

#include "src/tasks/AutomationStartup.hpp"

#include "src/tasks/Task.hpp"

// Static variables
const std::string AutomationStartup::JSON_TASKS_CMDS = "cmds";

//------------------------------------------------------------------------------
/**
 * @brief Constructor
 *
 * @param name: name of the task
 * @param cfg_task: map of json values (task config file)
 *
 */
AutomationStartup::AutomationStartup(const std::string &name, const val_json_t &cfg_task)
    : Automation(name, cfg_task)
{
    try
    {
        for (const auto &s : cfg_task.at(JSON_TASKS_CMDS))
            // cppcheck-suppress useStlAlgorithm
            list_cmds.push_back(s->value.toString());
    }
    catch (const std::out_of_range &oor)
    {
        NRPI_LOG(ERROR) << "Some parameters are missing!";
        throw;
    }
}

//------------------------------------------------------------------------------
/**
 * @brief Send the required data when the task init initialized
 *
 * @param data_channel: the data channel where we send the data
 *
 */
void AutomationStartup::init(DataChannelObserver *data_channel)
{
    UNUSED(data_channel);
    for (const auto &cmd : list_cmds)
    {
        NRPI_LOG(INFO) << "Running command " << cmd;
        std::array<char, 128> buffer;
        util::Popen pipe;
        try
        {
            pipe = util::extended_popen_read(cmd.c_str());
        }
        catch (const std::string &msg)
        {
            NRPI_LOG(ERROR) << "Couldn't launch command " << cmd;
            NRPI_LOG(ERROR) << msg;
            throw std::string("Error running the command");
        }
        std::string local_result = "";
        while (fgets(buffer.data(), sizeof(buffer), pipe.stdout) != nullptr)
            local_result += buffer.data();
        while (fgets(buffer.data(), sizeof(buffer), pipe.stderr) != nullptr)
            local_result += buffer.data();
        fclose(pipe.stdout);
        fclose(pipe.stderr);

        if (pipe.status != 0)
        {
            if (WIFSIGNALED(pipe.status))
            {
                NRPI_LOG(INFO) << "Command " << cmd << " stopped by a signal.";
            }
            else
            {
                NRPI_LOG(ERROR) << "Return code: " << pipe.status << " for command " << cmd;
                NRPI_LOG(ERROR) << local_result;
            }
        }
        else
        {
            if (local_result != "")
                NRPI_LOG(INFO) << "Output: " << local_result;
        }
    }
}
