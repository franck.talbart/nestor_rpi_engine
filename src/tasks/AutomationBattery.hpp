/************************************************************************
 NestoRPi Engine
 Copyright (C) 2015-2025 Franck Talbart

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

/* Author: Franck Talbart */

#ifndef DEF_AUTOMATION_BATTERY
#define DEF_AUTOMATION_BATTERY

#include "src/tasks/Automation.hpp"

class AutomationBattery : public Automation
{
  public:
    explicit AutomationBattery(const std::string &name, const val_json_t &cfg_task);
    void start_charge_battery(const Device *s);
    void stop_charge_battery(const Device *s);
    ~AutomationBattery(void);

  private:
    bool run(void) override;
    void periodical_charge(void);

    float max_voltage, max_tmp;
    double periodic_charge, min_time, max_time;
    std::atomic<short> sem;
    time_t t_begin;
    std::thread thread_periodical_charge;

    std::string battery_id, voltage_id, temp_id, relay_id;

    static const std::string JSON_TASKS_MAX_VOLTAGE;
    static const std::string JSON_TASKS_MIN_TIME;
    static const std::string JSON_TASKS_MAX_TIME;
    static const std::string JSON_TASKS_MAX_TMP;
    static const std::string JSON_TASKS_PERIODIC_CHARGE;

    static const std::string JSON_TASKS_BATTERY_ID;
    static const std::string JSON_TASKS_VOLTAGE_ID;
    static const std::string JSON_TASKS_TEMP_ID;
    static const std::string JSON_TASKS_RELAY_ID;
};
#endif // DEF_AUTOMATION_BATTERY
