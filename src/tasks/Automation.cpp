/************************************************************************
 NestoRPi Engine
 Copyright (C) 2015-2025 Franck Talbart

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

/* Author: Franck Talbart */

#include "src/tasks/Automation.hpp"

// Static variables
const std::string Automation::JSON_TASKS_TYPE_VALUE = "automation";

std::map<std::string, std::weak_ptr<Automation>> Automation::tasks_automation;
std::map<std::string, std::weak_ptr<Controller>> Automation::tasks_controller;

//------------------------------------------------------------------------------
/**
 * @brief Constructor
 *
 * @param name: name of the task
 * @param cfg_task: map of json values (task config file)
 *
 */
Automation::Automation(const std::string &name, const val_json_t &cfg_task)
    : Task(name, cfg_task), running(false), returned_val(true)
{
}

//------------------------------------------------------------------------------
/**
 * @brief Set the list of tasks
 *
 * @param tasks_automation_l: map of Automation
 * @param tasks_controller_l: map of Controller
 *
 */
void Automation::set_tasks(
    const std::map<std::string, std::shared_ptr<Automation>> &tasks_automation_l,
    const tasks_controller_t &tasks_controller_l)
{
    Automation::tasks_automation.insert(tasks_automation_l.begin(), tasks_automation_l.end());
    Automation::tasks_controller.insert(tasks_controller_l.begin(), tasks_controller_l.end());
}

//------------------------------------------------------------------------------
/**
 * @brief Auto start the task, must be overwritten to disable it
 *
 *
 */
void Automation::auto_run(void) { start(); }
//------------------------------------------------------------------------------
/**
 * @brief Start the task in a thread
 *
 * @param device: if called by a trigger, the device which triggered it
 *
 */
void Automation::start(Device *device)
{
    UNUSED(device);
    {
        std::unique_lock<std::mutex> lock(mtx_running);
        running = true;
    }
    cv.notify_all();

    thread_task = std::thread(
        [&]
        {
            try
            {
                this->returned_val = this->run();
            }
            catch (...)
            {
                NRPI_LOG(ERROR) << "Something wrong happened with the task: " << name;
            }
        });
}

//------------------------------------------------------------------------------
/**
 * @brief Stop the Task
 *
 * @param device: if called by a trigger, the device which triggered it
 * @return true if executed successfully, false otherwhise
 */
bool Automation::stop(Device *device)
{
    UNUSED(device);
    {
        std::unique_lock<std::mutex> lock(mtx_running);
        running = false;
    }
    cv.notify_all();

    if (thread_task.joinable()) // Maybe the thread is not started
        thread_task.join();
    return returned_val;
}
