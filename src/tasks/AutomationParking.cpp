/************************************************************************
 NestoRPi Engine
 Copyright (C) 2015-2025 Franck Talbart

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

/* Author: Franck Talbart */

/* This task is responsible for the parking mode */

#include "src/tasks/AutomationParking.hpp"

#include <unistd.h>

#include "src/devices/ActuatorRelay.hpp"
#include "src/tasks/ControllerRelay.hpp"
#include "src/tasks/Task.hpp"

// Static variables
const std::string AutomationParking::JSON_TASKS_ON_STOP = "on_stop";
const std::string AutomationParking::JSON_TASKS_VOLTAGE_STATION = "voltage_station";
const std::string AutomationParking::JSON_TASKS_WAIT_BEFORE_PARK = "wait_before_park";

const std::string AutomationParking::JSON_TASKS_PARKING_ID = "actuator_parking_id";
const std::string AutomationParking::JSON_TASKS_LIGHT_ID = "actuator_light_id";
const std::string AutomationParking::JSON_TASKS_VOLTAGE_ID = "sensor_voltage_station_id";
const std::string AutomationParking::JSON_TASKS_RELAY_ID = "task_relay_id";

//------------------------------------------------------------------------------
/**
 * @brief Constructor
 *
 * @param name: name of the task
 * @param cfg_task: map of json values (task config file)
 *
 */
AutomationParking::AutomationParking(const std::string &name, const val_json_t &cfg_task)
    : Automation(name, cfg_task)
{
    try
    {
        voltage_station = cfg_task.at(JSON_TASKS_VOLTAGE_STATION).toNumber();
        wait_before_park = cfg_task.at(JSON_TASKS_WAIT_BEFORE_PARK).toNumber();
        parking_id = cfg_task.at(JSON_TASKS_PARKING_ID).toString();
        voltage_id = cfg_task.at(JSON_TASKS_VOLTAGE_ID).toString();
        relay_id = cfg_task.at(JSON_TASKS_RELAY_ID).toString();
        on_stop = cfg_task.at(JSON_TASKS_ON_STOP);
    }
    catch (const std::out_of_range &oor)
    {
        NRPI_LOG(ERROR) << "Some parameters are missing!";
        throw;
    }

    // sem is an int because it can be called multiple times
    sem = 0;

    try
    {
        ADD_TRIGGER(Device::ON, actuators.at(parking_id), &AutomationParking::stop_check_parking);
        ADD_TRIGGER(Device::OFF, actuators.at(parking_id), &AutomationParking::start_check_parking);
        cpt = 0;
    }
    catch (const std::out_of_range &oor)
    {
        NRPI_LOG(ERROR) << "actuator not found";
        throw;
    }
}

//------------------------------------------------------------------------------
/**
 * @brief Start event
 *
 * @param s: the device which triggers it
 *
 */
void AutomationParking::start_check_parking(const Device *s)
{
    UNUSED(s);

    ++sem;
    if (sem == 1)
    {
        NRPI_LOG(INFO) << "Starting the automated parking mode service";

        cv.notify_all();
    }
}

//------------------------------------------------------------------------------
/**
 * @brief Stop event
 *
 * @param s: the device which triggers it
 *
 */
void AutomationParking::stop_check_parking(const Device *s)
{
    UNUSED(s);

    if (sem > 0)
        --sem;
    if (sem == 0)
    {
        cpt = 0;
        NRPI_LOG(INFO) << "Stopping the automated parking mode service";

        cv.notify_all();
    }
}

//------------------------------------------------------------------------------
/**
 * @brief Run the Task
 *
 * @return true if executed successfully, false otherwhise
 */
bool AutomationParking::run(void)
{
    while (true)
    {
        std::unique_lock<std::mutex> lk(mtx_running);
        cv.wait(lk, [this] { return (sem > 0) || !running; });

        if (!running)
            break;
        // We don't want to accidentally set the parking mode to on
        // if we are actually leaving the docking station
        cv.wait_for(lk, std::chrono::seconds(2), [&] { return !running; });
        if (!running)
            break;

        float voltage_battery = 0;
        try
        {
            voltage_battery = stof(sensors.at(voltage_id)->get_data());
        }
        catch (const std::out_of_range &oor)
        {
            NRPI_LOG(ERROR) << "sensor not found";
            return false;
        }

        if (voltage_battery >= voltage_station)
        {
            ++cpt;
        }
        else
        {
            cpt = 0;
        }

        try
        {
            // Automatic parking mode: wait a bit before enabling the parking mode
            if (cpt >= wait_before_park &&
                !dynamic_cast<ActuatorRelay *>(actuators.at(parking_id).get())->get_action("on"))
            {
                // If the robot is in its docking station, activate the parking mode
                NRPI_LOG(INFO) << "Running the 'on stop' events";
                for (const auto &action : on_stop)
                {
                    for (const auto &params : action->value)
                    {
                        std::vector<std::string> action_params;
                        for (const auto &param : params->value)
                            // cppcheck-suppress useStlAlgorithm
                            action_params.push_back(param->value.toString());
                        tasks_controller.at(action->key).lock()->run({action_params});
                    }
                    usleep(500000);
                }

                NRPI_LOG(INFO) << "Enabling the parking mode";
                tasks_controller.at(relay_id).lock()->run({parking_id, "on", "1"});
                cpt = 0;
            }
        }
        catch (const std::out_of_range &oor)
        {
            NRPI_LOG(ERROR) << "Actuator or controller not found";
            return false;
        }
    }
    return true;
}
