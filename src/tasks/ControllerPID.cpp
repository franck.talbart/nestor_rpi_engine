/************************************************************************
 NestoRPi Engine
 Copyright (C) 2015-2025 Franck Talbart

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

/* Author: Franck Talbart */

/* This task is responsible for the PID Controller (used to move the robot) */

#include "src/tasks/ControllerPID.hpp"

#include <unistd.h>

#include <charconv>
#include <chrono>
#include <cmath>
#include <ctime>
#include <string>
#include <thread>

#include "src/devices/ActuatorRelay.hpp"
#include "src/devices/SensorRotaryEncoder.hpp"

// Static variables
const std::string ControllerPID::JSON_TASKS_P = "P";
const std::string ControllerPID::JSON_TASKS_I = "I";
const std::string ControllerPID::JSON_TASKS_D = "D";
const std::string ControllerPID::JSON_TASKS_SAMPLING = "sampling";
const std::string ControllerPID::JSON_TASKS_MAX_NB_REV_RIGHT = "max_nb_rev_per_s_right";
const std::string ControllerPID::JSON_TASKS_MAX_NB_REV_LEFT = "max_nb_rev_per_s_left";

const std::string ControllerPID::JSON_TASKS_ACTION = "action";
const std::string ControllerPID::JSON_TASKS_VALUE = "value";
const std::string ControllerPID::JSON_TASKS_CONSTRAINT = "constraint";

const std::string ControllerPID::JSON_TASKS_DEVICES = "devices";
const std::string ControllerPID::JSON_TASKS_L298N_ID = "actuator_L298N_id";
const std::string ControllerPID::JSON_TASKS_WHEEL_1_ID = "sensor_wheel_1_id";
const std::string ControllerPID::JSON_TASKS_WHEEL_2_ID = "sensor_wheel_2_id";

const std::string ControllerPID::MSG_TASKS_MOTOR = "motor";
const std::string ControllerPID::MSG_TASKS_RIGHT = "right";
const std::string ControllerPID::MSG_TASKS_LEFT = "left";

//------------------------------------------------------------------------------
/**
 * @brief Constructor
 *
 * @param name: name of the task
 * @param cfg_task: map of json values (task config file)
 *
 */
ControllerPID::ControllerPID(const std::string &name, val_json_t &cfg_task)
    : Controller(name, cfg_task)
{
    try
    {
        kp = cfg_task.at(JSON_TASKS_P).toNumber();
        ki = cfg_task.at(JSON_TASKS_I).toNumber();
        kd = cfg_task.at(JSON_TASKS_D).toNumber();

        sampling = cfg_task.at(JSON_TASKS_SAMPLING).toNumber();
        max_nb_rev_right = cfg_task.at(JSON_TASKS_MAX_NB_REV_RIGHT).toNumber();
        max_nb_rev_left = cfg_task.at(JSON_TASKS_MAX_NB_REV_LEFT).toNumber();

        for (const auto &device_group : cfg_task.at(JSON_TASKS_DEVICES))
        {
            for (const auto &device : device_group->value)
            {
                devices[device_group->key][device->key] = device->value.toString();
            }
            order_per_s[device_group->key] = std::tuple(0, 0);
        }
    }
    catch (const std::out_of_range &oor)
    {
        NRPI_LOG(ERROR) << "Some parameters are missing in the task config file!";
        throw;
    }

    // Constraints: optional
    val_json_t::const_iterator i = cfg_task.find(JSON_TASKS_CONSTRAINT);
    if (i != cfg_task.end())
    {
        try
        {
            for (const auto &cons : i->second)
            {
                // Load the constraints
                std::map<std::string, std::shared_ptr<Actuator>>::const_iterator it_act_const =
                    actuators.find(cons->key);
                if (it_act_const != actuators.end())
                {
                    const_device.push_back(std::bind(
                        &ControllerPID::is_actuator_ok,
                        this,
                        std::string(cons->value(JSON_TASKS_ACTION.c_str()).toString()),
                        cons->value(JSON_TASKS_VALUE.c_str()).getTag() == gason::JSON_TRUE,
                        it_act_const->second));
                }
                else
                {
                    NRPI_LOG(WARN)
                        << "Don't know what " << cons->key << " is in the tasks cfg file.";
                }
            }
        }
        catch (const std::out_of_range &oor)
        {
            NRPI_LOG(ERROR) << "Some parameters are missing in the task config file!";
            throw;
        }
    }
    running = true;

    thread_process = std::thread(&ControllerPID::thread_control, this);
}

//---------------------------------------- --------------------------------------
/**
 * @brief Init the task
 *
 * @param data_channel: the data channel where we send the data
 *
 */
void ControllerPID::init(DataChannelObserver *data_channel)
{
    for (const auto &device_group : devices)
    {
        std::string L298N_id;
        try
        {
            L298N_id = devices[device_group.first].at(JSON_TASKS_L298N_ID);
        }
        catch (const std::out_of_range &oor)
        {
            NRPI_LOG(ERROR) << "Some parameters are missing in the task config file!";
            throw;
        }

        try
        {
            float val = dynamic_cast<ActuatorL298N *>(actuators.at(L298N_id).get())
                            ->get_action(ActuatorL298N::LEFT);
            this->send_msg_client({device_group.first, MSG_TASKS_LEFT, util::float_to_str(val)},
                                  data_channel);
            val = dynamic_cast<ActuatorL298N *>(actuators.at(L298N_id).get())
                      ->get_action(ActuatorL298N::RIGHT);
            this->send_msg_client({device_group.first, MSG_TASKS_RIGHT, util::float_to_str(val)},
                                  data_channel);
        }
        catch (const std::out_of_range &oor)
        {
            NRPI_LOG(ERROR) << "actuator not found";
        }
    }
}

//------------------------------------------------------------------------------
/**
 * @brief Set the speed of a motor
 *
 * @param motor: 0 => device group 1 => left / right 2 => -100 to 100
 * @return true if executed succesfully, false otherwhise
 */
bool ControllerPID::run(const std::vector<std::string> &motor)
{
    if (motor.size() != 3)
        return false;
    float val = std::atof(motor[2].c_str());
    // constraints
    for (const auto &cons : const_device)
        if (!cons())
            val = 0;

    try
    {
        if (motor[1] == MSG_TASKS_LEFT)
        {
            std::get<0>(order_per_s.at(motor[0])) = val / 100.0 * max_nb_rev_left;
        }
        else if (motor[1] == MSG_TASKS_RIGHT)
        {
            std::get<1>(order_per_s.at(motor[0])) = val / 100.0 * max_nb_rev_right;
        }
    }
    catch (const std::out_of_range &oor)
    {
        NRPI_LOG(ERROR) << "device group not found: " << motor[0];
        return false;
    }
    return true;
}

//------------------------------------------------------------------------------
/**
 * @brief Reset every motors
 */
void ControllerPID::reset(void)
{
    for (const auto &device_group : devices)
    {
        order_per_s[device_group.first] = std::tuple(0, 0);
    }
}

//------------------------------------------------------------------------------
/**
 * @brief Destructor
 *
 */
ControllerPID::~ControllerPID(void)
{
    running = false;
    thread_process.join();
}

//------------------------------------------------------------------------------
template <typename T1, typename T2>
static inline auto get_tuple_value(const std::tuple<T1, T2> &tup, int motor_id)
    -> decltype(std::get<0>(tup))
{
    if (motor_id == 0)
        return std::get<0>(tup);
    if (motor_id == 1)
        return std::get<1>(tup);
    throw std::out_of_range("Invalid motor_id index");
}

//------------------------------------------------------------------------------
template <typename T1, typename T2, typename ValueType>
static inline void set_tuple_value(std::tuple<T1, T2> &tup, int motor_id, ValueType new_value)
{
    if (motor_id == 0)
        std::get<0>(tup) = static_cast<T1>(new_value);
    else if (motor_id == 1)
        std::get<1>(tup) = static_cast<T2>(new_value);
    else
        throw std::out_of_range("Invalid motor_id index");
}

//------------------------------------------------------------------------------
/**
 * @brief This task controls multiple motors
 */
void ControllerPID::thread_control(void)
{
    std::map<std::string, std::tuple<int, int>> nb_of_tics_per_rev;
    std::map<std::string, std::tuple<short, short>> nb_try;
    std::map<std::string, std::tuple<float, float>> sum_error, old_error, old_cmd;
    std::map<std::string, std::function<double()>> get_nb_tics;

    // It's better to handle multiple motors from the same thread to keep them in sync

    while (running)
    {
        for (const auto &device_group : devices)
        {
            for (auto motor_id : {ActuatorL298N::LEFT, ActuatorL298N::RIGHT})
            {
                std::string wheel_id =
                    motor_id == ActuatorL298N::LEFT ? JSON_TASKS_WHEEL_1_ID : JSON_TASKS_WHEEL_2_ID;
                std::string sensor_id;
                try
                {
                    sensor_id = devices[device_group.first].at(wheel_id);
                }
                catch (const std::out_of_range &oor)
                {
                    continue; // wheel can be optional
                }
                if (sensor_id == "")
                    continue;

                try
                {
                    set_tuple_value(nb_of_tics_per_rev[device_group.first],
                                    motor_id,
                                    dynamic_cast<SensorRotaryEncoder *>(sensors.at(sensor_id).get())
                                        ->get_nb_of_tics_per_rev());
                }
                catch (const std::out_of_range &oor)
                {
                    NRPI_LOG(ERROR) << "sensor not found";
                    return;
                }

                int tic;

                try
                {
                    tic = std::abs(get_nb_tics.at(sensor_id)());
                }
                catch (const std::out_of_range &oor)
                {
                    get_nb_tics[sensor_id] =
                        dynamic_cast<SensorRotaryEncoder *>(sensors.at(sensor_id).get())
                            ->get_accessor_value();
                    tic = std::abs(get_nb_tics.at(sensor_id)());
                }

                int encoder_frequency = sampling * tic;
                float number_of_revolution_per_s =
                    (float)encoder_frequency /
                    get_tuple_value(nb_of_tics_per_rev[device_group.first], motor_id);
                int s = (get_tuple_value(order_per_s[device_group.first], motor_id) > 0) ? -1 : 1;
                float local_order_per_s =
                    std::abs(get_tuple_value(order_per_s[device_group.first], motor_id));
                float error = local_order_per_s - number_of_revolution_per_s;
                set_tuple_value(sum_error[device_group.first],
                                motor_id,
                                get_tuple_value(sum_error[device_group.first], motor_id) + error);
                float delta_error =
                    error - get_tuple_value(old_error[device_group.first], motor_id);
                set_tuple_value(old_error[device_group.first], motor_id, error);
                float cmd = kp * error +
                            ki * get_tuple_value(sum_error[device_group.first], motor_id) +
                            kd * delta_error;
                if (cmd < 0 || local_order_per_s == 0)
                {
                    cmd = 0;
                    set_tuple_value(sum_error[device_group.first], motor_id, 0);
                }

                int current_nb_try = get_tuple_value(nb_try[device_group.first], motor_id);

                if (error != 0 && error == local_order_per_s)
                {
                    set_tuple_value(nb_try[device_group.first], motor_id, current_nb_try + 1);
                    if (current_nb_try > sampling * 2)
                    {
                        set_tuple_value(nb_try[device_group.first], motor_id, 0);

                        NRPI_LOG(ERROR)
                            << "Something is wrong with the motor or the encoding wheel. "
                            << "The encoding wheel " << sensor_id << " returns 0";
                    }
                }

                cmd *= s;
                if (get_tuple_value(old_cmd[device_group.first], motor_id) != cmd)
                {
                    set_tuple_value(old_cmd[device_group.first], motor_id, cmd);
                    try
                    {
                        std::string L298N_id = devices[device_group.first].at(JSON_TASKS_L298N_ID);
                        dynamic_cast<ActuatorL298N *>(actuators.at(L298N_id).get())
                            ->set_action(motor_id, cmd);
                        float val = dynamic_cast<ActuatorL298N *>(actuators.at(L298N_id).get())
                                        ->get_action(motor_id);

                        this->send_msg_client(
                            {device_group.first,
                             (motor_id == ActuatorL298N::LEFT) ? MSG_TASKS_LEFT : MSG_TASKS_RIGHT,
                             util::float_to_str(val)});
                    }
                    catch (const std::out_of_range &oor)
                    {
                        NRPI_LOG(ERROR) << "L298N not found";
                        return;
                    }
                }
            }
        }
        std::this_thread::sleep_for(std::chrono::milliseconds(1000 / sampling));
    }
}

//------------------------------------------------------------------------------
/**
 * @brief check if the actuator status is equivalent to the given value
 *
 * @param action: the action to check
 * @param val: the operand
 * @param act: the actuator to check
 *
 * @return true if equivalent , false otherwhise
 */
bool ControllerPID::is_actuator_ok(const std::string &action,
                                   bool val,
                                   std::shared_ptr<Actuator> act) const
{
    return (dynamic_cast<ActuatorRelay *>(act.get())->get_action(action) == val);
}
