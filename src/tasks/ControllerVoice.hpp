/************************************************************************
 NestoRPi Engine
 Copyright (C) 2015-2025 Franck Talbart

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

/* Author: Franck Talbart */

#ifndef DEF_CONTROLLER_VOICE
#define DEF_CONTROLLER_VOICE

#include "src/tasks/Controller.hpp"

class ControllerVoice : public Controller
{
  public:
    explicit ControllerVoice(const std::string &name, val_json_t &cfg_task);
    bool run(const std::vector<std::string> &txt) override;

  private:
    std::string mbrola_id;

    static const std::string JSON_TASKS_MBROLA_ID;
    static const std::string MSG_TASKS_MBROLA;
};
#endif // DEF_CONTROLLER_VOICE
