/************************************************************************
 NestoRPi Engine
 Copyright (C) 2015-2025 Franck Talbart

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

/* Author: Franck Talbart */

#ifndef DEF_CONTROLLER_PID
#define DEF_CONTROLLER_PID

#include "src/devices/ActuatorL298N.hpp"
#include "src/tasks/Controller.hpp"

class ControllerPID : public Controller
{
  public:
    explicit ControllerPID(const std::string &name, val_json_t &cfg_task);
    void init(DataChannelObserver *) override;
    bool run(const std::vector<std::string> &motor) override;
    void reset(void) override;
    ~ControllerPID(void);

  private:
    void thread_control(void);
    bool is_actuator_ok(const std::string &action, bool val, std::shared_ptr<Actuator> act) const;

    float kp, ki, kd, max_nb_rev_right, max_nb_rev_left;
    std::map<std::string, std::map<std::string, std::string>> devices;
    int sampling;
    volatile std::atomic<bool> running;
    std::map<std::string, std::tuple<float, float>> order_per_s;
    std::atomic<float> order_per_s_left, order_per_s_right;
    std::thread thread_process;
    std::vector<std::function<bool()>> const_device;

    static const std::string JSON_TASKS_P;
    static const std::string JSON_TASKS_I;
    static const std::string JSON_TASKS_D;
    static const std::string JSON_TASKS_SAMPLING;
    static const std::string JSON_TASKS_MAX_NB_REV_RIGHT;
    static const std::string JSON_TASKS_MAX_NB_REV_LEFT;

    static const std::string JSON_TASKS_ACTION;
    static const std::string JSON_TASKS_VALUE;
    static const std::string JSON_TASKS_CONSTRAINT;

    static const std::string JSON_TASKS_DEVICES;
    static const std::string JSON_TASKS_L298N_ID;
    static const std::string JSON_TASKS_WHEEL_1_ID;
    static const std::string JSON_TASKS_WHEEL_2_ID;

    static const std::string MSG_TASKS_MOTOR;
    static const std::string MSG_TASKS_LEFT;
    static const std::string MSG_TASKS_RIGHT;
};
#endif // DEF_CONTROLLER_PID
