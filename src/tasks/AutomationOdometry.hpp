/************************************************************************
 NestoRPi Engine
 Copyright (C) 2015-2025 Franck Talbart

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

/* Author: Franck Talbart */

#ifndef DEF_AUTOMATION_ODOMETRY
#define DEF_AUTOMATION_ODOMETRY

#include "src/tasks/Automation.hpp"

class AutomationOdometry : public Automation
{
  public:
    explicit AutomationOdometry(const std::string &name, const val_json_t &cfg_task);
    void thread_send_msg(void);
    ~AutomationOdometry(void);

  private:
    void update_position(double distance_left, double distance_right);
    bool run(void) override;

    std::string encoding_left_id, encoding_right_id, motor_left_id, motor_right_id;
    double x, y, theta; // radian
    float wheel_base;
    std::thread thread_msg;

    static const std::string JSON_TASKS_WHEEL_BASE;

    static const std::string JSON_TASKS_ENCODING_LEFT_ID;
    static const std::string JSON_TASKS_ENCODING_RIGHT_ID;
    static const std::string JSON_TASKS_MOTOR_LEFT_ID;
    static const std::string JSON_TASKS_MOTOR_RIGHT_ID;
};
#endif // DEF_AUTOMATION_ODOMETRY
