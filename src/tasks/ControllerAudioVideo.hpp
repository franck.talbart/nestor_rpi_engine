/************************************************************************
 NestoRPi Engine
 Copyright (C) 2015-2025 Franck Talbart

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

/* Author: Franck Talbart */

#ifndef DEF_CONTROLLER_AUDIO_VIDEO
#define DEF_CONTROLLER_AUDIO_VIDEO

#include "src/tasks/Controller.hpp"

class ControllerAudioVideo : public Controller
{
  public:
    explicit ControllerAudioVideo(const std::string &name, val_json_t &cfg_task);
    void init(DataChannelObserver *data_channel) override;
    bool run(const std::vector<std::string> &audio_video) override;

  private:
    static const std::string MSG_TASKS_NB_VID_DEVICES;
    static const std::string MSG_TASKS_PIC;
    static const std::string MSG_TASKS_PIC_END;
    static const std::string MSG_TASKS_PIC_FILE;
    static const std::string MSG_TASKS_SOUND;
};

#endif // DEF_CONTROLLER_AUDIO_VIDEO
