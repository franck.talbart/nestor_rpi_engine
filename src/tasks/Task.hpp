/************************************************************************
 NestoRPi Engine
 Copyright (C) 2015-2025 Franck Talbart

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

/* Author: Franck Talbart */

#ifndef DEF_TASK
#define DEF_TASK

#include <atomic>
#include <vector>

#include "src/core/DataChannelObserver.hpp"
#include "src/core/Log.hpp"
#include "src/core/const.hpp"
#include "src/devices/Actuator.hpp"
#include "src/devices/Sensor.hpp"

class Task : public Uncopyable
{
  public:
    explicit Task(const std::string& name, const val_json_t& cfg_task);
    virtual void init(DataChannelObserver* data_channel) { UNUSED(data_channel); };
    virtual void reset(void);
    static void set_data_channel(DataChannelObserver* data_channel);
    static void remove_data_channel(DataChannelObserver* data_channel);
    static void remove_all_data_channel(void);
    static void set_devices(const actuators_t& actuators, const sensors_t& sensors);
    virtual ~Task(void){};

  protected:
    void send_msg_client(const std::vector<std::string>& msg,
                         DataChannelObserver* data_channel_loc = nullptr);
    virtual std::string get_type(void) const = 0;
    std::string name;
    val_json_t cfg_task;
    static actuators_t actuators;
    static sensors_t sensors;
    static std::vector<DataChannelObserver*> data_channel_observers;
};

#endif // DEF_TASK
