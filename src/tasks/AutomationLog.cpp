/************************************************************************
  NestoRPi Engine
  Copyright (C) 2015-2025 Franck Talbart

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ************************************************************************/

/* Author: Franck Talbart */

/* This task is used to transmit the data from a set of sensors */

#include "src/tasks/AutomationLog.hpp"

#include <unistd.h>

#include <iostream>

//------------------------------------------------------------------------------
/**
 * @brief Constructor
 *
 * @param name: name of the task
 * @param cfg_task: map of json values (task config file)
 *
 */
AutomationLog::AutomationLog(const std::string& name, const val_json_t& cfg_task)
    : Automation(name, cfg_task),
      filename(util::get_full_dir_from_cfg(CONFIG_LOG_PATH) + std::string(FILENAME_LOG_DAEMON))
{
}

//------------------------------------------------------------------------------
/**
 * @brief Run the Task
 *
 * @return true if executed successfully, false otherwhise
 */
bool AutomationLog::run(void)
{
    int last_position = 0;
    std::ifstream infile(filename);
    if (infile.is_open())
    {
        infile.seekg(0, std::ios::end);
        last_position = infile.tellg();
        infile.close();
    }

    while (true)
    {
        std::unique_lock<std::mutex> lock(mtx_running);
        if (!running)
            break;

        infile.open(filename);
        if (infile.is_open())
        {
            infile.seekg(0, std::ios::end);
            int filesize = infile.tellg();

            if (filesize < last_position)
                last_position = 0;

            while (last_position < filesize)
            {
                infile.seekg(last_position, std::ios::beg);
                std::string line;
                getline(infile, line);
                last_position = infile.tellg();
                if (last_position < 0)
                    break;
                this->send_msg_client({line});
            }
            cv.wait_for(lock, std::chrono::seconds(1), [&] { return !running; });
            if (!running)
                break;

            infile.close();
        }
    }
    return true;
}
