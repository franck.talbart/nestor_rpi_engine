/************************************************************************
 NestoRPi Engine
 Copyright (C) 2015-2025 Franck Talbart

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

/* Author: Franck Talbart */

/* This task is responsible for the video and audio streams. */

#include "src/tasks/ControllerAudioVideo.hpp"

#include <dirent.h>
#include <sys/types.h>

#include <filesystem>
#include <fstream>

#include "src/core/Camera.hpp"

// Static variables
const std::string ControllerAudioVideo::MSG_TASKS_NB_VID_DEVICES = "nb_dev";
const std::string ControllerAudioVideo::MSG_TASKS_PIC = "pic";
const std::string ControllerAudioVideo::MSG_TASKS_PIC_END = "end";
const std::string ControllerAudioVideo::MSG_TASKS_PIC_FILE = "pic_file";
const std::string ControllerAudioVideo::MSG_TASKS_SOUND = "sound";

//------------------------------------------------------------------------------
/**
 * @brief Return the sound file name depending on the id
 *        if id is -1, then the sound is randomly selected
 *
 * @param  id: the sound id number
 * @return the filename
 */
static std::string get_sound_name(short id)
{
    std::string path_random_sounds = "";
    std::string path_sounds = "";
    try
    {
        path_random_sounds = util::get_cfg_dir(PATH_RANDOM_SOUNDS);
        path_sounds = util::get_cfg_dir(PATH_SOUNDS);
    }
    catch (const std::string &err)
    {
        NRPI_LOG(ERROR) << err;
        return "";
    }

    // Random
    if (id == -1)
    {
        // Get the number of files

        DIR *rep = nullptr;
        rep = opendir(path_random_sounds.c_str());
        assert(rep != nullptr);
        struct dirent *ent;
        short cpt = 0;
        while ((ent = readdir(rep)) != nullptr) ++cpt;
        rewinddir(rep);
        // Generate the random number
        srand(time(nullptr));
        id = rand() % (cpt - 2); // -2 => . and ..
        cpt = 0;
        // Get the filename
        do
        {
            if ((ent = readdir(rep)) == nullptr)
                break;
            std::string rep_str = ent->d_name;
            if (rep_str != "." && rep_str != "..")
                ++cpt;
        } while (cpt <= id);
        if (ent == nullptr)
            return "";
        std::string result = std::string(ent->d_name);
        closedir(rep), rep = nullptr;
        return (path_random_sounds + result);
    }
    else //  id != -1
    {
        DIR *rep = opendir(path_sounds.c_str());
        assert(rep != nullptr);
        struct dirent *ent;
        short cpt = 0;
        //@todo: maybe we can factorize we the code below
        do
        {
            if ((ent = readdir(rep)) == nullptr)
                break;
            std::string rep_str = ent->d_name;
            if (rep_str != "." && rep_str != "..")
                ++cpt;
        } while (cpt <= id);
        if (ent == nullptr)
            return "";
        std::string result = std::string(ent->d_name);
        closedir(rep), rep = nullptr;
        return (path_sounds + result);
    }
}

//------------------------------------------------------------------------------
/**
 * @brief Constructor
 *
 * @param name: name of the task
 * @param cfg_task: map of json values (task config file)
 *
 */
ControllerAudioVideo::ControllerAudioVideo(const std::string &name, val_json_t &cfg_task)
    : Controller(name, cfg_task)
{
}

//------------------------------------------------------------------------------
std::string base64_encode(const std::string &input)
{
    const std::string base64_chars =
        "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

    std::string output;
    int val = 0, valb = -6;
    for (unsigned char c : input)
    {
        val = (val << 8) + c;
        valb += 8;
        while (valb >= 0)
        {
            output.push_back(base64_chars[(val >> valb) & 0x3F]);
            valb -= 6;
        }
    }
    if (valb > -6)
        output.push_back(base64_chars[((val << 8) >> (valb + 8)) & 0x3F]);

    while (output.size() % 4) output.push_back('=');

    return output;
}

//------------------------------------------------------------------------------
/**
 * @brief lauch the corresponding audio / video request
 *
 * @param audio_video: 0 => mode 1 ... => params
 * @return true if succeed, false otherwhise
 */
bool ControllerAudioVideo::run(const std::vector<std::string> &audio_video)
{
    try
    {
        std::string type = audio_video.at(0);
        if (type == MSG_TASKS_SOUND)
        {
            NRPI_LOG(INFO) << "Playing sound";
            std::thread thread = std::thread(
                [&]()
                {
                    std::string file = get_sound_name(std::stoi(audio_video.at(1)));
                    util::play_sound(file);
                });
            thread.detach();
        }
        else if (type == MSG_TASKS_PIC) // Take a picture
        {
            NRPI_LOG(INFO) << "Cheese :)";

            const short camera_id = std::stoi(audio_video.at(1));
            std::string filename = Camera::get_instance(camera_id)->capture_pic();

            std::string full_path = util::get_full_dir_from_cfg(CONFIG_RECORD_PATH);
            const std::filesystem::path full_filename = full_path + filename;

            if (std::filesystem::exists(full_filename) &&
                std::filesystem::is_regular_file(full_filename))
            {
                std::ifstream file(full_filename, std::ios::binary | std::ios::ate);
                file.seekg(0, std::ios::beg);
                std::ostringstream oss;
                oss << file.rdbuf();
                NRPI_LOG(INFO) << "Sending the picture to the client.";

                const std::string input_string = base64_encode(oss.str());
                const int length = input_string.length();
                const short chunk_size = 64;
                std::vector<std::string> chunks;
                const int num_chunks = length / chunk_size;
                const int remaining_chars = length % chunk_size;

                for (int i = 0; i < num_chunks; i++)
                {
                    std::string chunk = input_string.substr(i * chunk_size, chunk_size);
                    chunks.push_back(chunk);
                }

                if (remaining_chars > 0)
                {
                    std::string last_chunk =
                        input_string.substr(num_chunks * chunk_size, remaining_chars);
                    chunks.push_back(last_chunk);
                }
                for (const auto &chunk : chunks)
                {
                    this->send_msg_client({MSG_TASKS_PIC_FILE, filename, chunk});
                }
                this->send_msg_client({MSG_TASKS_PIC_FILE, filename, MSG_TASKS_PIC_END});
                file.close();
            }
            else
            {
                NRPI_LOG(ERROR) << "Picture not found.";
            }
        }
        else
        {
            NRPI_LOG(ERROR) << "Unknown mode " << type;
            return false;
        }
        return true;
    }
    catch (const std::out_of_range &oor)
    {
        NRPI_LOG(ERROR) << "Wrong message format ";
        return false;
    }
}

//------------------------------------------------------------------------------
/**
 * @brief Task init
 *
 * @param data_channel: the data channel where we send the data
 *
 */
void ControllerAudioVideo::init(DataChannelObserver *data_channel)
{
    short nb_devices = Camera::get_nb_devices();
    this->send_msg_client({MSG_TASKS_NB_VID_DEVICES, util::float_to_str(nb_devices)}, data_channel);
}
