/************************************************************************
 NestoRPi Engine
 Copyright (C) 2015-2025 Franck Talbart

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

/* Author: Franck Talbart */

/* This task is responsible for the Servo motors */

#include "src/tasks/ControllerServo.hpp"

#include <algorithm>

// Static variables
const std::string ControllerServo::JSON_TASKS_ACTUATORS_ID = "actuators_id";
const std::string ControllerServo::MSG_TASKS_PCA_9685 = "pca9685";

//------------------------------------------------------------------------------
/**
 * @brief Constructor
 *
 * @param name: name of the task
 * @param cfg_task: map of json values (task config file)
 *
 */
ControllerServo::ControllerServo(const std::string &name, val_json_t &cfg_task)
    : Controller(name, cfg_task)
{
    try
    {
        for (const auto &a : cfg_task.at(JSON_TASKS_ACTUATORS_ID))
            // cppcheck-suppress useStlAlgorithm
            actuators_list.push_back(a->value.toString());
    }
    catch (const std::out_of_range &oor)
    {
        NRPI_LOG(ERROR) << "Servo Controller: Some parameters are missing!";
        throw;
    }
}

//------------------------------------------------------------------------------
/**
 * @brief Init the task
 *
 * @param data_channel: the data channel where we send the data
 *
 */
void ControllerServo::init(DataChannelObserver *data_channel)
{
    try
    {
        for (const auto &actuator : actuators_list)
        {
            float val = dynamic_cast<ActuatorPca9685 *>(actuators.at(actuator).get())->get_action();
            this->send_msg_client({MSG_TASKS_PCA_9685, actuator, util::float_to_str(val)},
                                  data_channel);
        }
    }
    catch (const std::out_of_range &oor)
    {
        NRPI_LOG(ERROR) << "actuator not found";
    }
}

//------------------------------------------------------------------------------
/**
 * @brief Set the angle of a motor
 *
 * @param servo: vector 0 => pca9685 1 => servo name 2 => -100 to 100
 * @return true if executed succesfully, false otherwhise
 */
bool ControllerServo::run(const std::vector<std::string> &servo)
{
    if (servo.size() != 3)
        return false;

    float val = std::atof(servo[2].c_str());
    val = (val > 100) ? 100 : val;
    val = (val < -100) ? -100 : val;
    if (servo[0] == MSG_TASKS_PCA_9685)
    {
        const std::string servo_loc = servo[1];
        try
        {
            dynamic_cast<ActuatorPca9685 *>(actuators.at(servo_loc).get())->set_action(val);
            val = dynamic_cast<ActuatorPca9685 *>(actuators.at(servo_loc).get())->get_action();
            this->send_msg_client({MSG_TASKS_PCA_9685, servo_loc, util::float_to_str(val)});
        }
        catch (const std::out_of_range &oor)
        {
            NRPI_LOG(ERROR) << "actuator not found";
            return false;
        }
        return true;
    }
    return false;
}

//------------------------------------------------------------------------------
/**
 * @brief Reset every servos
 *
 *
 */
void ControllerServo::reset(void)
{
    Controller::reset();
    try
    {
        for (const auto &actuator : actuators_list)
        {
            float val = dynamic_cast<ActuatorPca9685 *>(actuators.at(actuator).get())->get_action();
            this->send_msg_client({MSG_TASKS_PCA_9685, actuator, util::float_to_str(val)});
            dynamic_cast<ActuatorPca9685 *>(actuators.at(actuator).get())->reset();
        }
    }
    catch (const std::out_of_range &oor)
    {
        NRPI_LOG(ERROR) << "actuator not found";
    }
}
