/************************************************************************
 NestoRPi Engine
 Copyright (C) 2015-2025 Franck Talbart

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

/* Author: Franck Talbart */

#include "src/tasks/Task.hpp"

#include <cxxabi.h>

// Static variables
actuators_t Task::actuators;
sensors_t Task::sensors;
std::vector<DataChannelObserver *> Task::data_channel_observers;

//------------------------------------------------------------------------------
/**
 * @brief Constructor
 *
 * @param name: name of the task
 * @param cfg_task: map of json values (task config file)
 *
 */
Task::Task(const std::string &name, const val_json_t &cfg_task) : name(name), cfg_task(cfg_task) {}
//------------------------------------------------------------------------------
/**
 * @brief Reset the task
 *
 *
 */
void Task::reset(void) {}
//------------------------------------------------------------------------------
/**
 * @brief Set a data channel (used to send data)
 *
 * @param data_channel: the data channel observer to set
 *
 */
void Task::set_data_channel(DataChannelObserver *data_channel)
{
    Task::data_channel_observers.push_back(data_channel);
}

//------------------------------------------------------------------------------
/**
 * @brief Remove a data channel (used to send data)
 *
 * @param data_channel: the data channel observer to set
 *
 */
void Task::remove_data_channel(DataChannelObserver *data_channel)
{
    Task::data_channel_observers.erase(
        std::remove(
            Task::data_channel_observers.begin(), Task::data_channel_observers.end(), data_channel),
        Task::data_channel_observers.end());
}

//------------------------------------------------------------------------------
/**
 * @brief Remove every data channels (used to send data)
 *
 *
 */
void Task::remove_all_data_channel(void) { Task::data_channel_observers.clear(); }
//------------------------------------------------------------------------------
/**
 * @brief Set the devices (actuators and sensors) for every tasks
 *
 * @param actuators: map of actuators
 * @param sensors: map of sensors
 *
 */
void Task::set_devices(const actuators_t &actuators, const sensors_t &sensors)
{
    Task::actuators = actuators;
    Task::sensors = sensors;
}

//------------------------------------------------------------------------------
/**
 * @brief Write a message to the client
 *
 * @param msg: the message to send
 * @param data_channel_loc: the data channel where to send the data (optional)
 *
 */
void Task::send_msg_client(const std::vector<std::string> &msg,
                           DataChannelObserver *data_channel_loc)
{
    std::string final_msg = get_type() + COMM_DELIMITER + name;

    for (const auto &m : msg) final_msg += COMM_DELIMITER + m;
    if (data_channel_loc != nullptr)
    {
        data_channel_loc->Send(final_msg);
    }
    else
    {
        for (const auto &data_channel : Task::data_channel_observers) data_channel->Send(final_msg);
    }
}
