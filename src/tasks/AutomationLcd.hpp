/************************************************************************
 NestoRPi Engine
 Copyright (C) 2015-2025 Franck Talbart

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

/* Author: Franck Talbart */

#ifndef DEF_AUTOMATION_LCD
#define DEF_AUTOMATION_LCD

#include <stack>

#include "src/tasks/Automation.hpp"

class AutomationLcd : public Automation
{
  public:
    explicit AutomationLcd(const std::string &name, const val_json_t &cfg_task);
    void init(DataChannelObserver *data_channel) override;
    void send_msg(const Device *s);
    void turn_on(const Device *s);
    void turn_off(const Device *s);

  private:
    void push_msg(const std::string &txt);
    bool run(void) override;

    short low_priority_timeout, high_priority_timeout;
    std::atomic<short> sem;
    std::mutex lock_vec;
    std::string actuator_lcd_id, actuator_voice_id;
    std::queue<std::string> messages;
    std::map<std::string, std::string> list_sensors;

    static const std::string JSON_TASKS_ACTUATOR_LCD_ID;
    static const std::string JSON_TASKS_ACTUATOR_VOICE_ID;
    static const std::string JSON_TASKS_ECO;
    static const std::string JSON_TASKS_LOW_PRIORITY_TIMEOUT;
    static const std::string JSON_TASKS_HIGH_PRIORITY_TIMEOUT;
    static const std::string JSON_TASKS_SENSORS;
};
#endif // DEF_AUTOMATION_LCD
