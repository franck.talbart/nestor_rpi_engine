/************************************************************************
 NestoRPi Engine
 Copyright (C) 2015-2025 Franck Talbart

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

/* Author: Franck Talbart */

/* This task is responsible for the odometry */

#include "src/tasks/AutomationOdometry.hpp"

#include <cmath>
#include <stdexcept>
#include <thread>

#include "src/core/Log.hpp"
#include "src/devices/ActuatorRelay.hpp"
#include "src/devices/SensorRotaryEncoder.hpp"

// Static variables
const std::string AutomationOdometry::JSON_TASKS_WHEEL_BASE = "wheel_base";

const std::string AutomationOdometry::JSON_TASKS_ENCODING_LEFT_ID = "sensor_encoding_left_id";
const std::string AutomationOdometry::JSON_TASKS_ENCODING_RIGHT_ID = "sensor_encoding_right_id";
const std::string AutomationOdometry::JSON_TASKS_MOTOR_LEFT_ID = "actuator_motor_left_id";
const std::string AutomationOdometry::JSON_TASKS_MOTOR_RIGHT_ID = "actuator_motor_right_id";

//------------------------------------------------------------------------------
/**
 * @brief Constructor
 *
 * @param name: name of the task
 * @param cfg_task: map of json values (task config file)
 *
 */
AutomationOdometry::AutomationOdometry(const std::string &name, const val_json_t &cfg_task)
    : Automation(name, cfg_task)
{
    try
    {
        wheel_base = cfg_task.at(JSON_TASKS_WHEEL_BASE).toNumber();

        encoding_left_id = cfg_task.at(JSON_TASKS_ENCODING_LEFT_ID).toString();
        encoding_right_id = cfg_task.at(JSON_TASKS_ENCODING_RIGHT_ID).toString();
    }
    catch (const std::out_of_range &oor)
    {
        NRPI_LOG(ERROR) << "Some parameters are missing in the task config file!";
        throw;
    }

    x = 0;
    y = 0;
    theta = 0;

    // Optional
    motor_left_id = cfg_task.find(JSON_TASKS_MOTOR_LEFT_ID) != cfg_task.end()
                        ? cfg_task.at(JSON_TASKS_MOTOR_LEFT_ID).toString()
                        : std::string("");
    motor_right_id = cfg_task.find(JSON_TASKS_MOTOR_RIGHT_ID) != cfg_task.end()
                         ? cfg_task.at(JSON_TASKS_MOTOR_RIGHT_ID).toString()
                         : std::string("");

    thread_msg = std::thread(&AutomationOdometry::thread_send_msg, this);
}

//------------------------------------------------------------------------------
/**
 * @brief Send in a loop the position of the robot to the data channels
 */
void AutomationOdometry::thread_send_msg(void)
{
    running = true;
    static double old_x = -1, old_y = -1;

    while (true)
    {
        std::unique_lock<std::mutex> lk(mtx_running);
        cv.wait_for(lk, std::chrono::milliseconds(100), [&] { return !running; });
        if (!running)
            break;

        if (old_x != x || old_y != y)
        {
            old_x = x;
            old_y = y;
            this->send_msg_client({std::to_string(x), std::to_string(y)});
        }
    }
}

//------------------------------------------------------------------------------
/**
 * @brief Compute the position of the robot based of the number of tics left / right
 * @param distance_left: in cm
 * @param distance_right: in cm
 *
 * @return true if executed successfully, false otherwhise
 */
void AutomationOdometry::update_position(double distance_left, double distance_right)
{
    if (motor_left_id != "" && motor_right_id != "")
    {
        if (dynamic_cast<ActuatorRelay *>(actuators.at(motor_left_id).get())
                ->get_action("backward"))
        {
            distance_left = -distance_left;
        }

        if (dynamic_cast<ActuatorRelay *>(actuators.at(motor_right_id).get())
                ->get_action("backward"))
        {
            distance_right = -distance_right;
        }
    }

    double delta_s = (distance_left + distance_right) / 2.0;
    double delta_theta = (distance_left - distance_right) / wheel_base;

    theta += delta_theta;

    if (theta > M_PI)
    {
        theta -= 2 * M_PI;
    }
    else if (theta < -M_PI)
    {
        theta += 2 * M_PI;
    }
    x += delta_s * cos(theta);
    y += delta_s * sin(theta);
}

//------------------------------------------------------------------------------
/**
 * @brief Run the Task
 *
 * @return true if executed successfully, false otherwhise
 */
bool AutomationOdometry::run(void)
{
    try
    {
        int refresh_interval =
            dynamic_cast<SensorRotaryEncoder *>(sensors.at(encoding_left_id).get())
                ->get_refresh_interval();
        auto get_distance_left =
            dynamic_cast<SensorRotaryEncoder *>(sensors.at(encoding_left_id).get())
                ->get_accessor_value(true);
        auto get_distance_right =
            dynamic_cast<SensorRotaryEncoder *>(sensors.at(encoding_right_id).get())
                ->get_accessor_value(true);

        while (true)
        {
            std::this_thread::sleep_for(std::chrono::microseconds(refresh_interval));
            if (!running)
                break;
            update_position(get_distance_left(), get_distance_right());
        }
    }
    catch (const std::out_of_range &oor)
    {
        NRPI_LOG(ERROR) << "sensor not found";
    }

    return true;
}

//------------------------------------------------------------------------------
/**
 * @brief Destructor
 */
AutomationOdometry::~AutomationOdometry(void) { thread_msg.join(); }
