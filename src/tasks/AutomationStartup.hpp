/************************************************************************
 NestoRPi Engine
 Copyright (C) 2015-2025 Franck Talbart

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

/* Author: Franck Talbart */

#ifndef DEF_AUTOMATION_STARTUP
#define DEF_AUTOMATION_STARTUP

#include "src/tasks/Automation.hpp"

class AutomationStartup : public Automation
{
  public:
    explicit AutomationStartup(const std::string &name, const val_json_t &cfg_task);
    void init(DataChannelObserver *data_channel) override;
    bool run(void) override { return true; };

  private:
    std::vector<std::string> list_cmds;

    static const std::string JSON_TASKS_CMDS;
};
#endif // DEF_AUTOMATION_STARTUP
