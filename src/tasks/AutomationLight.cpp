/************************************************************************
 NestoRPi Engine
 Copyright (C) 2015-2025 Franck Talbart

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

/* Author: Franck Talbart */

/* This task is responsible for the automated light */

#include "src/tasks/AutomationLight.hpp"

#include <unistd.h>

#include "src/devices/ActuatorRelay.hpp"
#include "src/tasks/ControllerRelay.hpp"
#include "src/tasks/Task.hpp"

// Static variables
const std::string AutomationLight::JSON_TASKS_BRIGHTNESS_THRESHOLD = "brightness_threshold";

const std::string AutomationLight::JSON_TASKS_LIGHT_ID = "actuator_light_id";
const std::string AutomationLight::JSON_TASKS_PHOTORESISTOR_ID = "sensor_photoresistor_id";
const std::string AutomationLight::JSON_TASKS_RELAY_ID = "task_relay_id";

//------------------------------------------------------------------------------
/**
 * @brief Constructor
 *
 * @param name: name of the task
 * @param cfg_task: map of json values (task config file)
 *
 */
AutomationLight::AutomationLight(const std::string &name, const val_json_t &cfg_task)
    : Automation(name, cfg_task), value(false)
{
    try
    {
        brightness_threshold = cfg_task.at(JSON_TASKS_BRIGHTNESS_THRESHOLD).toNumber();
        light_id = cfg_task.at(JSON_TASKS_LIGHT_ID).toString();
        photoresistor_id = cfg_task.at(JSON_TASKS_PHOTORESISTOR_ID).toString();
        relay_id = cfg_task.at(JSON_TASKS_RELAY_ID).toString();
    }
    catch (const std::out_of_range &oor)
    {
        NRPI_LOG(ERROR) << "Some parameters are missing!";
        throw;
    }

    sem = 0;
}

//------------------------------------------------------------------------------
/**
 * @brief Task init
 *
 * @param data_channel: the data channel where we send the data
 *
 */
void AutomationLight::init(DataChannelObserver *data_channel)
{
    // sem is a counter because we can have multiple users using the robot at the same time
    if (sem == 0)
        value = dynamic_cast<ActuatorRelay *>(actuators.at(light_id).get())->get_action("on");
    UNUSED(data_channel);

    ++sem;

    cv.notify_all();
}

//------------------------------------------------------------------------------
/**
 * @brief Run the Task
 *
 * @return true if executed successfully, false otherwhise
 */
bool AutomationLight::run(void)
{
    while (true)
    {
        std::unique_lock<std::mutex> lk(mtx_running);
        cv.wait(lk, [this] { return (sem != 0) || !running; });
        if (!running)
            break;
        cv.wait_for(lk, std::chrono::seconds(4), [&] { return !running; });
        if (!running)
            break;
        float brightness = 0;
        try
        {
            brightness = stof(sensors.at(photoresistor_id)->get_data());
        }
        catch (const std::out_of_range &oor)
        {
            NRPI_LOG(ERROR) << "sensor not found";
        }
        try
        {
            if (brightness <= brightness_threshold)
            {
                // Turn on
                if (sem > 0 && !value)
                {
                    NRPI_LOG(INFO) << "Turning on the light";
                    tasks_controller.at(relay_id).lock()->run({light_id, "on", "1"});
                    value = true;
                }
            }
            else if (value)
            {
                // Turn off
                NRPI_LOG(INFO) << "Turning off the light";
                tasks_controller.at(relay_id).lock()->run({light_id, "on", "0"});
                value = false;
            }
        }
        catch (const std::out_of_range &oor)
        {
            NRPI_LOG(ERROR) << "Actuator or controller not found";
        }
    }
    return true;
}

//------------------------------------------------------------------------------
/**
 * @brief Reset called at client disconnection
 *
 *
 */
void AutomationLight::reset(void)
{
    Automation::reset();
    if (sem > 0)
    {
        --sem;
    }
    // We turn off the light only if there is no more users on the robot
    if (sem == 0)
    {
        try
        {
            tasks_controller.at(relay_id).lock()->run({light_id, "on", "0"});
        }
        catch (const std::out_of_range &oor)
        {
            NRPI_LOG(ERROR) << "Actuator or controller not found";
        }

        value = 0;
    }

    cv.notify_all();
}
