/************************************************************************
 NestoRPi Engine
 Copyright (C) 2015-2025 Franck Talbart

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

/* Author: Franck Talbart */

#ifndef DEF_AUTOMATION_LIGHT
#define DEF_AUTOMATION_LIGHT

#include "src/tasks/Automation.hpp"

class AutomationLight : public Automation
{
  public:
    explicit AutomationLight(const std::string &name, const val_json_t &cfg_task);
    void init(DataChannelObserver *data_channel) override;
    void reset(void) override;

  private:
    bool run(void) override;

    std::atomic<short> sem;
    float brightness_threshold;
    std::string light_id, photoresistor_id, relay_id;
    std::atomic<bool> value;

    static const std::string JSON_TASKS_BRIGHTNESS_THRESHOLD;

    static const std::string JSON_TASKS_LIGHT_ID;
    static const std::string JSON_TASKS_PHOTORESISTOR_ID;
    static const std::string JSON_TASKS_RELAY_ID;
};
#endif // DEF_AUTOMATION_LIGHT
