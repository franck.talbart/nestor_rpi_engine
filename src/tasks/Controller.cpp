/************************************************************************
 NestoRPi Engine
 Copyright (C) 2015-2025 Franck Talbart

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

/* Author: Franck Talbart */

#include "src/tasks/Controller.hpp"

#include "src/core/util.hpp"

// Static variables
const std::string Controller::JSON_TASKS_ROLE_MINIMAL = "role_minimal";
const std::string Controller::JSON_TASKS_TYPE_VALUE = "controller";
const std::string Controller::MSG_TASKS_FORBIDDEN = "forbidden";

std::map<std::string, std::weak_ptr<Controller>> Controller::tasks_controller;

//------------------------------------------------------------------------------
/**
 * @brief Constructor
 *
 * @param name: name of the task
 * @param cfg_task: map of json values (task config file)
 *
 */
Controller::Controller(const std::string &name, val_json_t &cfg_task) : Task(name, cfg_task)
{
    try
    {
        role_minimal = util::str_to_role(cfg_task.at(JSON_TASKS_ROLE_MINIMAL).toString());
        cfg_task.erase(JSON_TASKS_ROLE_MINIMAL);
    }
    catch (const std::out_of_range &oor)
    {
        role_minimal = FULL;
    }
}

//------------------------------------------------------------------------------
/**
 * @brief Set the list of tasks
 *
 * @param tasks_controller_l: map of Controller
 *
 */
void Controller::set_tasks(const tasks_controller_t &tasks_controller_l)
{
    Controller::tasks_controller.insert(tasks_controller_l.begin(), tasks_controller_l.end());
}

//------------------------------------------------------------------------------
/**
 * @brief Run the action depending on the role
 *
 * @param role: user's role
 * @param actions: the action parameters
 * @return true if executed successfully, false otherwhise
 */
bool Controller::set_action(const role_type role, const std::vector<std::string> &actions)
{
    if (role >= role_minimal)
        return run(actions);
    throw std::string("Forbidden");
}
