/************************************************************************
 NestoRPi Engine
 Copyright (C) 2015-2025 Franck Talbart

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

/* Author: Franck Talbart */

#ifndef DEF_CONTROLLER_RELAY
#define DEF_CONTROLLER_RELAY

#include "src/tasks/Controller.hpp"

class ControllerRelay : public Controller
{
  public:
    explicit ControllerRelay(const std::string &name, val_json_t &cfg_task);
    bool run(const std::vector<std::string> &actuator) override;
    void init(DataChannelObserver *data_channel) override;
    void reset(void) override;

  private:
    bool is_sensor_ok(const std::string &op, float val, const std::shared_ptr<Sensor> &sens) const;
    bool is_actuator_ok(const std::string &action, bool val, std::shared_ptr<Actuator> act) const;
    std::map<
        std::string,
        std::map<std::string, std::map<std::string, std::map<std::string, std::function<bool()>>>>>
        constraints;
    std::map<std::string, std::map<std::string, gason::JsonValue>> on_one;

    static const std::string JSON_TASKS_TO_ZERO;
    static const std::string JSON_TASKS_TO_ONE;
    static const std::string JSON_TASKS_ON_ONE;
    static const std::string JSON_TASKS_OPERATOR;
    static const std::string JSON_TASKS_ACTION;
    static const std::string JSON_TASKS_VALUE;
};
#endif // DEF_CONTROLLER_RELAY
