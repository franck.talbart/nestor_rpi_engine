/************************************************************************
 NestoRPi Engine
 Copyright (C) 2015-2025 Franck Talbart

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

/* Author: Franck Talbart */

#ifndef DEF_AUTOMATION
#define DEF_AUTOMATION

#include <condition_variable>
#include <mutex>
#include <thread>

#include "src/tasks/Controller.hpp"
#include "src/tasks/Task.hpp"

class Automation : public Task
{
  public:
    explicit Automation(const std::string &name, const val_json_t &cfg_task);
    static void set_tasks(
        const std::map<std::string, std::shared_ptr<Automation>> &tasks_automation,
        const tasks_controller_t &tasks_controller);
    virtual void auto_run(void);
    virtual void start(Device *device = nullptr);
    virtual bool stop(Device *device = nullptr);
    virtual ~Automation(void){};

    const static std::string JSON_TASKS_TYPE_VALUE;

  protected:
    virtual bool run(void) = 0;
    std::string get_type(void) const override { return JSON_TASKS_TYPE_VALUE; }

    volatile bool running;
    std::condition_variable cv;
    std::mutex mtx_running;
    // Circular dependency: using weak_ptr
    static std::map<std::string, std::weak_ptr<Automation>> tasks_automation;
    static std::map<std::string, std::weak_ptr<Controller>> tasks_controller;
    std::thread thread_task;
    bool returned_val;
};

typedef std::map<std::string, std::shared_ptr<Automation>> tasks_automation_t;
#endif // DEF_AUTOMATION
