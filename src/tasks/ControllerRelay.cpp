/************************************************************************
 NestoRPi Engine
 Copyright (C) 2015-2025 Franck Talbart

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

/* Author: Franck Talbart */

/* This task manages the relays */

#include "src/tasks/ControllerRelay.hpp"

#include <unistd.h>

#include <cstring>
#include <functional>
#include <iostream>

#include "src/core/util.hpp"
#include "src/core/util_extensions.hpp"
#include "src/devices/ActuatorRelay.hpp"

// Static variables
const std::string ControllerRelay::JSON_TASKS_TO_ZERO = "constraint_to_zero";
const std::string ControllerRelay::JSON_TASKS_TO_ONE = "constraint_to_one";
const std::string ControllerRelay::JSON_TASKS_ON_ONE = "on_one";
const std::string ControllerRelay::JSON_TASKS_OPERATOR = "operator";
const std::string ControllerRelay::JSON_TASKS_ACTION = "action";
const std::string ControllerRelay::JSON_TASKS_VALUE = "value";

//------------------------------------------------------------------------------
/**
 * @brief Constructor
 *
 * @param name: name of the task
 * @param cfg_task: map of json values (task config file)
 *
 */
ControllerRelay::ControllerRelay(const std::string &name, val_json_t &cfg_task)
    : Controller(name, cfg_task)
{
    for (const auto &actuator : cfg_task)
    {
        std::map<std::string, std::map<std::string, std::map<std::string, std::function<bool()>>>>
            const_action;
        for (const auto &action : actuator.second)
        {
            // Load the constraints
            std::map<std::string, std::map<std::string, std::function<bool()>>> const_state;
            for (const auto &state : action->value)
            {
                std::map<std::string, std::function<bool()>> const_device;

                if (state->key == JSON_TASKS_TO_ZERO || state->key == JSON_TASKS_TO_ONE)
                {
                    for (const auto &device : state->value)
                    {
                        std::map<std::string, std::shared_ptr<Sensor>>::const_iterator
                            it_sens_const = sensors.find(device->key);
                        std::map<std::string, std::shared_ptr<Actuator>>::const_iterator
                            it_act_const = actuators.find(device->key);
                        if (it_sens_const != sensors.end())
                        {
                            const_device[device->key] = std::bind(
                                &ControllerRelay::is_sensor_ok,
                                this,
                                std::string(device->value(JSON_TASKS_OPERATOR.c_str()).toString()),
                                device->value(JSON_TASKS_VALUE.c_str()).toNumber(),
                                it_sens_const->second);
                        }
                        else if (it_act_const != actuators.end())
                        {
                            const_device[device->key] = std::bind(
                                &ControllerRelay::is_actuator_ok,
                                this,
                                std::string(device->value(JSON_TASKS_ACTION.c_str()).toString()),
                                device->value(JSON_TASKS_VALUE.c_str()).getTag() ==
                                    gason::JSON_TRUE,
                                it_act_const->second);
                        }
                        else
                        {
                            NRPI_LOG(WARN) << "I don't know what " << device->key
                                           << " is in the tasks cfg file.";
                        }
                    }
                    const_state[state->key] = const_device;
                }
                else if (state->key == JSON_TASKS_ON_ONE)
                {
                    on_one[actuator.first][action->key] = state->value;
                }
            }
            try
            {
                if (actuators.find(actuator.first) != actuators.end() &&
                    dynamic_cast<ActuatorRelay *>(actuators.at(actuator.first).get())
                        ->has_action(action->key))
                {
                    const_action[action->key] = const_state;
                }
                else
                {
                    NRPI_LOG(WARN) << "I don't know what the " << action->key
                                   << " action is in the tasks cfg file.";
                }
            }
            catch (const std::out_of_range &oor)
            {
                NRPI_LOG(ERROR) << "actuator not found";
            }
        }

        if (actuators.find(actuator.first) != actuators.end())
        {
            constraints[actuator.first] = const_action;
        }
        else
        {
            NRPI_LOG(WARN) << "I don't know what the " << actuator.first
                           << " actuator is in the tasks cfg file.";
        }
    }
}

//------------------------------------------------------------------------------
/**
 * @brief Set a status to a relay, if the constraints are verified
 *
 * @param actuator: vector 0 => actuator 1=> action name 2=> "0" or "1"
 * @return true: sucess, false otherwhise
 */
bool ControllerRelay::run(const std::vector<std::string> &actuator)
{
    if (actuator.size() != 3)
        return false;

    const std::string actuator_loc = actuator[0];
    const std::string action = actuator[1];
    bool val = std::stoi(actuator[2].c_str());

    std::map<std::string, std::shared_ptr<Actuator>>::const_iterator it_act =
        actuators.find(actuator_loc);

    if (it_act == actuators.end())
        return false;

    bool constraints_ok = true;
    try
    {
        std::map<std::string, std::map<std::string, std::function<bool()>>> constraint =
            constraints.at(actuator_loc).at(action); // at => if key does not exist, exception

        std::map<std::string, std::function<bool()>> devices_constraints =
            constraint[val ? JSON_TASKS_TO_ONE : JSON_TASKS_TO_ZERO];

        // All the constraints must be verified
        for (const auto &device : devices_constraints)
        {
            constraints_ok = device.second();
            if (!constraints_ok)
                break;
        }
    }
    catch (const std::out_of_range &oor)
    {
        constraints_ok = false;
    }
    if (constraints_ok)
    {
        if (val)
        {
            try
            {
                for (const auto &action_ : on_one.at(actuator_loc).at(action))
                {
                    for (const auto &params : action_->value)
                    {
                        std::vector<std::string> action_params;
                        for (const auto &param : params->value)
                            // cppcheck-suppress useStlAlgorithm
                            action_params.push_back(param->value.toString());
                        tasks_controller.at(action_->key).lock()->run({action_params});
                    }
                }
            }
            catch (const std::out_of_range &oor)
            {
            }
        }

        // Update the status
        dynamic_cast<ActuatorRelay *>(it_act->second.get())->set_action({action}, val);
        NRPI_LOG(INFO) << actuator_loc << ": " << action << " value: " << val;
        bool new_status = dynamic_cast<ActuatorRelay *>(it_act->second.get())->get_action(action);
        if (new_status != val)
        {
            NRPI_LOG(ERROR) << "Something is going wrong! Value of " << action << " not updated.";
        }
        this->send_msg_client({actuator_loc, action, std::to_string(new_status * 100)});
    }
    else
    {
        NRPI_LOG(INFO) << "Could not apply " << action << " on " << actuator_loc
                       << " because the constraints are not met";
    }
    return true;
}

//------------------------------------------------------------------------------
/**
 * @brief Send the required data when the task is initialized
 *
 * @param data_channel: the data channel where we send the data
 *
 */
void ControllerRelay::init(DataChannelObserver *data_channel)
{
    for (const auto &constraint : constraints)
    {
        std::map<std::string, std::shared_ptr<Actuator>>::const_iterator it_act =
            actuators.find(constraint.first);
        if (it_act != actuators.end())
        {
            std::map<std::string, bool> actions =
                dynamic_cast<ActuatorRelay *>(it_act->second.get())->get_all_actions();
            // send the status of every relays
            for (const auto &action : actions)
            {
                this->send_msg_client(
                    {constraint.first, action.first, std::to_string(action.second * 100)},
                    data_channel);
            }
        }
        else
        {
            NRPI_LOG(ERROR) << "Cannot find actuator " << constraint.first;
        }
    }
}

//------------------------------------------------------------------------------
/**
 * @brief Reset every relays
 *
 *
 */
void ControllerRelay::reset(void)
{
    Controller::reset();
    for (const auto &constraint : constraints)
    {
        std::map<std::string, std::shared_ptr<Actuator>>::const_iterator it_act =
            actuators.find(constraint.first);
        if (it_act != actuators.end())
        {
            dynamic_cast<ActuatorRelay *>(it_act->second.get())->reset();
            std::map<std::string, bool> actions =
                dynamic_cast<ActuatorRelay *>(it_act->second.get())->get_all_actions();
            // send the status of every relays
            for (const auto &action : actions)
            {
                this->send_msg_client(
                    {constraint.first, action.first, std::to_string(action.second * 100)});
            }
        }
    }
}

//------------------------------------------------------------------------------
/**
 * @brief check if the constraint is verified
 *
 * @param op: the operator = < > <= >=
 * @param val: the operand
 * @param sens: the sensor to check
 *
 * @return true if constraints are verified, false otherwhise
 */
bool ControllerRelay::is_sensor_ok(const std::string &op,
                                   float val,
                                   const std::shared_ptr<Sensor> &sens) const
{
    bool constraints_ok;

    if (op == "=")
    {
        constraints_ok = (stof(sens->get_data()) == val);
    }
    else if (op == "<")
    {
        constraints_ok = (stof(sens->get_data()) < val);
    }
    else if (op == ">")
    {
        constraints_ok = (stof(sens->get_data()) > val);
    }
    else if (op == "<=")
    {
        constraints_ok = (stof(sens->get_data()) <= val);
    }
    else if (op == ">=")
    {
        constraints_ok = (stof(sens->get_data()) >= val);
    }
    else
    {
        NRPI_LOG(ERROR) << "Unknown operator " << op;
        constraints_ok = false;
    }

    return constraints_ok;
}

//------------------------------------------------------------------------------
/**
 * @brief check if the actuator status is equivalent to the given value
 *
 * @param action: the action to check
 * @param val: the operand
 * @param act: the actuator to check
 *
 * @return true if equivalent , false otherwhise
 */
bool ControllerRelay::is_actuator_ok(const std::string &action,
                                     bool val,
                                     std::shared_ptr<Actuator> act) const
{
    return (dynamic_cast<ActuatorRelay *>(act.get())->get_action(action) == val);
}
