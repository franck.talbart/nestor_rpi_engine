/************************************************************************
 NestoRPi Engine
 Copyright (C) 2015-2025 Franck Talbart

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

/* Author: Franck Talbart */

/* This task is used to transmit the data from a set of sensors */

#include "src/tasks/AutomationMonitoring.hpp"

#include <errno.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include <algorithm>
#include <cstring>
#include <iomanip>
#include <iostream>
#include <string>

#include "src/tasks/Task.hpp"

// Static variables
const std::string AutomationMonitoring::JSON_TASKS_SENSORS_ID = "sensors_id";

//------------------------------------------------------------------------------
/**
 * @brief Constructor
 *
 * @param name: name of the task
 * @param cfg_task: map of json values (task config file)
 *
 */
AutomationMonitoring::AutomationMonitoring(const std::string &name, const val_json_t &cfg_task)
    : Automation(name, cfg_task)
{
    try
    {
        shm_path = util::get_cfg_value(CONFIG_SHM_PATH).toString();
        for (const auto &s : cfg_task.at(JSON_TASKS_SENSORS_ID))
            // cppcheck-suppress useStlAlgorithm
            list_sensors.push_back(s->value.toString());
    }
    catch (const std::out_of_range &oor)
    {
        NRPI_LOG(ERROR) << "Some parameters are missing!";
        throw;
    }
}

//------------------------------------------------------------------------------
/**
 *  @brief Destructor
 */
AutomationMonitoring::~AutomationMonitoring(void)
{
    for (const auto &d : dev) close(d.second);
}

//------------------------------------------------------------------------------
/**
 * @brief Run the Task
 *
 * @return true if executed successfully, false otherwhise
 */
bool AutomationMonitoring::run(void)
{
    while (true)
    {
        {
            std::unique_lock<std::mutex> lock(mtx_running);
            if (!running)
                break;
        }

        if (!refresh())
            return false;

        std::unique_lock<std::mutex> lk(mtx_running);
        cv.wait_for(lk, std::chrono::seconds(1), [&] { return !running; });
        if (!running)
            break;
    }
    return true;
}

//------------------------------------------------------------------------------
/**
 * @brief Send the sensor data to the client
 *
 * @param data_channel: the data channel where the data are sent (optional)
 * @return true if executed successfully, false otherwhise
 */
bool AutomationMonitoring::refresh(DataChannelObserver *data_channel)
{
    std::string res_sens;
    for (const auto &s : list_sensors)
    {
        // For each analogic sensors, we grab the data and send it
        try
        {
            res_sens = sensors.at(s)->get_data();
            this->send_msg_client({s, res_sens}, data_channel);
            // We also write into the shared memory to interact with a
            // monitoring tool such as Munin
            if (!write_shm(s, res_sens))
                return false;
        }
        catch (const std::out_of_range &oor)
        {
            NRPI_LOG(ERROR) << "sensor " << s << " not found";
        }
    }
    return true;
}

//------------------------------------------------------------------------------
/**
 * @brief Send the required data when the task init initialized
 *
 * @param data_channel: the data channel where we send the data
 *
 */
void AutomationMonitoring::init(DataChannelObserver *data_channel) { refresh(data_channel); }
//------------------------------------------------------------------------------
/**
 * @brief Write into the shared memory
 *
 * @param id: shm id
 * @param val: value to store
 * @return true if executed successfully, false otherwhise
 */
bool AutomationMonitoring::write_shm(const std::string &id, const std::string &val)
{
    char *data;
    std::string shm_dev = shm_path + "/" + PREFIX_SHM + id;
    const std::string val_eol = val + "\n";
    size_t size = val_eol.length();

    if (dev.find(shm_dev) == dev.end())
    {
        if ((dev[shm_dev] = open(shm_dev.c_str(), O_RDWR | O_CREAT, 0604)) == -1)
        {
            NRPI_LOG(ERROR) << "Can't open the shared mem";
            return false;
        }

        lseek(dev[shm_dev], size + 1, SEEK_SET);
        ssize_t len = write(dev[shm_dev], "", 1);
        if (len <= 0)
        {
            NRPI_LOG(ERROR) << "Can't write shared mem";
        }
        lseek(dev[shm_dev], 0, SEEK_SET);
    }

    if ((data = reinterpret_cast<char *>(mmap(
             nullptr, size, PROT_READ | PROT_WRITE, MAP_SHARED, dev[shm_dev], 0))) == MAP_FAILED)
    {
        NRPI_LOG(ERROR) << "Can't mmap";
        return false;
    }

    strncpy(data, val_eol.c_str(), size);

    if (munmap(data, size) == -1)
    {
        NRPI_LOG(ERROR) << "Error un-mmapping the file";
        return false;
    }
    return true;
}
