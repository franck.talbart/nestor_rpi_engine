/************************************************************************
 NestoRPi Engine
 Copyright (C) 2015-2025 Franck Talbart

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

/* Author: Franck Talbart */

/* This task is responsible for the charging mode of the battery */

#include "src/tasks/AutomationBattery.hpp"

#include <unistd.h>

#include <ctime>

#include "src/devices/ActuatorRelay.hpp"
#include "src/tasks/ControllerRelay.hpp"

// Static variables
const std::string AutomationBattery::JSON_TASKS_MAX_VOLTAGE = "max_voltage";
const std::string AutomationBattery::JSON_TASKS_MIN_TIME = "min_time";
const std::string AutomationBattery::JSON_TASKS_MAX_TIME = "max_time";
const std::string AutomationBattery::JSON_TASKS_MAX_TMP = "max_tmp";
const std::string AutomationBattery::JSON_TASKS_PERIODIC_CHARGE = "periodic_charge";

const std::string AutomationBattery::JSON_TASKS_BATTERY_ID = "actuator_battery_id";
const std::string AutomationBattery::JSON_TASKS_VOLTAGE_ID = "sensor_voltage_station_id";
const std::string AutomationBattery::JSON_TASKS_TEMP_ID = "sensor_temp_battery_id";
const std::string AutomationBattery::JSON_TASKS_RELAY_ID = "task_relay_id";

//------------------------------------------------------------------------------
/**
 * @brief Constructor
 *
 * @param name: name of the task
 * @param cfg_task: map of json values (task config file)
 *
 */
AutomationBattery::AutomationBattery(const std::string &name, const val_json_t &cfg_task)
    : Automation(name, cfg_task)
{
    try
    {
        max_voltage = cfg_task.at(JSON_TASKS_MAX_VOLTAGE).toNumber();
        min_time = cfg_task.at(JSON_TASKS_MIN_TIME).toNumber();
        max_time = cfg_task.at(JSON_TASKS_MAX_TIME).toNumber();
        max_tmp = cfg_task.at(JSON_TASKS_MAX_TMP).toNumber();
        periodic_charge = cfg_task.at(JSON_TASKS_PERIODIC_CHARGE).toNumber();

        battery_id = cfg_task.at(JSON_TASKS_BATTERY_ID).toString();
        voltage_id = cfg_task.at(JSON_TASKS_VOLTAGE_ID).toString();
        temp_id = cfg_task.at(JSON_TASKS_TEMP_ID).toString();
        relay_id = cfg_task.at(JSON_TASKS_RELAY_ID).toString();
    }
    catch (const std::out_of_range &oor)
    {
        NRPI_LOG(ERROR) << "Some parameters are missing in the task config file!";
        throw;
    }

    // sem is an int because it can be called multiple times
    sem = 0;
    t_begin = time(nullptr);

    try
    {
        ADD_TRIGGER(Device::ON, actuators.at(battery_id), &AutomationBattery::start_charge_battery);
        ADD_TRIGGER(Device::OFF, actuators.at(battery_id), &AutomationBattery::stop_charge_battery);
    }
    catch (const std::out_of_range &oor)
    {
        NRPI_LOG(ERROR) << "actuator not found";
        throw;
    }

    thread_periodical_charge = std::thread(&AutomationBattery::periodical_charge, this);
}

//------------------------------------------------------------------------------
/**
 * @brief Thread used to periodically charge the battery (increase the battery's life time)
 *
 *
 */
void AutomationBattery::periodical_charge(void)
{
    running = true;
    while (true)
    {
        std::unique_lock<std::mutex> lk(mtx_running);
        cv.wait_for(lk, std::chrono::seconds(600), [&] { return !running; });
        if (!running)
            break;

        time_t t_current = time(nullptr);
        double diff_t = difftime(t_current, t_begin);

        try
        {
            if (diff_t > periodic_charge)
            {
                NRPI_LOG(INFO) << "Launching automatic battery charging.";
                tasks_controller.at(relay_id).lock()->run({battery_id, "on", "1"});
            }
        }
        catch (const std::out_of_range &oor)
        {
            NRPI_LOG(ERROR) << "actuator not found";
        }
    }
}

//------------------------------------------------------------------------------
/**
 * @brief Start event
 *
 * @param s: the device which triggers it
 *
 */
void AutomationBattery::start_charge_battery(const Device *s)
{
    UNUSED(s);

    ++sem;
    if (sem == 1)
    {
        NRPI_LOG(INFO) << "Starting the charging mode";

        t_begin = time(nullptr);

        cv.notify_all();
    }
}

//------------------------------------------------------------------------------
/**
 * @brief Stop event
 *
 * @param s: the device which triggers it
 *
 */
void AutomationBattery::stop_charge_battery(const Device *s)
{
    UNUSED(s);

    if (sem > 0)
        --sem;

    if (sem == 0)
    {
        NRPI_LOG(INFO) << "Stopping the charging mode";
        cv.notify_all();
    }
}

//------------------------------------------------------------------------------
/**
 * @brief Run the Task
 *
 * @return true if executed successfully, false otherwhise
 */
bool AutomationBattery::run(void)
{
    time_t t_end = time(nullptr);
    float voltage_battery = 0;
    float temp_battery = 0;

    while (true)
    {
        std::unique_lock<std::mutex> lk(mtx_running);
        cv.wait(lk, [this] { return (sem > 0) || !running; });
        if (!running)
            break;
        // This sleep prevents the relay from switching too quickly
        cv.wait_for(lk, std::chrono::seconds(2), [&] { return !running; });
        if (!running)
            break;
        try
        {
            voltage_battery = stof(sensors.at(voltage_id)->get_data());
            temp_battery = stof(sensors.at(temp_id)->get_data());
        }
        catch (const std::out_of_range &oor)
        {
            NRPI_LOG(ERROR) << "sensor not found";
            return false;
        }

        t_end = time(nullptr);
        double diff_t = difftime(t_end, t_begin);

        // End of charging if voltage is ok, or if battery is too hot, or if
        // timeout is reached
        try
        {
            if (dynamic_cast<ActuatorRelay *>(actuators.at(battery_id).get())->get_action("on") &&
                ((diff_t > min_time && voltage_battery > max_voltage) || temp_battery > max_tmp ||
                 diff_t > max_time))
            {
                NRPI_LOG(INFO) << "Automated pause of battery charging.";
                NRPI_LOG(INFO) << "Voltage: " << voltage_battery << " V";
                NRPI_LOG(INFO) << "Temp: " << temp_battery << " °C";
                NRPI_LOG(INFO) << "Time: " << diff_t << " s";
                tasks_controller.at(relay_id).lock()->run({battery_id, "on", "0"});
            }
        }
        catch (const std::out_of_range &oor)
        {
            NRPI_LOG(ERROR) << "Actuator or controller not found";
            return false;
        }
    }

    return true;
}

//------------------------------------------------------------------------------
/**
 * @brief Destructor
 */
AutomationBattery::~AutomationBattery(void) { thread_periodical_charge.join(); }
