/************************************************************************
 NestoRPi Engine
 Copyright (C) 2015-2025 Franck Talbart

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

/* Author: Franck Talbart */

/* This task is responsible for the voice of the robot*/

#include "src/tasks/ControllerVoice.hpp"

#include "src/devices/ActuatorMbrola.hpp"

// Static variables
const std::string ControllerVoice::JSON_TASKS_MBROLA_ID = "actuator_mbrola_id";

const std::string ControllerVoice::MSG_TASKS_MBROLA = "mbrola";

//------------------------------------------------------------------------------
/**
 * @brief Constructor
 *
 * @param name: name of the task
 * @param cfg_task: map of json values (task config file)
 *
 */
ControllerVoice::ControllerVoice(const std::string &name, val_json_t &cfg_task)
    : Controller(name, cfg_task)
{
    try
    {
        mbrola_id = cfg_task.at(JSON_TASKS_MBROLA_ID).toString();
    }
    catch (const std::out_of_range &oor)
    {
        NRPI_LOG(ERROR) << "Voice Controller: Some parameters are missing!";
        throw;
    }
}

//------------------------------------------------------------------------------
/**
 * @brief Say the text
 *
 * @param txt: 0 => type of voice synthetizer 1=> text
 * @return true is success, false otherwhise
 */
bool ControllerVoice::run(const std::vector<std::string> &txt)
{
    if (txt.size() != 2)
        return false;
    try
    {
        if (txt.at(0) == MSG_TASKS_MBROLA)
        {
            NRPI_LOG(INFO) << "Saying " << txt.at(1);
            dynamic_cast<ActuatorMbrola *>(actuators.at(mbrola_id).get())->set_action(txt.at(1));
        }
    }
    catch (const std::out_of_range &oor)
    {
        NRPI_LOG(ERROR) << "actuator not found";
        return false;
    }

    return true;
}
