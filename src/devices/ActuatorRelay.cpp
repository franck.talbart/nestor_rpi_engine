/************************************************************************
 NestoRPi Engine
 Copyright (C) 2015-2025 Franck Talbart

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

/* Author: Franck Talbart */

#include "src/devices/ActuatorRelay.hpp"

#include <unistd.h>

#include <iostream>
#include <map>
#include <memory>
#include <string>
#include <thread>

#include "gason.hpp"
#include "src/core/util.hpp"

// Static variables
const std::string ActuatorRelay::JSON_DEVICES_INITIALIZE = "to_initialize";
std::mutex ActuatorRelay::lock;

//------------------------------------------------------------------------------
/**
 *  @brief Constructor, set the pins to 0 if needed
 *
 *  @param pins: map of pins
 *  @param val_actuator: map of json values (device config file)
 *
 */
ActuatorRelay::ActuatorRelay(const pins_t &pins, const val_json_t &val_actuator)
    : Actuator(pins, val_actuator)
{
    to_initialize = true;

    val_json_t::const_iterator i = val_actuator.find(JSON_DEVICES_INITIALIZE);
    if (i != val_actuator.end())
        to_initialize = (i->second.getTag() == gason::JSON_TRUE);

    // depending on to_initialize, setting somes pins to 0
    for (const auto &p : pins) GPIOS_PINMODE(p.second, GPIOS_OUTPUT);
    reset();
}

//------------------------------------------------------------------------------
/**
 *  @brief Destructor
 */
ActuatorRelay::~ActuatorRelay(void) { reset(); }
//------------------------------------------------------------------------------
/**
 *  @brief set the status of a pin and run the corresponding trigger if needed
 *
 *  @param name: the action name that will set the corresponding pins
 *  @param val: true: set is as up, down otherwhise
 *
 */
void ActuatorRelay::set_action(const std::string &name, bool val)
{
    if (this->has_action(name))
    {
        if (this->get_action(name) == val)
            return;
        lock.lock();
        // It is important to make sure that multiple relays are not
        // switched at the same time to avoid electrical noises
        // which would result into unexpected errors from the CPU
        // (stack overflow, etc ...)
        GPIOS_DIGITALWRITE(pins.at(name), val); // pins is const, so no operator []
        std::this_thread::sleep_for(std::chrono::milliseconds(40));
        lock.unlock();
        run_trigger(val);
    }
    else
    {
        NRPI_LOG(WARN) << "Action " << name << " not found";
    }
}

//------------------------------------------------------------------------------
/**
 *  @brief return the status of the pin
 *
 *  @param name: the action name
 *  @return true if up, false otherwhise
 */
bool ActuatorRelay::get_action(const std::string &name) const
{
    bool result = false;
    if (this->has_action(name))
    {
        result = GPIOS_DIGITALREAD(pins.at(name));
    }
    else
    {
        NRPI_LOG(WARN) << "Action " << name << " not found";
    }
    return result;
}

//------------------------------------------------------------------------------
/**
 *  @brief Refresh the trigger's status at init
 *
 *  @param number: the event number
 *  @param callback: the function to call when the event occurs. The
 *                   device which triggered the function is sent as a param.
 *
 */
void ActuatorRelay::add_trigger_on_event(short number, std::function<void(Device *)> callback)
{
    Device::add_trigger_on_event(number, callback);
    for (const auto &p : pins)
        if (number == get_action(p.first))
            callback(this);
}

//------------------------------------------------------------------------------
/**
 *  @brief set the pin to 0 if to_initialise is true
 */
void ActuatorRelay::reset(void)
{
    if (to_initialize)
        for (const auto &p : pins) GPIOS_DIGITALWRITE(p.second, 0);
}

//------------------------------------------------------------------------------
/**
 * @brief Check that the actuator provides the action
 *
 * @param name: name of the action
 * @return true if the action exists, false otherwhise
 */
bool ActuatorRelay::has_action(const std::string &name) const
{
    return (pins.find(name) != pins.end());
}

//------------------------------------------------------------------------------
/**
 * @brief Return the status of all the actions
 *
 * @return map of name / bool
 */
std::map<std::string, bool> ActuatorRelay::get_all_actions(void) const
{
    std::map<std::string, bool> res;
    for (const auto &p : pins) res[p.first] = get_action(p.first);
    return res;
}
