/************************************************************************
 NestoRPi Engine
 Copyright (C) 2015-2025 Franck Talbart

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

/* Author: Franck Talbart */

#ifndef DEF_SENSOR_MCP_3008
#define DEF_SENSOR_MCP_3008

#include "src/devices/Sensor.hpp"

class SensorMcp3008 : public Sensor
{
  public:
    SensorMcp3008(const pins_t &pins, const val_json_t &val_device);
    void measure(void) override;
    std::string get_data(void) override;

  private:
    float compute(const std::string &formula);
    float process_formula(std::string &formula);
    void replace(std::string &str, const std::string &old_str, const std::string &new_str);
    int spi_getadc(const short channel);

    GPIOS::spi spi_dev;
    int mean, speed;
    short NB_VAL, precision_decimal;
    static const short MAX_NB_VAL = 16; // For perf purposes, must be a squared number
    short hist_val[MAX_NB_VAL], oldest_val;
    short mcp_channel;
    std::string formula_str;

    static const short RX_LEN = 3;

    static const std::string JSON_DEVICES_FORMULA;
    static const std::string JSON_DEVICES_MCP_CHANNEL;
    static const std::string JSON_DEVICES_NB_VAL;
    static const std::string JSON_DEVICES_PRECISION_DECIMAL;
    static const std::string JSON_DEVICES_SPEED;

    static const std::string TOKEN_VAL;
};

#endif // DEF_SENSOR_MCP_3008
