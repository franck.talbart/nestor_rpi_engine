/************************************************************************
 NestoRPi Engine
 Copyright (C) 2015-2025 Franck Talbart

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

/* Author: Franck Talbart */

#include "src/devices/SensorMcp3008.hpp"

#include <linux/spi/spidev.h>
#include <linux/types.h>
#include <stdio.h>
#include <sys/ioctl.h>

#include <algorithm>
#include <cassert>
#include <cstdint>
#include <iomanip>
#include <iostream>
#include <iterator>
#include <sstream>
#include <stack>
#include <string>

#include "src/core/util.hpp"

// Static variables
const std::string SensorMcp3008::JSON_DEVICES_FORMULA = "formula";
const std::string SensorMcp3008::JSON_DEVICES_MCP_CHANNEL = "mcp_channel";
const std::string SensorMcp3008::JSON_DEVICES_NB_VAL = "nb_val";
const std::string SensorMcp3008::JSON_DEVICES_PRECISION_DECIMAL = "precision_decimal";
const std::string SensorMcp3008::JSON_DEVICES_SPEED = "speed";

const std::string SensorMcp3008::TOKEN_VAL = "val";

//------------------------------------------------------------------------------
/**
 * @brief Constructor, initialize the variables
 *
 * @param pins: map of pins
 * @param val_sensor: map of json values (device config file)
 *
 */
SensorMcp3008::SensorMcp3008(const pins_t &pins, const val_json_t &val_sensor)
    : Sensor(pins, val_sensor)
{
    mcp_channel = 0;

    try
    {
        speed = val_sensor.at(JSON_DEVICES_SPEED).toNumber();
        precision_decimal = val_sensor.at(JSON_DEVICES_PRECISION_DECIMAL).toNumber();
    }
    catch (const std::out_of_range &oor)
    {
        NRPI_LOG(ERROR) << "Some parameters are missing! ";
        throw;
    }

    spi_dev = GPIOS_SPISETUP(speed);

    // Default value: if no formula is specified, just return the value
    formula_str = TOKEN_VAL;
    val_json_t::const_iterator it = val_sensor.find(JSON_DEVICES_MCP_CHANNEL);

    if (it != val_sensor.end())
        mcp_channel = it->second.toNumber();

    assert(mcp_channel >= 0 && mcp_channel <= 7);

    it = val_sensor.find(JSON_DEVICES_FORMULA);

    // formula is used to do math on the value
    if (it != val_sensor.end())
        formula_str = it->second.toString();

    NB_VAL = val_sensor.find(JSON_DEVICES_NB_VAL) != val_sensor.end()
                 ? val_sensor.at(JSON_DEVICES_NB_VAL)
                 : 16; // For perf purposes, must be a squared nb
    NB_VAL = NB_VAL > MAX_NB_VAL ? MAX_NB_VAL : NB_VAL;

    for (int i = 0; i < NB_VAL; ++i) hist_val[i] = 0;
    oldest_val = 0;
    mean = 0;
}

//------------------------------------------------------------------------------
/**
 * @brief Compute the anologic value
 *
 *
 */
void SensorMcp3008::measure(void)
{
    int value = spi_getadc(mcp_channel);
    int old_mean = mean;

    // fast sliding average
    mean = mean + value - hist_val[oldest_val];
    hist_val[oldest_val] = value;
    ++oldest_val;
    // fast equivalent of if(oldest_val == NB_VAL) oldest_val = 0;
    oldest_val &= NB_VAL - 1;

    // if the new value is different from the previous one, run the trigger
    if (old_mean != mean)
        run_trigger(mean);
}

//------------------------------------------------------------------------------
/**
 * @brief Return the analogic value after computing it with the specified
 * formula
 *
 * @return the result
 */
std::string SensorMcp3008::get_data(void)
{
    std::string replace_formula = formula_str;
    // Put the value into the formula
    replace(replace_formula, TOKEN_VAL, std::to_string(mean / float(NB_VAL)));
    // Compute & return
    std::ostringstream out;
    out << std::setprecision(precision_decimal) << process_formula(replace_formula);
    return out.str();
}

//------------------------------------------------------------------------------
/**
 * @brief compute the result of the formula (without the parenthesis)
 *
 * WARNING: this is not fully functionnal: we should create a tree to do this
 * but for my simple needs that's enough
 * @param: formula: the formula to compute
 * @return the result
 */
float SensorMcp3008::compute(const std::string &formula)
{
    std::stack<float> operand;
    std::stack<char> symbol;
    float val = 0, o1, o2;
    std::string tmp_val;
    for (int i = formula.size() - 1; i >= 0; --i)
    {
        switch (formula[i])
        {
            // symbol
            case '+':
            case '-':
            case '*':
            case '/':
                symbol.push(formula[i]);
                break;
            // ignore whitespace
            case ' ':
                break;
            // operand
            default:
                // convert string to float
                tmp_val = formula[i];
                while (i - 1 >= 0 &&
                       ((formula[i - 1] >= '0' && formula[i - 1] <= '9') || formula[i - 1] == '.'))
                    tmp_val = formula[--i] + tmp_val;
                operand.push(std::stof(tmp_val));
                // Compute when symbol is / or * since it's the priority
                if (!symbol.empty() && (symbol.top() == '*' || symbol.top() == '/'))
                {
                    o1 = operand.top();
                    // pop does not return the value
                    operand.pop();
                    o2 = operand.top();
                    operand.pop();

                    if (symbol.top() == '*')
                    {
                        val = o1 * o2;
                    }
                    else if (symbol.top() == '/')
                    {
                        val = 0;
                        if (o2 != 0)
                            val = o1 / o2;
                    }

                    symbol.pop();
                    // add the result of the operation to the stack
                    operand.push(val);
                }
        }
    }

    // doing + and -
    while (!symbol.empty())
    {
        o1 = operand.top();
        operand.pop();
        if (!operand.empty())
        {
            o2 = operand.top();
            operand.pop();
        }
        else
        { // If the result is just < 0 and there is no real operation to do
            o2 = 0;
            if (symbol.top() == '-')
                o1 *= -1;
        }

        if (symbol.top() == '+')
        {
            val = o1 + o2;
        }
        else if (symbol.top() == '-')
        {
            val = o1 - o2;
        }

        symbol.pop();
        operand.push(val);
    }

    return operand.top();
}

//------------------------------------------------------------------------------
/**
 * @brief Process the formula by selecting each formula inside the parenthis and
 * compute it
 *
 * @param formula: the formula to compute
 * @return the result
 */
float SensorMcp3008::process_formula(std::string &formula)
{
    formula = "(" + formula + ")";
    int id_e;
    // for each parenthesis we compute the formula
    while ((id_e = formula.find(")")) != (int)std::string::npos)
    {
        int id_s = id_e;
        while (formula[--id_s] != '(')
            ;
        // compute the formula inside the parenthesis
        float val = compute(formula.substr(id_s + 1, id_e - id_s - 1));
        // replace the formula with the result
        formula.erase(id_s, id_e + 1 - id_s);
        formula.insert(id_s, std::to_string(val));
    };

    return compute(formula);
}

//------------------------------------------------------------------------------
/**
 * @brief a token by another in a string
 *
 * @param str: the string to process
 * @param old_str: the old token
 * @param new_str: the new token
 */
void SensorMcp3008::replace(std::string &str,
                            const std::string &old_str,
                            const std::string &new_str)
{
    size_t pos = 0;
    while ((pos = str.find(old_str, pos)) != std::string::npos)
    {
        str.replace(pos, old_str.length(), new_str);
        pos += new_str.length();
    }
}

//------------------------------------------------------------------------------
int SensorMcp3008::spi_getadc(const short channel)
{
    assert(spi_dev.dev >= 0);
    if (channel < 0 || channel > 7)
        return -1;
    int ret;

    struct spi_ioc_transfer xfer[3];
    uint8_t buf[RX_LEN] = {1, (uint8_t)(128 | (channel << 4)), 0};
    memset(xfer, 0, sizeof(struct spi_ioc_transfer) * 3);

    xfer[0].tx_buf = (unsigned long)&buf[0];
    xfer[0].rx_buf = (unsigned long)&buf[0];
    xfer[0].len = 1;
    xfer[0].speed_hz = spi_dev.speed;
    xfer[0].bits_per_word = spi_dev.bits;

    xfer[1].tx_buf = (unsigned long)&buf[1];
    xfer[1].rx_buf = (unsigned long)&buf[1];
    xfer[1].len = 1;
    xfer[1].speed_hz = spi_dev.speed;
    xfer[1].bits_per_word = spi_dev.bits;

    xfer[2].tx_buf = (unsigned long)&buf[2];
    xfer[2].rx_buf = (unsigned long)&buf[2];
    xfer[2].len = 1;
    xfer[2].speed_hz = spi_dev.speed;
    xfer[2].bits_per_word = spi_dev.bits;
#ifndef DEBUG_MODE
    ret = ioctl(spi_dev.dev, SPI_IOC_MESSAGE(3), xfer);
    if (ret < 0)
    {
        NRPI_LOG(ERROR) << "SPI_IOC_MESSAGE failed " << ret;
        return -1;
    }
#endif

    ret = (((uint32_t)buf[1]) << 8) & 0b1100000000;
    ret |= (buf[2] & 0xff);
    return ret;
}
