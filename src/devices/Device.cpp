/************************************************************************
 NestoRPi Engine
 Copyright (C) 2015-2025 Franck Talbart

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

/* Author: Franck Talbart */

#include "src/devices/Device.hpp"

//------------------------------------------------------------------------------
/**
 * @brief Constructor
 *
 * @param pins: map of pins
 * @param cfg: map of json values (device config file)
 *
 */
Device::Device(const pins_t &pins, const val_json_t &cfg) : pins(pins) { UNUSED(cfg); }

//------------------------------------------------------------------------------
/**
 * @brief Used to add a trigger: will called the specified function if
 *        the event occurs to the device
 *
 * @param number: the event number
 * @param callback: the function to call when the event occurs. The
 *                  device which triggered the function is sent as a param.
 *
 */
void Device::add_trigger_on_event(short number, std::function<void(Device *)> callback)
{
    assert(number >= 0);
    assert(callback != nullptr);
    std::vector<std::function<void(Device *)>> *list_triggers;

    // If the event is already recorded into the map of triggers
    std::map<short, std::vector<std::function<void(Device *)>>>::iterator i =
        callbacks.find(number);
    if (i != callbacks.end())
    {
        // we get the list of already existing callbacks
        list_triggers = &i->second;
        // we add the new callback to the list
        list_triggers->push_back(callback);
    }
    // If the event does not exist yet
    else
    {
        // create the new list
        list_triggers = new std::vector<std::function<void(Device *)>>();
        // add the callback to the new list
        list_triggers->push_back(callback);
        // add the list to the map
        callbacks[number] = *list_triggers;
        delete list_triggers;
        list_triggers = nullptr;
    }
}

//------------------------------------------------------------------------------
/**
 * @brief Trigger the functions. Must be used in the devices code only
 *        if a special event happpens
 *
 * @param number: the event number (defined in the class itself!)
 *
 */
void Device::run_trigger(short number)
{
    assert(number >= 0);
    std::map<short, std::vector<std::function<void(Device *)>>>::iterator i =
        callbacks.find(number);
    if (i != callbacks.end())
        // We don't encapsulate this in a smart pointer to prevent it from
        // being prematurely destroyed
        for (const auto &callback : i->second) callback(this);
}
