/************************************************************************
 NestoRPi Engine
 Copyright (C) 2015-2025 Franck Talbart

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

/* Author: Franck Talbart */

#include "src/devices/ActuatorMbrola.hpp"

#include "src/core/mbrola.hpp"

// Static variables

//------------------------------------------------------------------------------
/**
 *  @brief Constructor
 *
 *  @param pins: map of pins
 *  @param val_actuator: map of json values (device config file)
 *
 */
ActuatorMbrola::ActuatorMbrola(const pins_t& pins, const val_json_t& val_actuator)
    : Actuator(pins, val_actuator)
{
}

//------------------------------------------------------------------------------
/**
 *  @brief Say the text with mbrola
 *
 *  @param txt: the text to say
 *
 */
void ActuatorMbrola::set_action(const std::string& txt)
{
    text = txt;
    run_trigger(ON);
    mbrola::talk(txt);
}

//------------------------------------------------------------------------------
/**
 *  @brief Get the text
 *
 *  @return the text
 */
std::string ActuatorMbrola::get_text(void) const { return text; }
