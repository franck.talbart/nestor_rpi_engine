/************************************************************************
 NestoRPi Engine
 Copyright (C) 2015-2025 Franck Talbart

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

/* Author: Franck Talbart */

#ifndef DEF_DEVICE
#define DEF_DEVICE

#include "src/core/Log.hpp"
#include "src/core/Uncopyable.hpp"
#include "src/core/util.hpp"

// Master class used by sensor and actuator
// Device can't be copied since used pins are unique
class Device : public Uncopyable
{
  public:
    // Generic defined events. Subclass must define their own events.
    enum state_device
    {
        OFF,
        ON
    };

    Device(const pins_t &pins, const val_json_t &);
    virtual ~Device(void){};
    virtual void add_trigger_on_event(short number, std::function<void(Device *)> callback);

  protected:
    void run_trigger(short number);

    // Map of pins used by the device
    const pins_t pins;

  private:
    // Callbacks is a map where event number => vector of functions to call
    std::map<short, std::vector<std::function<void(Device *)>>> callbacks;
};

// Syntactic sugar
using std::placeholders::_1;
#define ADD_TRIGGER(number, device, trigger) \
    device->add_trigger_on_event(number, std::bind(trigger, this, _1));

#endif // DEF_DEVICE
