/************************************************************************
 NestoRPi Engine
 Copyright (C) 2015-2025 Franck Talbart

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

/* Author: Franck Talbart */

#include "src/devices/ActuatorPca9685.hpp"

const std::string ActuatorPca9685::JSON_DEVICES_DEFAULT_VAL = "default_val";
const std::string ActuatorPca9685::JSON_DEVICES_ENABLE = "enable";
const std::string ActuatorPca9685::JSON_DEVICES_PWM_MAX = "pwm_max";
const std::string ActuatorPca9685::JSON_DEVICES_PWM_MIN = "pwm_min";
const std::string ActuatorPca9685::JSON_DEVICES_PWM = "pwm";

//------------------------------------------------------------------------------
/**
 *  @brief Constructor
 *
 *  @param pins: map of pins
 *  @param val_actuator: map of json values (device config file)
 *
 */
ActuatorPca9685::ActuatorPca9685(const pins_t& pins, const val_json_t& val_actuator)
    : Actuator(pins, val_actuator)
{
    try
    {
        pwm_max = val_actuator.at(JSON_DEVICES_PWM_MAX).toNumber();
        pwm_min = val_actuator.at(JSON_DEVICES_PWM_MIN).toNumber();
        pwm = pins.at(JSON_DEVICES_PWM);
    }
    catch (const std::out_of_range& oor)
    {
        NRPI_LOG(ERROR) << "Some parameters are missing! ";
        throw;
    }

    // Optional
    bool enable = val_actuator.find(JSON_DEVICES_ENABLE) != val_actuator.end()
                      ? val_actuator.at(JSON_DEVICES_ENABLE).getTag() == gason::JSON_TRUE
                      : true;
    if (!enable)
        pwm = 0;

    GPIOS_PINMODE(pwm, GPIOS_PWM_OUTPUT);
    GPIOS_PWMSETRANGE(pwm, pwm_min, pwm_max);

    val = val_actuator.find(JSON_DEVICES_DEFAULT_VAL) != val_actuator.end()
              ? val_actuator.at(JSON_DEVICES_DEFAULT_VAL).toNumber()
              : 0;
    running = true;

    GPIOS_PWMTURNOFF(pwm);
    thread_a = std::thread(&ActuatorPca9685::thread_action, this);
}

//------------------------------------------------------------------------------
/**
 *  @brief Stop the motors
 */
void ActuatorPca9685::reset(void) { GPIOS_PWMTURNOFF(pwm); }

//------------------------------------------------------------------------------
/**
 *  @brief Thread  used to control the servo
 */
void ActuatorPca9685::thread_action(void)
{
    std::unique_lock<std::mutex> lk(m);

    float old_val = 101;
    while (running)
    {
        cv.wait(lk, [&]() { return (old_val != val) || !running; });
        if (!running)
            return;
        old_val = val;
        GPIOS_PWMWRITE(pwm, val);
    }
}

//------------------------------------------------------------------------------
/**
 *  @brief set the val of the servo
 *
 *  @param val_loc: range: -100 -> 100
 *
 */
void ActuatorPca9685::set_action(float val_loc)
{
    {
        std::unique_lock<std::mutex> lk(m);
        val = val_loc;
    }
    cv.notify_all();
}

//------------------------------------------------------------------------------
/**
 *  @brief get the val of the servo
 *
 *  @return val
 */
float ActuatorPca9685::get_action(void) const { return val; }

//------------------------------------------------------------------------------
/**
 *  @brief Stop the servo
 */
ActuatorPca9685::~ActuatorPca9685(void)
{
    {
        std::unique_lock<std::mutex> lk(m);
        running = false;
    }
    cv.notify_all();

    if (thread_a.joinable())
        thread_a.join();
    GPIOS_PWMTURNOFF(pwm);
}
