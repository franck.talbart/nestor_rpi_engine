/************************************************************************
 NestoRPi Engine
 Copyright (C) 2015-2025 Franck Talbart

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
/* Author: Franck Talbart */

#include "src/devices/ActuatorL298N.hpp"

#include <cmath>

// Static variables
const std::string ActuatorL298N::JSON_DEVICES_INI1 = "ini1";
const std::string ActuatorL298N::JSON_DEVICES_INI2 = "ini2";
const std::string ActuatorL298N::JSON_DEVICES_INI3 = "ini3";
const std::string ActuatorL298N::JSON_DEVICES_INI4 = "ini4";

const std::string ActuatorL298N::JSON_DEVICES_PWM_ENA = "pwm_enA";
const std::string ActuatorL298N::JSON_DEVICES_PWM_ENB = "pwm_enB";

const std::string ActuatorL298N::JSON_DEVICES_PWM_HARD = "pwm_hard";
const std::string ActuatorL298N::JSON_DEVICES_PWM_MAX = "pwm_max";

//------------------------------------------------------------------------------
/**
 *  @brief Constructor
 *
 *  @param pins: map of pins
 *  @param val_actuator: map of json values (device config file)
 *
 */
ActuatorL298N::ActuatorL298N(const pins_t& pins, const val_json_t& val_actuator)
    : Actuator(pins, val_actuator)
{
    try
    {
        ini1 = pins.at(JSON_DEVICES_INI1);
        ini2 = pins.at(JSON_DEVICES_INI2);

        pwm_enA = pins.at(JSON_DEVICES_PWM_ENA);
    }
    catch (const std::out_of_range& oor)
    {
        NRPI_LOG(ERROR) << "Some parameters are missing! ";
        throw;
    }

    pwm_hard = val_actuator.find(JSON_DEVICES_PWM_HARD) != val_actuator.end()
                   ? val_actuator.at(JSON_DEVICES_PWM_HARD).getTag() == gason::JSON_TRUE
                   : false;

    ini3 = pins.find(JSON_DEVICES_INI3) != pins.end() ? pins.at(JSON_DEVICES_INI3) : -1;
    ini4 = pins.find(JSON_DEVICES_INI4) != pins.end() ? pins.at(JSON_DEVICES_INI4) : -1;
    pwm_enB = pins.find(JSON_DEVICES_PWM_ENB) != pins.end() ? pins.at(JSON_DEVICES_PWM_ENB) : 0;

    // If pin == -1, the macro will ignore it
    for (short pin : {ini1, ini2, ini3, ini4}) GPIOS_PINMODE(pin, GPIOS_OUTPUT);

    for (short pin : {pwm_enA, pwm_enB}) GPIOS_PINMODE(pin, GPIOS_PWM_OUTPUT);

    try
    {
        if (pwm_hard)
        {
            pwm_max = val_actuator.at(JSON_DEVICES_PWM_MAX).toNumber();
            for (short pin : {pwm_enA, pwm_enB}) GPIOS_PWMSETRANGE(pin, 0, pwm_max);
        }
    }
    catch (const std::out_of_range& oor)
    {
        NRPI_LOG(ERROR) << "Some parameters are missing! ";
        throw;
    }

    if (!pwm_hard)
    {
        for (short pin : {pwm_enA, pwm_enB}) GPIOS_SOFTPWMCREATE(pin, 0); // 0 => initial value
    }

    valA = valB = 0;
}

//------------------------------------------------------------------------------
/**
 *  @brief Destructor
 */
ActuatorL298N::~ActuatorL298N(void) { reset(); }
//------------------------------------------------------------------------------
/**
 *  @brief operate the motor motor_id and trigger the events of necessary
 *
 *  @param motor_id: left or right
 *  @param val: -100 < val < 100    backward when < 0, foward when > 0
 *
 */
void ActuatorL298N::set_action(const motor motor_id, float val)
{
    pin_t iniA, iniB, pwm_en;

    float& old_val = motor_id ? valB : valA;
    if (motor_id == RIGHT)
        iniA = ini3, iniB = ini4, pwm_en = pwm_enB;
    else
        iniA = ini1, iniB = ini2, pwm_en = pwm_enA;

    if (val != old_val)
    {
        if (val > 0)
        { // Forward
            GPIOS_DIGITALWRITE(iniA, GPIOS_HIGH);
            GPIOS_DIGITALWRITE(iniB, GPIOS_LOW);
        }

        else if (val < 0)
        { // Backward
            GPIOS_DIGITALWRITE(iniA, GPIOS_LOW);
            GPIOS_DIGITALWRITE(iniB, GPIOS_HIGH);
        }
        else // 0
        {
            GPIOS_DIGITALWRITE(iniA, GPIOS_LOW);
            GPIOS_DIGITALWRITE(iniB, GPIOS_LOW);
        }

        if (old_val == 0)
            run_trigger(ON);
        if (val == 0)
            run_trigger(OFF);
        old_val = val;
    }

    val = std::abs(val) > 100 ? 100 : std::abs(val);

    if (pwm_hard)
    {
        GPIOS_PWMWRITE(pwm_en, val);
    }
    else // soft
    {
        GPIOS_SOFTPWMWRITE(pwm_en, (int)val);
    }
}

//------------------------------------------------------------------------------
/**
 *  @brief get the val of the motor
 *
 *  @param motor_id: left or right
 *  @return val
 */
float ActuatorL298N::get_action(const motor motor_id) const { return (motor_id) ? valB : valA; }

//------------------------------------------------------------------------------
/**
 *  @brief Stop the motors
 */
void ActuatorL298N::reset(void)
{
    // If pin == -1, the macro will ignore it
    for (short pin : {ini1, ini2, ini3, ini4}) GPIOS_DIGITALWRITE(pin, 0);

    if (pwm_hard)
    {
        for (short pin : {pwm_enA, pwm_enB}) GPIOS_PWMTURNOFF(pin);
    }
    else // soft
    {
        for (short pin : {pwm_enA, pwm_enB}) GPIOS_SOFTPWMWRITE(pin, 0);
    }
}
