/************************************************************************
 NestoRPi Engine
 Copyright (C) 2015-2025 Franck Talbart

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

/* Author: Franck Talbart */

#ifndef DEF_SENSOR_HC_SR04
#define DEF_SENSOR_HC_SR04

#include "src/devices/Sensor.hpp"

class SensorHcSr04 : public Sensor
{
  public:
    SensorHcSr04(const pins_t& pins, const val_json_t& val_device);
    void measure(void) override;
    std::string get_data(void) override;

#if 0
    float get_ratio(void);
    float get_distance_max(void) const;
#endif

  private:
    float measure_once(void);

    float value, distance_max;
    std::mutex lock;
    pin_t echo, trig;

    static const std::string JSON_DEVICES_TRIGGER;
    static const std::string JSON_DEVICES_ECHO;
    static const std::string JSON_DEVICES_DISTANCE_MAX;
    static const short NB_MEASURE;
    static const int TIME_OUT;
};

#endif // DEF_SENSOR_HC_SR04
