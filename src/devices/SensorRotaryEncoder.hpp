/************************************************************************
 NestoRPi Engine
 Copyright (C) 2015-2025 Franck Talbart

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

/* Author: Franck Talbart */

#ifndef DEF_SENSOR_ROTARY_ENCODER
#define DEF_SENSOR_ROTARY_ENCODER

#include <atomic>

#include "src/devices/Sensor.hpp"

class SensorRotaryEncoder : public Sensor
{
  public:
    SensorRotaryEncoder(const pins_t &pins, const val_json_t &val_device);
    std::function<double()> get_accessor_value(bool compute_cm = false);
    void measure(void) override;
    std::string get_data(void) override;
    int get_nb_of_tics_per_rev(void);
    int get_refresh_interval(void);
    ~SensorRotaryEncoder(void);

  private:
    void refresh_measure(void) override;
    void get_total_nb_tics(void);
    void write_distance(void);
    void perform_write_distance(void);
    std::atomic<int> total_nb_tics;
    pin_t sensor_a, sensor_b;
    double cst;
    short nb_of_pulses, max_index_values;
    std::string filename_log;
    bool state, reverse;
    std::thread thread_write;
    // Deque => Access O(1) + allows insertion of atomic<int>
    std::deque<std::atomic<int>> values;

    const short transitions[16] = {
        0,
        1,
        -1,
        0, // last_state_direction = 00
        -1,
        0,
        0,
        1, // last_state_direction = 01
        1,
        0,
        0,
        -1, // last_state_direction = 10
        0,
        -1,
        1,
        0 // last_state_direction = 11
    };
    short last_state_direction = 0, usec_max;

    enum step_encoder
    {
        BACKWARD,
        FORWARD
    };

    enum step_encoder step;

#ifdef DEBUG_MODE
    bool digit_read = false;
#undef GPIOS_DIGITALREAD
#define GPIOS_DIGITALREAD(PIN) digit_read
#endif // DEBUG_MODE

    static const std::string JSON_DEVICES_SENSOR_A;
    static const std::string JSON_DEVICES_SENSOR_B;
    static const std::string JSON_DEVICES_DIAMETER;
    static const std::string JSON_DEVICES_MAX_RPM;
    static const std::string JSON_DEVICES_NB_OF_PULSES;
    static const std::string JSON_DEVICES_REVERSE;

    static const std::string FILENAME_LOG;
};

#endif // DEF_SENSOR_ROTARY_ENCODER
