/************************************************************************
 NestoRPi Engine
 Copyright (C) 2015-2025 Franck Talbart

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

/* Author: Franck Talbart */

#ifndef DEF_ACTUATOR_L298N
#define DEF_ACTUATOR_L298N

#include <mutex>

#include "src/devices/Actuator.hpp"

class ActuatorL298N : public Actuator
{
  public:
    enum motor
    {
        LEFT,
        RIGHT
    };

    ActuatorL298N(const pins_t& pins, const val_json_t& val_actuator);
    ~ActuatorL298N();

    void set_action(const motor motor_id, float val);
    float get_action(const motor motor_id) const;
    void reset(void);

  private:
    bool pwm_hard;
    pin_t ini1, ini2, ini3, ini4;
    pin_t pwm_enA, pwm_enB;
    short pwm_max;
    float valA, valB;

    static const std::string JSON_DEVICES_INI1;
    static const std::string JSON_DEVICES_INI2;
    static const std::string JSON_DEVICES_INI3;
    static const std::string JSON_DEVICES_INI4;

    static const std::string JSON_DEVICES_PWM_ENA;
    static const std::string JSON_DEVICES_PWM_ENB;
    static const std::string JSON_DEVICES_PWM_HARD;
    static const std::string JSON_DEVICES_PWM_MAX;
};

#endif // DEF_ACTUATOR_L298N
