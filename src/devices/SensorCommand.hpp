/************************************************************************
 NestoRPi Engine
 Copyright (C) 2015-2025 Franck Talbart

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

/* Author: Franck Talbart */

#ifndef DEF_SENSOR_COMMAND
#define DEF_SENSOR_COMMAND

#include "src/devices/Sensor.hpp"

class SensorCommand : public Sensor
{
  public:
    SensorCommand(const pins_t &pins, const val_json_t &val_device);
    void measure(void) override;
    std::string get_data(void) override;

  private:
    std::string command, old_result, result;
    std::mutex lock;
    short fail_retry;

    static const std::string JSON_DEVICES_COMMAND;
    static const std::string JSON_DEVICES_FAIL_RETRY;
};

#endif // DEF_SENSOR_COMMAND
