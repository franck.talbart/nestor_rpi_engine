/************************************************************************
 NestoRPi Engine
 Copyright (C) 2015-2025 Franck Talbart

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

/* Author: Franck Talbart */

#include "src/devices/SensorRotaryEncoder.hpp"

#include <unistd.h>

#include <cmath>
#include <fstream>
#include <iostream>
#include <string>
#include <thread>

#include "src/core/const.hpp"
#include "src/core/util.hpp"

// Static variables
const std::string SensorRotaryEncoder::JSON_DEVICES_SENSOR_A = "sensor_a";
const std::string SensorRotaryEncoder::JSON_DEVICES_SENSOR_B = "sensor_b";
const std::string SensorRotaryEncoder::JSON_DEVICES_DIAMETER = "diameter";
const std::string SensorRotaryEncoder::JSON_DEVICES_MAX_RPM = "max_rpm";
const std::string SensorRotaryEncoder::JSON_DEVICES_NB_OF_PULSES = "nb_of_pulses_per_rev";
const std::string SensorRotaryEncoder::JSON_DEVICES_REVERSE = "reverse";

const std::string SensorRotaryEncoder::FILENAME_LOG = "rotary_encoder_";
//------------------------------------------------------------------------------
/**
 * @brief Constructor, initialize the variables and open the log files to
 *        display the total distance
 *
 * @param pins: map of pins
 * @param val_sensor: map of json values (device config file)
 *
 */
SensorRotaryEncoder::SensorRotaryEncoder(const pins_t& pins, const val_json_t& val_sensor)
    : Sensor(pins, val_sensor)
{
    float diameter = 0;
    short max_rpm = 0;
    try
    {
        sensor_a = pins.at(JSON_DEVICES_SENSOR_A);
        diameter = val_sensor.at(JSON_DEVICES_DIAMETER).toNumber();
        max_rpm = val_sensor.at(JSON_DEVICES_MAX_RPM).toNumber();
        nb_of_pulses = val_sensor.at(JSON_DEVICES_NB_OF_PULSES).toNumber();
    }
    catch (const std::out_of_range& oor)
    {
        NRPI_LOG(ERROR) << "Some parameters are missing!";
        throw;
    }

    // Optional
    sensor_b = pins.find(JSON_DEVICES_SENSOR_B) != pins.end() ? pins.at(JSON_DEVICES_SENSOR_B) : -1;
    reverse = val_sensor.find(JSON_DEVICES_REVERSE) != val_sensor.end()
                  ? val_sensor.at(JSON_DEVICES_REVERSE)
                  : false;

    // const used to compute the distance
    cst = M_PI * diameter / nb_of_pulses;

    GPIOS_PINMODE(sensor_a, GPIOS_INPUT);
    if (sensor_b != -1)
        GPIOS_PINMODE(sensor_b, GPIOS_INPUT);

    // Filename of the log where we record the total distance
    filename_log = util::get_full_dir_from_cfg(CONFIG_LOG_PATH) +
                   SensorRotaryEncoder::FILENAME_LOG + std::to_string(sensor_a) + ".log";
    get_total_nb_tics();
    NRPI_LOG(DEBUG) << "Total distance of rotary encoder " << sensor_a << ": "
                    << total_nb_tics * cst << " cm";

    // /3 for safety so we are sure to catch every signal
    refresh_interval = (1.0 / (ceil(max_rpm / 60.0) * nb_of_pulses) * 1000000) / 3;

#ifdef DEBUG_MODE
    refresh_interval = 100000;
#endif // DEBUG_MODE

    state = GPIOS_DIGITALREAD(sensor_a);
    max_index_values = 0;
    step = FORWARD;
    last_state_direction = 0;

    usec_max = (float)1 / ((float)max_rpm / 60 * nb_of_pulses) * 1000000;
    thread_write = std::thread(&SensorRotaryEncoder::perform_write_distance, this);
}

//------------------------------------------------------------------------------
/**
 * @brief Override refresh_measure to set the highest priority for the thread
 */
void SensorRotaryEncoder::refresh_measure(void)
{
    util::set_thread_high_priority(pthread_self());
    Sensor::refresh_measure();
}

//------------------------------------------------------------------------------
/**
 * @brief Return an accessor
 * @return Return an accessor to get the number of tics.
 */
std::function<double()> SensorRotaryEncoder::get_accessor_value(bool compute_cm)
{
    double local_cst = cst;
    if (compute_cm == false)
        local_cst = 1;
    values.emplace_back(0);
    int index = max_index_values++;
    return [this, index, local_cst]() -> double { return values[index].exchange(0) * local_cst; };
}

//------------------------------------------------------------------------------
/**
 * @brief Thread used to measure the distance
 *
 */
void SensorRotaryEncoder::measure(void)
{
    // software interrupt did not work very well so I prefer to use a loop here

#ifdef DEBUG_MODE
    digit_read = !digit_read;
#endif // DEBUG_MODE
    auto start_total = std::chrono::high_resolution_clock::now();

    bool val_sensor_a = GPIOS_DIGITALREAD(sensor_a);
    int direction = 0;
    if (sensor_b != -1)
    {
        bool val_sensor_b = GPIOS_DIGITALREAD(sensor_b);
        short current_state = (val_sensor_a << 1) | val_sensor_b;
        short transition_index = (last_state_direction << 2 | current_state);
        direction = transitions[transition_index];
        if (reverse)
            direction *= -1;
        last_state_direction = current_state;
    }
    if (val_sensor_a != state)
    {
        state = val_sensor_a;
        if (direction == 1) // forward
        {
            if (step == BACKWARD)
            {
                step = FORWARD;
                std::fill(values.begin(), values.end(), 0);
            }
            std::for_each(values.begin(), values.end(), [](std::atomic<int>& value) { ++value; });
        }
        else if (direction == -1) // backward
        {
            if (step == FORWARD)
            {
                step = BACKWARD;
                std::fill(values.begin(), values.end(), 0);
            }
            std::for_each(values.begin(), values.end(), [](std::atomic<int>& value) { --value; });
        }
        // if direction == 0 ==> stable
        else
        {
            std::for_each(values.begin(), values.end(), [](std::atomic<int>& value) { ++value; });
        }
        ++total_nb_tics;
    }
    auto end_total = std::chrono::high_resolution_clock::now();
    if (duration_cast<std::chrono::microseconds>(end_total - start_total).count() > usec_max)
    {
        NRPI_LOG(WARN) << "Tic missed (not fast enough), pin: " << sensor_a << " time interval: "
                       << std::chrono::duration_cast<std::chrono::microseconds>(end_total -
                                                                                start_total)
                              .count()
                       << " µs";
    }
}

//------------------------------------------------------------------------------
/**
 * @brief Thread responsible for recording total_nb_tics into the log file
 *
 */
void SensorRotaryEncoder::perform_write_distance(void)
{
    int old_total_nb_tics = total_nb_tics;
    while (true)
    {
        std::unique_lock<std::mutex> lock(mtx_refresh);
        cv.wait_for(lock, std::chrono::seconds(100), [&] { return !refreshing; });

        if (old_total_nb_tics != total_nb_tics)
        {
            write_distance();
            old_total_nb_tics = total_nb_tics;
        }

        if (!refreshing)
            break;
    }
}

//------------------------------------------------------------------------------
/**
 * @brief Return the distance
 * @return the distance in cm
 */
std::string SensorRotaryEncoder::get_data(void)
{
    // Value is the total number of tics
    // cst convert it to cm
    return std::to_string(int(total_nb_tics * cst));
}

//------------------------------------------------------------------------------
/**
 * @brief Return the number of tics per revolutions
 * @return the number of tics per revolutions
 */
int SensorRotaryEncoder::get_nb_of_tics_per_rev(void) { return nb_of_pulses; }

//------------------------------------------------------------------------------
/**
 * @brief Return the time to sleep of the measure thread (microsec)
 * @return the time
 */
int SensorRotaryEncoder::get_refresh_interval(void) { return refresh_interval; }

//------------------------------------------------------------------------------
/**
 * @brief Destructor
 */
SensorRotaryEncoder::~SensorRotaryEncoder(void)
{
    if (thread_write.joinable())
        thread_write.join();
}

//------------------------------------------------------------------------------
/**
 * @brief Get the nb tics recorded into the log file
 *
 */
void SensorRotaryEncoder::get_total_nb_tics(void)
{
    // Open the log file
    std::ifstream file_log(filename_log, std::ios::in);
    float res = 0;

    // Get the content
    if (file_log)
    {
        std::string content;
        getline(file_log, content);
        if (content != "")
            res = std::stof(content);
        file_log.close();
    }
    total_nb_tics = res / cst;
}

//------------------------------------------------------------------------------
/**
 * @brief Write the total distance into the log file
 * @param dist: the total distance to record
 *
 */
void SensorRotaryEncoder::write_distance(void)
{
    std::ofstream file_log(filename_log, std::ios::out | std::ios::trunc);

    if (file_log)
    {
        file_log << total_nb_tics * cst;
        file_log.close();
    }
    else
        NRPI_LOG(WARN) << "Cannot write into the rotary encoder log file.";
}
