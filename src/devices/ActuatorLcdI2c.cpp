/************************************************************************
 NestoRPi Engine
 Copyright (C) 2015-2025 Franck Talbart

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

/* Author: Franck Talbart */

#include "src/devices/ActuatorLcdI2c.hpp"

#include <unistd.h>

#include "src/core/Log.hpp"
#include "src/core/util.hpp"

// Static variables
const std::string ActuatorLcdI2c::JSON_DEVICES_I2C_ADDR = "i2c_addr";
const std::string ActuatorLcdI2c::JSON_DEVICES_NB_LINES = "nb_lines";
const std::string ActuatorLcdI2c::JSON_DEVICES_SCREEN_WIDTH = "screen_width";
const std::string ActuatorLcdI2c::JSON_DEVICES_SLEEP_INTERVAL = "sleep_interval";

const bool ActuatorLcdI2c::LCD_CHR = 1; // Sending data
const bool ActuatorLcdI2c::LCD_CMD = 0; // Sending command
const int ActuatorLcdI2c::ENABLE = 0x4; // Enable bit

//------------------------------------------------------------------------------
/**
 *  @brief Constructor
 *
 *  @param pins: map of pins
 *  @param val_actuator: map of json values (device config file)
 *
 */
ActuatorLcdI2c::ActuatorLcdI2c(const pins_t& pins, const val_json_t& val_actuator)
    : Actuator(pins, val_actuator)
{
    int i2c_addr = 0;
    try
    {
        i2c_addr = val_actuator.at(JSON_DEVICES_I2C_ADDR).toNumber();
        screen_width = val_actuator.at(JSON_DEVICES_SCREEN_WIDTH).toNumber();
        nb_lines = val_actuator.at(JSON_DEVICES_NB_LINES).toNumber();
        sleep_interval = val_actuator.at(JSON_DEVICES_SLEEP_INTERVAL).toNumber();
    }
    catch (const std::out_of_range& oor)
    {
        NRPI_LOG(ERROR) << "Some parameters are missing! ";
        throw;
    }

    i2c_dev = GPIOS_I2CSETUP(i2c_addr);

    lcd_backlight = 0x08;
    init();
}

//------------------------------------------------------------------------------
/**
 *  @brief Turn on / off the LCD
 *
 *  @param status: true: turn on, false: turn off
 *
 */
void ActuatorLcdI2c::turn(bool status)
{
    NRPI_LOG(DEBUG) << "Turning " << (status ? "ON" : "OFF") << " the LCD";
    lcd_backlight = (status) ? 0x08 : 0x00;
    clear();
}

//------------------------------------------------------------------------------
/**
 *  @brief clear the LCD
 *
 *
 */
void ActuatorLcdI2c::clear(void)
{
    send_byte(0x01, LCD_CMD);
    send_byte(0x02, LCD_CMD);
}

//------------------------------------------------------------------------------
/**
 *  @brief Go to location on LCD
 *
 *  @param line: location
 *
 */
void ActuatorLcdI2c::location(unsigned char line)
{
#ifndef DEBUG_MODE
    send_byte(line, LCD_CMD);
#else
    UNUSED(line);
#endif // DEBUG_MODE
}

//------------------------------------------------------------------------------
/**
 *  @brief output any string
 *
 *  @param str: the string
 *
 */
void ActuatorLcdI2c::type_line(const std::string& str)
{
#ifndef DEBUG_MODE
    std::string result = str;
    result.erase(0, result.find_first_not_of("\t\n\v\f\r "));
    for (const char& c : result) send_byte(c, LCD_CHR);
#endif // DEBUG_MODE
    NRPI_LOG(DEBUG) << str;
}
//------------------------------------------------------------------------------
/**
 *  @brief Send byte to the pins
 *
 *  @param bits: the data
 *  @param mode: 1 = data, 0 = command
 *
 */
void ActuatorLcdI2c::send_byte(unsigned char bits, unsigned char mode)
{
    unsigned char bits_high;
    unsigned char bits_low;
    // uses the two half byte writes to LCD
    bits_high = mode | (bits & 0xF0) | lcd_backlight;
    bits_low = mode | ((bits << 4) & 0xF0) | lcd_backlight;

    // High bits
    if (!GPIOS_I2CWRITE(i2c_dev, &bits_high, 1))
        NRPI_LOG(ERROR) << "Cannot write on the I2C bus";
    toggle_enable(bits_high);

    // Low bits
    if (!GPIOS_I2CWRITE(i2c_dev, &bits_low, 1))
        NRPI_LOG(ERROR) << "Cannot write on the I2C bus";

    toggle_enable(bits_low);
}

//------------------------------------------------------------------------------
/**
 *  @brief Toggle enable pin on LCD
 *
 *  @param bits: the data
 *
 */
void ActuatorLcdI2c::toggle_enable(unsigned char bits)
{
    struct timespec delay;
    delay.tv_sec = 0;
    delay.tv_nsec = 500000; // 500 micro sec

    // Toggle enable pin on LCD display
    nanosleep(&delay, NULL);
    unsigned char b = bits | ENABLE;

#ifdef DEBUG_MODE
    UNUSED(b);
#endif

    if (!GPIOS_I2CWRITE(i2c_dev, &b, 1))
        NRPI_LOG(ERROR) << "Cannot write on the I2C bus";

    nanosleep(&delay, NULL);

    // cppcheck-suppress unreadVariable
    b = bits & ~ENABLE;
    if (!GPIOS_I2CWRITE(i2c_dev, &b, 1))
        NRPI_LOG(ERROR) << "Cannot write on the I2C bus";

    nanosleep(&delay, NULL);
}

//------------------------------------------------------------------------------
/**
 *  @brief Intialize LCD
 *
 *
 */
void ActuatorLcdI2c::init(void)
{
    // Initialise display
    send_byte(0x33, LCD_CMD); // Initialise
    send_byte(0x32, LCD_CMD); // Initialise
    send_byte(0x06, LCD_CMD); // Cursor move direction
    send_byte(0x0C, LCD_CMD); // 0x0F On, Blink Off
    send_byte(0x28, LCD_CMD); // Data length, number of lines, font size
    clear();
    struct timespec delay;
    delay.tv_sec = 0;
    delay.tv_nsec = 500000; // 500 micro sec
    nanosleep(&delay, NULL);
}

//------------------------------------------------------------------------------
/**
 *  @brief Destructor
 */
ActuatorLcdI2c::~ActuatorLcdI2c(void) { clear(); }
//------------------------------------------------------------------------------
/**
 *  @brief Display the text on the LCD screen
 *
 *  @param line: LINE1 / LINE2
 *  @param txt: the text to display
 *
 */
void ActuatorLcdI2c::set_action(num_line line, const std::string& txt)
{
    NRPI_LOG(DEBUG) << "Displaying on LCD: " << txt << " line: " << line;
    std::string tmp_txt = txt;
    if (line != AUTOMATIC)
    {
        location(line);
        type_line(tmp_txt);
    }
    else
    { // Automatic
        do
        {
            for (unsigned i = 0; i < tmp_txt.length(); i += screen_width)
            {
                if (i == 0)
                {
                    location(LINE1);
                }
                else if (i == (unsigned)screen_width)
                {
                    location(LINE2);
                }
                type_line(tmp_txt.substr(i, screen_width));
            }
            tmp_txt = tmp_txt.substr(1, tmp_txt.length() - 1);
            sleep(sleep_interval);
        } while (tmp_txt.length() >= (unsigned)screen_width * nb_lines);
    }
}
