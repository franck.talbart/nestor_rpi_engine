/************************************************************************
 NestoRPi Engine
 Copyright (C) 2015-2025 Franck Talbart

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

/* Author: Franck Talbart */

#ifndef DEF_ACTUATOR_RELAY
#define DEF_ACTUATOR_RELAY

#include "src/devices/Actuator.hpp"

class ActuatorRelay : public Actuator
{
  public:
    ActuatorRelay(const pins_t &pins, const val_json_t &val_actuator);
    ~ActuatorRelay(void);
    void set_action(const std::string &name, bool val);
    bool get_action(const std::string &name) const;
    void add_trigger_on_event(short number, std::function<void(Device *)> callback) override;
    void reset(void);
    bool has_action(const std::string &name) const;
    std::map<std::string, bool> get_all_actions(void) const;

  private:
    bool to_initialize;

    static const std::string JSON_DEVICES_INITIALIZE;
    static std::mutex lock;
};

#endif // DEF_ACTUATOR_RELAY
