/************************************************************************
 NestoRPi Engine
 Copyright (C) 2015-2025 Franck Talbart

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

/* Author: Franck Talbart */
#include "src/devices/Sensor.hpp"

#include <unistd.h>

#include <thread>

// Static variables
const std::string Sensor::JSON_DEVICES_REFRESH_INTERVAL = "refresh_interval";
const std::string Sensor::JSON_DEVICES_TYPE_VALUE = "sensor";

//------------------------------------------------------------------------------
/**
 * @brief Constructor, initialize the variables
 *
 * @param pins: map of pins
 * @param val_sensor: map of json values (device config file)
 *
 */
Sensor::Sensor(const pins_t &pins, const val_json_t &val_sensor) : Device(pins, val_sensor)
{
    // sec to micro sec => 1 000 000
    refresh_interval = val_sensor.find(JSON_DEVICES_REFRESH_INTERVAL) != val_sensor.end()
                           ? val_sensor.at(JSON_DEVICES_REFRESH_INTERVAL) * 1000000
                           : -1;
    refreshing = true;
}

//------------------------------------------------------------------------------
/**
 * @brief Start the thread that call measure()()
 *
 *
 * WARNING: do not merge this method in the constructor, as this may result in random
 * errors at start up like "pure virtual method called"
 */
void Sensor::start_refresh(void)
{
    if (refresh_interval >= 0)
        thread_refresh = std::thread(&Sensor::refresh_measure, this);
}

//------------------------------------------------------------------------------
/**
 * @brief Stop the thread that call measure()()
 *
 *
 * WARNING: do not merge this method in the destructor, as it is called AFTER the
 * child destruction. It would result in a thread using deleted variables before
 * its destruction.
 */
void Sensor::stop_refresh(void)
{
    {
        std::unique_lock<std::mutex> lock(mtx_refresh);
        refreshing = false;
    }
    cv.notify_all();
    if (thread_refresh.joinable())
        thread_refresh.join();
}

//------------------------------------------------------------------------------
/**
 * @brief Periodically runs measure()
 *
 *
 */
void Sensor::refresh_measure(void)
{
    while (true) // do not use refreshing to keep it under a lock
    {
        try
        {
            measure();
        }
        catch (const std::string &msg)
        {
            NRPI_LOG(ERROR) << msg;
            {
                std::unique_lock<std::mutex> lock_refresh(mtx_refresh);
                refreshing = false;
                cv.notify_all();
            }
        }
        catch (...)
        {
            NRPI_LOG(ERROR) << "Something wrong happened with the sensor! Stopping ...";
            {
                std::unique_lock<std::mutex> lock_refresh(mtx_refresh);
                refreshing = false;
                cv.notify_all();
            }
        }
        std::unique_lock<std::mutex> lock(mtx_refresh);
        cv.wait_for(lock, std::chrono::microseconds(refresh_interval), [&] { return !refreshing; });
        if (!refreshing)
            break;
    }
}
