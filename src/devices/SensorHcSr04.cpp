/************************************************************************
 NestoRPi Engine
 Copyright (C) 2015-2025 Franck Talbart

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

/* Author: Franck Talbart */

#ifdef DEBUG_MODE
#define micros() 0
#endif // DEBUG_MODE

#include "src/devices/SensorHcSr04.hpp"

#include <unistd.h>

#include <algorithm>
#include <cstdlib>
#include <iomanip>
#include <iostream>
#include <map>
#include <memory>
#include <sstream>
#include <stdexcept>
#include <string>

#include "gason.hpp"
#include "src/core/util.hpp"
#include "src/devices/Sensor.hpp"

// Static variables
const std::string SensorHcSr04::JSON_DEVICES_TRIGGER = "trig";
const std::string SensorHcSr04::JSON_DEVICES_ECHO = "echo";
const std::string SensorHcSr04::JSON_DEVICES_DISTANCE_MAX = "distance_max";
const short SensorHcSr04::NB_MEASURE = 4;
const int SensorHcSr04::TIME_OUT = 1000; // Far more than 1 000 usec in reality

//------------------------------------------------------------------------------
/**
 * @brief Constructor, initialize the variables
 *
 * @param pins: map of pins
 * @param val_sensor: map of json values (device config file)
 *
 */
SensorHcSr04::SensorHcSr04(const pins_t &pins, const val_json_t &val_sensor)
    : Sensor(pins, val_sensor)
{
    try
    {
        trig = pins.at(JSON_DEVICES_TRIGGER);
        echo = pins.at(JSON_DEVICES_ECHO);
        distance_max = val_sensor.at(JSON_DEVICES_DISTANCE_MAX).toNumber();
    }
    catch (const std::out_of_range &oor)
    {
        NRPI_LOG(ERROR) << "Some parameters are missing! ";
        throw;
    }

    GPIOS_PINMODE(trig, GPIOS_OUTPUT);
    GPIOS_PINMODE(echo, GPIOS_INPUT);

    // TRIG pin must start LOW
    GPIOS_DIGITALWRITE(trig, GPIOS_LOW);

    // distance in cm
    value = -1;

    usleep(30000);
}

//------------------------------------------------------------------------------
/**
 * @brief Measure on time the distance
 *
 * @return The distance in cm, -1 if echo start or end is failing, -2 if the app is ending
 * failing
 */

#ifndef DEBUG_MODE
float SensorHcSr04::measure_once(void)
{
    lock.lock();

    struct timespec delay;
    delay.tv_sec = 0;
    delay.tv_nsec = 2000000; // 2000 micro sec
    nanosleep(&delay, NULL);

    int i = 0;
    long travel_time;
    struct timespec start_time, end_time;
    // Send trig pulse
    GPIOS_DIGITALWRITE(trig, GPIOS_HIGH);
    delay.tv_sec = 0;
    delay.tv_nsec = 20000; // 20 micro sec
    nanosleep(&delay, NULL);

    GPIOS_DIGITALWRITE(trig, GPIOS_LOW);

    // Wait for echo start
    while (GPIOS_DIGITALREAD(echo) == GPIOS_LOW && i < TIME_OUT)
    {
        {
            std::unique_lock<std::mutex> lock_(mtx_refresh);
            if (!refreshing)
            {
                return -2;
            }
        }
        ++i;
        usleep(1);
    }

    if (i == TIME_OUT)
    {
        lock.unlock();
        return -1;
    }

    // Wait for echo end
    i = 0;
    clock_gettime(CLOCK_REALTIME, &start_time);

    while (GPIOS_DIGITALREAD(echo) == GPIOS_HIGH && i < TIME_OUT)
    {
        {
            std::unique_lock<std::mutex> lock_(mtx_refresh);
            if (!refreshing)
                return -2;
        }
        ++i;
        usleep(1);
    }

    clock_gettime(CLOCK_REALTIME, &end_time);

    if (i == TIME_OUT)
    {
        lock.unlock();
        return -1;
    }

    travel_time = (end_time.tv_sec - start_time.tv_sec) * 1000000LL +
                  (end_time.tv_nsec - start_time.tv_nsec) / 1000;

    lock.unlock();

    /* Get distance in cm
     The speed of sound is 340 m/s or 29 microseconds per centimeter.
     Taking half the total distance (there and back) we can use the divisor 58.
   */
    return travel_time / 58.0;
}
#endif // DEBUG_MODE

//------------------------------------------------------------------------------
/**
 * @brief Measure the distance several time and compute the median
 */
void SensorHcSr04::measure(void)
{
#ifndef DEBUG_MODE
    float measures[NB_MEASURE];

    for (int i = 0; i < NB_MEASURE; ++i)
    {
        {
            std::unique_lock<std::mutex> lock_(mtx_refresh);
            if (!refreshing)
                return;
        }

        float measure_ = 0;
        short cpt = 0;
        do
        {
            measure_ = this->measure_once();
            if (measure_ == -2) // stop
            {
                return;
            }
            ++cpt;
        } while (measure_ < 0 && cpt < 10);
        if (measure_ < 0)
            continue;
        measures[i] = measure_;
    }

    std::sort(std::begin(measures), std::end(measures));

    float old_value = value;
    // Median
    if ((NB_MEASURE % 2) == 0)
    {
        value = (measures[NB_MEASURE / 2] + measures[(NB_MEASURE / 2) - 1]) / 2.0;
    }
    else
    {
        value = measures[NB_MEASURE / 2];
    }

    // Run the trigger if the value has changed since the last call
    if (old_value != value)
        run_trigger(value);
#endif // DEBUG_MODE
}

//------------------------------------------------------------------------------
/**
 * @brief Return the distance
 *
 * @return distance in cm, -1 if something is failing
 */
std::string SensorHcSr04::get_data(void)
{
    std::ostringstream out;
    out << std::setprecision(4) << value;
    return out.str();
}
#if 0
//------------------------------------------------------------------------------
/**
 * @brief Return the ratio distance / max
 *
 * @return the ratio
 */
float SensorHcSr04::get_ratio(void)
{
    if (value == -1)
    {
        NRPI_LOG(WARN) << "Trig " << trig << " and echo " << echo
                  << " does not work properly.";
        return -1;
    }
    return value / distance_max;
}

//------------------------------------------------------------------------------
/**
 * @brief Return the maximum distance supported by the sensor
 *
 * @return the distance in cm
 */
float SensorHcSr04::get_distance_max(void) const { return distance_max; }
#endif
