/************************************************************************
 NestoRPi Engine
 Copyright (C) 2015-2025 Franck Talbart

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

/* Author: Franck Talbart */

#ifndef DEF_ACTUATOR_PCA9685
#define DEF_ACTUATOR_PCA9685

#include <unistd.h>

#include <atomic>
#include <condition_variable>
#include <mutex>
#include <thread>

#include "src/devices/Actuator.hpp"

class ActuatorPca9685 : public Actuator
{
  public:
    ActuatorPca9685(const pins_t& pins, const val_json_t& val_actuator);
    ActuatorPca9685(ActuatorPca9685 const&) = delete; // delete the copy constructor
    void operator=(ActuatorPca9685 const&) = delete;  // delete the copy-assignment operator
    void reset(void);
    void set_action(float val);
    float get_action(void) const;
    ~ActuatorPca9685(void);

  private:
    void thread_action(void);
    pin_t pwm;
    short val, pwm_min, pwm_max;
    std::thread thread_a;
    volatile std::atomic<bool> running;
    std::mutex m;
    std::condition_variable cv;

    static const std::string JSON_DEVICES_DEFAULT_VAL;
    static const std::string JSON_DEVICES_ENABLE;
    static const std::string JSON_DEVICES_PWM_MAX;
    static const std::string JSON_DEVICES_PWM_MIN;
    static const std::string JSON_DEVICES_PWM;
};

#endif // DEF_ACTUATOR_PCA9685
