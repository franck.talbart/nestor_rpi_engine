/************************************************************************
 NestoRPi Engine
 Copyright (C) 2015-2025 Franck Talbart

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

/* Author: Franck Talbart */

#ifndef DEF_ACTUATOR_LCDI2C
#define DEF_ACTUATOR_LCDI2C

#include <queue>

#include "src/devices/Actuator.hpp"

class ActuatorLcdI2c : public Actuator
{
  public:
    enum num_line
    {
        LINE1 = 0x80,
        LINE2 = 0xC0,
        AUTOMATIC = -1
    };

    ActuatorLcdI2c(const pins_t& pins, const val_json_t& val_actuator);
    ~ActuatorLcdI2c(void);
    void set_action(num_line line, const std::string& txt);
    void turn(bool status);
    void clear(void);

  private:
    void location(unsigned char line);
#if 0
    void type_char(char val);
#endif
    void type_line(const std::string& str);
    void send_byte(unsigned char bits, unsigned char mode);
    void toggle_enable(unsigned char bits);
    void init(void);

    std::shared_ptr<GPIOS::i2c> i2c_dev;
    int lcd_backlight;
    short screen_width, nb_lines;
    float sleep_interval;

    static const std::string JSON_DEVICES_I2C_ADDR;
    static const std::string JSON_DEVICES_NB_LINES;
    static const std::string JSON_DEVICES_SCREEN_WIDTH;
    static const std::string JSON_DEVICES_SLEEP_INTERVAL;

    static const bool LCD_CHR, LCD_CMD;
    static const int ENABLE;
};

#endif // DEF_ACTUATOR_LCDI2C
