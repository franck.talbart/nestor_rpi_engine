/************************************************************************
 NestoRPi Engine
 Copyright (C) 2015-2025 Franck Talbart

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

/* Author: Franck Talbart */

#include "src/devices/SensorCommand.hpp"

// Static variables
const std::string SensorCommand::JSON_DEVICES_COMMAND = "command";
const std::string SensorCommand::JSON_DEVICES_FAIL_RETRY = "fail_retry";

//------------------------------------------------------------------------------
/**
 * @brief Constructor, initialize the variables
 *
 * @param pins: map of pins
 * @param val_sensor: map of json values (device config file)
 *
 */
SensorCommand::SensorCommand(const pins_t &pins, const val_json_t &val_sensor)
    : Sensor(pins, val_sensor)
{
    try
    {
        command = val_sensor.at(JSON_DEVICES_COMMAND).toString();
    }
    catch (const std::out_of_range &oor)
    {
        NRPI_LOG(ERROR) << "Some parameters are missing! ";
        throw;
    }

    try // Optional
    {
        fail_retry = val_sensor.at(JSON_DEVICES_FAIL_RETRY).toNumber();
    }
    catch (const std::out_of_range &oor)
    {
        fail_retry = 0;
    }

    // Fix a stupid bug in gcc where there is a crash on old_result = "0"
    // (GCC12, stdxx20) - Oct 18 24
    old_result = std::string("0");
    result = std::string("0");
}

//------------------------------------------------------------------------------
/**
 * @brief Compute the value
 *
 *
 */
void SensorCommand::measure(void)
{
    std::array<char, 128> buffer;
    util::Popen pipe;
    NRPI_LOG(DEBUG) << "Running command " << command.c_str();
    try
    {
        pipe = util::extended_popen_read(command.c_str());
    }
    catch (const std::string &msg)
    {
        NRPI_LOG(ERROR) << "Couldn't launch command " << command;
        NRPI_LOG(ERROR) << msg;
        result = "-1";
        throw std::string("Error running the command");
    }

    NRPI_LOG(DEBUG) << "Finished running command " << command.c_str();
    std::string local_result = "";
    while (fgets(buffer.data(), sizeof(buffer), pipe.stdout) != nullptr)
        local_result += buffer.data();
    while (fgets(buffer.data(), sizeof(buffer), pipe.stderr) != nullptr)
        local_result += buffer.data();
    fclose(pipe.stdout);
    fclose(pipe.stderr);

    if (pipe.status != 0)
    {
        if (WIFSIGNALED(pipe.status))
        { // Signal CTRL+C for instance
            {
                std::unique_lock<std::mutex> lock_refresh(mtx_refresh);
                refreshing = false;
            }
            cv.notify_all();
            NRPI_LOG(INFO) << "Command " << command << " stopped by a signal.";
        }
        else
        {
            NRPI_LOG(ERROR) << "Return code: " << pipe.status << " for command " << command;
            NRPI_LOG(ERROR) << local_result;
            result = "-1";
            if (--fail_retry < 0)
                throw std::string("Error running the command");
        }
    }
    else
    {
        // We update result just one time (here) so that measure / get_data are thread safe, and no
        // lock used.
        lock.lock();
        result = local_result;
        lock.unlock();
        // if the new value is different from the previous one, run the trigger
        if (old_result != local_result)
        {
            run_trigger(ON);
            old_result = local_result;
        }
    }
}

//------------------------------------------------------------------------------
/**
 * @brief Return the value
 *
 * @return the result
 */
std::string SensorCommand::get_data(void)
{
    std::string data;
    lock.lock();
    data = result.erase(result.find_last_not_of(" \n\r\t") + 1);
    lock.unlock();
    return data;
}
