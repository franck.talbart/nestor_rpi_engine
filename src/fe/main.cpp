/************************************************************************
 NestoRPi Engine
 Copyright (C) 2015-2025 Franck Talbart

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

/* Author: Franck Talbart */

/**
 Front-end: this code provides the Main function
*/

#include <getopt.h>

#include <csignal>
#include <cstdlib>
#include <fstream>
#include <iomanip>
#include <iostream>

#include "p2p/base/basic_packet_socket_factory.h"
#include "p2p/test/stun_server.h"
#include "p2p/test/turn_server.h"
#include "rtc_base/ssl_adapter.h"
#include "rtc_base/thread.h"
#include "src/core/GPIOS.hpp"
#include "src/core/HttpServerRequestHandler.hpp"
#include "src/core/Log.hpp"
#include "src/core/PeerConnectionManager.hpp"
#include "src/core/const.hpp"
#include "src/core/debug.hpp"
#include "src/core/util_devices.tpp"
#include "src/core/util_extensions.hpp"
#include "src/core/util_tasks.tpp"

// Static variable
type_log _LOG::level;
std::ofstream _LOG::log_file;

#ifndef DEBUG_MODE
GPIOS GPIOS::NRPI_GPIOS;
#endif // DEBUG

//------------------------------------------------------------------------------
/**
 * @brief Signal handler for SIGINT / SIGTERM
 *
 * @param signal id
 *
 */
void sig_handler(int signal)
{
    NRPI_LOG(INFO) << "Got signal " << signal;
    // Data channels must be removed first
    Task::remove_all_data_channel();
    if (rtc::Thread::Current() != nullptr)
    {
        rtc::Thread::Current()->Quit();
    }
    else
    {
        NRPI_LOG(INFO) << "Current thread is null, exiting ...";
        exit(EXIT_SUCCESS);
    }
}

//------------------------------------------------------------------------------
/**
 * @brief Clean up the environment
 *
 * @param tasks_automation: the list of automated tasks
 * @param map_sensors: the list of sensors
 *
 */
void quit(tasks_automation_t tasks_automation, sensors_t map_sensors)
{
    NRPI_LOG(INFO) << "* Stopping the tasks ...";
    for (const auto &task : tasks_automation)
    {
        std::string name = "    * " + task.first + " ...";
        NRPI_LOG(INFO, false) << std::left << std::setw(50) << name;
        if (!task.second->stop())
        {
            NRPI_LOG(ERROR, true, false) << COLOR(COLOR_RED, "[KO]");
        }
        else
        {
            NRPI_LOG(ERROR, true, false) << COLOR(COLOR_GREEN, "[OK]");
        }
    }

    NRPI_LOG(INFO) << "* Stopping the sensors ...";
    for (const auto &sensor : map_sensors)
    {
        NRPI_LOG(INFO) << "    * " << sensor.first << " ...";
        sensor.second->stop_refresh();
    }
    _LOG::close_log_file();

    rtc::CleanupSSL();
}

//------------------------------------------------------------------------------
/**
 * @brief Executed after main()
 */
void atexit_handler(void) { NRPI_LOG(INFO) << "Bye bye!" << std::endl; }
//------------------------------------------------------------------------------
/**
 * @brief Display the usage
 * @return EXIT_SUCCESS
 */
static int usage(char *const *argv)
{
    std::cout << "USAGE: " << argv[0] << " [parameter]" << std::endl;
    std::cout << argv[0] << " -d / --daemon: daemon mode" << std::endl;
    std::cout << argv[0] << " -v / --version: version" << std::endl;
    std::cout << argv[0] << " -h / --help: this help" << std::endl;
    return EXIT_SUCCESS;
}

//------------------------------------------------------------------------------
/**
 * @brief Display the version
 * @return EXIT_SUCCESS
 */
static int version(void)
{
    std::cout << "NestoRPi Engine version " << util::get_version() << std::endl;
    return EXIT_SUCCESS;
}

//------------------------------------------------------------------------------
class TurnAuth : public cricket::TurnAuthInterface
{
  public:
    // cppcheck-suppress unusedFunction
    virtual bool GetKey(absl::string_view username, absl::string_view realm, std::string *key)
    {
        return cricket::ComputeStunCredentialHash(
            std::string(username), std::string(realm), std::string(username), key);
    }
};

//------------------------------------------------------------------------------
class TurnRedirector : public cricket::TurnRedirectInterface
{
  public:
    explicit TurnRedirector() {}

    // cppcheck-suppress unusedFunction
    virtual bool ShouldRedirect(const rtc::SocketAddress &, rtc::SocketAddress *out)
    {
        UNUSED(out);
        return true;
    }
};

//------------------------------------------------------------------------------
/* Main */
int main(int argc, char **argv)
{
#ifdef DEBUG_MODE
    _LOG::level = DEBUG;
#else  // DEBUG_MODE
    _LOG::level = INFO;
#endif // DEBUG_MODE
    static struct option long_options[] = {{"help", no_argument, 0, 'h'},
                                           {"version", no_argument, 0, 'v'},
                                           {"daemon", no_argument, 0, 'd'},
                                           {0, 0, 0, 0}};

    bool daemon = false;
    int option_index = 0, c;
    while ((c = getopt_long(argc, argv, "hvd", long_options, &option_index)) != -1)
    {
        switch (c)
        {
            case 'h':
                return usage(argv);
            case 'v':
                return version();
            case 'd':
                daemon = true;
                break;
            default:
                return usage(argv);
        }
    }

    std::cout << "===========================================================" << std::endl;
    std::cout << "NestoRPi Engine - 2015-2025" << std::endl;
    std::cout << "Franck Talbart - franck@talbart.fr - www.franck.talbart.fr" << std::endl;
    std::cout << "===========================================================" << std::endl
              << std::endl;

    std::string name, stun_url, turn_url, ssl_certificate = "", http_address, http_port;
    TurnAuth turn_auth;
    TurnRedirector turn_redirector;

    webrtc::AudioDeviceModule::AudioLayer audio_layer =
        webrtc::AudioDeviceModule::kPlatformDefaultAudio;

    const std::string cfg_path = util::get_cfg_dir() + FILENAME_CONFIG;
    std::string default_local_stun_url = "0.0.0.0:3478";
    std::string default_local_turn_url = "turn:turn#0.0.0.0:3478";

    // Mandatory
    try
    {
        http_address = util::get_cfg_value(CONFIG_HTTP_ADDRESS, cfg_path).toString();
        http_port = util::get_cfg_value(CONFIG_HTTP_PORT).toString();
        name = util::get_cfg_value(CONFIG_ROBOT_NAME).toString();
    }
    catch (const std::string &err)
    {
        std::cerr << err << std::endl;
        ;
        return (EXIT_FAILURE);
    }

    _LOG::open_log_file();

    NRPI_LOG(INFO) << "* NestoRPi Engine is starting ..." << std::endl;
    std::unique_ptr<cricket::StunServer> stun_server;
    rtc::ThreadManager::Instance()->WrapCurrentThread();
    rtc::Thread *thread_rtc = rtc::Thread::Current();

    try
    {
        stun_url = util::get_cfg_value(CONFIG_STUN_URL).toString();
    }
    catch (const std::string &err)
    {
        // Start STUN server if needed
        stun_url = default_local_stun_url;
        rtc::SocketAddress server_addr;
        server_addr.FromString(stun_url);
        rtc::AsyncUDPSocket *server_socket =
            rtc::AsyncUDPSocket::Create(thread_rtc->socketserver(), server_addr);
        if (server_socket)
        {
            stun_server.reset(new cricket::StunServer(server_socket));
            NRPI_LOG(INFO) << "* STUN listening at " << server_addr.ToString();
        }
    }

    std::unique_ptr<cricket::TurnServer> turn_server;
    // Optional
    try
    {
        turn_url = util::get_cfg_value(CONFIG_TURN_URL).toString();
    }
    catch (const std::string &err)
    {
        turn_url = default_local_turn_url;
        std::istringstream is(turn_url);
        std::string addr;
        std::getline(is, addr, '#');
        std::getline(is, addr, '#');
        rtc::SocketAddress server_addr;
        server_addr.FromString(addr);
        turn_server.reset(new cricket::TurnServer(rtc::Thread::Current()));
        turn_server->set_auth_hook(&turn_auth);
        turn_server->set_redirect_hook(&turn_redirector);

        rtc::AsyncUDPSocket *server_socket =
            rtc::AsyncUDPSocket::Create(thread_rtc->socketserver(), server_addr);
        if (server_socket)
        {
            NRPI_LOG(INFO) << "* TURN listening UDP at " << server_addr.ToString();
            turn_server->AddInternalSocket(server_socket, cricket::PROTO_UDP);
        }
        rtc::Socket *tcp_server_socket =
            thread_rtc->socketserver()->CreateSocket(AF_INET, SOCK_STREAM);
        if (tcp_server_socket)
        {
            NRPI_LOG(INFO) << "* TURN listening TCP at " << server_addr.ToString();
            tcp_server_socket->Bind(server_addr);
            tcp_server_socket->Listen(5);
            turn_server->AddInternalServerSocket(tcp_server_socket, cricket::PROTO_TCP);
        }

        is.str(turn_url);
        is.clear();
        std::getline(is, addr, '@');
        std::getline(is, addr, '@');
        rtc::SocketAddress external_server_addr;
        external_server_addr.FromString(addr);
        NRPI_LOG(INFO) << "* TURN external addr:" << external_server_addr.ToString();
        turn_server->SetExternalSocketFactory(
            new rtc::BasicPacketSocketFactory(thread_rtc->socketserver()),
            rtc::SocketAddress(external_server_addr.ipaddr(), 0));
    }

    try
    {
        ssl_certificate =
            util::get_cfg_dir() + util::get_cfg_value(CONFIG_SSL_CERTIFICATE).toString();
    }
    catch (const std::string &err)
    {
    }

    try
    {
        audio_layer = util::get_cfg_value(CONFIG_AUDIO_LAYER).toString()
                          ? (webrtc::AudioDeviceModule::AudioLayer)atoi(optarg)
                          : webrtc::AudioDeviceModule::kDummyAudio;
    }
    catch (const std::string &err)
    {
    }

    http_address = http_address + ":" + http_port;

    if (daemon)
    {
        NRPI_LOG(INFO) << "* Starting the daemon ..." << std::endl;
        util::daemonize();
    }

    const int result_handler = std::atexit(atexit_handler);
    if (result_handler != 0)
        NRPI_LOG(ERROR) << "Registration failed";

    // CTRL + C or SIGTERM => stop
    if (signal(SIGINT, sig_handler) == SIG_ERR)
        NRPI_LOG(ERROR, true) << "Can't catch SIGINT";
    if (signal(SIGTERM, sig_handler) == SIG_ERR)
        NRPI_LOG(ERROR, true) << "Can't catch SIGTERM";

    // Segfault
    if (signal(SIGSEGV, util::print_back_trace) == SIG_ERR)
        NRPI_LOG(ERROR, true) << "Can't catch SIGSEGV";

// Coredump
#ifdef DEBUG_MODE
    util::setup_coredump();
#endif

    NRPI_LOG(INFO, false) << std::left << std::setw(50) << "* Initializing the GPIOS ...";
    if (!GPIOS_INIT())
    {
        NRPI_LOG(ERROR, true, false) << COLOR(COLOR_RED, "[KO]");
        _LOG::close_log_file();
        return (EXIT_FAILURE);
    }

    NRPI_LOG(INFO, true, false) << COLOR(COLOR_GREEN, "[OK]");

    // Extensions
    NRPI_LOG(INFO, false) << std::left << std::setw(50) << "* Initializing the extensions ...";
    if (!load_extensions())
    {
        NRPI_LOG(ERROR, true, false) << COLOR(COLOR_RED, "[KO]");
        _LOG::close_log_file();
        return (EXIT_FAILURE);
    }

    NRPI_LOG(INFO, true, false) << COLOR(COLOR_GREEN, "[OK]");

    tasks_automation_t tasks_automation;
    tasks_controller_t tasks_controller;

    actuators_t map_actuators;
    sensors_t map_sensors;

    try
    {
        // Actuators
        NRPI_LOG(INFO, false) << std::left << std::setw(50) << "* Initializing the actuators ...";
        map_actuators = load_devices<Actuator>();
        NRPI_LOG(INFO, true, false) << COLOR(COLOR_GREEN, "[OK]");

        // Sensors
        NRPI_LOG(INFO, false) << std::left << std::setw(50) << "* Initializing the sensors ...";
        map_sensors = load_devices<Sensor>();
        NRPI_LOG(INFO, true, false) << COLOR(COLOR_GREEN, "[OK]");
    }
    catch (...)
    {
        NRPI_LOG(ERROR, true, false) << COLOR(COLOR_RED, "[KO]");
        quit(tasks_automation, map_sensors);
        return (EXIT_FAILURE);
    }

    NRPI_LOG(INFO) << "* Starting the sensors ...";
    for (const auto &sensor : map_sensors)
    {
        NRPI_LOG(INFO) << "    * " << sensor.first << " ...";
        sensor.second->start_refresh();
    }

    // Tasks
    NRPI_LOG(INFO) << "* Initializing the tasks ...";
    Task::set_devices(map_actuators, map_sensors);

    try
    {
        tasks_automation = load_tasks<Automation>(FILENAME_TASKS_AUTOMATION);
        tasks_controller = load_tasks<Controller>(FILENAME_TASKS_CONTROLLER);
    }
    catch (...)
    {
        NRPI_LOG(ERROR, true, false) << COLOR(COLOR_RED, "Error while initializing the tasks!");
        quit(tasks_automation, map_sensors);
        return (EXIT_FAILURE);
    }

    // Run the tasks if needed
    NRPI_LOG(INFO) << "* Starting the tasks ...";
    Controller::set_tasks(tasks_controller);
    Automation::set_tasks(tasks_automation, tasks_controller);
    for (const auto &automation : tasks_automation)
    {
        NRPI_LOG(INFO) << "    * " << automation.first << " ...";
        automation.second->auto_run();
    }

    NRPI_LOG(INFO, false) << std::left << std::setw(50) << "* Initializing the Web server ...";
    rtc::InitializeSSL();

    // WebRTC server
    std::list<std::string> ice_server_list;
    ice_server_list.push_back(std::string("stun:") + stun_url);
    if (!turn_url.empty())
    {
        ice_server_list.push_back(std::string("turn:") + turn_url);
    }

    PeerConnectionManager *web_rtc_server =
        new PeerConnectionManager(ice_server_list, audio_layer, tasks_controller, tasks_automation);
    if (!web_rtc_server->InitializePeerConnection())
    {
        NRPI_LOG(ERROR, true, false) << COLOR(COLOR_RED, "[KO]");
        quit(tasks_automation, map_sensors);
        return (EXIT_FAILURE);
    }

    // HTTP server
    std::vector<std::string> options;
    options.push_back("document_root");
    options.push_back(util::get_full_dir(PATH_WEB_ROOT));
    options.push_back("enable_directory_listing");
    options.push_back("no");
    options.push_back("access_control_allow_origin");
    options.push_back("*");
    options.push_back("listening_ports");
    options.push_back(http_address);
    options.push_back("enable_keep_alive");
    options.push_back("yes");
    options.push_back("keep_alive_timeout_ms");
    options.push_back("1000");
    if (!ssl_certificate.empty())
    {
        options.push_back("ssl_certificate");
        options.push_back(ssl_certificate);
    }

    // HTTP api callbacks
    std::map<std::string, HttpServerRequestHandler::httpFunction> func;
    func["/api/getVideoDeviceList"] = [web_rtc_server](const struct mg_request_info *req_info,
                                                       const Json::Value &in) -> Json::Value
    {
        UNUSED(req_info);
        UNUSED(in);
        const Json::Value res = web_rtc_server->getVideoDeviceList();
        return res;
    };

    func["/api/getAudioDeviceList"] = [web_rtc_server](const struct mg_request_info *req_info,
                                                       const Json::Value &in) -> Json::Value
    {
        UNUSED(req_info);
        UNUSED(in);
        const Json::Value res = web_rtc_server->getAudioDeviceList();
        return res;
    };

    func["/api/getIceServers"] = [web_rtc_server](const struct mg_request_info *req_info,
                                                  const Json::Value &in) -> Json::Value
    {
        UNUSED(req_info);
        UNUSED(in);
        const Json::Value res = web_rtc_server->getIceServers(req_info->remote_addr);
        return res;
    };

    func["/api/call"] = [web_rtc_server](const struct mg_request_info *req_info,
                                         const Json::Value &in) -> Json::Value
    {
        std::string peer_id;
        std::string options_local;

        NRPI_LOG(INFO) << "Call initiated by " << req_info->remote_addr;
        if (req_info->query_string)
        {
            CivetServer::getParam(req_info->query_string, "peer_id", peer_id);
            CivetServer::getParam(req_info->query_string, "options", options_local);
        }

        const Json::Value res = web_rtc_server->call(peer_id, options_local, in);
        return res;
    };

    func["/api/hangup"] = [web_rtc_server](const struct mg_request_info *req_info,
                                           const Json::Value &in) -> Json::Value
    {
        std::string peer_id, user, passwd, token;
        UNUSED(in);
        if (req_info->query_string)
        {
            CivetServer::getParam(req_info->query_string, "peer_id", peer_id);
            CivetServer::getParam(req_info->query_string, "user", user);
            CivetServer::getParam(req_info->query_string, "passwd", passwd);
            CivetServer::getParam(req_info->query_string, "token", token);
        }
        const Json::Value res = web_rtc_server->hangUp(peer_id, user, passwd, token);
        return res;
    };

    func["/api/getIceCandidate"] = [web_rtc_server](const struct mg_request_info *req_info,
                                                    const Json::Value &in) -> Json::Value
    {
        std::string peer_id;
        UNUSED(in);
        if (req_info->query_string)
        {
            CivetServer::getParam(req_info->query_string, "peer_id", peer_id);
        }
        const Json::Value res = web_rtc_server->getIceCandidateList(peer_id);
        return res;
    };

    func["/api/addIceCandidate"] = [web_rtc_server](const struct mg_request_info *req_info,
                                                    const Json::Value &in) -> Json::Value
    {
        std::string peer_id;
        if (req_info->query_string)
        {
            CivetServer::getParam(req_info->query_string, "peer_id", peer_id);
        }
        const Json::Value res = web_rtc_server->addIceCandidate(peer_id, in);
        return res;
    };

    func["/api/getPeerConnectionList"] = [web_rtc_server](const struct mg_request_info *req_info,
                                                          const Json::Value &in) -> Json::Value
    {
        UNUSED(req_info);
        UNUSED(in);
        const Json::Value res = web_rtc_server->getPeerConnectionList();
        return res;
    };

    func["/api/getStreamList"] = [web_rtc_server](const struct mg_request_info *req_info,
                                                  const Json::Value &in) -> Json::Value
    {
        UNUSED(req_info);
        UNUSED(in);
        const Json::Value res = web_rtc_server->getStreamList();
        return res;
    };

    func["/api/version"] = [](const struct mg_request_info *req_info,
                              const Json::Value &in) -> Json::Value
    {
        UNUSED(req_info);
        UNUSED(in);
        Json::Value answer(util::get_version());
        return answer;
    };

    func["/api/name"] = [&name](const struct mg_request_info *req_info,
                                const Json::Value &in) -> Json::Value
    {
        UNUSED(req_info);
        UNUSED(in);
        Json::Value answer(name);
        return answer;
    };

    func["/api/metrics"] = [](const struct mg_request_info *req_info,
                              const Json::Value &in) -> Json::Value
    {
        UNUSED(req_info);
        UNUSED(in);
        const Json::Value result = util::get_metrics();
        return result;
    };

    // Must be at the end to grab every function
    func["/api/help"] = [func](const struct mg_request_info *req_info,
                               const Json::Value &in) -> Json::Value
    {
        UNUSED(req_info);
        UNUSED(in);
        Json::Value answer;
        for (auto it : func)
        {
            answer.append(it.first);
        }
        return answer;
    };

    try
    {
        HttpServerRequestHandler http_server(func, options);
        NRPI_LOG(INFO, true, false) << COLOR(COLOR_GREEN, "[OK]") << std::endl;
        NRPI_LOG(INFO) << "* HTTP listening at " << http_address;
        NRPI_LOG(INFO) << "* Use CTRL+C to stop the server." << std::endl;
        // Main loop
        thread_rtc->Run();
    }
    catch (const CivetException &ex)
    {
        NRPI_LOG(ERROR, true, false) << COLOR(COLOR_RED, "[KO]");
        rtc::Thread::Current()->Quit();
        quit(tasks_automation, map_sensors);

        delete web_rtc_server, web_rtc_server = nullptr;
        _LOG::close_log_file();
        return (EXIT_FAILURE);
    }

    NRPI_LOG(INFO) << "* Stopping the NestoRPi Engine ..." << std::endl;
    delete web_rtc_server, web_rtc_server = nullptr;
    quit(tasks_automation, map_sensors);

    NRPI_LOG(INFO, false) << "  * Garbage collecting ..." << std::endl;
    _LOG::close_log_file();
    return EXIT_SUCCESS;
}
