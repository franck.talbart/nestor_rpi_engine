/************************************************************************
 NestoRPi Engine
 Copyright (C) 2015-2025 Franck Talbart
************************************************************************/

/* Custom buffer to avoid memcpy (direct use of pointers) */
#ifndef DEBUG_MODE

#include "HWVideoEncoderFactory.hpp"

#include "absl/memory/memory.h"
#include "absl/strings/match.h"
#include "api/video_codecs/sdp_video_format.h"
#include "media/base/codec.h"
#include "media/base/media_constants.h"
#include "modules/video_coding/codecs/h264/include/h264.h"
#include "modules/video_coding/codecs/vp8/include/vp8.h"
#include "modules/video_coding/codecs/vp9/include/vp9.h"
#include "src/core/Log.hpp"
#include "src/core/h264_format.hpp"
#include "src/core/v4l2m2m_encoder.hpp"

// cppcheck-suppress unusedFunction
std::vector<webrtc::SdpVideoFormat> HWVideoEncoderFactory::GetSupportedFormats(void) const
{
    std::vector<webrtc::SdpVideoFormat> supported_codecs;

    supported_codecs.push_back(webrtc::SdpVideoFormat(cricket::kVp8CodecName));
    std::vector<webrtc::SdpVideoFormat> VP9_codecs = webrtc::SupportedVP9Codecs();
    std::copy(VP9_codecs.begin(), VP9_codecs.end(), std::back_inserter(supported_codecs));

    std::vector<webrtc::SdpVideoFormat> h264_codecs = {
        create_H264_format(
            webrtc::H264Profile::kProfileBaseline, webrtc::H264Level::kLevel3_1, "1"),
        create_H264_format(
            webrtc::H264Profile::kProfileBaseline, webrtc::H264Level::kLevel3_1, "0"),
        create_H264_format(
            webrtc::H264Profile::kProfileConstrainedBaseline, webrtc::H264Level::kLevel3_1, "1"),
        create_H264_format(
            webrtc::H264Profile::kProfileConstrainedBaseline, webrtc::H264Level::kLevel3_1, "0")};

    std::copy(h264_codecs.begin(), h264_codecs.end(), std::back_inserter(supported_codecs));

    return supported_codecs;
}

std::unique_ptr<webrtc::VideoEncoder> HWVideoEncoderFactory::Create(
    const webrtc::Environment& env, const webrtc::SdpVideoFormat& format)
{
    if (absl::EqualsIgnoreCase(format.name, cricket::kVp8CodecName))
        return webrtc::CreateVp8Encoder(env);

    if (absl::EqualsIgnoreCase(format.name, cricket::kVp9CodecName))
        return webrtc::CreateVp9Encoder(env);

    if (absl::EqualsIgnoreCase(format.name, cricket::kH264CodecName))
    {
        return std::unique_ptr<webrtc::VideoEncoder>(absl::make_unique<V4l2m2mEncoder>());
    }

    NRPI_LOG(ERROR) << "Trying to create encoder of unsupported format " << format.name;
    return nullptr;
}

#endif // DEBUG_MODE
