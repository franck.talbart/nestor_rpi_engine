/************************************************************************
 NestoRPi Engine
 Copyright (C) 2015-2025 Franck Talbart

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

/* Author: Franck Talbart */

#include "src/core/DataChannelObserver.hpp"

#include <unistd.h>

#include "src/core/Log.hpp"
#include "src/core/const.hpp"
#include "src/tasks/Automation.hpp"
#include "src/tasks/Controller.hpp"

//------------------------------------------------------------------------------
/**
 * @brief Constructor DataChannelObserver initialization
 *
 * @param data_channel: the data channel to observer
 * @param tasks_controller: the list of tasks controller
 * @param tasks_automation: the list of tasks_automation
 * @param role: FORBIDDEN / MINIMAL / FULL
 *
 */
DataChannelObserver::DataChannelObserver(
    rtc::scoped_refptr<webrtc::DataChannelInterface> data_channel,
    const std::map<std::string, std::shared_ptr<Controller>> &tasks_controller,
    const std::map<std::string, std::shared_ptr<Automation>> &tasks_automation,
    const role_type role)
    : data_channel(data_channel),
      tasks_controller(tasks_controller),
      tasks_automation(tasks_automation),
      role(role)
{
    data_channel->RegisterObserver(this);
    Task::set_data_channel(this);
    running = true;
    thread_data = std::thread([&] { this->thread_process(); });
}

//------------------------------------------------------------------------------
/**
 * @brief Destructor
 */
DataChannelObserver::~DataChannelObserver(void)
{
    running = false;
    cv.notify_all();
    Task::remove_data_channel(this);
    if (data_channel.get())
    {
        data_channel->UnregisterObserver();
        data_channel->Close();
    }
    thread_data.join();
}

//------------------------------------------------------------------------------
/**
 * @brief Event called when the state of the observed data channel change
 *
 */
// cppcheck-suppress unusedFunction
void DataChannelObserver::OnStateChange(void)
{
    const std::string state = webrtc::DataChannelInterface::DataStateString(data_channel->state());
    NRPI_LOG(DEBUG) << "channel: " << data_channel->label() << " state: " << state;
    if (state == "open")
    {
        NRPI_LOG(INFO) << "  * New client connected";
        try
        {
            for (const auto &task : tasks_automation) task.second->init(this);
            for (const auto &task : tasks_controller) task.second->init(this);
        }
        catch (...)
        {
            NRPI_LOG(ERROR) << "Something wrong happened while initializing the tasks ...";
        }
    }
}

//------------------------------------------------------------------------------
/**
 * @brief Send a string into the data channel (client)
 *
 * @param msg: the string to send
 *
 */
void DataChannelObserver::Send(const std::string &msg)
{
    try
    {
        if (data_channel.get())
        {
            webrtc::DataBuffer buffer(msg);
            data_channel->Send(buffer);
        }
    }
    catch (...)
    {
    }
}

//------------------------------------------------------------------------------
/**
 * @brief Event called when a message is received from the client
 *        The message is parsed and processed (task executions)
 *
 * @param buffer: the received buffer
 *
 */
// cppcheck-suppress unusedFunction
void DataChannelObserver::OnMessage(const webrtc::DataBuffer &buffer)
{
    std::string msg(reinterpret_cast<const char *>(buffer.data.data()), buffer.data.size());
    NRPI_LOG(DEBUG) << "channel:" << data_channel->label() << " msg:" << msg;
    if ((msg_type)std::stoi(msg) == PING)
    {
        this->Send(std::to_string(PING));
    }
    else
    {
        {
            std::unique_lock<std::mutex> lk(mtx_msgs);
            msgs.push(msg);
        }
        cv.notify_all();
    }
}

//------------------------------------------------------------------------------
/**
 * @brief Single thread used to process the data
 *
 */
void DataChannelObserver::thread_process(void)
{
    while (true)
    {
        std::string msg = "";
        {
            std::unique_lock<std::mutex> lk(mtx_msgs);
            cv.wait(lk, [this] { return (msgs.size() > 0) || !running; });
            if (!running)
                break;

            msg = msgs.front();
            msgs.pop();
            NRPI_LOG(DEBUG) << "channel:" << data_channel->label() << " msg:" << msg;
        }

        // get the messages from the clients, and run the corresponding action
        std::string name = "unamed";
        try
        {
            const std::vector<std::string> token = util::msg_to_tokens(msg);
            msg_type msg_t = (msg_type)std::stoi(token.at(0));
            name = token.at(1);

            // I use "at" everywhere to be sure we will throw an error if
            // a key is not found in a map / vector

            switch (msg_t)
            {
                case TASK_CONTROLLER:
                {
                    if (tasks_controller.find(name) == tasks_controller.end())
                    {
                        NRPI_LOG(ERROR) << "Unknow controller " << name;
                    }
                    else
                    {
                        try
                        {
                            if (!tasks_controller.at(name)->set_action(
                                    role,
                                    std::vector<std::string>(token.begin() + 2,
                                                             token.begin() + token.size())))
                                NRPI_LOG(ERROR) << "Something went wrong with controller " << name;
                        }
                        catch (const std::string &e) // Permission issue
                        {
                            NRPI_LOG(WARN)
                                << "Channel: " << data_channel->label()
                                << " can't perform this action (permission denied) on " << name;
                            const std::string msg_err =
                                std::to_string(PERMISSION) + COMM_DELIMITER + name;
                            this->Send(msg_err);
                        }
                    }
                    break;
                }
                case TASK_AUTOMATION: // run a task or stop it
                {
                    if (tasks_automation.find(name) == tasks_automation.end())
                    {
                        NRPI_LOG(ERROR) << "Unknow automation " << name;
                    }
                    else
                    {
                        if ((bool)std::stoi(token.at(2))) // start the task
                        {
                            tasks_automation.at(token.at(1))->start();
                        }
                        else // stop the task
                        {
                            if (!tasks_automation.at(token.at(1))->stop())
                                NRPI_LOG(ERROR)
                                    << "Can't stop the task " << token.at(1) << "properly!";
                        }
                    }
                    break;
                }
                default:
                    NRPI_LOG(ERROR) << "Token not found!!";
            }
        }
        catch (const std::invalid_argument &ia)
        {
            NRPI_LOG(ERROR) << "Wrong message! Msg: -=>" << msg << "<=-";
        }
        catch (const std::out_of_range &oor)
        {
            NRPI_LOG(ERROR) << "Wrong format received! Msg: -=>" << msg << "<=-";
        }
        catch (...)
        {
            NRPI_LOG(ERROR) << "Something wrong happened with the task: " << name;
        }
    }
}
