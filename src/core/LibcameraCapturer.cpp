/************************************************************************
 NestoRPi Engine
 Copyright (C) 2015-2025 Franck Talbart

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
/* Author: Franck Talbart */

#ifndef DEBUG_MODE
#include "src/core/LibcameraCapturer.hpp"

#include <modules/video_capture/video_capture_factory.h>
#include <third_party/libyuv/include/libyuv.h>

#include "core/buffer_sync.hpp"
#include "image/image.hpp"
#include "src/core/Log.hpp"
#include "src/core/i420_buffer.hpp"
//------------------------------------------------------------------------------
class RPiCamJpegApp : public RPiCamApp
{
  public:
    RPiCamJpegApp() : RPiCamApp(std::make_unique<StillOptions>()) {}

    StillOptions *GetOptions() const { return static_cast<StillOptions *>(options_.get()); }
};

//------------------------------------------------------------------------------
rtc::scoped_refptr<LibcameraCapturer> LibcameraCapturer::Create(
    short dev, const std::map<std::string, std::string> &opts)
{
    auto capture = rtc::make_ref_counted<LibcameraCapturer>(dev, opts);
    return capture;
}

//------------------------------------------------------------------------------
LibcameraCapturer::LibcameraCapturer(short dev, const std::map<std::string, std::string> &opts)
    : running(false), last_picture_filename(""), picture(false)
{
    width = std::stoi(opts.at("width"));
    height = std::stoi(opts.at("height"));

    options = app.GetOptions();

    argc = opts.size() * 2 + 3;

    argv = new char *[argc];
    short cpt = 0;
    argv[cpt++] = strdup("");
    argv[cpt++] = strdup("--camera");
    argv[cpt] = static_cast<char *>(util::xmalloc(sizeof(char) * 8));
    assert(argv[cpt]);
    snprintf(argv[cpt++], 7, "%d", dev);
    device = dev;

    for (const auto &opt : opts)
    {
        argv[cpt++] = strdup(("--" + opt.first).c_str());
        argv[cpt++] = strdup(opt.second.c_str());
    }

    if (!options->Parse(argc, static_cast<char **>(argv)))
    {
        for (short i = 0; i < argc; ++i) free(argv[i]), argv[i] = nullptr;
        delete[] argv;
        throw std::runtime_error("libcam wrong opts");
    }
    options->nopreview = true;
    options->codec = "yuv420";
}

//------------------------------------------------------------------------------
LibcameraCapturer::~LibcameraCapturer(void)
{
    for (short i = 0; i < argc; ++i) free(argv[i]), argv[i] = nullptr;
    delete[] argv;
}
//------------------------------------------------------------------------------
void LibcameraCapturer::StopCapture(void)
{
    if (running)
    {
        running = false;
        if (!t_process_capture_thread.empty())
        {
            t_process_capture_thread.Finalize();
        }
        if (!t_capture_thread.empty())
        {
            t_capture_thread.Finalize();
        }
    }
}

//------------------------------------------------------------------------------
// Must have functions required by the abstract class
webrtc::MediaSourceInterface::SourceState LibcameraCapturer::state(void) const
{
    return SourceState::kLive;
}

//------------------------------------------------------------------------------
// Must have functions required by the abstract class
// cppcheck-suppress unusedFunction
bool LibcameraCapturer::remote(void) const { return false; }

//------------------------------------------------------------------------------
// Must have functions required by the abstract class
// cppcheck-suppress unusedFunction
bool LibcameraCapturer::is_screencast(void) const { return false; }

//------------------------------------------------------------------------------
// Must have functions required by the abstract class
// cppcheck-suppress unusedFunction
std::optional<bool> LibcameraCapturer::needs_denoising(void) const { return false; }

//------------------------------------------------------------------------------
void LibcameraCapturer::StartCapture(void)
{
    // Start the camera only once
    if (!running)
    {
        NRPI_LOG(INFO) << "StartCapture: starting!";
        running = true;
        t_capture_thread = rtc::PlatformThread::SpawnJoinable(
            [this]() { capture_thread(); },
            "capture_thread",
            rtc::ThreadAttributes().SetPriority(rtc::ThreadPriority::kHigh));

        t_process_capture_thread = rtc::PlatformThread::SpawnJoinable(
            [this]() { process_capture_thread(); },
            "process_capture_thread",
            rtc::ThreadAttributes().SetPriority(rtc::ThreadPriority::kHigh));

        NRPI_LOG(INFO) << "StartCapture: complete!";
    }
}

//------------------------------------------------------------------------------
void LibcameraCapturer::start_camera(void)
{
    app.OpenCamera();
    // JPEG_COLOURSPACE => yuv420
    app.ConfigureVideo(RPiCamEncoder::FLAG_VIDEO_JPEG_COLOURSPACE);
    app.StartEncoder();
    app.StartCamera();
}

//------------------------------------------------------------------------------
void LibcameraCapturer::end_camera(void)
{
    app.StopCamera();
    app.StopEncoder();
    app.Teardown();
    app.CloseCamera();
}

//------------------------------------------------------------------------------
void LibcameraCapturer::capture_thread(void)
{
    {
        std::unique_lock<std::mutex> lock(mutex_capture);
        // Clean the queue
        std::queue<RPiCamEncoder::Msg> empty;
        std::swap(capture_queue, empty);
    }
    NRPI_LOG(INFO) << "capture_thread: start";
    while (true)
    {
        if (!running)
            return;
        RPiCamEncoder::Msg msg = app.Wait();
        {
            std::lock_guard<std::mutex> lock(mutex_capture);
            capture_queue.push(msg);
        }
        condition_capture.notify_one();
    }
}

//------------------------------------------------------------------------------
void LibcameraCapturer::process_capture_thread(void)
{
    NRPI_LOG(INFO) << "process_capture_thread: start";
    std::unique_ptr<Output> output = std::unique_ptr<Output>(Output::Create(options));
    app.SetEncodeOutputReadyCallback(std::bind(&Output::OutputReady,
                                               output.get(),
                                               std::placeholders::_1,
                                               std::placeholders::_2,
                                               std::placeholders::_3,
                                               std::placeholders::_4));
    app.SetMetadataReadyCallback(std::bind(&Output::MetadataReady, output.get(), _1));

    start_camera();
    RPiCamApp::Msg msg = RPiCamApp::MsgType::Quit;
    while (true)
    {
        if (picture)
        {
            end_camera();

            last_picture_filename = take_picture();
            std::lock_guard<std::mutex> lock(mutex_picture);
            picture = false;
            condition_picture.notify_all();

            start_camera();
            continue;
        }

        while (true)
        {
            if (!running)
            {
                end_camera();
                return;
            }
            {
                std::unique_lock<std::mutex> lock(mutex_capture);
                if (!capture_queue.empty())
                {
                    msg = capture_queue.front();
                    capture_queue.pop();
                    break;
                }
                // For some unkown reasons, Wait()
                // may be blocked. Adding a timeout and
                // restarting the camera seems to fix the problem.
                // This may add a performance penalty
                else if (condition_capture.wait_for(lock, std::chrono::milliseconds(500)) ==
                         std::cv_status::timeout)
                {
                    NRPI_LOG(ERROR) << "Device timeout detected (wrapper), restarting ...";
                    end_camera();
                    start_camera();
                    // Clean the queue
                    std::queue<RPiCamEncoder::Msg> empty;
                    std::swap(capture_queue, empty);
                }
            }
        }

        if (msg.type == RPiCamApp::MsgType::Timeout)
        {
            NRPI_LOG(ERROR) << "Device timeout detected (msg), restarting ...";
            end_camera();
            start_camera();
            std::unique_lock<std::mutex> lock(mutex_capture);
            // Clean the queue
            std::queue<RPiCamEncoder::Msg> empty;
            std::swap(capture_queue, empty);
            continue;
        }
        if (msg.type == RPiCamEncoder::MsgType::Quit)
        {
            end_camera();
            return;
        }
        else if (msg.type != RPiCamEncoder::MsgType::RequestComplete)
            throw std::runtime_error("unrecognised message");

        CompletedRequestPtr &completed_request = std::get<CompletedRequestPtr>(msg.payload);
        app.EncodeBuffer(completed_request, app.VideoStream());
        libcamera::FrameBuffer *buffer = completed_request->buffers[app.VideoStream()];
        OnFrameCaptured(buffer);
    }
}

//------------------------------------------------------------------------------
std::string LibcameraCapturer::ask_for_picture(void)
{
    picture = true;
    std::unique_lock<std::mutex> lock(mutex_picture);
    condition_picture.wait(lock, [this]() { return !picture; });
    return last_picture_filename;
}

//------------------------------------------------------------------------------
std::string LibcameraCapturer::take_picture(void)
{
    NRPI_LOG(INFO) << "Taking a picture";
    RPiCamJpegApp app_pic;
    StillOptions *options_pic = app_pic.GetOptions();

    if (!options_pic->Parse(argc, static_cast<char **>(argv)))
    {
        for (short i = 0; i < argc; ++i) free(argv[i]), argv[i] = nullptr;
        delete[] argv;
        throw std::runtime_error("libcam wrong opts");
    }

    options_pic->nopreview = true;
    options_pic->denoise = "auto";
    options_pic->buffer_count = 2;

    app_pic.OpenCamera();
    app_pic.ConfigureStill();
    app_pic.StartCamera();
    while (true)
    {
        RPiCamApp::Msg msg = app_pic.Wait();

        if (msg.type != RPiCamApp::MsgType::RequestComplete)
            return "";

        if (app_pic.StillStream())
        {
            app_pic.StopCamera();

            libcamera::Stream *stream = app_pic.StillStream();
            StreamInfo info = app_pic.GetStreamInfo(stream);
            CompletedRequestPtr &payload = std::get<CompletedRequestPtr>(msg.payload);
            BufferReadSync r(&app_pic, payload->buffers[stream]);
            const std::vector<libcamera::Span<uint8_t>> mem = r.Get();
            std::string full_path = util::get_full_dir_from_cfg(CONFIG_RECORD_PATH);
            std::string time = util::get_time();
            const std::string filename = time + ".jpg";
            const std::string full_filename = full_path + filename;
            options_pic->output = full_filename;
            jpeg_save(mem,
                      info,
                      payload->metadata,
                      options_pic->output,
                      app_pic.CameraModel(),
                      options_pic);
            NRPI_LOG(INFO) << "Picture recorded in " << full_filename;
            app_pic.StopCamera();
            app_pic.Teardown();
            app_pic.CloseCamera();
            return filename;
        }
    }
}

//------------------------------------------------------------------------------
void LibcameraCapturer::OnFrameCaptured(libcamera::FrameBuffer *buffer)
{
    BufferReadSync r(&app, buffer);
    libcamera::Span span = r.Get()[0];

    rtc::scoped_refptr<webrtc::NRPI_I420Buffer> i420_buffer =
        webrtc::NRPI_I420Buffer::Create(width, height);
    i420_buffer->SetDataY(span.data());

    webrtc::VideoFrame video_frame =
        webrtc::VideoFrame::Builder().set_video_frame_buffer(i420_buffer).build();

    broadcaster.OnFrame(video_frame);
}

//------------------------------------------------------------------------------
void LibcameraCapturer::AddOrUpdateSink(rtc::VideoSinkInterface<webrtc::VideoFrame> *sink,
                                        const rtc::VideoSinkWants &wants)
{
    broadcaster.AddOrUpdateSink(sink, wants);
}

//------------------------------------------------------------------------------
void LibcameraCapturer::RemoveSink(rtc::VideoSinkInterface<webrtc::VideoFrame> *sink)
{
    broadcaster.RemoveSink(sink);
}

#endif // DEBUG_MODE
