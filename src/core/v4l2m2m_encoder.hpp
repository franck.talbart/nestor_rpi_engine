/************************************************************************
 NestoRPi Engine
 Copyright (C) 2015-2025 Franck Talbart

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

/* Author: Franck Talbart */
#ifndef DEBUG_MODE

#ifndef V4L2M2M_ENCODER_H_
#define V4L2M2M_ENCODER_H_

#include <atomic>
#include <condition_variable>
#include <mutex>
#include <queue>
#include <thread>

// Linux
#include <linux/videodev2.h>

// WebRTC
#include <common_video/include/bitrate_adjuster.h>
#include <modules/video_coding/codecs/h264/include/h264.h>

class V4l2m2mEncoder : public webrtc::VideoEncoder
{
  public:
    explicit V4l2m2mEncoder(void);
    ~V4l2m2mEncoder(void) override;

    int32_t InitEncode(const webrtc::VideoCodec *codec_settings,
                       const VideoEncoder::Settings &settings) override;
    int32_t RegisterEncodeCompleteCallback(webrtc::EncodedImageCallback *callback) override;
    int32_t Release(void) override;
    int32_t Encode(const webrtc::VideoFrame &frame,
                   const std::vector<webrtc::VideoFrameType> *frame_types) override;
    void SetRates(const RateControlParameters &parameters) override;
    webrtc::VideoEncoder::EncoderInfo GetEncoderInfo(void) const override;

    int32_t configure(int width, int height, int fps);
    void release(void);

  private:
    struct BufferDescription
    {
        void *mem;
        size_t size;
    };

    void process_poll(void);
    void process_output(void);
    void request_buffers(const enum v4l2_buf_type type,
                         int &num_buffers,
                         struct BufferDescription *buffers);

    int fd, width, height, framerate, i420_buffer_size;
    unsigned int bitrate_bps;
    std::mutex mtx;
    std::string profile, level;

    webrtc::EncodedImage encoded_image;
    webrtc::EncodedImageCallback *callback;
    std::unique_ptr<webrtc::BitrateAdjuster> bitrate_adjuster;

    // We want at least as many output buffers as there are in the camera queue
    // (we always want to be able to queue them when they arrive). Make loads
    // of capture buffers, as this is our buffering mechanism in case of delays
    // dealing with the output bitstream.
    static constexpr int NUM_OUTPUT_BUFFERS = 6;
    static constexpr int NUM_CAPTURE_BUFFERS = 12;

    volatile std::atomic<bool> abort;
    BufferDescription buffers[NUM_CAPTURE_BUFFERS];
    int num_capture_buffers, num_output_buffers;
    std::thread poll_thread, output_thread;
    BufferDescription input_buffers_available[NUM_OUTPUT_BUFFERS];
    struct OutputItem
    {
        void *mem;
        size_t bytes_used;
        size_t length;
        unsigned int index;
        bool keyframe;
        int64_t timestamp;
    };
    std::queue<OutputItem> output_queue;
    volatile std::atomic<int64_t> timestamp;
    std::mutex output_mutex;
    std::condition_variable output_cond_var;
};

#endif // V4L2M2M_ENCODER_H_
#endif // DEBUG_MODE
