/************************************************************************
 NestoRPi Engine
 Copyright (C) 2015-2025 Franck Talbart

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

/* Author: Franck Talbart
   webrtc-streamer written by Michel Promonet
   and updated for the NestoRPi Engine */

#include "src/core/RequestHandler.hpp"

#include "src/core/HttpServerRequestHandler.hpp"
#include "src/core/Log.hpp"

bool RequestHandler::handle(CivetServer *server, struct mg_connection *conn)
{
    bool ret = false;
    const struct mg_request_info *req_info = mg_get_request_info(conn);

    NRPI_LOG(DEBUG) << "uri:" << req_info->request_uri;

    HttpServerRequestHandler *http_server = static_cast<HttpServerRequestHandler *>(server);

    HttpServerRequestHandler::httpFunction fct = http_server->get_function(req_info->request_uri);
    if (fct != nullptr)
    {
        // read input
        Json::Value in = this->getInputMessage(req_info, conn);

        // invoke API implementation
        Json::Value out(fct(req_info, in));

        // fill out
        if (out.isNull() == false)
        {
            std::string answer(Json::writeString(m_writer_builder, out));

            mg_printf(conn, "HTTP/1.1 200 OK\r\n");
            mg_printf(conn, "Access-Control-Allow-Origin: *\r\n");
            mg_printf(conn, "Content-Type: text/plain\r\n");
            mg_printf(conn, "Content-Length: %zd\r\n", answer.size());
            mg_printf(conn, "Connection: close\r\n");
            mg_printf(conn, "\r\n");
            mg_printf(conn, "%s", answer.c_str());

            ret = true;
        }
    }

    return ret;
}

// cppcheck-suppress unusedFunction
bool RequestHandler::handleGet(CivetServer *server, struct mg_connection *conn)
{
    return handle(server, conn);
}

// cppcheck-suppress unusedFunction
bool RequestHandler::handlePost(CivetServer *server, struct mg_connection *conn)
{
    return handle(server, conn);
}

Json::Value RequestHandler::getInputMessage(const struct mg_request_info *req_info,
                                            struct mg_connection *conn)
{
    Json::Value jmessage;

    // read input
    long long tlen = req_info->content_length;
    if (tlen > 0)
    {
        std::string body;
        long long nlen = 0;
        const long long buf_size = 1024;
        char buf[buf_size];
        while (nlen < tlen)
        {
            long long rlen = tlen - nlen;
            if (rlen > buf_size)
            {
                rlen = buf_size;
            }
            rlen = mg_read(conn, buf, (size_t)rlen);
            if (rlen <= 0)
            {
                break;
            }
            body.append(buf, rlen);

            nlen += rlen;
        }

        // parse in
        std::unique_ptr<Json::CharReader> reader(m_reader_builder.newCharReader());
        std::string errors;
        if (!reader->parse(body.c_str(), body.c_str() + body.size(), &jmessage, &errors))
        {
            NRPI_LOG(INFO) << "Received unknown message:" << body << " errors:" << errors;
        }
    }
    return jmessage;
}
