/************************************************************************
 NestoRPi Engine
 Copyright (C) 2015-2025 Franck Talbart

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

/* Author: Franck Talbart */

#include "src/core/PeerConnectionManager.hpp"

PeerConnectionManager::SetSessionDescriptionObserver *
PeerConnectionManager::SetSessionDescriptionObserver::Create(
    webrtc::PeerConnectionInterface *pc,
    std::promise<const webrtc::SessionDescriptionInterface *> &promise)
{
    return new rtc::RefCountedObject<SetSessionDescriptionObserver>(pc, promise);
}

// cppcheck-suppress unusedFunction
void PeerConnectionManager::SetSessionDescriptionObserver::OnSuccess(void)
{
    std::string sdp;
    if (!cancelled)
    {
        if (pc->local_description())
        {
            promise.set_value(pc->local_description());
            pc->local_description()->ToString(&sdp);
        }
        else if (pc->remote_description())
        {
            promise.set_value(pc->remote_description());
            pc->remote_description()->ToString(&sdp);
        }
    }
}

// cppcheck-suppress unusedFunction
void PeerConnectionManager::SetSessionDescriptionObserver::OnFailure(webrtc::RTCError error)
{
    NRPI_LOG(ERROR) << "On failure event: " << error.message();
    if (!cancelled)
    {
        promise.set_value(nullptr);
    }
}
