/************************************************************************
 NestoRPi Engine
 Copyright (C) 2015-2025 Franck Talbart
************************************************************************/

#ifndef DEBUG_MODE

#ifndef DEF_HW_VIDEO_ENCODER_FACTORY
#define DEF_HW_VIDEO_ENCODER_FACTORY

#include <memory>
#include <vector>

#include "api/video_codecs/sdp_video_format.h"
#include "api/video_codecs/video_encoder.h"
#include "api/video_codecs/video_encoder_factory.h"

class HWVideoEncoderFactory : public webrtc::VideoEncoderFactory
{
  public:
    std::vector<webrtc::SdpVideoFormat> GetSupportedFormats(void) const override;
    std::unique_ptr<webrtc::VideoEncoder> Create(const webrtc::Environment& env,
                                                 const webrtc::SdpVideoFormat& format) override;
};

#endif // DEF_HW_VIDEO_ENCODER_FACTORY
#endif // DEBUG_MODE
