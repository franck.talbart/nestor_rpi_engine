/************************************************************************
 NestoRPi Engine
 Copyright (C) 2015-2025 Franck Talbart

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

/* Author: Franck Talbart */
// cf api/video/i420_buffer.cc

#ifndef DEF_API_VIDEO_I420_BUFFER_H_
#define DEF_API_VIDEO_I420_BUFFER_H_

#include <stdint.h>

#include <memory>

#include "api/scoped_refptr.h"
#include "api/video/video_frame_buffer.h"

namespace webrtc
{
// Plain I420 buffer in standard memory.
class RTC_EXPORT NRPI_I420Buffer : public I420BufferInterface
{
  public:
    static rtc::scoped_refptr<NRPI_I420Buffer> Create(int width, int height);

    int width() const override;
    int height() const override;
    const uint8_t* DataY() const override;
    void SetDataY(uint8_t*);
    const uint8_t* DataU() const override;
    const uint8_t* DataV() const override;

    int StrideY() const override;
    int StrideU() const override;
    int StrideV() const override;

  protected:
    NRPI_I420Buffer(int width, int height);

    ~NRPI_I420Buffer() override;

  private:
    const int width_;
    const int height_;
    const int stride_y_;
    const int stride_u_;
    const int stride_v_;
    uint8_t* data_;
};

} // namespace webrtc

#endif // DEF_API_VIDEO_I420_BUFFER_H_
