/************************************************************************
 NestoRPi Engine
 Copyright (C) 2015-2025 Franck Talbart

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

/* Author: Franck Talbart */

/**
 Debug mode
*/

#include "src/core/debug.hpp"

#include <dlfcn.h>

#include "src/core/Log.hpp"

// Original prototype of dlclose
typedef int (*orig_dlclose_type)(void *handle);
orig_dlclose_type orig_dlclose = NULL;

static bool fake_syscalls = false;

//------------------------------------------------------------------------------
/* @ brief Fake dlclose that does nothing
 * ASAN can't keep track of all the libraries loaded during the process lifetime.
 * This is a known bug that won't be fixed soon.
 * https://github.com/google/sanitizers/issues/89#issuecomment-406316683
 * A workaround is to never call dlclose.
 */
// cppcheck-suppress unusedFunction
int dlclose(void *handle)
{
    if (fake_syscalls)
    {
        UNUSED(handle);
        return 0;
    }
    else
    {
        return orig_dlclose(handle);
    }
}

__attribute__((constructor)) static void enable_fake_syscalls(void)
{
    orig_dlclose = reinterpret_cast<orig_dlclose_type>(dlsym(RTLD_NEXT, "dlclose"));
    if (!orig_dlclose)
    {
        NRPI_LOG(ERROR) << "Error while grabbing dlclose";
        exit(EXIT_FAILURE);
    }

#ifdef DEBUG_MODE
    fake_syscalls = true;
#endif // DEBUG_MODE
}

void debug::disable_fake_syscalls(void) { fake_syscalls = false; }
