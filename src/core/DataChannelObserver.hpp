/************************************************************************
 NestoRPi Engine
 Copyright (C) 2015-2025 Franck Talbart

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

/* Author: Franck Talbart */

#ifndef DEF_DATA_CHANNEL_OBSERVER
#define DEF_DATA_CHANNEL_OBSERVER

#include <atomic>
#include <condition_variable>
#include <map>
#include <mutex>
#include <queue>
#include <thread>

#include "api/data_channel_interface.h"
#include "src/core/util.hpp"

class Controller;
class Automation;

class DataChannelObserver : public webrtc::DataChannelObserver
{
  public:
    DataChannelObserver(rtc::scoped_refptr<webrtc::DataChannelInterface> data_channel,
                        const std::map<std::string, std::shared_ptr<Controller>>& tasks_controller,
                        const std::map<std::string, std::shared_ptr<Automation>>& tasks_automation,
                        const role_type role);
    virtual ~DataChannelObserver(void);
    virtual void OnStateChange(void);
    void Send(const std::string& msg);
    virtual void OnMessage(const webrtc::DataBuffer& buffer);

  protected:
    void thread_process(void);

    rtc::scoped_refptr<webrtc::DataChannelInterface> data_channel;
    std::map<std::string, std::shared_ptr<Controller>> tasks_controller;
    std::map<std::string, std::shared_ptr<Automation>> tasks_automation;
    std::queue<std::string> msgs;
    std::thread thread_data;
    volatile std::atomic<bool> running;
    std::mutex mtx_msgs;
    std::condition_variable cv;
    role_type role;
};

#endif // DEF_DATA_CHANNEL_OBSERVER
