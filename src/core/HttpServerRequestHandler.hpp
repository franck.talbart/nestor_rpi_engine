/************************************************************************
 NestoRPi Engine
 Copyright (C) 2015-2025 Franck Talbart

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

/* Author: Franck Talbart
   webrtc-streamer written by Michel Promonet
   and updated for the NestoRPi Engine */

#ifndef DEF_HTTP_SERVER_REQUEST_HANDLER
#define DEF_HTTP_SERVER_REQUEST_HANDLER

#include <functional>
#include <list>
#include <map>
#include <memory>

#include "CivetServer.h"
#include "json/json.h"
#include "src/core/RequestHandler.hpp"

class HttpServerRequestHandler : public CivetServer
{
  public:
    typedef std::function<Json::Value(const struct mg_request_info *req_info, const Json::Value &)>
        httpFunction;
    HttpServerRequestHandler(std::map<std::string, httpFunction> &func,
                             const std::vector<std::string> &options);
    httpFunction get_function(const std::string &uri);

  protected:
    std::map<std::string, httpFunction> func;
    std::vector<std::shared_ptr<RequestHandler>> request_handlers;
};

static struct CivetCallbacks _callbacks;

#endif // DEF_HTTP_SERVER_REQUEST_HANDLER
