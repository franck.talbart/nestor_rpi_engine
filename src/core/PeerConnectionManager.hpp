/************************************************************************
 NestoRPi Engine
 Copyright (C) 2015-2025 Franck Talbart

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

/* Author: Franck Talbart */

#ifndef DEF_PEER_CONNECTION_MANAGER
#define DEF_PEER_CONNECTION_MANAGER

#include <future>
#include <mutex>
#include <regex>
#include <string>
#include <thread>

#include "api/peer_connection_interface.h"
#include "src/core/Camera.hpp"
#include "src/core/DataChannelObserver.hpp"
#include "src/core/Log.hpp"
#include "src/tasks/Automation.hpp"
#include "src/tasks/Controller.hpp"

class PeerConnectionManager
{
    class VideoSink : public rtc::VideoSinkInterface<webrtc::VideoFrame>
    {
      public:
        explicit VideoSink(const rtc::scoped_refptr<webrtc::VideoTrackInterface> &track)
            : m_track(track)
        {
            NRPI_LOG(DEBUG) << __PRETTY_FUNCTION__ << " track:" << m_track->id();
            m_track->AddOrUpdateSink(this, rtc::VideoSinkWants());
        }
        virtual ~VideoSink()
        {
            NRPI_LOG(DEBUG) << __PRETTY_FUNCTION__ << " track:" << m_track->id();
            m_track->RemoveSink(this);
        }

        // VideoSinkInterface implementation
        virtual void OnFrame(const webrtc::VideoFrame &video_frame)
        {
            rtc::scoped_refptr<webrtc::I420BufferInterface> buffer(
                video_frame.video_frame_buffer()->ToI420());
            NRPI_LOG(DEBUG) << __PRETTY_FUNCTION__ << " frame:" << buffer->width() << "x"
                            << buffer->height();
        }

      protected:
        rtc::scoped_refptr<webrtc::VideoTrackInterface> m_track;
    };

    class SetSessionDescriptionObserver : public webrtc::SetSessionDescriptionObserver
    {
      public:
        static SetSessionDescriptionObserver *Create(
            webrtc::PeerConnectionInterface *pc,
            std::promise<const webrtc::SessionDescriptionInterface *> &promise);
        virtual void OnSuccess(void);
        virtual void OnFailure(webrtc::RTCError error);
        void cancel() { cancelled = true; }

      protected:
        SetSessionDescriptionObserver(
            webrtc::PeerConnectionInterface *pc,
            std::promise<const webrtc::SessionDescriptionInterface *> &promise)
            : pc(pc), promise(promise), cancelled(false){};

      private:
        webrtc::PeerConnectionInterface *pc;
        std::promise<const webrtc::SessionDescriptionInterface *> &promise;
        bool cancelled;
    };

    class CreateSessionDescriptionObserver : public webrtc::CreateSessionDescriptionObserver
    {
      public:
        static CreateSessionDescriptionObserver *Create(
            webrtc::PeerConnectionInterface *pc,
            std::promise<const webrtc::SessionDescriptionInterface *> &promise)
        {
            return new rtc::RefCountedObject<CreateSessionDescriptionObserver>(pc, promise);
        }
        virtual void OnSuccess(webrtc::SessionDescriptionInterface *desc)
        {
            std::string sdp;
            desc->ToString(&sdp);
            if (!cancelled)
            {
                pc->SetLocalDescription(SetSessionDescriptionObserver::Create(pc, promise), desc);
            }
        }
        virtual void OnFailure(webrtc::RTCError error)
        {
            NRPI_LOG(ERROR) << error.message();
            if (!cancelled)
            {
                promise.set_value(nullptr);
            }
        }
        void cancel() { cancelled = true; }

      protected:
        CreateSessionDescriptionObserver(
            webrtc::PeerConnectionInterface *pc,
            std::promise<const webrtc::SessionDescriptionInterface *> &promise)
            : pc(pc), promise(promise), cancelled(false){};

      private:
        webrtc::PeerConnectionInterface *pc;
        std::promise<const webrtc::SessionDescriptionInterface *> &promise;
        bool cancelled;
    };

    class PeerConnectionStatsCollectorCallback : public webrtc::RTCStatsCollectorCallback
    {
      public:
        PeerConnectionStatsCollectorCallback(void) {}
        void clearReport(void) { report.clear(); }
        Json::Value getReport(void) { return report; }

      protected:
        virtual void OnStatsDelivered(
            const rtc::scoped_refptr<const webrtc::RTCStatsReport> &mreport)
        {
            for (const webrtc::RTCStats &stats : *mreport)
            {
                Json::Value stats_members;
                for (auto &attribute : stats.Attributes())
                {
                    stats_members[attribute.name()] = attribute.ToString();
                }
                report[stats.id()] = stats_members;
            }
        }

        Json::Value report;
    };

    class PeerConnectionObserver : public webrtc::PeerConnectionObserver
    {
      public:
        PeerConnectionObserver(PeerConnectionManager *peer_connection_manager,
                               const std::string &peer_id,
                               const webrtc::PeerConnectionInterface::RTCConfiguration &config,
                               const role_type &role,
                               const std::string &token);
        virtual ~PeerConnectionObserver(void);
        Json::Value getIceCandidateList(void);
        std::string getToken(void);
        Json::Value getStats(void);
        rtc::scoped_refptr<webrtc::PeerConnectionInterface> getPeerConnection(void);
        virtual void OnAddStream(rtc::scoped_refptr<webrtc::MediaStreamInterface> stream);
        virtual void OnRemoveStream(rtc::scoped_refptr<webrtc::MediaStreamInterface> stream);
        virtual void OnDataChannel(rtc::scoped_refptr<webrtc::DataChannelInterface> channel);
        virtual void OnRenegotiationNeeded(void);
        virtual void OnIceCandidate(const webrtc::IceCandidateInterface *candidate);
        virtual void OnSignalingChange(webrtc::PeerConnectionInterface::SignalingState state);
        virtual void OnIceConnectionChange(
            webrtc::PeerConnectionInterface::IceConnectionState state);
        virtual void OnIceGatheringChange(webrtc::PeerConnectionInterface::IceGatheringState) {}

      private:
        PeerConnectionManager *peer_connection_manager;
        const std::string peer_id;
        rtc::scoped_refptr<webrtc::PeerConnectionInterface> pc;
        role_type role;
        std::string token;
        std::unique_ptr<DataChannelObserver> remote_channel;
        Json::Value ice_candidate_list;
        rtc::scoped_refptr<PeerConnectionStatsCollectorCallback> stats_callback;
        tasks_controller_t tasks_controller;
        tasks_automation_t tasks_automation;
        std::unique_ptr<VideoSink> video_sink;
        bool deleting;
        bool is_reset;
    };

  public:
    PeerConnectionManager(const std::list<std::string> &ice_server_list,
                          webrtc::AudioDeviceModule::AudioLayer audio_layer,
                          const tasks_controller_t &tasks_controller,
                          const tasks_automation_t &tasks_automation);
    ~PeerConnectionManager(void);
    bool InitializePeerConnection(void);
    const Json::Value getIceCandidateList(const std::string &peer_id);
    const Json::Value addIceCandidate(const std::string &peer_id, const Json::Value &jmessage);
    const Json::Value getVideoDeviceList(void);
    const Json::Value getAudioDeviceList(void);
    const Json::Value getIceServers(const std::string &client_ip);
    const Json::Value hangUp(const std::string &peer_id,
                             const std::string &user,
                             const std::string &passwd,
                             const std::string &token);
    const Json::Value call(const std::string &peer_id,
                           const std::string &options,
                           const Json::Value &jmessage);
    const Json::Value getPeerConnectionList(void);
    const Json::Value getStreamList(void);
    tasks_controller_t getTasksController(void);
    tasks_automation_t getTasksAutomation(void);

  protected:
    std::shared_ptr<PeerConnectionObserver> CreatePeerConnection(const std::string &peer_id,
                                                                 const role_type &role,
                                                                 const std::string &token);
    bool AddStreams(webrtc::PeerConnectionInterface *peer_connection, const std::string &options);
    rtc::scoped_refptr<webrtc::VideoTrackInterface> CreateVideoTrack(
        const std::string &video_url, const std::map<std::string, std::string> &opts);
    rtc::scoped_refptr<webrtc::AudioTrackInterface> CreateAudioTrack(
        const std::string &audio_url, const std::map<std::string, std::string> &opts);
    rtc::scoped_refptr<webrtc::VideoTrackSourceInterface> CreateVideoSource(
        const std::string &video_url, const std::map<std::string, std::string> &opts);
    rtc::scoped_refptr<webrtc::AudioSourceInterface> CreateAudioSource(
        const std::string &audio_url);
    bool streamStillUsed(const std::string &stream_label);
    const std::list<std::string> getVideoCaptureDeviceList(void);
    rtc::scoped_refptr<webrtc::PeerConnectionInterface> getPeerConnection(
        const std::string &peer_id);
    const std::string sanitizeLabel(const std::string &label);
    void create_audio_module(webrtc::AudioDeviceModule::AudioLayer audio_layer);
    typedef std::pair<std::shared_ptr<Camera>, rtc::scoped_refptr<webrtc::AudioSourceInterface>>
        AudioVideoPair;
    std::unique_ptr<webrtc::TaskQueueFactory> task_queue_factory;

    rtc::scoped_refptr<webrtc::AudioDeviceModule> audio_device_module;

    std::unique_ptr<rtc::Thread> signaling_thread;
    std::unique_ptr<rtc::Thread> worker_thread;
    rtc::scoped_refptr<webrtc::PeerConnectionFactoryInterface> peer_connection_factory;
    std::list<std::string> ice_server_list;
    std::mutex peer_map_mutex;
    std::map<std::string, std::shared_ptr<PeerConnectionObserver>> peer_connection_obs_map;
    std::map<std::string, AudioVideoPair> stream_map;
    std::mutex stream_map_mutex;
    std::map<std::string, std::string> videoaudiomap;
    tasks_controller_t tasks_controller;
    tasks_automation_t tasks_automation;
    std::map<std::string, rtc::scoped_refptr<webrtc::AudioSourceInterface>> audio_sources;

    static const char CANDIDATE_SDP_MID_NAME[];
    static const char CANDIDATE_SDP_MLINE_INDEX_NAME[];
    static const char CANDIDATE_SDP_NAME[];

    static const char SESSION_DESCRIPTION_TYPE_NAME[];
    static const char SESSION_DESCRIPTION_SDP_NAME[];
};
#endif // DEF_PEER_CONNECTION_MANAGER
