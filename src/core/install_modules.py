#!/usr/bin/env python3

# ************************************************************************
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# ************************************************************************

# Author: Franck Talbart

"""
    This script is used to automatically generate the files required by the
    framework to enable tasks and devices.
    They are discovered and integrated into the framework.
    This code must be called everytime a new task or a new device is added into
    the corresponding directory. It is executed during the compilation process.
    Usage:
    $ ./install_modules.py
"""

import glob
import logging
import sys

from pathlib import Path

# -------------------------------------------------------------------------------
# Consts
BLACKLIST = [["Automation.hpp", "Controller.hpp"], ["Actuator.hpp", "Sensor.hpp"]]
LINE_INCLUDE = '#include "{}"\n'
LINE_LIST = ["REGISTER_CLASS_TASK({});\n", "REGISTER_CLASS_DEVICE({});\n"]
OUTPUT_FILE_INCLUDE = ["include_tasks", "include_devices"]
OUTPUT_FILE_LIST = ["list_tasks", "list_devices"]
PATH = ["tasks", "devices"]
SEARCH_NAMES = [
    ["Automation*.hpp", "Controller*.hpp"],
    ["Actuator*.hpp", "Sensor*.hpp"],
]

# -------------------------------------------------------------------------------
def main():
    """
    Generates the list_devices file by browsing the current directory
    """
    logging.basicConfig(level=logging.INFO)
    logging.info("Generating list_tasks and list_devices")

    for (
        path,
        output_file_list,
        output_file_include,
        search_names,
        blacklist,
        line_list,
    ) in zip(
        PATH, OUTPUT_FILE_LIST, OUTPUT_FILE_INCLUDE, SEARCH_NAMES, BLACKLIST, LINE_LIST
    ):
        list_files = []
        path = Path(sys.argv[0]).parent / "../" / Path(path)

        # Generates the list of files
        for names in search_names:
            list_files.extend([f.name for f in Path(path).glob(names)])

        # Removes unwanted files
        final_list = [file_ for file_ in list_files if file_ not in blacklist]

        # Write list_devices
        with open(path / Path(output_file_list), mode="w", encoding="utf-8") as f:
            f.writelines(line_list.format(item[:-4]) for item in final_list)

        with open(path / Path(output_file_include), mode="w", encoding="utf-8") as f:
            f.writelines(LINE_INCLUDE.format(item) for item in final_list)

    logging.info("Done!")


# -------------------------------------------------------------------------------
if __name__ == "__main__":
    sys.exit(main())
