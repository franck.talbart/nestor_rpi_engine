/************************************************************************
 NestoRPi Engine
 Copyright (C) 2015-2025 Franck Talbart

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

/* Author: Franck Talbart */

#include "src/core/PeerConnectionManager.hpp"

PeerConnectionManager::PeerConnectionObserver::PeerConnectionObserver(
    PeerConnectionManager *peer_connection_manager,
    const std::string &peer_id,
    const webrtc::PeerConnectionInterface::RTCConfiguration &config,
    const role_type &role,
    const std::string &token)
    : peer_connection_manager(peer_connection_manager),
      peer_id(peer_id),
      role(role),
      token(token),
      remote_channel(nullptr),
      ice_candidate_list(Json::arrayValue),
      deleting(false)
{
    NRPI_LOG(DEBUG) << "CreatePeerConnection peer id:" << peer_id;
    webrtc::PeerConnectionDependencies dependencies(this);

    webrtc::RTCErrorOr<rtc::scoped_refptr<webrtc::PeerConnectionInterface>> result =
        peer_connection_manager->peer_connection_factory->CreatePeerConnectionOrError(
            config, std::move(dependencies));
    if (result.ok())
    {
        pc = result.MoveValue();
    }
    else
    {
        NRPI_LOG(ERROR) << __FUNCTION__ << "CreatePeerConnection peerid:" << peer_id
                        << " error:" << result.error().message();
    }

    tasks_controller = peer_connection_manager->getTasksController();
    tasks_automation = peer_connection_manager->getTasksAutomation();
    stats_callback = new rtc::RefCountedObject<PeerConnectionStatsCollectorCallback>();
    is_reset = false;
}

PeerConnectionManager::PeerConnectionObserver::~PeerConnectionObserver(void)
{
    if (pc.get())
    {
        // warning: pc->close call OnIceConnectionChange
        deleting = true;
        pc->Close();
    }
}

Json::Value PeerConnectionManager::PeerConnectionObserver::getIceCandidateList(void)
{
    return ice_candidate_list;
}

std::string PeerConnectionManager::PeerConnectionObserver::getToken(void) { return token; }

// cppcheck-suppress unusedFunction
Json::Value PeerConnectionManager::PeerConnectionObserver::getStats(void)
{
    stats_callback->clearReport();
    pc->GetStats(stats_callback.get());
    int count = 10;
    while ((stats_callback->getReport().empty()) && (--count > 0))
    {
        std::this_thread::sleep_for(std::chrono::milliseconds(1000));
    }
    return Json::Value(stats_callback->getReport());
}

rtc::scoped_refptr<webrtc::PeerConnectionInterface>
PeerConnectionManager::PeerConnectionObserver::getPeerConnection(void)
{
    return pc;
}

// cppcheck-suppress unusedFunction
void PeerConnectionManager::PeerConnectionObserver::OnAddStream(
    rtc::scoped_refptr<webrtc::MediaStreamInterface> stream)
{
    NRPI_LOG(DEBUG) << " nb video tracks:" << stream->GetVideoTracks().size();
    NRPI_LOG(DEBUG) << " nb audio tracks:" << stream->GetAudioTracks().size();

    webrtc::VideoTrackVector video_tracks = stream->GetVideoTracks();
    if (video_tracks.size() > 0)
    {
        video_sink.reset(new VideoSink(video_tracks.at(0)));
    }
}

// cppcheck-suppress unusedFunction
void PeerConnectionManager::PeerConnectionObserver::OnRemoveStream(
    rtc::scoped_refptr<webrtc::MediaStreamInterface> stream)
{
    UNUSED(stream);
    video_sink.reset();
}

// cppcheck-suppress unusedFunction
void PeerConnectionManager::PeerConnectionObserver::OnDataChannel(
    rtc::scoped_refptr<webrtc::DataChannelInterface> channel)
{
    remote_channel.reset(
        new DataChannelObserver(channel, tasks_controller, tasks_automation, role));
}

// cppcheck-suppress unusedFunction
void PeerConnectionManager::PeerConnectionObserver::OnRenegotiationNeeded(void)
{
    NRPI_LOG(DEBUG) << " peer_id:" << peer_id;
}

// cppcheck-suppress unusedFunction
void PeerConnectionManager::PeerConnectionObserver::OnSignalingChange(
    webrtc::PeerConnectionInterface::SignalingState state)
{
    NRPI_LOG(DEBUG) << " state:" << state << " peer_id:" << peer_id;
}

// cppcheck-suppress unusedFunction
void PeerConnectionManager::PeerConnectionObserver::OnIceConnectionChange(
    webrtc::PeerConnectionInterface::IceConnectionState state)
{
    NRPI_LOG(DEBUG) << " state:" << state << " peer_id:" << peer_id;

    // This is a security so even if the connection is dropped unexpectedly,
    // we reset the tasks and the robot does not move without the client.
    if ((state == webrtc::PeerConnectionInterface::kIceConnectionClosed ||
         state == webrtc::PeerConnectionInterface::kIceConnectionDisconnected) &&
        !is_reset)
    {
        NRPI_LOG(INFO) << " State:" << state << " peer_id:" << peer_id;
        NRPI_LOG(INFO) << " * Resetting ...";
        for (const auto &task_controller : tasks_controller) task_controller.second->reset();
        for (const auto &task_automation : tasks_automation) task_automation.second->reset();
        // We call reset only onced (the tasks may use mutexes)
        is_reset = true;
    }

    if ((state == webrtc::PeerConnectionInterface::kIceConnectionFailed) ||
        (state == webrtc::PeerConnectionInterface::kIceConnectionClosed))
    {
        ice_candidate_list.clear();
        if (!deleting)
        {
            std::thread([this]() { peer_connection_manager->hangUp(peer_id, "", "", token); })
                .detach();
        }
    }

    if (state == webrtc::PeerConnectionInterface::kIceConnectionClosed)
        NRPI_LOG(INFO) << "  * Connection closed";
}
