/************************************************************************
 NestoRPi Engine
 Copyright (C) 2015-2025 Franck Talbart

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

/* Author: Franck Talbart */

/**
 * This file contains the general macros.
 */

#ifndef DEF_CONST
#define DEF_CONST

// Colors
#define COLOR_BLUE "1;34"
#define COLOR_CYAN "1;36"
#define COLOR_GREEN "1;32"
#define COLOR_RED "1;31"
#define COLOR_YELLOW "1;33"

// Config
#define CONFIG_AUDIO_DEV "audio_dev"
#define CONFIG_AUDIO_LAYER "audio_layer"
#define CONFIG_CAMERA "cameras"
#define CONFIG_CAMERA_FPS "fps"
#define CONFIG_CAMERA_HEIGHT "video_height"
#define CONFIG_CAMERA_LIBCAMERA "libcamera"
#define CONFIG_CAMERA_NAME "name"
#define CONFIG_CAMERA_ROTATE "rotate"
#define CONFIG_CAMERA_WIDTH "video_width"
#define CONFIG_FORCE_H264_BITRATE "force_h264_bitrate"
#define CONFIG_H264_PROFILE "h264_profile"
#define CONFIG_HTTP_ADDRESS "http_address"
#define CONFIG_HTTP_PORT "http_port"
#define CONFIG_HW_ENCODER "hw_encoder"
#define CONFIG_LOG_PATH "log_path"
#define CONFIG_MBROLA_PARAMS "mbrola_params"
#define CONFIG_PICTURE_HEIGHT "picture_height"
#define CONFIG_PICTURE_WIDTH "picture_width"
#define CONFIG_RECORD_PATH "record_path"
#define CONFIG_ROBOT_NAME "robot_name"
#define CONFIG_ROLE_MINIMAL_AUDIO "role_minimal_audio"
#define CONFIG_ROLE_MINIMAL_VIDEO "role_minimal_video"
#define CONFIG_SHM_PATH "shm_path"
#define CONFIG_SSL_CERTIFICATE "ssl_certificate"
#define CONFIG_STUN_URL "stun_url"
#define CONFIG_TURN_URL "turn_url"

// Filenames
#define FILENAME_CONFIG "config.json"
#define FILENAME_CREDENTIALS "credentials"
#define FILENAME_DEVICES "devices.json"
#define FILENAME_EXTENSIONS "extensions.json"
#define FILENAME_INCLUDE_DEVICES "src/devices/include_devices"
#define FILENAME_INCLUDE_TASKS "src/tasks/include_tasks"
#define FILENAME_LIST_DEVICES "src/devices/list_devices"
#define FILENAME_LIST_TASKS "src/tasks/list_tasks"
#define FILENAME_LOG_DAEMON "nestorpi.log"
#define FILENAME_TASKS_AUTOMATION "tasks_automation.json"
#define FILENAME_TASKS_CONTROLLER "tasks_controller.json"

// JSON consts
#define JSON_DEVICES_DEVICE "device"
#define JSON_DEVICES_PINS "pins"
#define JSON_DEVICES_TYPE "type"
#define JSON_EXTENSIONS_ADDRESS "address"
#define JSON_EXTENSIONS_BASE "base"
#define JSON_EXTENSIONS_FREQUENCY "frequency"
#define JSON_EXTENSIONS_MCP23008 "MCP23008"
#define JSON_EXTENSIONS_MCP23017 "MCP23017"
#define JSON_EXTENSIONS_PCA9685 "PCA9685"
#define JSON_EXTENSIONS_TYPE "type"
#define JSON_TASKS_TYPE "type"

// Paths
#define PATH_CFG "cfg/"
#define PATH_RANDOM_SOUNDS "random_sounds/"
#define PATH_SOUNDS "sounds/"
#define PATH_WEB_ROOT "html/"

// Misc
#define COMM_DELIMITER "-del-"
#define CRED_DELIMITER ":"
#define DEFAULT_USER "default"
#define PREFIX_SHM "nestor_"

#endif // DEF_CONST
