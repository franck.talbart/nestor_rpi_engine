/************************************************************************
 NestoRPi Engine
 Copyright (C) 2015-2025 Franck Talbart

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

/* Author: Franck Talbart
   webrtc-streamer written by Michel Promonet
   and updated for the NestoRPi Engine */

#ifndef DEF_REQUEST_HANDLER
#define DEF_REQUEST_HANDLER

#include "CivetServer.h"
#include "json/json.h"

class RequestHandler : public CivetHandler
{
  public:
    bool handle(CivetServer *server, struct mg_connection *conn);
    bool handleGet(CivetServer *server, struct mg_connection *conn) override;
    bool handlePost(CivetServer *server, struct mg_connection *conn) override;

  private:
    Json::Value getInputMessage(const struct mg_request_info *req_info, struct mg_connection *conn);
    Json::StreamWriterBuilder m_writer_builder;
    Json::CharReaderBuilder m_reader_builder;
};

#endif // DEF_REQUEST_HANDLER
