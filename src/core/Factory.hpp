/************************************************************************
 NestoRPi Engine
 Copyright (C) 2015-2025 Franck Talbart

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

/* Author: Franck Talbart */

#ifndef DEF_FACTORY
#define DEF_FACTORY

#include "src/devices/Device.hpp"
#include "src/tasks/Task.hpp"

/**
 * This factory is used to create different devices and tasks
 */

// Device
//------------------------------------------------------------------------------
/**
 * @brief Create an instance of type T (device)
 *
 * @param pins: map of pins
 * @param val_device: map containing the Json config values of the device
 * @return instance of a Device type
 */
template <class T>
Device *constructor_device(const pins_t &pins, const val_json_t &val_device)
{
    return new T(pins, val_device);
}

// Task
//------------------------------------------------------------------------------
/**
 * @brief Create an instance of type T (task)
 *
 * @param cfg_task: map containing the Json config values of the task
 * @return instance of a Task type
 */
template <class T>
Task *constructor_task(const std::string &name, val_json_t &cfg_task)
{
    return new T(name, cfg_task);
}

//------------------------------------------------------------------------------
struct Factory
{
    // Device
    typedef Device *(*constructor_t_device)(const pins_t &, const val_json_t &);
    typedef std::map<std::string, constructor_t_device> map_type_device;
    map_type_device classes_device;

    // Task
    typedef Task *(*constructor_t_task)(const std::string &, val_json_t &);
    typedef std::map<std::string, constructor_t_task> map_type_task;

    map_type_task classes_task;

    //--------------------------------------------------------------------------
    /**
     * @brief Register a device class
     *
     * @param n: the class name
     *
     */
    template <class T>
    void register_class_device(std::string const &n)
    {
        classes_device.insert(std::make_pair(n, &constructor_device<T>));
    }

    //--------------------------------------------------------------------------
    /**
     * @brief Register a task class
     *
     * @param n: the class name
     *
     */
    template <class T>
    void register_class_task(std::string const &n)
    {
        classes_task.insert(std::make_pair(n, &constructor_task<T>));
    }

    //--------------------------------------------------------------------------
    /**
     * @brief Return the constructor of a device type class
     *        Will throw and exception if class is not found
     * @param n: the class name
     * @return the corresponding constructor
     */
    constructor_t_device construct_device(const std::string &n)
    {
        map_type_device::iterator i = classes_device.find(n);
        if (i == classes_device.end())
            throw n + std::string(" not found!");
        return i->second;
    }

    //--------------------------------------------------------------------------
    /**
     * @brief Return the constructor of a task type class
     *        Will throw and exception if class is not found
     * @param n: the class name
     * @return the corresponding constructor
     */
    constructor_t_task construct_task(const std::string &n)
    {
        map_type_task::iterator i = classes_task.find(n);
        if (i == classes_task.end())
            throw n + std::string(" not found!");
        return i->second;
    }
};

Factory g_factory;

// Syntactic sugar
#define REGISTER_CLASS_DEVICE(n) g_factory.register_class_device<n>(#n)
#define REGISTER_CLASS_TASK(n) g_factory.register_class_task<n>(#n)

#endif // DEF_FACTORY
