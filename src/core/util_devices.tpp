/************************************************************************
 NestoRPi Engine
 Copyright (C) 2015-2025 Franck Talbart

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

/* Author: Franck Talbart */

/**
 * This module provides functions to load the devices
 */

#ifndef DEF_UTIL_DEVICES
#define DEF_UTIL_DEVICES

#include "src/core/const.hpp"
#include "src/core/util.hpp"
#include FILENAME_INCLUDE_DEVICES
#include "src/core/Factory.hpp"
#include "src/core/Log.hpp"

extern Factory g_factory;

//------------------------------------------------------------------------------
/**
 * @brief Load the devices and returns the corresponding map
 *
 * @return  map of devices
 */
template <typename T>
std::map<std::string, std::shared_ptr<T>> load_devices(void)
{
#include FILENAME_LIST_DEVICES // user just have to write his gpios device
                               // classes in the devices directory
                               // it will be automatically
                               // included in the project

    // Get the list of devices
    const std::string path = util::get_cfg_dir() + std::string(FILENAME_DEVICES);

    std::shared_ptr<char> file_content = nullptr;
    try
    {
        file_content = util::prepare_file(path);
    }
    catch (const std::string& msg)
    {
        NRPI_LOG(ERROR) << "Error while opening the JSON file " << path;
        throw std::string(msg);
    }

    char* endptr;
    gason::JsonValue root;
    gason::JsonAllocator allocator;
    int status = gason::jsonParse(file_content.get(), &endptr, &root, allocator);

    if (status != gason::JSON_PARSE_OK)
    {
        NRPI_LOG(ERROR) << "Error while parsing the JSON file " << path;
        throw std::string("wrong json");
    }

    std::map<std::string, std::shared_ptr<T>> result;

    for (const auto& value : root) // C++ 11 :)
    {
        pins_t pins;
        val_json_t val_device;
        std::string class_name;
        std::string type_device;
        for (const auto& it : value->value)
        {
            // Get the pin names
            if (it->key == std::string(JSON_DEVICES_PINS))
            {
                for (const auto& p : it->value)
                    pins[std::string(p->key)] = (int)p->value.toNumber();
            }
            // Device name
            else if (it->key == std::string(JSON_DEVICES_DEVICE))
            {
                class_name = std::string(static_cast<char*>(it->value.toString()));
                class_name[0] = toupper(class_name[0]);
                class_name = T::JSON_DEVICES_TYPE_VALUE + class_name;
                class_name[0] = toupper(class_name[0]);
            }
            // Device type
            else if (it->key == std::string(JSON_DEVICES_TYPE))
            {
                type_device = std::string(static_cast<char*>(it->value.toString()));
            }
            // other values
            else
            {
                val_device[std::string(it->key)] = it->value;
            }
        }

        // Instantiate the corresponding class using the factory
        if (type_device == T::JSON_DEVICES_TYPE_VALUE)
        {
            T* device = nullptr;
            try
            {
                device = dynamic_cast<T*>(g_factory.construct_device(class_name)(pins, val_device));
            }
            catch (const std::string& msg)
            {
                NRPI_LOG(ERROR) << msg;
                throw std::string(msg);
            }
            result[std::string(value->key)] = std::shared_ptr<T>(device);
        }
    }
    return result;
}

#endif // DEF_UTIL_DEVICES
