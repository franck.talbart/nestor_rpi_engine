/************************************************************************
 NestoRPi Engine
 Copyright (C) 2015-2025 Franck Talbart

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

/* Author: Franck Talbart
   webrtc-streamer written by Michel Promonet
   and updated for the NestoRPi Engine */

#ifndef DEF_VCM_CAPTURER
#define DEF_VCM_CAPTURER

#include <map>

#include "media/base/video_broadcaster.h"
#include "modules/video_capture/video_capture_factory.h"

class VcmCapturer : public rtc::VideoSinkInterface<webrtc::VideoFrame>,
                    public rtc::VideoSourceInterface<webrtc::VideoFrame>
{
  public:
    static VcmCapturer *Create(const std::string &video_url,
                               const std::map<std::string, int> &opts);
    virtual ~VcmCapturer(void);
    void OnFrame(const webrtc::VideoFrame &frame) override;

  private:
    VcmCapturer(void) : vcm(nullptr) {}
    bool Init(size_t width, size_t height, size_t target_fps, const std::string &video_url);
    void Destroy(void);
    void AddOrUpdateSink(rtc::VideoSinkInterface<webrtc::VideoFrame> *sink,
                         const rtc::VideoSinkWants &wants) override;
    void RemoveSink(rtc::VideoSinkInterface<webrtc::VideoFrame> *sink) override;

    rtc::scoped_refptr<webrtc::VideoCaptureModule> vcm;
    rtc::VideoBroadcaster broadcaster;
};

#endif // DEF_VCM_CAPTURER
