/************************************************************************
 NestoRPi Engine
 Copyright (C) 2015-2025 Franck Talbart

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

/* Author: Franck Talbart */

#ifndef DEBUG_MODE

#ifndef DEF_LIBCAMERA_CAPTURER
#define DEF_LIBCAMERA_CAPTURER

#include <api/video/i420_buffer.h>
#include <media/base/adapted_video_track_source.h>
#include <rtc_base/platform_thread.h>

#include <atomic>
#include <queue>

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "core/rpicam_app.hpp"
#include "core/rpicam_encoder.hpp"
#include "core/still_options.hpp"
#pragma GCC diagnostic pop
#include "gason.hpp"
#include "output/output.hpp"

class LibcameraCapturer : public rtc::AdaptedVideoTrackSource
{
  public:
    static rtc::scoped_refptr<LibcameraCapturer> Create(
        short dev, const std::map<std::string, std::string>& opts);
    LibcameraCapturer(short dev, const std::map<std::string, std::string>& opts);
    ~LibcameraCapturer(void);
    webrtc::MediaSourceInterface::SourceState state(void) const;
    bool remote(void) const;
    bool is_screencast(void) const;
    std::optional<bool> needs_denoising(void) const;
    void StartCapture(void);
    void StopCapture(void);
    void capture_thread(void);
    void process_capture_thread(void);
    void OnFrameCaptured(libcamera::FrameBuffer* buffer);
    std::string ask_for_picture(void);

  private:
    void AddOrUpdateSink(rtc::VideoSinkInterface<webrtc::VideoFrame>* sink,
                         const rtc::VideoSinkWants& wants);
    void RemoveSink(rtc::VideoSinkInterface<webrtc::VideoFrame>* sink);
    std::string take_picture(void);
    void start_camera(void);
    void end_camera(void);

    rtc::VideoBroadcaster broadcaster;
    size_t width, height;
    VideoOptions* options;
    volatile std::atomic<bool> running;
    short device;
    char** argv;
    int argc;
    RPiCamEncoder app;
    std::string last_picture_filename;
    bool picture;
    std::mutex mutex_capture, mutex_picture, mutex_start_stop;
    std::condition_variable condition_capture, condition_picture;
    std::queue<RPiCamApp::Msg> capture_queue;

  protected:
    rtc::PlatformThread t_capture_thread, t_process_capture_thread;
};

#endif // DEF_LIBCAMERA_CAPTURER
#endif // DEBUG_MODE
