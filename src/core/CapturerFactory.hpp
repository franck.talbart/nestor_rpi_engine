/************************************************************************
 NestoRPi Engine
 Copyright (C) 2015-2025 Franck Talbart

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

/* Author: Franck Talbart
   webrtc-streamer written by Michel Promonet
   and updated for the NestoRPi Engine */

#ifndef DEF_CAPTURER_FACTORY
#define DEF_CAPTURER_FACTORY
#include <regex>

#include "api/peer_connection_interface.h"
#include "src/core/VcmCapturer.hpp"

class CapturerFactory
{
  public:
    static const std::list<std::string> get_audio_capture_device_list(
        rtc::scoped_refptr<webrtc::AudioDeviceModule> audio_device_module);
    static rtc::scoped_refptr<webrtc::AudioSourceInterface> create_audio_source(
        const std::string &audio_url,
        rtc::scoped_refptr<webrtc::PeerConnectionFactoryInterface> peer_connection_factory,
        rtc::scoped_refptr<webrtc::AudioDeviceModule> audio_device_module);
};
#endif // DEF_CAPTURER_FACTORY
