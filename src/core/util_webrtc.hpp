/************************************************************************
 NestoRPi Engine
 Copyright (C) 2015-2025 Franck Talbart

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

/* Author: Franck Talbart */

#ifndef DEF_UTIL_WEBRTC
#define DEF_UTIL_WEBRTC

#include <string>

#include "src/core/PeerConnectionManager.hpp"
namespace util_webrtc
{
struct IceServer
{
    std::string url;
    std::string user;
    std::string pass;
};

const std::string get_server_ip_from_client_ip(int client_ip);

IceServer get_ice_server_from_url(const std::string& url, const std::string& client_ip = "");

webrtc::PeerConnectionFactoryDependencies create_peer_connection_factory_dependencies(
    rtc::Thread* signaling_thread,
    rtc::Thread* worker_thread,
    rtc::scoped_refptr<webrtc::AudioDeviceModule> audio_device_module);

} // namespace util_webrtc

#endif // DEF_UTIL_WEBRTC
