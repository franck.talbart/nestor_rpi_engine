/************************************************************************
 NestoRPi Engine
 Copyright (C) 2015-2025 Franck Talbart

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
/* Author: Franck Talbart */

#ifndef DEBUG_MODE
#include "src/core/GPIOS.hpp"

#include <fcntl.h>
#include <gpiod.h>
#include <linux/spi/spidev.h>
#include <linux/types.h>
#include <sys/ioctl.h>
#include <unistd.h>

extern "C"
{
#include <i2c/smbus.h>
#include <linux/i2c-dev.h>
}

#include <stdexcept>

#include "src/core/Log.hpp"

// Static variables
const std::string GPIOS::I2C_DEVICE = "/dev/i2c-1";
const std::string GPIOS::SPI_DEVICE = "/dev/spidev0.0";

//------------------------------------------------------------------------------
bool GPIOS::i2c_open(std::shared_ptr<i2c> i2c_dev,
                     const pin_t base,
                     const int addr,
                     const short number_pins,
                     const gpio_type type)
{
    int fd = open(GPIOS::I2C_DEVICE.c_str(), O_RDWR);
    if (fd < 0)
    {
        NRPI_LOG(ERROR) << "Can't open the I2C bus";
        return false;
    }

    i2c_dev->fd = fd;
    i2c_dev->addr = addr;
    i2c_dev->base_pin = base;
    i2c_dev->number_pins = number_pins;
    i2c_dev->type = type;

    if (ioctl(fd, I2C_SLAVE, addr) < 0)
    {
        NRPI_LOG(ERROR) << "Can't setup the I2C adress";
        return false;
    }

    return true;
}

//------------------------------------------------------------------------------
bool GPIOS::spi_open(spi &s, const uint32_t speed)
{
    s.dev = -1;
    s.mode = SPI_MODE_0;
    s.bits = 8;
    s.speed = speed;

    int ret = open(GPIOS::SPI_DEVICE.c_str(), O_RDWR);
    if (ret < 0)
    {
        NRPI_LOG(ERROR) << "Can't open the device " << GPIOS::SPI_DEVICE.c_str();
        return false;
    }

    s.dev = ret;
    ret = ioctl(s.dev, SPI_IOC_WR_MODE, &s.mode);
    if (ret < 0)
    {
        NRPI_LOG(ERROR) << "Can't set spi mode 'SPI_IOC_WR_MODE'";
        return false;
    }

    ret = ioctl(s.dev, SPI_IOC_RD_MODE, &s.mode);
    if (ret < 0)
    {
        NRPI_LOG(ERROR) << "Can't set spi mode 'SPI_IOC_RD_MODE'";
        return false;
    }

    ret = ioctl(s.dev, SPI_IOC_WR_BITS_PER_WORD, &s.bits);
    if (ret < 0)
    {
        NRPI_LOG(ERROR) << "Can't set bits per word 'SPI_IOC_WR_BITS_PER_WORD'";
        return false;
    }

    ret = ioctl(s.dev, SPI_IOC_RD_BITS_PER_WORD, &s.bits);
    if (ret < 0)
    {
        NRPI_LOG(ERROR) << "Can't get bits per word 'SPI_IOC_RD_BITS_PER_WORD'";
        return false;
    }

    ret = ioctl(s.dev, SPI_IOC_WR_MAX_SPEED_HZ, &s.speed);
    if (ret < 0)
    {
        NRPI_LOG(ERROR) << "Can't set max speed hz 'SPI_IOC_WR_MAX_SPEED_HZ'";
        return false;
    }

    ret = ioctl(s.dev, SPI_IOC_RD_MAX_SPEED_HZ, &s.speed);
    if (ret < 0)
    {
        NRPI_LOG(ERROR) << "Can't get max speed hz 'SPI_IOC_RD_MAX_SPEED_HZ'";
        return false;
    }

    return true;
}

//------------------------------------------------------------------------------
static unsigned char read_register(const int i2c_fd, unsigned char reg)
{
    write(i2c_fd, &reg, 1);
    read(i2c_fd, &reg, 1);
    return reg;
}

//------------------------------------------------------------------------------
GPIOS::GPIOS(void) : gpiochip(nullptr), running(true)
{
    for (int i = 0; i < GPIOS::MAX_NUMBER_GPIOS; ++i)
    {
        gpios[i].line = nullptr;
    }
    spi_dev.dev = -1;
}

//------------------------------------------------------------------------------
bool GPIOS::init(void)
{
    // RPI5
    gpiochip = gpiod_chip_open_by_name("gpiochip4");
    if (gpiochip == nullptr)
        gpiochip = gpiod_chip_open_by_name("gpiochip0");

    return gpiochip != nullptr;
}

//------------------------------------------------------------------------------
GPIOS::~GPIOS(void)
{
    running = false;

    for (int i = 0; i < GPIOS::MAX_NUMBER_GPIOS; ++i)
    {
        if (gpios[i].soft_pwm_thread != nullptr && gpios[i].soft_pwm_thread->period > 0 &&
            gpios[i].soft_pwm_thread->pwm_thread.joinable())
        {
            gpios[i].soft_pwm_thread->pwm_thread.join();
        }

        if (gpios[i].type == LIBGPIOD)
        {
            if (gpios[i].line != nullptr)
                gpiod_line_release(gpios[i].line);
        }
    }

    if (gpiochip)
        gpiod_chip_close(gpiochip);

    if (spi_dev.dev > -1)
        close(spi_dev.dev);

    for (auto i2c_dev : i2c_devices) close(i2c_dev->fd);
}

//------------------------------------------------------------------------------
void GPIOS::digital_write(const pin_t pin, const bool value)
{
    assert(pin < GPIOS::MAX_NUMBER_GPIOS);
    if (gpios[pin].type == LIBGPIOD)
    {
        struct gpiod_line *line = gpios[pin].line;
        assert(line != nullptr);

        if (gpiod_line_set_value(line, static_cast<int>(value)) < 0)
        {
            NRPI_LOG(ERROR) << "Can't set GPIO " << pin << " to " << value;
        }
    }
    else if (gpios[pin].type == MCP23017)
    {
        unsigned char data[2];
        pin_t local_pin = pin - gpios[pin].i2c_dev->base_pin;
        unsigned char *gpios_dest = nullptr;

        if (local_pin < 8)
        {
            data[0] = GPIOS::MCP23017_GPIO_REG_A;
            gpios_dest = &(gpios[pin].i2c_dev->gpio_A);
        }
        else
        {
            data[0] = GPIOS::MCP23017_GPIO_REG_B;
            gpios_dest = &(gpios[pin].i2c_dev->gpio_B);
            local_pin -= 8;
        }

        if (value == false)
        { // LOW
            *gpios_dest &= ~(1 << (local_pin));
        }
        else
        { // HIGH
            *gpios_dest |= (1 << (local_pin));
        }

        data[1] = *gpios_dest;
        if (!i2c_write(gpios[pin].i2c_dev, data, 2))
        {
            NRPI_LOG(ERROR) << "Failure I2C write on pin " << pin;
        }
    }
}

//------------------------------------------------------------------------------
bool GPIOS::digital_read(const pin_t pin)
{
    assert(pin < GPIOS::MAX_NUMBER_GPIOS);
    if (gpios[pin].type == LIBGPIOD)
    {
        struct gpiod_line *line = gpios[pin].line;
        assert(line != nullptr);

        short value;
        value = gpiod_line_get_value(line);
        if (value < 0)
        {
            NRPI_LOG(ERROR) << "Can't get GPIO value of " << pin;
            return false;
        }
        return value;
    }
    else if (gpios[pin].type == MCP23017)
    {
        pin_t local_pin = pin - gpios[pin].i2c_dev->base_pin;
        unsigned char gpios_state;

        if (local_pin < 8)
        {
            gpios_state = GPIOS::MCP23017_GPIO_REG_A;
        }
        else
        {
            gpios_state = GPIOS::MCP23017_GPIO_REG_B;
            local_pin -= 8;
        }
        {
            std::lock_guard<std::mutex> lock(i2c_mutex);
            gpios_state = read_register(gpios[pin].i2c_dev->fd, gpios_state);
            return gpios_state & (1 << local_pin);
        }
    }
    return false;
}

//------------------------------------------------------------------------------
void GPIOS::pin_mode(const pin_t pin, enum mode mode)
{
    assert(pin < GPIOS::MAX_NUMBER_GPIOS);
    bool found = false;
    for (auto i2c_dev : i2c_devices)
    {
        if (i2c_dev->base_pin >= 0 && pin >= i2c_dev->base_pin &&
            pin < i2c_dev->base_pin + i2c_dev->number_pins)
        // cppcheck-suppress useStlAlgorithm
        {
            gpios[pin].type = i2c_dev->type;
            gpios[pin].i2c_dev = i2c_dev;
            found = true;
            break;
        }
    }

    gpios[pin].soft_pwm_thread = nullptr;

    if (!found)
    {
        gpios[pin].type = LIBGPIOD;
    }

    if (gpios[pin].type == LIBGPIOD)
    {
        assert(gpios[pin].line == nullptr);

        struct gpiod_line *line = gpiod_chip_get_line(gpiochip, pin);
        if (!line)
        {
            NRPI_LOG(ERROR) << "Can't get gpio line for " << pin;
            return;
        }
        gpios[pin].line = line;

        if (mode == input)
        {
            if (gpiod_line_request_input(line, "gpio_mode") < 0)
            {
                NRPI_LOG(ERROR) << "Can't set the GPIO " << pin << " to input mode";
            }
        }
        else if (mode == output || mode == pwm_output)
        {
            if (gpiod_line_request_output(line, "gpio_mode", 0) < 0)
            {
                NRPI_LOG(ERROR) << "Can't set the GPIO " << pin << " to output mode";
            }
        }
    }
    else if (gpios[pin].type == MCP23017)
    {
        unsigned char data[2];
        pin_t local_pin = pin - gpios[pin].i2c_dev->base_pin;
        unsigned char *iodir_dest = nullptr;

        if (local_pin < 8)
        {
            data[0] = GPIOS::MCP23017_IODIR_REG_A;
            iodir_dest = &(gpios[pin].i2c_dev->iodir_A);
        }
        else
        {
            data[0] = GPIOS::MCP23017_IODIR_REG_B;
            iodir_dest = &(gpios[pin].i2c_dev->iodir_B);
            local_pin -= 8;
        }
        if (mode == output)
        { // OUTPUT
            *iodir_dest &= ~(1 << (local_pin));
        }
        else
        { // INPUT
            *iodir_dest |= (1 << (local_pin));
        }

        data[1] = *iodir_dest;
        if (!i2c_write(gpios[pin].i2c_dev, data, 2))
        {
            NRPI_LOG(ERROR) << "Failure I2C write on pin " << pin;
        }
    }
}

//------------------------------------------------------------------------------
void GPIOS::soft_pwm_thread(const pin_t pin)
{
    assert(pin < GPIOS::MAX_NUMBER_GPIOS);

    while (running)
    {
        int time_high =
            gpios[pin].soft_pwm_thread->period * gpios[pin].soft_pwm_thread->duty_cycle / 100;
        int time_low = gpios[pin].soft_pwm_thread->period - time_high;

        digital_write(pin, 1);
        std::this_thread::sleep_for(std::chrono::milliseconds(time_high));
        digital_write(pin, 0);
        std::this_thread::sleep_for(std::chrono::milliseconds(time_low));
    }
}

//------------------------------------------------------------------------------
void GPIOS::soft_pwm_create(const pin_t pin, const short initial_value)
{
    assert(pin < GPIOS::MAX_NUMBER_GPIOS);
    gpios[pin].soft_pwm_thread = std::make_shared<soft_pwm>();
    gpios[pin].soft_pwm_thread->duty_cycle = initial_value;
    gpios[pin].soft_pwm_thread->period = GPIOS::SOFT_PWM_PERIOD;
    gpios[pin].soft_pwm_thread->pwm_thread = std::thread(&GPIOS::soft_pwm_thread, this, pin);
}

//------------------------------------------------------------------------------
void GPIOS::soft_pwm_write(const pin_t pin, const short value)
{
    assert(pin < GPIOS::MAX_NUMBER_GPIOS);
    gpios[pin].soft_pwm_thread->duty_cycle = value;
}

//------------------------------------------------------------------------------
bool GPIOS::mcp_23017_setup(const pin_t base, const int addr)
{
    std::lock_guard<std::mutex> lock(i2c_mutex);
    std::shared_ptr<i2c> i2c_dev = std::make_shared<i2c>();
    bool res = i2c_open(i2c_dev, base, addr, MCP23017_SIZE, MCP23017);
    if (!res)
        return false;

    i2c_dev->gpio_A = read_register(i2c_dev->fd, GPIOS::MCP23017_GPIO_REG_A);
    i2c_dev->gpio_B = read_register(i2c_dev->fd, GPIOS::MCP23017_GPIO_REG_B);
    i2c_dev->iodir_A = read_register(i2c_dev->fd, GPIOS::MCP23017_IODIR_REG_A);
    i2c_dev->iodir_B = read_register(i2c_dev->fd, GPIOS::MCP23017_IODIR_REG_B);
    i2c_devices.push_back(i2c_dev);

    return true;
}

//------------------------------------------------------------------------------
bool GPIOS::pca_9685_setup(const pin_t base, const int addr, const int frequency)
{
    std::lock_guard<std::mutex> lock(i2c_mutex);
    std::shared_ptr<i2c> i2c_dev = std::make_shared<i2c>();
    bool res = i2c_open(i2c_dev, base, addr, PCA9685_SIZE, PCA9685);
    if (!res)
        return false;

    int prescale_value = (int)((25e6 / (4096.0 * frequency)) + 0.5) - 1;

    // sleep mode
    if (i2c_smbus_write_byte_data(i2c_dev->fd, PCA9685_MODE1, 0x10) < 0)
    {
        NRPI_LOG(ERROR) << "Can't set up the PCA9685";
        return false;
    }

    // prescale
    if (i2c_smbus_write_byte_data(i2c_dev->fd, PCA9685_PRESCALE, prescale_value) < 0)
    {
        NRPI_LOG(ERROR) << "Can't set up the frequency to the PCA9685";
        return false;
    }

    // wake mode
    if (i2c_smbus_write_byte_data(i2c_dev->fd, PCA9685_MODE1, 0x00) < 0)
    {
        NRPI_LOG(ERROR) << "Can't set up the PCA9685";
        return false;
    }

    usleep(5000); // Wait for the PCA9685 to be back to business
    i2c_devices.push_back(i2c_dev);

    return true;
}

//------------------------------------------------------------------------------
GPIOS::spi GPIOS::spi_setup(const uint32_t speed)
{
    if (!spi_open(spi_dev, speed))
    {
        throw std::runtime_error("can't open spi");
    }
    return spi_dev;
}

//------------------------------------------------------------------------------
std::shared_ptr<GPIOS::i2c> GPIOS::i2c_setup(const int addr)
{
    std::lock_guard<std::mutex> lock(i2c_mutex);
    std::shared_ptr<i2c> i2c_dev = std::make_shared<i2c>();
    bool res = i2c_open(i2c_dev, -1, addr, 0, EXTERNAL);
    if (!res)
        throw std::runtime_error("can't open i2c");
    i2c_devices.push_back(i2c_dev);
    return i2c_dev;
}

//------------------------------------------------------------------------------
bool GPIOS::i2c_write(std::shared_ptr<i2c> i2c_dev, const void *data, const int size)
{
    std::lock_guard<std::mutex> lock(i2c_mutex);
    if (write(i2c_dev->fd, data, size) == size)
        return true;
    return false;
}

//------------------------------------------------------------------------------
void GPIOS::pwm_set_range(const pin_t pin, const int min_pulse, const int max_pulse)
{
    assert(pin < GPIOS::MAX_NUMBER_GPIOS);
    assert(min_pulse < max_pulse);
    gpios[pin].pwm_min_pulse = min_pulse;
    gpios[pin].pwm_max_pulse = max_pulse;
}

//------------------------------------------------------------------------------
void GPIOS::pwm_write(const pin_t pin, const float ratio)
{
    assert(pin < GPIOS::MAX_NUMBER_GPIOS);
    std::shared_ptr<i2c> i2c_dev = gpios[pin].i2c_dev;
    std::lock_guard<std::mutex> lock(i2c_mutex);
    int pulse = ratio / 100.0 * (gpios[pin].pwm_max_pulse - gpios[pin].pwm_min_pulse) +
                gpios[pin].pwm_min_pulse;
    int on = 0;
    int off = pulse;
    short channel = pin - i2c_dev->base_pin;
    assert(channel >= 0);

    i2c_smbus_write_byte_data(i2c_dev->fd, PCA9685_PULSE_ON + 4 * channel, on & 0xFF);
    i2c_smbus_write_byte_data(i2c_dev->fd, PCA9685_PULSE_ON + 4 * channel + 1, on >> 8);
    i2c_smbus_write_byte_data(i2c_dev->fd, PCA9685_PULSE_ON + 4 * channel + 2, off & 0xFF);
    i2c_smbus_write_byte_data(i2c_dev->fd, PCA9685_PULSE_ON + 4 * channel + 3, off >> 8);
}

//------------------------------------------------------------------------------
void GPIOS::pwm_turn_off(const pin_t pin)
{
    assert(pin < GPIOS::MAX_NUMBER_GPIOS);
    std::shared_ptr<i2c> i2c_dev = gpios[pin].i2c_dev;
    short channel = pin - i2c_dev->base_pin;
    i2c_smbus_write_byte_data(i2c_dev->fd, PCA9685_PULSE_ON + (4 * channel) + 2, 0x00);
    i2c_smbus_write_byte_data(i2c_dev->fd, PCA9685_PULSE_ON + (4 * channel) + 3, 0x00);
}

#endif // DEBUG_MODE
