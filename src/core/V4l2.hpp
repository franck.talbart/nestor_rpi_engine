/************************************************************************
 NestoRPi Engine
 Copyright (C) 2015-2025 Franck Talbart

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

/* Author: Franck Talbart */

#ifndef DEF_V4L2
#define DEF_V4L2

#include "gason.hpp"
#include "src/core/Camera.hpp"

class V4l2 : public Camera
{
  public:
    static const std::list<std::string> get_video_capture_device_list(void);
    explicit V4l2(const std::string& capture_dev,
                  size_t width,
                  size_t height,
                  short fps,
                  short rotate,
                  std::map<std::string, gason::JsonValue> opts);
    ~V4l2(void);

    std::string get_name(void) override;
    std::string capture_pic(void) override;
    rtc::scoped_refptr<webrtc::VideoTrackSourceInterface> get_video_source(void) override;
    void start(void) override;
    void stop(void) override;

  private:
    int fd_v4l2;
    rtc::scoped_refptr<webrtc::VideoTrackSourceInterface> video_track_source;
};
#endif // DEF_V4L2
