/************************************************************************
 NestoRPi Engine
 Copyright (C) 2015-2025 Franck Talbart

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

/* Author: Franck Talbart */

#ifndef DEF_LOG
#define DEF_LOG

#include <ctime>
#include <fstream>
#include <iostream>
#include <mutex>

#include "src/core/Uncopyable.hpp"
#include "src/core/const.hpp"
#include "src/core/util.hpp"

/**
 * Log manager
 * Usage: NRPI_LOG(DEBUG) << "hello" << 12
 */

enum type_log
{
    DEBUG,
    INFO,
    WARN,
    ERROR
};

#define __FILENAME__ (strrchr(__FILE__, '/') ? strrchr(__FILE__, '/') + 1 : __FILE__)
#define NRPI_LOG(...) _LOG(__FILENAME__, __FUNCTION__, __VA_ARGS__)

//------------------------------------------------------------------------------
class _LOG : public Uncopyable
{
  public:
    static void open_log_file()
    {
        std::string log_filename =
            util::get_full_dir_from_cfg(CONFIG_LOG_PATH) + std::string(FILENAME_LOG_DAEMON);
        _LOG::log_file.open(log_filename, std::ios_base::app);
    }

    static void close_log_file() { _LOG::log_file.close(); }

    //----------------------------------------------------------------------
    /**
     * @brief Constructor. Usage: _NRPI_LOG(DEBUG) << "hello"
     *
     * @param file: the filename where the log has been called
     * @param func: the function where the log has been called
     * @param type: DEBUG / INFO / WARN / ERROR
     * @param eol: if true, add a end of line
     * @param headers: if true, display time + type
     *
     */
    _LOG(const std::string &file,
         const std::string &func,
         type_log type,
         bool eol = true,
         bool headers = true)
        : msg_level(type), eol(eol)
    {
        // Remove the extension
        std::string file_local = file;
        file_local.erase(file_local.find_last_of("."), std::string::npos);
        if (headers)
        {
            operator<<(get_clock()) << (" [" + get_label(type) + "] [" + file_local);
            if (type == DEBUG)
            {
                operator<<(": " + func + "]\t");
            }
            else
            {
                operator<<("]\t");
            }
        }
    }

    //----------------------------------------------------------------------
    /**
     * @brief Destructor, display EOL if asked
     *
     *
     */
    ~_LOG()
    {
        if (msg_level >= _LOG::level)
        {
            if (eol)
            {
                std::cout << std::endl;
                log_file << std::endl;
            }
            else
            {
                // We still want to flush even without EOL
                // Example:  [INFO] [main]	    * startup ... [OK] We want to flush before "OK"
                std::cout << std::flush;
            }
        }
    }

    //----------------------------------------------------------------------
    /**
     * @brief Operator overloading
     *
     * @param msg: the message
     * @return itself
     */
    template <class T>
    _LOG &operator<<(const T &msg)
    {
        if (msg_level >= _LOG::level)
        {
            std::cout << msg;
            log_file << msg;
        }
        return *this;
    }

    //----------------------------------------------------------------------
    /**
     * @brief Operator overloading, endl support
     *
     * @param *os: the std::endl function
     * @return itself
     */
    _LOG &operator<<(std::ostream &(*os)(std::ostream &))
    {
        if (msg_level >= _LOG::level)
        {
            if (msg_level == ERROR)
                std::cerr << os;
            else
                std::cout << os;

            log_file << os;
        }
        return *this;
    }

    // default log level
    static type_log level;

  private:
    type_log msg_level;
    bool eol;
    static std::ofstream log_file;

    //----------------------------------------------------------------------
    /**
     * @brief Return the label in a string format
     *
     * @param type: the log type
     * @return the string label
     */
    inline std::string get_label(type_log type) const
    {
        std::string label;
        switch (type)
        {
            case DEBUG:
                label = COLOR(COLOR_CYAN, "DEBUG");
                break;
            case INFO:
                label = COLOR(COLOR_GREEN, "INFO");
                break;
            case WARN:
                label = COLOR(COLOR_YELLOW, "WARN");
                break;
            case ERROR:
                label = COLOR(COLOR_RED, "ERROR");
                break;
        }
        return label;
    }

    //----------------------------------------------------------------------
    /**
     * @brief Return the current date
     *
     * @return the date
     */
    inline std::string get_clock(void) const
    {
        time_t rawtime;
        struct tm *timeinfo;
        char buffer[80];

        time(&rawtime);
        timeinfo = localtime(&rawtime);

        strftime(buffer, 80, "%d-%m-%Y %I:%M:%S", timeinfo);
        return std::string(buffer);
    }
};
#endif // DEF_LOG
