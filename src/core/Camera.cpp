/************************************************************************
 NestoRPi Engine
 Copyright (C) 2015-2025 Franck Talbart

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

/* Author: Franck Talbart */

/* This module provides facilities to manage the camera (v4l2 or libcamera) */
#include "src/core/Camera.hpp"

#include "gason.hpp"
#include "src/core/Libcamera.hpp"
#include "src/core/Log.hpp"
#include "src/core/V4l2.hpp"
#include "src/core/util.hpp"

// Static variables
short Camera::nb_devices = 0;

//------------------------------------------------------------------------------
/**
 * @brief Singleton: return the instance

 * @param id: device id (starting at 0) to select a device
 */
std::shared_ptr<Camera> Camera::get_instance(const short id)
{
    static std::map<short, std::shared_ptr<Camera>> instances; // Guaranteed to be destroyed
                                                               // Instantiated on first use
    if (instances.find(id) == instances.end())
    {
        std::map<std::string, gason::JsonValue> opts;
        std::string capture_dev = "";
        short cpt = 0;
        bool libcamera = false;
        size_t width = 1024, height = 768;
        short fps = 30, rotate = 0;

        for (const auto& dev : util::get_cfg_value(CONFIG_CAMERA))
        {
            if (cpt++ == id)
            {
                for (const auto& params_dev : dev->value)
                {
                    if (params_dev->key == std::string(CONFIG_CAMERA_NAME))
                    {
                        capture_dev = params_dev->value.toString();
                    }
                    else if (params_dev->key == std::string(CONFIG_CAMERA_LIBCAMERA))
                    {
                        libcamera = params_dev->value.getTag() == gason::JSON_TRUE;
                    }
                    else if (params_dev->key == std::string(CONFIG_CAMERA_WIDTH))
                    {
                        width = params_dev->value.toNumber();
                    }
                    else if (params_dev->key == std::string(CONFIG_CAMERA_HEIGHT))
                    {
                        height = params_dev->value.toNumber();
                    }
                    else if (params_dev->key == std::string(CONFIG_CAMERA_FPS))
                    {
                        fps = params_dev->value.toNumber();
                    }
                    else if (params_dev->key == std::string(CONFIG_CAMERA_ROTATE))
                    {
                        rotate = params_dev->value.toNumber();
                    }
                    else
                    {
                        opts[params_dev->key] = params_dev->value;
                    }
                }
            }
        }

        Camera::nb_devices = cpt;

        if (capture_dev == "")
            throw std::string("Cannot find the video device " + std::to_string(id));

        std::shared_ptr<Camera> camera_instance = nullptr;

        if (libcamera)
        {
            camera_instance = std::shared_ptr<Camera>(
                new Libcamera(capture_dev, width, height, fps, rotate, opts));
        }
        else
        {
            camera_instance =
                std::shared_ptr<Camera>(new V4l2(capture_dev, width, height, fps, rotate, opts));
        }
        instances[id] = camera_instance;
    }
    return instances[id];
}

//------------------------------------------------------------------------------
/**
 * @brief Get the number of available devices
 */
short Camera::get_nb_devices(void) { return Camera::nb_devices; }
