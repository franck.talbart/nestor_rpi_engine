/************************************************************************
 NestoRPi Engine
 Copyright (C) 2015-2025 Franck Talbart

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

/* Author: Franck Talbart */

#include "src/core/Libcamera.hpp"

#include "src/core/LibcameraCapturer.hpp"
#include "src/core/Log.hpp"
#include "src/core/const.hpp"
#include "src/core/util.hpp"

//------------------------------------------------------------------------------
/**
 * @brief Generates a list of Libcamera video capture devices
 *
 * @return list of video capture devices
 */
const std::list<std::string> Libcamera::get_video_capture_device_list(void)
{
    static std::list<std::string> video_device_list;
    if (video_device_list.empty())
    {
#ifndef DEBUG_MODE
        // Disable any libcamera logging for this bit.
        libcamera::logSetTarget(libcamera::LoggingTargetNone);

        std::unique_ptr<libcamera::CameraManager> cm = std::make_unique<libcamera::CameraManager>();
        int ret = cm->start();
        if (ret)
            throw std::runtime_error("camera manager failed to start, code " +
                                     std::to_string(-ret));

        std::vector<std::shared_ptr<libcamera::Camera>> cameras = cm->cameras();
        // Do not show USB webcams as these are not supported in rpicam-apps!
        auto rem =
            std::remove_if(cameras.begin(),
                           cameras.end(),
                           [](auto& cam) { return cam->id().find("/usb") != std::string::npos; });
        cameras.erase(rem, cameras.end());

        if (cameras.size() != 0)
        {
            unsigned int idx = 0;
            for (auto const& cam : cameras)
            {
                cam->acquire();
                video_device_list.push_back(std::to_string(idx++) + " : " +
                                            *cam->properties().get(libcamera::properties::Model) +
                                            " (" + cam->id() + ")");
                cam->release();
            }
        }
        cameras.clear();
        cm->stop();
#endif // DEBUG_MODE
    }
    return video_device_list;
}

//------------------------------------------------------------------------------
/**
 * @brief Constructor
 */
Libcamera::Libcamera(const std::string& capture_dev,
                     size_t width,
                     size_t height,
                     short fps,
                     short rotate,
                     const std::map<std::string, gason::JsonValue>& opts_libcamera)
    : Camera(), nb_clients(0)
{
#ifndef DEBUG_MODE
    std::map<std::string, std::string> opts;

    opts["width"] = std::to_string(width);
    opts["height"] = std::to_string(height);
    opts["framerate"] = std::to_string(fps);
    opts["rotation"] = std::to_string(rotate);

    for (const auto& opt : opts_libcamera) opts[opt.first.c_str()] = opt.second.toString();
    video_track_source = LibcameraCapturer::Create(std::stoi(capture_dev), opts);
#else  // DEBUG_MODE
    UNUSED(capture_dev);
    UNUSED(width);
    UNUSED(height);
    UNUSED(fps);
    UNUSED(rotate);
    UNUSED(opts_libcamera);
#endif // DEBUG_MODE
}

//------------------------------------------------------------------------------
std::string Libcamera::get_name(void) { return "unicam"; }

//------------------------------------------------------------------------------
std::string Libcamera::capture_pic(void)
{
#ifndef DEBUG_MODE
    return video_track_source->ask_for_picture();
#else
    return "";
#endif // DEBUG_MODE
}

//------------------------------------------------------------------------------
rtc::scoped_refptr<webrtc::VideoTrackSourceInterface> Libcamera::get_video_source(void)
{
#ifndef DEBUG_MODE
    return video_track_source;
#else
    return nullptr;
#endif // DEBUG_MODE
}

//------------------------------------------------------------------------------
void Libcamera::start(void)
{
    std::lock_guard<std::mutex> lock(mutex_start_stop);
#ifndef DEBUG_MODE
    video_track_source->StartCapture();
#endif // DEBUG_MODE
    nb_clients++;
}

//------------------------------------------------------------------------------
void Libcamera::stop(void)
{
    std::lock_guard<std::mutex> lock(mutex_start_stop);
    if (--nb_clients == 0)
    {
#ifndef DEBUG_MODE
        video_track_source->StopCapture();
#endif // DEBUG_MODE
    }
}
