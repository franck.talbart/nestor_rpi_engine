/************************************************************************
 NestoRPi Engine
 Copyright (C) 2015-2025 Franck Talbart

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

/* Author: Franck Talbart */
// Contains the non standard functions

#include "src/core/platform.hpp"

#include <string.h>

#include "src/core/util.hpp"
//------------------------------------------------------------------------------
/**
 * @brief Home-made strndup.
 *
 * @param str the string to duplicate
 * @param len the string length
 * @return char *value the string, nullptr if it fails
 */
char *xstrndup(const char *str, const int len)
{
    if (str == nullptr)
        return nullptr;
    char *result = static_cast<char *>(util::xmalloc(len + 1));
    result = strncpy(result, str, len);
    result[len] = '\0';
    return result;
}
