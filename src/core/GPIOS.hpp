/************************************************************************
 NestoRPi Engine
 Copyright (C) 2015-2025 Franck Talbart

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

/* Author: Franck Talbart */

#ifndef DEF_GPIOS
#define DEF_GPIOS

#include <memory>
typedef short pin_t;

#include <array>
#include <atomic>
#include <mutex>
#include <thread>
#include <vector>

enum gpio_type
{
    LIBGPIOD,
    MCP23017,
    PCA9685,
    EXTERNAL
};

class GPIOS
{
  public:
    enum mode
    {
        input,
        output,
        pwm_output
    };

    struct i2c
    {
        int fd;
        int addr;
        pin_t base_pin;
        short number_pins;
        gpio_type type;
        unsigned char gpio_A;
        unsigned char gpio_B;
        unsigned char iodir_A;
        unsigned char iodir_B;
    };

    struct spi
    {
        int dev;
        uint8_t mode;
        uint8_t bits;
        uint32_t speed;
    };

#ifndef DEBUG_MODE
    GPIOS(void);
    ~GPIOS(void);
    bool init(void);
    void digital_write(const short pin, const bool value);
    bool digital_read(const short pin);
    void pin_mode(short pin, enum mode mode);
    void soft_pwm_thread(const pin_t pin);
    void soft_pwm_create(const pin_t pin, const short initial_value);
    void soft_pwm_write(const pin_t pin, const short value);
    bool mcp_23017_setup(const pin_t base, const int addr);
    bool pca_9685_setup(const pin_t base, const int addr, const int frequency);
    spi spi_setup(const uint32_t speed);
    std::shared_ptr<i2c> i2c_setup(const int addr);
    bool i2c_write(std::shared_ptr<i2c> i2c_dev, const void *data, const int size);
    void pwm_set_range(const pin_t pin, const int min_pulse, const int max_pulse);
    void pwm_write(const pin_t pin, const float ratio);
    void pwm_turn_off(const pin_t pin);

    static GPIOS NRPI_GPIOS;

  private:
    struct gpiod_chip *gpiochip;
    volatile bool running;

    struct soft_pwm
    {
        std::atomic<short> duty_cycle;
        short period;
        std::thread pwm_thread;
    };

    struct gpio
    {
        gpio_type type;
        struct gpiod_line *line; // libgpiod
        std::shared_ptr<soft_pwm> soft_pwm_thread;
        std::shared_ptr<i2c> i2c_dev;
        int pwm_min_pulse;
        int pwm_max_pulse;
    };

    bool i2c_open(std::shared_ptr<i2c> i2c_dev,
                  const pin_t base,
                  const int addr,
                  const short number_pins,
                  const gpio_type type);
    bool spi_open(spi &s, const uint32_t speed);

    std::vector<std::shared_ptr<i2c>> i2c_devices;
    std::mutex i2c_mutex;
    spi spi_dev;

    // General
    static const short SOFT_PWM_PERIOD = 20; // 20ms = 50Hz, standard frequency
    static const short MAX_NUMBER_GPIOS = 400;
    static const std::string I2C_DEVICE;
    static const std::string SPI_DEVICE;

    // MCP23017
    static const int MCP23017_IODIR_REG_A = 0x00;
    static const int MCP23017_IODIR_REG_B = 0x01;
    static const int MCP23017_GPIO_REG_A = 0x12;
    static const int MCP23017_GPIO_REG_B = 0x13;
    static const short MCP23017_SIZE = 16;

    // PCA9685
    static const int PCA9685_MODE1 = 0x00;
    static const int PCA9685_PRESCALE = 0xFE;
    static const int PCA9685_PULSE_ON = 0x06;
    static const short PCA9685_SIZE = 16;

    std::array<struct gpio, GPIOS::MAX_NUMBER_GPIOS> gpios;
#endif // DEBUG_MODE
};
#endif // DEF_GPIOS
