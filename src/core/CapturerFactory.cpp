/************************************************************************
 NestoRPi Engine
 Copyright (C) 2015-2025 Franck Talbart

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

/* Author: Franck Talbart
   webrtc-streamer written by Michel Promonet
   and updated for the NestoRPi Engine */

#include "src/core/CapturerFactory.hpp"

#include <regex>

#ifndef DEBUG_MODE
#include "src/core/LibcameraCapturer.hpp"
#endif // DEBUG_MODE
#include "src/core/Log.hpp"
#include "src/core/TrackSource.hpp"
#include "src/core/VcmCapturer.hpp"

//------------------------------------------------------------------------------
/**
 * @brief Generates a list of audio capture devices
 *
 * @return list of video capture devices
 */
const std::list<std::string> CapturerFactory::get_audio_capture_device_list(
    rtc::scoped_refptr<webrtc::AudioDeviceModule> audio_device_module)
{
    static std::list<std::string> audio_device_list;
    if (audio_device_list.empty())
    {
        int16_t num_audio_devices = audio_device_module->RecordingDevices();
        NRPI_LOG(DEBUG) << "Nb audio devices: " << num_audio_devices;

        for (int i = 0; i < num_audio_devices; ++i)
        {
            char name[webrtc::kAdmMaxDeviceNameSize] = {0};
            char id[webrtc::kAdmMaxGuidSize] = {0};
            if (audio_device_module->RecordingDeviceName(i, name, id) != -1)
            {
                NRPI_LOG(DEBUG) << "audio device name: " << name << " id:" << id;
                audio_device_list.push_back(name);
            }
            else
            {
                // Important to record to keep the proper order id
                audio_device_list.push_back("Not supported");
            }
        }
    }
    return audio_device_list;
}

//------------------------------------------------------------------------------
/**
 * @brief Creates an audio source object
 *
 * @param audio_url: the audio URL
 * @param peer_connection_factory
 * @param audio_device_module
 * @return audio source
 */
rtc::scoped_refptr<webrtc::AudioSourceInterface> CapturerFactory::create_audio_source(
    const std::string &audio_url,
    rtc::scoped_refptr<webrtc::PeerConnectionFactoryInterface> peer_connection_factory,
    rtc::scoped_refptr<webrtc::AudioDeviceModule> audio_device_module)
{
    rtc::scoped_refptr<webrtc::AudioSourceInterface> audio_source;
    audio_device_module->Init();
    std::list<std::string> audio_device_list =
        CapturerFactory::get_audio_capture_device_list(audio_device_module);

    int i = 0, id_audio_device = -1;
    for (const auto &device : audio_device_list)
    {
        if (audio_url == device)
        {
            id_audio_device = i;
            break;
        }
        ++i;
    }
    if ((id_audio_device >= 0) && (id_audio_device < (signed int)audio_device_list.size()))
    {
        audio_device_module->SetRecordingDevice(id_audio_device);
        cricket::AudioOptions opt;
        audio_source = peer_connection_factory->CreateAudioSource(opt);
    }
    return audio_source;
}
