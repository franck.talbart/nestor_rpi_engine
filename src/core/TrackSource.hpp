/************************************************************************
 NestoRPi Engine
 Copyright (C) 2015-2025 Franck Talbart

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

/* Author: Franck Talbart
   webrtc-streamer written by Michel Promonet
   and updated for the NestoRPi Engine */

#ifndef DEF_TRACK_SOURCE
#define DEF_TRACK_SOURCE

#include <regex>

#include "api/peer_connection_interface.h"
#include "pc/video_track_source.h"

template <class T>
class TrackSource : public webrtc::VideoTrackSource
{
  public:
    static rtc::scoped_refptr<TrackSource> Create(const std::string &video_url,
                                                  const std::map<std::string, int> &opts)
    {
        std::unique_ptr<T> capturer = absl::WrapUnique(T::Create(video_url, opts));
        if (!capturer)
        {
            return nullptr;
        }
        return rtc::make_ref_counted<TrackSource>(std::move(capturer));
    }

  protected:
    explicit TrackSource(std::unique_ptr<T> capturer)
        : webrtc::VideoTrackSource(/*remote=*/false), capturer_(std::move(capturer))
    {
    }

  private:
    rtc::VideoSourceInterface<webrtc::VideoFrame> *source(void) override { return capturer_.get(); }
    std::unique_ptr<T> capturer_;
};

#endif // DEF_TRACK_SOURCE
