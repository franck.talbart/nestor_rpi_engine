/************************************************************************
 NestoRPi Engine
 Copyright (C) 2015-2025 Franck Talbart

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

/* Author: Franck Talbart */

#ifndef DEF_UTIL
#define DEF_UTIL

#include <functional>
#include <map>
#include <memory>
#include <string>
#include <vector>

#include "gason.hpp"
#include "rtc_base/strings/json.h"
#include "src/core/GPIOS.hpp"

// No debug mode: use the GPIOs
#ifndef DEBUG_MODE
// PIN can be < 0 to disable it
#define GPIOS_INPUT GPIOS::mode::input
#define GPIOS_OUTPUT GPIOS::mode::output
#define GPIOS_PWM_OUTPUT GPIOS::mode::pwm_output
#define GPIOS_HIGH 1
#define GPIOS_LOW 0
#define GPIOS_INIT() GPIOS::NRPI_GPIOS.init()
#define GPIOS_DIGITALWRITE(PIN, VALUE) \
    ((PIN >= 0) ? GPIOS::NRPI_GPIOS.digital_write(PIN, VALUE) : (void)nullptr)
#define GPIOS_DIGITALREAD(PIN) ((PIN >= 0) ? GPIOS::NRPI_GPIOS.digital_read(PIN) : 0)
#define GPIOS_MCP23017SETUP(BASE, ADDRESS) GPIOS::NRPI_GPIOS.mcp_23017_setup(BASE, ADDRESS)
#define GPIOS_PCA9685SETUP(BASE, ADDRESS, FREQUENCY) \
    GPIOS::NRPI_GPIOS.pca_9685_setup(BASE, ADDRESS, FREQUENCY)
#define GPIOS_PINMODE(PIN, MODE) \
    ((PIN >= 0) ? GPIOS::NRPI_GPIOS.pin_mode(PIN, MODE) : (void)nullptr)
#define GPIOS_PWMSETRANGE(PIN, MIN, MAX) GPIOS::NRPI_GPIOS.pwm_set_range(PIN, MIN, MAX)
#define GPIOS_PWMWRITE(PIN, VALUE) \
    ((PIN >= 0) ? GPIOS::NRPI_GPIOS.pwm_write(PIN, VALUE) : (void)nullptr)
#define GPIOS_PWMTURNOFF(PIN) ((PIN >= 0) ? GPIOS::NRPI_GPIOS.pwm_turn_off(PIN) : (void)nullptr)
#define GPIOS_SOFTPWMCREATE(PIN, INITIAL_VAL) \
    ((PIN >= 0) ? GPIOS::NRPI_GPIOS.soft_pwm_create(PIN, INITIAL_VAL) : (void)nullptr)
#define GPIOS_SOFTPWMWRITE(PIN, VALUE) \
    ((PIN >= 0) ? GPIOS::NRPI_GPIOS.soft_pwm_write(PIN, VALUE) : (void)nullptr)
#define GPIOS_SPISETUP(SPEED) GPIOS::NRPI_GPIOS.spi_setup(SPEED)
#define GPIOS_I2CSETUP(ADDR) GPIOS::NRPI_GPIOS.i2c_setup(ADDR)
#define GPIOS_I2CWRITE(I2CDEV, DATA, SIZE) GPIOS::NRPI_GPIOS.i2c_write(I2CDEV, DATA, SIZE)
#else // DEBUG_MODE
#define GPIOS_INPUT 0
#define GPIOS_OUTPUT 1
#define GPIOS_PWM_OUTPUT 2
#define GPIOS_HIGH 1
#define GPIOS_LOW 0
#define GPIOS_INIT() 1
#define GPIOS_DIGITALWRITE(PIN, VALUE) \
    if (PIN >= 0)                      \
    NRPI_LOG(DEBUG) << "Digital Write, pin " << PIN << " value: " << VALUE
#define GPIOS_DIGITALREAD(PIN) 0
#define GPIOS_MCP23017SETUP(BASE, ADDRESS) 1
#define GPIOS_PCA9685SETUP(BASE, ADDRESS, FREQUENCY) 1
#define GPIOS_PINMODE(PIN, MODE) \
    if (PIN >= 0)                \
    NRPI_LOG(DEBUG) << "Pin mode, pin " << PIN << " in mode: " << MODE
#define GPIOS_PWMSETRANGE(PIN, MIN, MAX) \
    NRPI_LOG(DEBUG) << "pwmSetRange, pin " << PIN << " range min " << MIN << " max " << MAX
#define GPIOS_PWMWRITE(PIN, VALUE) \
    if (PIN >= 0)                  \
    NRPI_LOG(DEBUG) << "Pwm Write, pin " << PIN << " value: " << VALUE
#define GPIOS_SOFTPWMCREATE(PIN, INITIAL_VAL) \
    if (PIN >= 0)                             \
    NRPI_LOG(DEBUG) << "SoftPwm Create, pin " << PIN << " init " << INITIAL_VAL
#define GPIOS_SOFTPWMWRITE(PIN, VALUE) \
    if (PIN >= 0)                      \
    NRPI_LOG(DEBUG) << "Pwm Write, pin " << PIN << " value: " << VALUE
#define GPIOS_PWMTURNOFF(PIN) \
    if (PIN >= 0)             \
    NRPI_LOG(DEBUG) << "Pwm turn off, pin " << PIN
#define GPIOS_SPISETUP(SPEED) \
    {                         \
    }
#define GPIOS_I2CSETUP(DEV_ID) \
    nullptr;                   \
    (void)DEV_ID
#define GPIOS_I2CWRITE(I2CDEV, DATA, SIZE) 1
#endif // DEBUG_MODE

#define COLOR(n, text) "\033[1;" n "m" text "\033[0m"
#define NRPI_CFG_DIR "NRPI_CFG_DIR"
#define NRPI_ROOT_DIR "NRPI_ROOT_DIR"
#define STR(n) #n
#define UNUSED(n) (void)n
using std::placeholders::_1;
using std::placeholders::_2;

typedef std::map<const std::string, pin_t> pins_t; // Map of pins
typedef std::map<const std::string, gason::JsonValue> val_json_t;

enum msg_type
{
    TASK_CONTROLLER,
    TASK_AUTOMATION,
    PING,
    PERMISSION
};

enum role_type
{ // from the lower to the higher
    FORBIDDEN,
    MINIMAL,
    FULL
};

namespace util
{
struct Popen
{
    FILE *stdout;
    FILE *stderr;
    int status;
};

const std::string get_full_dir(const std::string &dir);
const std::string get_full_dir_from_cfg(const std::string &path);
const std::string get_cfg_dir(const std::string &dir = "");
const std::string get_time(void);
const std::string get_version(void);
const Json::Value get_metrics(void);
role_type str_to_role(const std::string &role);
role_type login(const std::string &user, const std::string &passwd);
util::Popen extended_popen_read(const char *program);
void daemonize(void);
void *xmalloc(size_t size);
std::shared_ptr<char> prepare_file(const std::string &filename);
std::vector<std::string> msg_to_tokens(const std::string &msg);
gason::JsonValue get_cfg_value(const std::string &key, const std::string &path = "");
void print_back_trace(int sig);
void setup_coredump(void);
std::string float_to_str(const float val);
void play_sound(const std::string &filename);
void set_thread_high_priority(pthread_t thread);
} // namespace util

#endif // DEF_UTIL
