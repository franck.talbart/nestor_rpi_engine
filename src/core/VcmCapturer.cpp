/************************************************************************
 NestoRPi Engine
 Copyright (C) 2015-2025 Franck Talbart

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

/* Author: Franck Talbart
   webrtc-streamer written by Michel Promonet
   and updated for the NestoRPi Engine */

#include "src/core/VcmCapturer.hpp"

#include "src/core/Log.hpp"

VcmCapturer *VcmCapturer::Create(const std::string &video_url,
                                 const std::map<std::string, int> &opts)
{
    std::unique_ptr<VcmCapturer> vcm_capturer(new VcmCapturer());
    if (!vcm_capturer->Init(opts.at("width"), opts.at("height"), opts.at("fps"), video_url))
    {
        NRPI_LOG(WARN) << "Failed to create VcmCapturer(w = " << opts.at("width")
                       << ", h = " << opts.at("height") << ", fps = " << opts.at("fps") << ")";
        return nullptr;
    }
    return vcm_capturer.release();
}

VcmCapturer::~VcmCapturer(void) { Destroy(); }
void VcmCapturer::OnFrame(const webrtc::VideoFrame &frame) { broadcaster.OnFrame(frame); }
bool VcmCapturer::Init(size_t width, size_t height, size_t target_fps, const std::string &video_url)
{
    std::unique_ptr<webrtc::VideoCaptureModule::DeviceInfo> device_info(
        webrtc::VideoCaptureFactory::CreateDeviceInfo());

    int num_video_devices = device_info->NumberOfDevices();
    NRPI_LOG(INFO) << "nb video devices:" << num_video_devices;
    std::string device_id;
    for (int i = 0; i < num_video_devices; ++i)
    {
        const uint32_t k_size = 256;
        char name[k_size] = {0};
        char id[k_size] = {0};
        if (device_info->GetDeviceName(i, name, k_size, id, k_size) == 0)
        {
            if (video_url == name)
            {
                device_id = id;
                break;
            }
        }
    }

    if (device_id.empty())
    {
        NRPI_LOG(WARN) << "device not found:" << video_url;
        Destroy();
        return false;
    }

    vcm = webrtc::VideoCaptureFactory::Create(device_id.c_str());
    vcm->RegisterCaptureDataCallback(this);

    webrtc::VideoCaptureCapability capability;
    capability.width = static_cast<int32_t>(width);
    capability.height = static_cast<int32_t>(height);
    capability.maxFPS = static_cast<int32_t>(target_fps);
    capability.videoType = webrtc::VideoType::kI420;
    if (device_info->GetBestMatchedCapability(vcm->CurrentDeviceName(), capability, capability) < 0)
    {
        device_info->GetCapability(vcm->CurrentDeviceName(), 0, capability);
    }
    if (vcm->StartCapture(capability) != 0)
    {
        Destroy();
        return false;
    }

    RTC_CHECK(vcm->CaptureStarted());

    return true;
}

void VcmCapturer::Destroy(void)
{
    if (vcm)
    {
        vcm->StopCapture();
        vcm->DeRegisterCaptureDataCallback();
        vcm = nullptr;
    }
}

void VcmCapturer::AddOrUpdateSink(rtc::VideoSinkInterface<webrtc::VideoFrame> *sink,
                                  const rtc::VideoSinkWants &wants)
{
    broadcaster.AddOrUpdateSink(sink, wants);
}

void VcmCapturer::RemoveSink(rtc::VideoSinkInterface<webrtc::VideoFrame> *sink)
{
    broadcaster.RemoveSink(sink);
}
