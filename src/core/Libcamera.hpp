/************************************************************************
 NestoRPi Engine
 Copyright (C) 2015-2025 Franck Talbart

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

/* Author: Franck Talbart */

#ifndef DEF_CAMERA_LIBCAMERA
#define DEF_CAMERA_LIBCAMERA
#include <mutex>

#include "gason.hpp"
#include "src/core/Camera.hpp"
#include "src/core/LibcameraCapturer.hpp"

class Libcamera : public Camera
{
  public:
    static const std::list<std::string> get_video_capture_device_list(void);
    explicit Libcamera(const std::string &capture_dev,
                       size_t width,
                       size_t height,
                       short fps,
                       short rotate,
                       const std::map<std::string, gason::JsonValue> &opts_libcamera);
    std::string get_name(void) override;
    std::string capture_pic(void) override;
    rtc::scoped_refptr<webrtc::VideoTrackSourceInterface> get_video_source(void) override;
    void start(void) override;
    void stop(void) override;

  private:
#ifndef DEBUG_MODE
    rtc::scoped_refptr<LibcameraCapturer> video_track_source;
#endif // DEBUG_MODE
    short nb_clients;
    std::mutex mutex_start_stop;
};
#endif // DEF_CAMERA_LIBCAMERA
