/************************************************************************
 NestoRPi Engine
 Copyright (C) 2015-2025 Franck Talbart

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

/* Author: Franck Talbart */

// For more info, checkout
// nestor_rpi_engine/third_party/rpicam-apps/encoder/h264_encoder.cpp

/* Note: from a performance standpoint, it would be much better to use DMA instead of MMAP but the
 * libwebrtc's API prevents to do it easily. A solution would be to directly encode in
 * LibcameraCapture and do a passthrough to webrtc. I see multiple issues with this solution:
 * - no adapted bitrate anymore
 * - apart from h264, can't use any other codec
 * - we still need to implement a V4L2M2 encoder for external cameras
 * */
#ifndef DEBUG_MODE

#include "src/core/v4l2m2m_encoder.hpp"

#include <fcntl.h>
#include <poll.h>
#include <stdio.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <time.h>
#include <unistd.h>

#include "src/core/Log.hpp"
#include "src/core/util.hpp"
#include "third_party/webrtc/src/modules/video_coding/include/video_codec_interface.h"
#include "third_party/webrtc/src/modules/video_coding/include/video_error_codes.h"

// Options
static const char *LEVEL = "4.2";     // No perf difference with 3.1
static const unsigned int INTRA = 12; // No perf impact when set
static const bool INLINE_HEADERS = true;
static const bool HEADER_MODE_JOINED_WITH_1ST_FRAME = true;

static const char *DEVICE_NAME = "/dev/video11";

//------------------------------------------------------------------------------
static int xioctl(int fd, unsigned long ctl, void *arg)
{
    int ret, num_tries = 10;
    do
    {
        ret = ioctl(fd, ctl, arg);
    } while (ret == -1 && errno == EINTR && num_tries-- > 0);
    return ret;
}

//------------------------------------------------------------------------------
// cppcheck-suppress uninitMemberVar
V4l2m2mEncoder::V4l2m2mEncoder(void)
    : fd(-1), width(0), height(0), framerate(30), bitrate_bps(0), abort(false)
{
    try
    {
        // profile: main or baseline, baseline: better perf but low quality, main: + 100 ms
        profile = util::get_cfg_value(CONFIG_H264_PROFILE).toString();
    }
    catch (const std::out_of_range &oor)
    {
        profile = "main";
    }

    level = LEVEL;
    // From https://en.wikipedia.org/wiki/Advanced_Video_Coding#Levels
    double mbps = ((width + 15) >> 4) * ((height + 15) >> 4) * framerate;
    if (mbps > 245760.0)
    {
        level = "4.2";
    }
}

//------------------------------------------------------------------------------
V4l2m2mEncoder::~V4l2m2mEncoder(void) {}

//------------------------------------------------------------------------------
// cppcheck-suppress unusedFunction
int32_t V4l2m2mEncoder::InitEncode(const webrtc::VideoCodec *codec_settings,
                                   const VideoEncoder::Settings &settings)
{
    UNUSED(settings);
    bitrate_adjuster.reset(new webrtc::BitrateAdjuster(1, 1));

    NRPI_LOG(INFO) << "Init encoder";
    if (codec_settings->codecType != webrtc::kVideoCodecH264)
    {
        return WEBRTC_VIDEO_CODEC_ERROR;
    }

    width = codec_settings->width;
    height = codec_settings->height;
    encoded_image.timing_.flags = webrtc::VideoSendTiming::TimingFrameFlags::kInvalid;
    encoded_image.content_type_ = webrtc::VideoContentType::UNSPECIFIED;

    // Do not set this in non blocking mode, it is CPU consuming
    fd = open(DEVICE_NAME, O_RDWR, 0);
    if (fd < 0)
        throw std::runtime_error("failed to open V4L2 H264 encoder");
    NRPI_LOG(INFO) << "Opened H264Encoder on " << DEVICE_NAME << " as fd " << fd;

    configure(width, height, framerate);

    return WEBRTC_VIDEO_CODEC_OK;
}

//------------------------------------------------------------------------------
// cppcheck-suppress unusedFunction
int32_t V4l2m2mEncoder::RegisterEncodeCompleteCallback(webrtc::EncodedImageCallback *callback_)
{
    callback = callback_;
    return WEBRTC_VIDEO_CODEC_OK;
}

//------------------------------------------------------------------------------
// cppcheck-suppress unusedFunction
int32_t V4l2m2mEncoder::Release(void)
{
    std::lock_guard<std::mutex> lock(mtx);
    release();

    return WEBRTC_VIDEO_CODEC_OK;
}

//------------------------------------------------------------------------------
// cppcheck-suppress unusedFunction
int32_t V4l2m2mEncoder::Encode(const webrtc::VideoFrame &frame,
                               const std::vector<webrtc::VideoFrameType> *frame_types)
{
    UNUSED(frame_types);
    // We must make sure that the timestamp is correct
    // otherwhise the video speed may do crazy thing on the browser.
    // The first approach was to copy it in the buffer but the encoding may
    // deliver the buffers out of order. Finally, the only fast solution is to copy it
    // in timestamp and use it when sending the frame.
    timestamp = frame.rtp_timestamp();
    std::lock_guard<std::mutex> lock(mtx);

    rtc::scoped_refptr<webrtc::VideoFrameBuffer> frame_buffer = frame.video_frame_buffer();
    v4l2_buffer buf = {};
    v4l2_plane planes[VIDEO_MAX_PLANES] = {};
    buf.type = V4L2_BUF_TYPE_VIDEO_OUTPUT_MPLANE;
    buf.memory = V4L2_MEMORY_MMAP;
    buf.length = 1;
    buf.m.planes = planes;

    int ret = xioctl(fd, VIDIOC_DQBUF, &buf);
    if (ret == 0)
    {
        auto i420_buffer = frame_buffer->GetI420();

        memcpy(static_cast<uint8_t *>(input_buffers_available[buf.index].mem),
               i420_buffer->DataY(),
               i420_buffer_size);
        input_buffers_available[buf.index].size = i420_buffer_size;

        if (xioctl(fd, VIDIOC_QBUF, &buf) < 0)
            throw std::runtime_error("failed to queue input to codec");
    }
    return WEBRTC_VIDEO_CODEC_OK;
}

//------------------------------------------------------------------------------
// cppcheck-suppress unusedFunction
void V4l2m2mEncoder::SetRates(const RateControlParameters &parameters)
{
    std::lock_guard<std::mutex> lock(mtx);

    int force_bitrate = util::get_cfg_value(CONFIG_FORCE_H264_BITRATE).toNumber();
    if (parameters.bitrate.get_sum_bps() <= 0 || parameters.framerate_fps <= 0 || fd < 0 ||
        force_bitrate > 0)
    {
        return;
    }

    bitrate_adjuster->SetTargetBitrateBps(parameters.bitrate.get_sum_bps());
    uint32_t adjusted_bitrate_bps = bitrate_adjuster->GetAdjustedBitrateBps();

    if (bitrate_bps != adjusted_bitrate_bps)
    {
        bitrate_bps = adjusted_bitrate_bps;
        v4l2_control ctrl = {};
        ctrl.id = V4L2_CID_MPEG_VIDEO_BITRATE;
        ctrl.value = bitrate_bps;
        if (xioctl(fd, VIDIOC_S_CTRL, &ctrl) < 0)
            NRPI_LOG(ERROR) << "Encoder failed set bitrate: " << bitrate_bps << " bps";
        NRPI_LOG(INFO) << "Bitrate_bps: " << bitrate_bps;
    }

    if (framerate != parameters.framerate_fps && framerate > 0.0)
    {
        struct v4l2_streamparm streamparms;
        streamparms.type = V4L2_BUF_TYPE_VIDEO_OUTPUT_MPLANE;
        streamparms.parm.capture.timeperframe.numerator = 90000.0 / framerate;
        streamparms.parm.capture.timeperframe.denominator = 90000;
        if (xioctl(fd, VIDIOC_S_PARM, &streamparms) < 0)
        {
            NRPI_LOG(ERROR) << "ioctl Setting Fps";
        }
        else
        {
            framerate = parameters.framerate_fps;
            NRPI_LOG(INFO) << "Framerate: " << framerate;
        }
    }

    return;
}

//------------------------------------------------------------------------------
// cppcheck-suppress unusedFunction
webrtc::VideoEncoder::EncoderInfo V4l2m2mEncoder::GetEncoderInfo(void) const
{
    EncoderInfo info;
    info.supports_native_handle = true;
    info.implementation_name = "h264_v4l2m2m";
    return info;
}

//------------------------------------------------------------------------------
// Request that the necessary buffers are allocated.
void V4l2m2mEncoder::request_buffers(const enum v4l2_buf_type type,
                                     int &num_buffers,
                                     struct BufferDescription *buffers)
{
    v4l2_requestbuffers reqbufs = {};
    reqbufs.count =
        (type == V4L2_BUF_TYPE_VIDEO_OUTPUT_MPLANE) ? NUM_OUTPUT_BUFFERS : NUM_CAPTURE_BUFFERS;
    reqbufs.type = type;
    reqbufs.memory = V4L2_MEMORY_MMAP;
    if (xioctl(fd, VIDIOC_REQBUFS, &reqbufs) < 0)
        throw std::runtime_error("request for buffers failed");
    NRPI_LOG(INFO) << "Got " << reqbufs.count << " buffers";
    num_buffers = reqbufs.count;

    // We have to maintain a list of the buffers we can use when our caller gives
    // us another frame to encode.
    for (unsigned int i = 0; i < reqbufs.count; i++)
    {
        v4l2_plane planes[VIDEO_MAX_PLANES];
        v4l2_buffer buffer = {};
        buffer.type = type;
        buffer.memory = V4L2_MEMORY_MMAP;
        buffer.index = i;
        buffer.length = 1;
        buffer.m.planes = planes;
        if (xioctl(fd, VIDIOC_QUERYBUF, &buffer) < 0)
            throw std::runtime_error("failed to capture query buffer " + std::to_string(i));
        buffers[i].mem = mmap(0,
                              buffer.m.planes[0].length,
                              PROT_READ | PROT_WRITE,
                              MAP_SHARED,
                              fd,
                              buffer.m.planes[0].m.mem_offset);
        if (buffers[i].mem == MAP_FAILED)
            throw std::runtime_error("failed to mmap buffer " + std::to_string(i));
        buffers[i].size = buffer.m.planes[0].length;
        // Whilst we're going through all the output buffers, we may as well queue
        // them ready for the output to write into.

        if (xioctl(fd, VIDIOC_QBUF, &buffer) < 0)
            throw std::runtime_error("failed to queue buffer " + std::to_string(i));
    }
}

//------------------------------------------------------------------------------
int32_t V4l2m2mEncoder::configure(int width, int height, int fps)
{
    // Profile
    static const std::map<std::string, int> profile_map = {
        {"baseline", V4L2_MPEG_VIDEO_H264_PROFILE_BASELINE},
        {"main", V4L2_MPEG_VIDEO_H264_PROFILE_MAIN},
        {"high", V4L2_MPEG_VIDEO_H264_PROFILE_HIGH}};
    auto it_profile = profile_map.find(profile);
    if (it_profile == profile_map.end())
        throw std::runtime_error("no such profile " + std::string(profile));
    v4l2_control ctrl = {};
    ctrl.id = V4L2_CID_MPEG_VIDEO_H264_PROFILE;
    ctrl.value = it_profile->second;
    if (xioctl(fd, VIDIOC_S_CTRL, &ctrl) < 0)
        throw std::runtime_error("failed to set profile");

    // Level
    static const std::map<std::string, int> level_map = {{"3.1", V4L2_MPEG_VIDEO_H264_LEVEL_3_1},
                                                         {"4", V4L2_MPEG_VIDEO_H264_LEVEL_4_0},
                                                         {"4.1", V4L2_MPEG_VIDEO_H264_LEVEL_4_1},
                                                         {"4.2", V4L2_MPEG_VIDEO_H264_LEVEL_4_2}};
    auto it_level = level_map.find(level);
    if (it_level == level_map.end())
        throw std::runtime_error("no such level " + std::string(level));
    ctrl.id = V4L2_CID_MPEG_VIDEO_H264_LEVEL;
    ctrl.value = it_level->second;
    if (xioctl(fd, VIDIOC_S_CTRL, &ctrl) < 0)
        throw std::runtime_error("failed to set level");

    // Intra period
    if (INTRA)
    {
        ctrl.id = V4L2_CID_MPEG_VIDEO_H264_I_PERIOD;
        ctrl.value = INTRA;
        if (xioctl(fd, VIDIOC_S_CTRL, &ctrl) < 0)
            throw std::runtime_error("failed to set intra period");
    }

    // Inline
    if (INLINE_HEADERS)
    {
        ctrl.id = V4L2_CID_MPEG_VIDEO_REPEAT_SEQ_HEADER;
        ctrl.value = 1;
        if (xioctl(fd, VIDIOC_S_CTRL, &ctrl) < 0)
            throw std::runtime_error("failed to set inline headers");
    }

    if (HEADER_MODE_JOINED_WITH_1ST_FRAME)
    {
        ctrl.id = V4L2_CID_MPEG_VIDEO_HEADER_MODE;
        ctrl.value = V4L2_MPEG_VIDEO_HEADER_MODE_JOINED_WITH_1ST_FRAME;
        if (xioctl(fd, VIDIOC_S_CTRL, &ctrl) < 0)
            throw std::runtime_error("failed to set inline headers");
    }

    // Bitrate
    bitrate_bps = width * height * fps / 10;

    int force_bitrate = util::get_cfg_value(CONFIG_FORCE_H264_BITRATE).toNumber();
    if (force_bitrate > 0)
        bitrate_bps = force_bitrate;
    ctrl.id = V4L2_CID_MPEG_VIDEO_BITRATE;
    ctrl.value = bitrate_bps;
    if (xioctl(fd, VIDIOC_S_CTRL, &ctrl) < 0)
        NRPI_LOG(ERROR) << "Encoder failed set bitrate: " << bitrate_bps << " bps";

    // Framerate
    struct v4l2_streamparm parm = {};
    parm.type = V4L2_BUF_TYPE_VIDEO_OUTPUT_MPLANE;
    parm.parm.output.timeperframe.numerator = 1;
    ;
    parm.parm.output.timeperframe.denominator = fps;
    if (xioctl(fd, VIDIOC_S_PARM, &parm) < 0)
        throw std::runtime_error("failed to set streamparm");

    // Output
    v4l2_format fmt = {};
    fmt.type = V4L2_BUF_TYPE_VIDEO_OUTPUT_MPLANE;
    fmt.fmt.pix_mp.width = width;
    fmt.fmt.pix_mp.height = height;
    fmt.fmt.pix_mp.pixelformat = V4L2_PIX_FMT_YUV420;
    fmt.fmt.pix_mp.plane_fmt[0].bytesperline = width;
    fmt.fmt.pix_mp.field = V4L2_FIELD_ANY;
    fmt.fmt.pix_mp.num_planes = 1;
    if (xioctl(fd, VIDIOC_S_FMT, &fmt) < 0)
        throw std::runtime_error("failed to set output format");

    // Capture
    fmt = {};
    fmt.type = V4L2_BUF_TYPE_VIDEO_CAPTURE_MPLANE;
    fmt.fmt.pix_mp.width = width;
    fmt.fmt.pix_mp.height = height;
    fmt.fmt.pix_mp.pixelformat = V4L2_PIX_FMT_H264;
    fmt.fmt.pix_mp.field = V4L2_FIELD_ANY;
    fmt.fmt.pix_mp.colorspace = V4L2_COLORSPACE_DEFAULT;
    fmt.fmt.pix_mp.num_planes = 1;
    fmt.fmt.pix_mp.plane_fmt[0].bytesperline = 0;
    fmt.fmt.pix_mp.plane_fmt[0].sizeimage = 512 << 10;
    if (xioctl(fd, VIDIOC_S_FMT, &fmt) < 0)
        throw std::runtime_error("failed to set capture format");

    // Codec
    ctrl.id = V4L2_CID_MPEG_VIDEO_BITRATE_MODE;
    // VBR => better perf then CBR in high resolution ?
    ctrl.value = V4L2_MPEG_VIDEO_BITRATE_MODE_VBR;
    if (xioctl(fd, VIDIOC_S_CTRL, &ctrl) < 0)
        NRPI_LOG(ERROR) << "Encoder failed set bitrate: " << bitrate_bps << " bps";

    // The output queue
    // is the input to the encoder
    request_buffers(V4L2_BUF_TYPE_VIDEO_OUTPUT_MPLANE, num_output_buffers, input_buffers_available);

    request_buffers(V4L2_BUF_TYPE_VIDEO_CAPTURE_MPLANE, num_capture_buffers, buffers);

    // Enable streaming
    v4l2_buf_type type = V4L2_BUF_TYPE_VIDEO_OUTPUT_MPLANE;
    if (xioctl(fd, VIDIOC_STREAMON, &type) < 0)
        throw std::runtime_error("failed to start output streaming");
    type = V4L2_BUF_TYPE_VIDEO_CAPTURE_MPLANE;
    if (xioctl(fd, VIDIOC_STREAMON, &type) < 0)
        throw std::runtime_error("failed to start capture streaming");
    NRPI_LOG(INFO) << "Codec streaming started";

    i420_buffer_size = (width * height) + ((width + 1) / 2) * ((height + 1) / 2) * 2;

    NRPI_LOG(INFO) << "Width: " << width;
    NRPI_LOG(INFO) << "Height: " << height;
    NRPI_LOG(INFO) << "Framerate: " << framerate;
    NRPI_LOG(INFO) << "Bitrate_bps: " << bitrate_bps;
    NRPI_LOG(INFO) << "Key_frame_interval: " << INTRA;

    output_thread = std::thread(&V4l2m2mEncoder::process_output, this);
    poll_thread = std::thread(&V4l2m2mEncoder::process_poll, this);

    return 1;
}

//------------------------------------------------------------------------------
void V4l2m2mEncoder::release(void)
{
    abort = true;
    if (poll_thread.joinable())
        poll_thread.join();
    if (output_thread.joinable())
        output_thread.join();

    // Turn off streaming on both the output and capture queues, and "free" the
    // buffers that we requested. The capture ones need to be "munmapped" first.

    v4l2_buf_type type = V4L2_BUF_TYPE_VIDEO_OUTPUT_MPLANE;
    if (xioctl(fd, VIDIOC_STREAMOFF, &type) < 0)
        NRPI_LOG(ERROR) << "Failed to stop output streaming";
    type = V4L2_BUF_TYPE_VIDEO_CAPTURE_MPLANE;
    if (xioctl(fd, VIDIOC_STREAMOFF, &type) < 0)
        NRPI_LOG(ERROR) << "Failed to stop capture streaming";

    for (int i = 0; i < num_output_buffers; i++)
        if (munmap(input_buffers_available[i].mem, input_buffers_available[i].size) < 0)
            NRPI_LOG(ERROR) << "Failed to unmap buffer";
    v4l2_requestbuffers reqbufs = {};
    reqbufs.count = 0;
    reqbufs.type = V4L2_BUF_TYPE_VIDEO_OUTPUT_MPLANE;
    reqbufs.memory = V4L2_MEMORY_MMAP;
    if (xioctl(fd, VIDIOC_REQBUFS, &reqbufs) < 0)
        NRPI_LOG(ERROR) << "Request to free output buffers failed";

    for (int i = 0; i < num_capture_buffers; i++)
        if (munmap(buffers[i].mem, buffers[i].size) < 0)
            NRPI_LOG(ERROR) << "Failed to unmap buffer";
    reqbufs = {};
    reqbufs.count = 0;
    reqbufs.type = V4L2_BUF_TYPE_VIDEO_CAPTURE_MPLANE;
    reqbufs.memory = V4L2_MEMORY_MMAP;
    if (xioctl(fd, VIDIOC_REQBUFS, &reqbufs) < 0)
        NRPI_LOG(ERROR) << "Request to free capture buffers failed";

    close(fd);
    NRPI_LOG(INFO) << "H264Encoder closed";

    fd = -1;
}

//------------------------------------------------------------------------------
// This thread is waiting for the encoder to finish stuff. It will either:
// - receive "output" buffers (codec inputs), which we must return to the caller
// - receive encoded buffers, which we pass to the application.
void V4l2m2mEncoder::process_poll(void)
{
    v4l2_buffer buf = {};
    v4l2_plane planes[VIDEO_MAX_PLANES] = {};
    buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE_MPLANE;
    buf.memory = V4L2_MEMORY_MMAP;
    buf.length = 1;
    buf.m.planes = planes;

    while (true)
    {
        // We need poll because ioctl is in blocking mode and we need a way to exit the thread
        pollfd p = {fd, POLLIN, 0};
        int ret = poll(&p, 1, 200);
        if (abort)
            break;
        if (ret == -1)
        {
            if (errno == EINTR)
                continue;
            throw std::runtime_error("unexpected errno " + std::to_string(errno) + " from poll");
        }
        if (p.revents & POLLIN)
        {
            int ret_io = xioctl(fd, VIDIOC_DQBUF, &buf);
            if (ret_io == 0)
            {
                // We push this encoded buffer to another thread so that our
                // application can take its time with the data without blocking the
                // encode process.
                OutputItem item = {buffers[buf.index].mem,
                                   buf.m.planes[0].bytesused,
                                   buf.m.planes[0].length,
                                   buf.index,
                                   !!(buf.flags & V4L2_BUF_FLAG_KEYFRAME),
                                   timestamp};
                {
                    std::lock_guard<std::mutex> lock(output_mutex);
                    output_queue.push(item);
                }
                output_cond_var.notify_one();
            }
        }
    }
}

//------------------------------------------------------------------------------
// Handle the output buffers in another thread so as not to block the encoder. The
// application can take its time, after which we return this buffer to the encoder for
// re-use.
void V4l2m2mEncoder::process_output(void)
{
    OutputItem item;
    webrtc::CodecSpecificInfo codec_specific;
    codec_specific.codecType = webrtc::kVideoCodecH264;
    codec_specific.codecSpecific.H264.packetization_mode =
        webrtc::H264PacketizationMode::NonInterleaved;

    while (true)
    {
        {
            std::unique_lock<std::mutex> lock(output_mutex);
            while (true)
            {
                // Must check the abort first, to allow items in the output
                // queue to have a callback.
                if (abort)
                    return;

                if (!output_queue.empty())
                {
                    item = output_queue.front();
                    output_queue.pop();
                    break;
                }
                else
                {
                    using namespace std::chrono_literals;
                    output_cond_var.wait_for(lock, 200ms);
                }
            }
        }
        if (item.timestamp != 0)
        {
            auto encoded_imagebuffer = webrtc::EncodedImageBuffer::Create(
                static_cast<uint8_t *>(item.mem), item.bytes_used);

            encoded_image.SetEncodedData(encoded_imagebuffer);
            encoded_image.SetRtpTimestamp(item.timestamp);
            encoded_image._encodedWidth = width;
            encoded_image._encodedHeight = height;
            encoded_image._frameType = webrtc::VideoFrameType::kVideoFrameDelta;
            if (item.keyframe)
                encoded_image._frameType = webrtc::VideoFrameType::kVideoFrameKey;

            auto result = callback->OnEncodedImage(encoded_image, &codec_specific);
            if (result.error != webrtc::EncodedImageCallback::Result::OK)
            {
                NRPI_LOG(ERROR) << "OnEncodedImage failed error:" << result.error;
            }

            bitrate_adjuster->Update(item.bytes_used);

            v4l2_buffer buf = {};
            v4l2_plane planes[VIDEO_MAX_PLANES] = {};
            buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE_MPLANE;
            buf.memory = V4L2_MEMORY_MMAP;
            buf.index = item.index;
            buf.length = 1;
            buf.m.planes = planes;
            buf.m.planes[0].bytesused = 0;
            buf.m.planes[0].length = item.length;
            if (xioctl(fd, VIDIOC_QBUF, &buf) < 0)
                throw std::runtime_error("failed to re-queue encoded buffer");
        }
    }
}

#endif // DEBUG_MODE
