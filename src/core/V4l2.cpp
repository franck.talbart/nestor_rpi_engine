/************************************************************************
 NestoRPi Engine
 Copyright (C) 2015-2025 Franck Talbart

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

/* Author: Franck Talbart */
#include "src/core/V4l2.hpp"

#include <fcntl.h>
#include <linux/videodev2.h>
#include <signal.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <unistd.h>

#include "src/core/Log.hpp"
#include "src/core/TrackSource.hpp"
#include "src/core/VcmCapturer.hpp"
#include "src/core/util.hpp"

//------------------------------------------------------------------------------
/**
 * @brief Generates a list of V4L2 video capture devices
 *
 * @return list of video capture devices
 */
const std::list<std::string> V4l2::get_video_capture_device_list(void)
{
    static std::list<std::string> video_device_list;
    if (video_device_list.empty())
    {
        std::unique_ptr<webrtc::VideoCaptureModule::DeviceInfo> info(
            webrtc::VideoCaptureFactory::CreateDeviceInfo());
        if (info)
        {
            int num_videoDevices = info->NumberOfDevices();
            NRPI_LOG(DEBUG) << "Nb video devices: " << num_videoDevices;
            for (int i = 0; i < num_videoDevices; ++i)
            {
                const uint32_t kSize = 256;
                char name[kSize] = {0};
                char id[kSize] = {0};
                if (info->GetDeviceName(i, name, kSize, id, kSize) != -1)
                {
                    NRPI_LOG(DEBUG) << "Video device name: " << name << " id:" << id;
                    video_device_list.push_back(name);
                }
            }
        }
    }
    return video_device_list;
}

//------------------------------------------------------------------------------
/**
 * @brief Constructor
 * ??
 *
 */
V4l2::V4l2(const std::string& capture_dev,
           size_t width,
           size_t height,
           short fps,
           short rotate,
           std::map<std::string, gason::JsonValue> opts)
    : Camera()
{
    UNUSED(opts);
    if ((fd_v4l2 = open(capture_dev.c_str(), O_RDWR, 0)) < 0)
    {
        throw std::string("Unable to open " + capture_dev);
    }
    if (rotate != 0)
    {
        struct v4l2_control ctrl;
        ctrl.id = V4L2_CID_ROTATE;
        ctrl.value = rotate;
        if (ioctl(fd_v4l2, VIDIOC_S_CTRL, &ctrl) < 0)
        {
            NRPI_LOG(ERROR) << "Set ctrl rotate failed";
        }
    }

    std::map<std::string, int> local_opts;
    local_opts["width"] = width;
    local_opts["height"] = height;
    local_opts["fps"] = fps;
    video_track_source = TrackSource<VcmCapturer>::Create(this->get_name(), local_opts);
}

//------------------------------------------------------------------------------
V4l2::~V4l2(void)
{
    if (fd_v4l2 > 0)
        close(fd_v4l2);
}

//------------------------------------------------------------------------------
/**
 * @brief Return the name of the video device
 *
 * @return the device name
 */
std::string V4l2::get_name(void)
{
    std::string name;
    assert(fd_v4l2 > 0);
    struct v4l2_capability video_cap;
    if (ioctl(fd_v4l2, VIDIOC_QUERYCAP, &video_cap) == -1)
    {
        NRPI_LOG(ERROR) << "Can't get capabilities: " << errno;
    }
    else
    {
        name = std::string(reinterpret_cast<char*>(video_cap.card));
    }
    return name;
}

//------------------------------------------------------------------------------
/**
 * @brief Capture a picture
 *
 * @return filename
 */
std::string V4l2::capture_pic(void)
{
    assert(fd_v4l2 > 0);
    //@TODO
    NRPI_LOG(ERROR) << "Taking a picture is only supported with libcamera";
    return "";
}

//------------------------------------------------------------------------------
rtc::scoped_refptr<webrtc::VideoTrackSourceInterface> V4l2::get_video_source(void)
{
    return video_track_source;
}

//------------------------------------------------------------------------------
void V4l2::start(void) {}
//------------------------------------------------------------------------------
void V4l2::stop(void) {}
