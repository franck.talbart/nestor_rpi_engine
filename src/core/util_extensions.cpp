/************************************************************************
 NestoRPi Engine
 Copyright (C) 2015-2025 Franck Talbart

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

/* Author: Franck Talbart */

/**
 * This module provides functions to load the extensions
 */

#include "src/core/util_extensions.hpp"

#include <sys/stat.h>

#include "src/core/Log.hpp"
#include "src/core/const.hpp"
#include "src/core/util.hpp"

//------------------------------------------------------------------------------
/**
 * @brief Load the extensions and initialize them
 *
 * @return  bool: true if success, false ortherwhise
 */

bool load_extensions(void)
{
    // Get the list of extensions
    const std::string path = util::get_cfg_dir() + std::string(FILENAME_EXTENSIONS);

    struct stat buffer;
    if (stat(path.c_str(), &buffer) != 0)
    {
        NRPI_LOG(ERROR) << "File not found: " << path.c_str();
        return false; // File does not exist, nothing to do
    }

    std::shared_ptr<char> file_content = nullptr;
    try
    {
        file_content = util::prepare_file(path);
    }
    catch (const std::string &err)
    {
        NRPI_LOG(ERROR) << err;
        return false;
    }

    char *endptr = nullptr;
    gason::JsonValue root;
    gason::JsonAllocator allocator;
    int status = gason::jsonParse(file_content.get(), &endptr, &root, allocator);

    if (status != gason::JSON_PARSE_OK)
    {
        NRPI_LOG(ERROR) << "Error while parsing the JSON file " << path;
        return false;
    }

    for (const auto &value : root) // C++ 11 :)
    {
        std::string type_extension = "";
        int base_extension = 100;
        int address_extension = 0;
        int frequency_extension = 0;
        for (const auto &it : value->value)
        {
            if (it->key == std::string(JSON_EXTENSIONS_TYPE))
            {
                type_extension = std::string(static_cast<char *>(it->value.toString()));
            }
            else if (it->key == std::string(JSON_EXTENSIONS_BASE))
            {
                base_extension = it->value.toNumber();
            }
            else if (it->key == std::string(JSON_EXTENSIONS_ADDRESS))
            {
                address_extension = it->value.toNumber();
            }
            else if (it->key == std::string(JSON_EXTENSIONS_FREQUENCY))
            {
                frequency_extension = it->value.toNumber();
            }
            else
            {
                NRPI_LOG(ERROR) << "Unknow key " << it->key;
                return false;
            }
        }
#ifdef DEBUG_MODE
        UNUSED(base_extension);
        UNUSED(address_extension);
        UNUSED(frequency_extension);
#endif // DEBUG_MODE
        if (type_extension == JSON_EXTENSIONS_MCP23017)
        {
            if (!GPIOS_MCP23017SETUP(base_extension, address_extension))
            {
                NRPI_LOG(ERROR) << "Can't initialize MCP23017: not found";
                return false;
            }
        }
        else if (type_extension == JSON_EXTENSIONS_PCA9685)
        {
            if (!GPIOS_PCA9685SETUP(base_extension, address_extension, frequency_extension))
            {
                NRPI_LOG(ERROR) << "Can't initialize PCA9685: not found";
                return false;
            }
        }
        else
        {
            NRPI_LOG(ERROR) << "Unknow extension " << type_extension;
            return false;
        }
    }
    return true;
}
