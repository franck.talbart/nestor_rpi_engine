/************************************************************************
 NestoRPi Engine
 Copyright (C) 2015-2025 Franck Talbart

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

#include "src/core/h264_format.hpp"

#include "absl/types/optional.h"
#include "api/video_codecs/sdp_video_format.h"

// modules/video_coding/codecs/h264/h264.cc from WebRTC
webrtc::SdpVideoFormat create_H264_format(webrtc::H264Profile profile,
                                          webrtc::H264Level level,
                                          const std::string& packetization_mode)
{
    const std::optional<std::string> profile_string =
        webrtc::H264ProfileLevelIdToString(webrtc::H264ProfileLevelId(profile, level));
    return webrtc::SdpVideoFormat(cricket::kH264CodecName,
                                  {{cricket::kH264FmtpProfileLevelId, *profile_string},
                                   {cricket::kH264FmtpLevelAsymmetryAllowed, "1"},
                                   {cricket::kH264FmtpPacketizationMode, packetization_mode}});
}
