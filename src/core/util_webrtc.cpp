/************************************************************************
 NestoRPi Engine
 Copyright (C) 2015-2025 Franck Talbart

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

/* Author: Franck Talbart */

/**
 * This module provides general utility functions for webrtc
 */

#include "src/core/util_webrtc.hpp"

#include <ifaddrs.h>

#include "api/audio_codecs/builtin_audio_decoder_factory.h"
#include "api/audio_codecs/builtin_audio_encoder_factory.h"
#include "api/enable_media.h"
#include "api/rtc_event_log/rtc_event_log_factory.h"
#include "api/task_queue/default_task_queue_factory.h"
#include "api/video_codecs/video_decoder_factory_template.h"
#include "api/video_codecs/video_decoder_factory_template_libvpx_vp8_adapter.h"
#include "api/video_codecs/video_decoder_factory_template_libvpx_vp9_adapter.h"
#include "api/video_codecs/video_encoder_factory_template.h"
#include "api/video_codecs/video_encoder_factory_template_libvpx_vp8_adapter.h"
#include "api/video_codecs/video_encoder_factory_template_libvpx_vp9_adapter.h"
#include "media/engine/webrtc_media_engine.h"
#include "src/core/HWVideoEncoderFactory.hpp"

//------------------------------------------------------------------------------
/**
 * @brief Returns the server IP
 *
 * @param  client_ip: the client IP
 * @return server IP
 */
const std::string util_webrtc::get_server_ip_from_client_ip(int client_ip)
{
    std::string serverAddress;
    char host[NI_MAXHOST];
    struct ifaddrs *ifaddr = nullptr;
    if (getifaddrs(&ifaddr) == 0)
    {
        for (struct ifaddrs *ifa = ifaddr; ifa != nullptr; ifa = ifa->ifa_next)
        {
            if ((ifa->ifa_netmask != nullptr) && (ifa->ifa_netmask->sa_family == AF_INET) &&
                (ifa->ifa_addr != nullptr) && (ifa->ifa_addr->sa_family == AF_INET))
            {
                struct sockaddr_in *addr = reinterpret_cast<struct sockaddr_in *>(ifa->ifa_addr);
                struct sockaddr_in *mask = reinterpret_cast<struct sockaddr_in *>(ifa->ifa_netmask);
                if ((addr->sin_addr.s_addr & mask->sin_addr.s_addr) ==
                    (client_ip & mask->sin_addr.s_addr))
                {
                    if (getnameinfo(ifa->ifa_addr,
                                    sizeof(struct sockaddr_in),
                                    host,
                                    sizeof(host),
                                    nullptr,
                                    0,
                                    NI_NUMERICHOST) == 0)
                    {
                        serverAddress = host;
                        break;
                    }
                }
            }
        }
    }
    freeifaddrs(ifaddr);
    return serverAddress;
}

//------------------------------------------------------------------------------
/**
 * @brief Returns a ICE server based on the URL
 *
 * @param  url: the URL
 * @param  client_ip: the client IP
 * @return ICE server object
 */
util_webrtc::IceServer util_webrtc::get_ice_server_from_url(const std::string &url,
                                                            const std::string &client_ip)
{
    IceServer srv;
    srv.url = url;

    std::size_t pos = url.find_first_of(':');
    if (pos != std::string::npos)
    {
        std::string protocol = url.substr(0, pos);
        std::string uri = url.substr(pos + 1);
        std::string credentials;

        std::size_t pos_local = uri.find('#');
        if (pos_local != std::string::npos)
        {
            credentials = uri.substr(0, pos_local);
            uri = uri.substr(pos_local + 1);
        }

        // cppcheck-suppress stlIfStrFind
        if ((uri.find("0.0.0.0:") == 0) && (client_ip.empty() == false))
        {
            // answer with ip that is on same network as client
            std::string clienturl = get_server_ip_from_client_ip(inet_addr(client_ip.c_str()));
            clienturl += uri.substr(uri.find_first_of(':'));
            uri = clienturl;
        }
        srv.url = protocol + ":" + uri;

        if (!credentials.empty())
        {
            pos = credentials.find(':');
            if (pos == std::string::npos)
            {
                srv.user = credentials;
            }
            else
            {
                srv.user = credentials.substr(0, pos);
                srv.pass = credentials.substr(pos + 1);
            }
        }
    }

    return srv;
}

//------------------------------------------------------------------------------
/**
 * @brief Create dependencies for a Peer Connection
 *
 * @return the dependencies
 */
webrtc::PeerConnectionFactoryDependencies util_webrtc::create_peer_connection_factory_dependencies(
    rtc::Thread *signaling_thread,
    rtc::Thread *worker_thread,
    rtc::scoped_refptr<webrtc::AudioDeviceModule> audio_device_module)
{
    bool hw_encoder = true;

    // Optional
    try
    {
        hw_encoder = util::get_cfg_value(CONFIG_HW_ENCODER).getTag() == gason::JSON_TRUE;
    }
    catch (const std::string &err)
    {
    }

    webrtc::PeerConnectionFactoryDependencies dependencies;
    dependencies.network_thread = nullptr;
    dependencies.worker_thread = worker_thread;
    dependencies.signaling_thread = signaling_thread;
    dependencies.task_queue_factory = webrtc::CreateDefaultTaskQueueFactory();
    dependencies.event_log_factory =
        absl::make_unique<webrtc::RtcEventLogFactory>(dependencies.task_queue_factory.get());
    dependencies.adm = std::move(audio_device_module);
    rtc::scoped_refptr<webrtc::AudioEncoderFactory> audio_encoder_factory =
        webrtc::CreateBuiltinAudioEncoderFactory();
    rtc::scoped_refptr<webrtc::AudioDecoderFactory> audio_decoder_factory =
        webrtc::CreateBuiltinAudioDecoderFactory();
    std::unique_ptr<webrtc::VideoEncoderFactory> video_encoder_factory = std::make_unique<
        webrtc::VideoEncoderFactoryTemplate<webrtc::LibvpxVp8EncoderTemplateAdapter,
                                            webrtc::LibvpxVp9EncoderTemplateAdapter>>();

#ifndef DEBUG_MODE
    if (hw_encoder)
    {
        video_encoder_factory = std::unique_ptr<webrtc::VideoEncoderFactory>(
            absl::make_unique<HWVideoEncoderFactory>());
    }
#else
    UNUSED(hw_encoder);
#endif // DEBUG_MODE
    std::unique_ptr<webrtc::VideoDecoderFactory> video_decoder_factory = std::make_unique<
        webrtc::VideoDecoderFactoryTemplate<webrtc::LibvpxVp8DecoderTemplateAdapter,
                                            webrtc::LibvpxVp9DecoderTemplateAdapter>>();

    dependencies.audio_encoder_factory = audio_encoder_factory;
    dependencies.audio_decoder_factory = audio_decoder_factory;
    dependencies.video_encoder_factory = std::move(video_encoder_factory);
    dependencies.video_decoder_factory = std::move(video_decoder_factory);

    webrtc::EnableMedia(dependencies);

    return dependencies;
}
