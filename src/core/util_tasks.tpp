/************************************************************************
 NestoRPi Engine
 Copyright (C) 2015-2025 Franck Talbart

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

/* Author: Franck Talbart */

/**
 * This module provides functions to load the tasks
 */

#ifndef DEF_UTIL_TASKS
#define DEF_UTIL_TASKS

#include "src/core/Log.hpp"
#include "src/core/util.hpp"
#include FILENAME_INCLUDE_TASKS
#include "src/core/Factory.hpp"

extern Factory g_factory;

//------------------------------------------------------------------------------
/**
 * @brief Load the tasks and returns the corresponding map
 *
 * @return  map of tasks
 */

template <typename T>
std::map<std::string, std::shared_ptr<T>> load_tasks(const std::string &filename)
{
#include FILENAME_LIST_TASKS // user has to write his tasks in
                             // the corresponding directory
                             // it will be automatically
                             // included in the project

    // Get the list of tasks
    const std::string path = util::get_cfg_dir() + filename;

    // We keep the content during the whole process's life because the data
    // may be accessed at anytime

    static std::shared_ptr<char> file_content = nullptr;
    try
    {
        file_content = util::prepare_file(path);
    }
    catch (const std::string &msg)
    {
        throw std::string("Cannot open the file");
    }

    char *endptr = nullptr;
    gason::JsonValue root;

    // We keep the allocator during the whole process's life because the data
    // may be accessed at anytime
    static gason::JsonAllocator allocator;
    int status = gason::jsonParse(file_content.get(), &endptr, &root, allocator);

    if (status != gason::JSON_PARSE_OK)
    {
        NRPI_LOG(ERROR) << "Error while parsing the JSON file " << path;
        throw std::string("wrong json file");
    }

    std::map<std::string, std::shared_ptr<T>> result;

    for (const auto &value : root) // C++ 11 :)
    {
        std::string instance_task_name = value->key;
        std::string task_type;
        val_json_t cfg_task;

        for (const auto &it : value->value)
        {
            if (it->key == std::string(JSON_TASKS_TYPE))
            {
                task_type = std::string(static_cast<char *>(it->value.toString()));
                task_type[0] = toupper(task_type[0]);
                task_type = T::JSON_TASKS_TYPE_VALUE + task_type;
                task_type[0] = toupper(task_type[0]);
            }
            else
            {
                cfg_task[std::string(it->key)] = it->value;
            }
        }

        if (!task_type.empty())
        {
            T *task = nullptr;
            try
            {
                // Create the task instance thanks to the factory
                task = dynamic_cast<T *>(
                    g_factory.construct_task(task_type)(instance_task_name, cfg_task));
            }
            catch (const std::string &msg)
            {
                NRPI_LOG(ERROR) << msg;
                throw;
            }
            result[instance_task_name] = std::shared_ptr<T>(task);
        }
        else
        {
            NRPI_LOG(ERROR) << "Type of the task " << instance_task_name
                            << " is missing in the config file.";
        }
    }
    return result;
}

#endif // DEF_UTIL_TASKS
