/************************************************************************
 NestoRPi Engine
 Copyright (C) 2015-2025 Franck Talbart

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

/* Author: Franck Talbart
   webrtc-streamer written by Michel Promonet
   and updated for the NestoRPi Engine */

#include "src/core/HttpServerRequestHandler.hpp"

#include <string.h>

#include <iostream>

#include "src/core/Log.hpp"

//------------------------------------------------------------------------------
/**
 * @brief log function used for a callback
 *
 * @param conn: unused
 * @param message: the message to display
 * @return 0
 */
static int log_message(const struct mg_connection *conn, const char *message)
{
    UNUSED(conn);
    if (!strstr(message, "alert certificate unknown"))
        NRPI_LOG(ERROR) << message;
    return 0;
}

//------------------------------------------------------------------------------
/**
 * @brief return civet callback
 *
 * @return call back
 */
const struct CivetCallbacks *get_civet_callbacks(void)
{
    _callbacks.log_message = &log_message;
    return &_callbacks;
}

//------------------------------------------------------------------------------
/**
 * @brief HttpServerRequestHandler constructor
 *
 * @param func
 * @param options
 */
HttpServerRequestHandler::HttpServerRequestHandler(std::map<std::string, httpFunction> &func,
                                                   const std::vector<std::string> &options)
    : CivetServer(options, get_civet_callbacks()), func(func)
{
    // register handlers
    for (auto it : func)
    {
        request_handlers.push_back(std::shared_ptr<RequestHandler>(new RequestHandler()));
        this->addHandler(it.first, request_handlers.back().get());
    }
}

//------------------------------------------------------------------------------
/**
 * @brief function getter
 *
 * @param uri: used to selection the function
 * @return the function
 */
HttpServerRequestHandler::httpFunction HttpServerRequestHandler::get_function(
    const std::string &uri)
{
    httpFunction fct = nullptr;
    std::map<std::string, httpFunction>::iterator it = func.find(uri);
    if (it != func.end())
        fct = it->second;

    return fct;
}
