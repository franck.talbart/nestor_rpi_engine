/************************************************************************
 NestoRPi Engine
 Copyright (C) 2015-2025 Franck Talbart

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

/* Author: Franck Talbart */

#include "api/video/i420_buffer.h"

#include "api/make_ref_counted.h"
#include "src/core/i420_buffer.hpp"

namespace webrtc
{
//------------------------------------------------------------------------------
NRPI_I420Buffer::NRPI_I420Buffer(int width, int height)
    : width_(width),
      height_(height),
      stride_y_(width),
      stride_u_((width + 1) / 2),
      stride_v_((width + 1) / 2),
      data_(nullptr)
{
}

//------------------------------------------------------------------------------
NRPI_I420Buffer::~NRPI_I420Buffer() {}

//------------------------------------------------------------------------------
rtc::scoped_refptr<NRPI_I420Buffer> NRPI_I420Buffer::Create(int width, int height)
{
    return rtc::make_ref_counted<NRPI_I420Buffer>(width, height);
}

//------------------------------------------------------------------------------
int NRPI_I420Buffer::width() const { return width_; }

//------------------------------------------------------------------------------
int NRPI_I420Buffer::height() const { return height_; }

//------------------------------------------------------------------------------
const uint8_t* NRPI_I420Buffer::DataY() const { return data_; }

//------------------------------------------------------------------------------
void NRPI_I420Buffer::SetDataY(uint8_t* data) { data_ = data; }

//------------------------------------------------------------------------------
// cppcheck-suppress unusedFunction
const uint8_t* NRPI_I420Buffer::DataU() const { return data_ + stride_y_ * height_; }

//------------------------------------------------------------------------------
// cppcheck-suppress unusedFunction
const uint8_t* NRPI_I420Buffer::DataV() const
{
    return data_ + stride_y_ * height_ + stride_u_ * ((height_ + 1) / 2);
}

//------------------------------------------------------------------------------
// cppcheck-suppress unusedFunction
int NRPI_I420Buffer::StrideY() const { return stride_y_; }

//------------------------------------------------------------------------------
// cppcheck-suppress unusedFunction
int NRPI_I420Buffer::StrideU() const { return stride_u_; }

//------------------------------------------------------------------------------
// cppcheck-suppress unusedFunction
int NRPI_I420Buffer::StrideV() const { return stride_v_; }

} // namespace webrtc
