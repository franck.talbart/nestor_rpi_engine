/************************************************************************
 NestoRPi Engine
 Copyright (C) 2015-2025 Franck Talbart

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

/* Author: Franck Talbart */

#include <memory>
#include <string>

#include "api/peer_connection_interface.h"

#ifndef DEF_CAMERA
#define DEF_CAMERA

class Camera // Singleton
{
  public:
    static std::shared_ptr<Camera> get_instance(const short id);
    static short get_nb_devices(void);
    virtual std::string get_name(void) = 0;
    virtual std::string capture_pic(void) = 0;
    virtual rtc::scoped_refptr<webrtc::VideoTrackSourceInterface> get_video_source(void) = 0;
    virtual void start(void) = 0;
    virtual void stop(void) = 0;

    // Make sure they
    // are unacceptable otherwise you may accidentally get copies of
    // your singleton appearing.
    Camera(Camera const&) = delete;
    virtual ~Camera() = default;
    void operator=(Camera const&) = delete;

  protected:
    explicit Camera(){};

  private:
    static short nb_devices;
};

#endif // DEF_CAMERA
