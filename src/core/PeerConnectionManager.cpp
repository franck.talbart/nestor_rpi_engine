/************************************************************************
 NestoRPi Engine
 Copyright (C) 2015-2025 Franck Talbart

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

/* Author: Franck Talbart */

#include "src/core/PeerConnectionManager.hpp"

#include <algorithm>
#include <fstream>
#include <functional>
#include <iostream>
#include <utility>

#include "api/task_queue/default_task_queue_factory.h"
#include "modules/audio_device/include/fake_audio_device.h"
#include "src/core/Camera.hpp"
#include "src/core/CapturerFactory.hpp"
#include "src/core/Libcamera.hpp"
#include "src/core/Log.hpp"
#include "src/core/V4l2.hpp"
#include "src/core/const.hpp"
#include "src/core/util_webrtc.hpp"

// Static variables
// Names used for a IceCandidate JSON object.
const char PeerConnectionManager::CANDIDATE_SDP_MID_NAME[] = "sdpMid";
const char PeerConnectionManager::CANDIDATE_SDP_MLINE_INDEX_NAME[] = "sdpMLineIndex";
const char PeerConnectionManager::CANDIDATE_SDP_NAME[] = "candidate";

// Names used for a SessionDescription JSON object.
const char PeerConnectionManager::SESSION_DESCRIPTION_TYPE_NAME[] = "type";
const char PeerConnectionManager::SESSION_DESCRIPTION_SDP_NAME[] = "sdp";

//------------------------------------------------------------------------------
/**
 * @brief Constructor
 *
 * @param ice_server_list: list of str containing the stun / turn server(s)
 * @param audio_layer: AudioLayer object
 * @param tasks_controller: map of Controller
 * @param tasks_automation: map of Automation
 *
 */
PeerConnectionManager::PeerConnectionManager(
    const std::list<std::string> &ice_server_list,
    const webrtc::AudioDeviceModule::AudioLayer audio_layer,
    const tasks_controller_t &tasks_controller,
    const tasks_automation_t &tasks_automation)
    : task_queue_factory(webrtc::CreateDefaultTaskQueueFactory()),
      audio_device_module(webrtc::AudioDeviceModule::Create(audio_layer, task_queue_factory.get())),
      ice_server_list(ice_server_list),
      tasks_controller(tasks_controller),
      tasks_automation(tasks_automation)
{
    signaling_thread = rtc::Thread::Create();
    worker_thread = rtc::Thread::Create();
    worker_thread->SetName("worker", nullptr);
    worker_thread->Start();
    worker_thread->BlockingCall([this, audio_layer] { this->create_audio_module(audio_layer); });

    signaling_thread->SetName("signaling", nullptr);
    signaling_thread->Start();
    peer_connection_factory = webrtc::CreateModularPeerConnectionFactory(
        util_webrtc::create_peer_connection_factory_dependencies(
            signaling_thread.get(), worker_thread.get(), audio_device_module));
}

//------------------------------------------------------------------------------
PeerConnectionManager::~PeerConnectionManager(void)
{
    for (auto it = stream_map.cbegin(); it != stream_map.cend();)
    {
        std::string stream_label = it->first;
        std::lock_guard<std::mutex> mlock(stream_map_mutex);
        stream_map[stream_label].first->stop();
        it = stream_map.erase(it);
    }
}

//------------------------------------------------------------------------------
void PeerConnectionManager::create_audio_module(webrtc::AudioDeviceModule::AudioLayer audio_layer)
{
    audio_device_module = webrtc::AudioDeviceModule::Create(audio_layer, task_queue_factory.get());
    if (audio_device_module->Init() != 0)
    {
        NRPI_LOG(WARN) << "audio init fails -> disable audio capture";
        audio_device_module = new webrtc::FakeAudioDeviceModule();
    }
}

//------------------------------------------------------------------------------
/**
 * @brief Returns video device List as JSON list
 *
 * @return video device list
 */
const Json::Value PeerConnectionManager::getVideoDeviceList(void)
{
    Json::Value value(Json::arrayValue);

    std::list<std::string> video_capture_device = V4l2::get_video_capture_device_list();
    for (const auto &video_device : video_capture_device) value.append(video_device);
    video_capture_device = Libcamera::get_video_capture_device_list();
    for (const auto &video_device : video_capture_device) value.append(video_device);

    return value;
}

//------------------------------------------------------------------------------
/**
 * @brief Returns audio device List as JSON list
 *
 * @return audio device list
 */
const Json::Value PeerConnectionManager::getAudioDeviceList(void)
{
    Json::Value value(Json::arrayValue);
    const std::list<std::string> audio_capture_device =
        CapturerFactory::get_audio_capture_device_list(audio_device_module);
    for (const auto &audio_device : audio_capture_device) value.append(audio_device);

    return value;
}

//------------------------------------------------------------------------------
/**
 * @brief Returns iceServer as JSON list
 *
 * @param client_ip: the client TP
 * @return iceServer
 */
const Json::Value PeerConnectionManager::getIceServers(const std::string &client_ip)
{
    Json::Value urls(Json::arrayValue);

    for (const auto &ice_server : ice_server_list)
    {
        Json::Value server;
        Json::Value url_list(Json::arrayValue);
        util_webrtc::IceServer srv = util_webrtc::get_ice_server_from_url(ice_server, client_ip);
        NRPI_LOG(DEBUG) << "ICE URL:" << srv.url;
        url_list.append(srv.url);
        server["urls"] = url_list;
        if (srv.user.length() > 0)
            server["username"] = srv.user;
        if (srv.pass.length() > 0)
            server["credential"] = srv.pass;
        urls.append(server);
    }

    Json::Value ice_servers;
    ice_servers["iceServers"] = urls;

    return ice_servers;
}

//------------------------------------------------------------------------------
/**
 * @brief Returns the PeerConnection based on the peer ID
 *
 * @param peer_id: the peer ID
 * @return peer connection
 */
rtc::scoped_refptr<webrtc::PeerConnectionInterface> PeerConnectionManager::getPeerConnection(
    const std::string &peer_id)
{
    rtc::scoped_refptr<webrtc::PeerConnectionInterface> peer_connection;
    std::map<std::string, std::shared_ptr<PeerConnectionObserver>>::iterator it =
        peer_connection_obs_map.find(peer_id);
    if (it != peer_connection_obs_map.end())
    {
        peer_connection = it->second->getPeerConnection().get();
    }
    return peer_connection;
}

//------------------------------------------------------------------------------
/**
 * @brief Add a ICE candidate to a peer connection
 *
 * @param peer_id: the peer ID
 * @param jmessage: the message to process
 * @return JSON: true if success, false otherwhise
 */
const Json::Value PeerConnectionManager::addIceCandidate(const std::string &peer_id,
                                                         const Json::Value &jmessage)
{
    bool result = false;
    std::string sdp_mid;
    int sdp_mlineindex = 0;
    std::string sdp;
    if (!rtc::GetStringFromJsonObject(jmessage, CANDIDATE_SDP_MID_NAME, &sdp_mid) ||
        !rtc::GetIntFromJsonObject(jmessage, CANDIDATE_SDP_MLINE_INDEX_NAME, &sdp_mlineindex) ||
        !rtc::GetStringFromJsonObject(jmessage, CANDIDATE_SDP_NAME, &sdp))
    {
        NRPI_LOG(WARN) << "Can't parse received message:" << jmessage;
    }
    else
    {
        std::unique_ptr<webrtc::IceCandidateInterface> candidate(
            webrtc::CreateIceCandidate(sdp_mid, sdp_mlineindex, sdp, nullptr));
        if (!candidate.get())
        {
            NRPI_LOG(WARN) << "Can't parse received candidate message.";
        }
        else
        {
            std::lock_guard<std::mutex> peerlock(peer_map_mutex);
            rtc::scoped_refptr<webrtc::PeerConnectionInterface> peer_connection =
                this->getPeerConnection(peer_id);
            if (peer_connection)
            {
                if (!peer_connection->AddIceCandidate(candidate.get()))
                {
                    NRPI_LOG(WARN) << "Failed to apply the received candidate";
                }
                else
                {
                    result = true;
                }
            }
        }
    }
    Json::Value answer;
    if (result)
        answer = result;

    return answer;
}

//------------------------------------------------------------------------------
/**
 * @brief anto-answer to a call
 *
 * @param peer_id: the peer ID
 * @param options: options of the local stream
 * @param jmessage: the message to process
 * @return JSON answer
 */
const Json::Value PeerConnectionManager::call(const std::string &peer_id,
                                              const std::string &options,
                                              const Json::Value &jmessage)
{
    Json::Value answer;
    std::string sdp;

    if (!rtc::GetStringFromJsonObject(jmessage, SESSION_DESCRIPTION_SDP_NAME, &sdp))
    {
        NRPI_LOG(WARN) << "Can't parse received message.";
    }
    else
    {
        std::istringstream is(options);
        std::map<std::string, std::string> opts;
        std::string key, value;

        while (std::getline(std::getline(is, key, '='), value, ';')) opts[key] = value;

        std::string username = "";
        if (opts.find("username") != opts.end())
        {
            username = opts.at("username");
        }
        std::string password = "";
        if (opts.find("password") != opts.end())
        {
            password = opts.at("password");
        }
        role_type role;
        try
        {
            role = util::login(username, password);
        }
        catch (const std::string &err)
        {
            NRPI_LOG(ERROR) << err;
            return answer;
        }

        std::string token = "";
        if (opts.find("token") != opts.end())
        {
            token = opts.at("token");
        }

        std::unique_ptr<webrtc::SessionDescriptionInterface> session_description(
            webrtc::CreateSessionDescription(webrtc::SdpType::kOffer, sdp, nullptr));
        if (!session_description)
        {
            NRPI_LOG(WARN) << "Can't parse received session description message.";
            return answer;
        }

        std::shared_ptr<PeerConnectionObserver> peer_connection_observer =
            this->CreatePeerConnection(peer_id, role, token);
        if (!peer_connection_observer)
        {
            NRPI_LOG(ERROR) << "Failed to initialize PeerConnectionObserver";
        }
        else if (!peer_connection_observer->getPeerConnection().get())
        {
            NRPI_LOG(ERROR) << "Failed to initialize PeerConnection";
        }
        else
        {
            rtc::scoped_refptr<webrtc::PeerConnectionInterface> peer_connection =
                peer_connection_observer->getPeerConnection();
            NRPI_LOG(DEBUG) << "nbStreams local:" << peer_connection->GetSenders().size()
                            << " remote:" << peer_connection->GetReceivers().size()
                            << " localDescription:" << peer_connection->local_description();

            // register peer id
            {
                std::lock_guard<std::mutex> peerlock(peer_map_mutex);
                peer_connection_obs_map.insert(
                    std::pair<std::string, std::shared_ptr<PeerConnectionObserver>>(
                        peer_id, peer_connection_observer));
            }

            // add local stream
            if (!this->AddStreams(peer_connection.get(), options))
            {
                NRPI_LOG(WARN) << "Can't add stream";
            }

            // set remote offer
            std::promise<const webrtc::SessionDescriptionInterface *> remote_promise;
            rtc::scoped_refptr<PeerConnectionManager::SetSessionDescriptionObserver>
                remote_session_observer(
                    PeerConnectionManager::SetSessionDescriptionObserver::Create(
                        peer_connection.get(), remote_promise));
            peer_connection->SetRemoteDescription(remote_session_observer.get(),
                                                  session_description.release());
            // waiting for remote description
            std::future<const webrtc::SessionDescriptionInterface *> remote_future =
                remote_promise.get_future();
            if (remote_future.wait_for(std::chrono::milliseconds(5000)) ==
                std::future_status::ready)
            {
                NRPI_LOG(DEBUG) << "remote_description is ready";
            }
            else
            {
                remote_session_observer->cancel();
                NRPI_LOG(WARN) << "remote_description is nullptr";
            }

            // create answer
            webrtc::PeerConnectionInterface::RTCOfferAnswerOptions rtc_options;
            std::promise<const webrtc::SessionDescriptionInterface *> local_promise;
            rtc::scoped_refptr<CreateSessionDescriptionObserver> local_session_observer(
                CreateSessionDescriptionObserver::Create(peer_connection.get(), local_promise));
            peer_connection->CreateAnswer(local_session_observer.get(), rtc_options);

            // waiting for answer
            std::future<const webrtc::SessionDescriptionInterface *> localfuture =
                local_promise.get_future();
            if (localfuture.wait_for(std::chrono::milliseconds(5000)) == std::future_status::ready)
            {
                // answer with the created answer
                const webrtc::SessionDescriptionInterface *desc = localfuture.get();
                if (desc)
                {
                    std::string local_sdp;
                    desc->ToString(&local_sdp);

                    answer[SESSION_DESCRIPTION_TYPE_NAME] = desc->type();
                    answer[SESSION_DESCRIPTION_SDP_NAME] = local_sdp;
                }
                else
                {
                    NRPI_LOG(ERROR) << "Failed to create answer";
                }
            }
            else
            {
                NRPI_LOG(ERROR) << "Failed to create answer";
                local_session_observer->cancel();
            }
        }
    }
    return answer;
}

//------------------------------------------------------------------------------
/**
 * @brief Check if a stream is still used or not
 *
 * @param stream_label: the label of the stream to test
 * @return true if still used, false otherwhise
 */
bool PeerConnectionManager::streamStillUsed(const std::string &stream_label)
{
    bool still_used = false;
    for (const auto &it : peer_connection_obs_map)
    {
        rtc::scoped_refptr<webrtc::PeerConnectionInterface> peer_connection =
            it.second->getPeerConnection();
        std::vector<rtc::scoped_refptr<webrtc::RtpSenderInterface>> localstreams =
            peer_connection->GetSenders();
        for (auto stream : localstreams)

        {
            std::vector<std::string> stream_vector = stream->stream_ids();
            if (stream_vector.size() > 0)
            {
                if (stream_vector[0] == stream_label)
                {
                    still_used = true;
                    break;
                }
            }
        }
    }

    return still_used;
}

//------------------------------------------------------------------------------
/**
 * @brief Hang up a call
 *
 * @param peer_id: the peer ID
 * @param user: usename
 * @param passwd: hashed password
 * @param token: the call ID
 * @return JSON answer (true if success, false otherwhise)
 */
const Json::Value PeerConnectionManager::hangUp(const std::string &peer_id,
                                                const std::string &user,
                                                const std::string &passwd,
                                                const std::string &token)
{
    bool result = false;
    role_type role;

    try
    {
        role = util::login(user, passwd);
    }
    catch (const std::string &err)
    {
        NRPI_LOG(ERROR) << err;
        Json::Value answer = result;
        return answer;
    }
    std::shared_ptr<PeerConnectionObserver> pc_observer;
    {
        std::lock_guard<std::mutex> peerlock(peer_map_mutex);
        std::map<std::string, std::shared_ptr<PeerConnectionObserver>>::iterator it_pco =
            peer_connection_obs_map.find(peer_id);
        if (it_pco != peer_connection_obs_map.end())
        {
            pc_observer = it_pco->second;
            if (role != FULL && token != pc_observer->getToken())
            {
                NRPI_LOG(ERROR) << "The user does not have the permission to hang up!";
                Json::Value answer = result;
                return answer;
            }
            NRPI_LOG(INFO) << "Remove PeerConnection peer id:" << peer_id;
            peer_connection_obs_map.erase(it_pco);
        }

        if (pc_observer != nullptr)
        {
            rtc::scoped_refptr<webrtc::PeerConnectionInterface> peer_connection =
                pc_observer->getPeerConnection();

            std::vector<rtc::scoped_refptr<webrtc::RtpSenderInterface>> localstreams =
                peer_connection->GetSenders();
            for (auto stream : localstreams)
            {
                std::vector<std::string> stream_vector = stream->stream_ids();
                if (stream_vector.size() > 0)
                {
                    std::string stream_label = stream_vector[0];
                    bool still_used = this->streamStillUsed(stream_label);

                    if (!still_used)
                    {
                        NRPI_LOG(INFO) << "hangUp stream is no more used " << stream_label;
                        std::lock_guard<std::mutex> mlock(stream_map_mutex);
                        std::map<std::string,
                                 std::pair<std::shared_ptr<Camera>,
                                           rtc::scoped_refptr<webrtc::AudioSourceInterface>>>::
                            iterator it_str = stream_map.find(stream_label);
                        if (it_str != stream_map.end())
                        {
                            stream_map[stream_label].first->stop();
                            stream_map.erase(it_str);
                        }

                        NRPI_LOG(INFO) << "hangUp stream closed " << stream_label;
                    }
                    peer_connection->RemoveTrackOrError(stream);
                }
            }

            // stream_map may not be empty at this point if multicam feature was used
            for (auto it = stream_map.cbegin(); it != stream_map.cend();)
            {
                std::string stream_label = it->first;
                bool still_used = this->streamStillUsed(stream_label);
                if (!still_used)
                {
                    std::lock_guard<std::mutex> mlock(stream_map_mutex);
                    stream_map[stream_label].first->stop();
                    it = stream_map.erase(it);
                }
                else
                {
                    ++it;
                }
            }
            result = true;
        }
    }
    Json::Value answer;
    if (result)
    {
        answer = result;
    }

    NRPI_LOG(DEBUG) << "Peer id: " << peer_id << " result:" << result;
    return answer;
}

//------------------------------------------------------------------------------
/**
 * @brief Get the list of ICE candidates based on the peer ID
 *
 * @param peer_id: the peer ID
 * @return list of ICE candidates
 */
const Json::Value PeerConnectionManager::getIceCandidateList(const std::string &peer_id)
{
    Json::Value value;
    std::lock_guard<std::mutex> peerlock(peer_map_mutex);
    std::map<std::string, std::shared_ptr<PeerConnectionObserver>>::iterator it =
        peer_connection_obs_map.find(peer_id);
    if (it != peer_connection_obs_map.end())
    {
        std::shared_ptr<PeerConnectionObserver> obs = it->second;
        if (obs)
        {
            value = obs->getIceCandidateList();
        }
        else
        {
            NRPI_LOG(DEBUG) << "No observer for peer:" << peer_id;
        }
    }
    return value;
}

//------------------------------------------------------------------------------
/**
 * @brief Get the list of PeerConnection
 *
 * @return list of peer connections
 */
const Json::Value PeerConnectionManager::getPeerConnectionList(void)
{
    Json::Value value(Json::arrayValue);
    std::lock_guard<std::mutex> peerlock(peer_map_mutex);
    for (const auto &it : peer_connection_obs_map)
    {
        Json::Value content;

        // get local SDP
        rtc::scoped_refptr<webrtc::PeerConnectionInterface> peer_connection =
            it.second->getPeerConnection();
        if ((peer_connection) && (peer_connection->local_description()))
        {
            content["pc_state"] = (int)(peer_connection->peer_connection_state());
            content["signaling_state"] = (int)(peer_connection->signaling_state());
            content["ice_state"] = (int)(peer_connection->ice_connection_state());

            std::string sdp;
            peer_connection->local_description()->ToString(&sdp);
            content["sdp"] = sdp;

            Json::Value streams;
            std::vector<rtc::scoped_refptr<webrtc::RtpSenderInterface>> localstreams =
                peer_connection->GetSenders();
            for (auto local_stream : localstreams)
            {
                if (local_stream != nullptr)
                {
                    rtc::scoped_refptr<webrtc::MediaStreamTrackInterface> media_track =
                        local_stream->track();
                    if (media_track)
                    {
                        Json::Value track;
                        track["kind"] = media_track->kind();
                        if (track["kind"] == "video")
                        {
                            webrtc::VideoTrackInterface *video_track =
                                (webrtc::VideoTrackInterface *)media_track.get();
                            webrtc::VideoTrackSourceInterface::Stats stats;
                            if (video_track->GetSource())
                            {
                                track["state"] = video_track->GetSource()->state();
                                if (video_track->GetSource()->GetStats(&stats))
                                {
                                    track["width"] = stats.input_width;
                                    track["height"] = stats.input_height;
                                }
                            }
                        }
                        else if (track["kind"] == "audio")
                        {
                            webrtc::AudioTrackInterface *audio_track =
                                (webrtc::AudioTrackInterface *)media_track.get();
                            if (audio_track->GetSource())
                            {
                                track["state"] = audio_track->GetSource()->state();
                            }
                            int level = 0;
                            if (audio_track->GetSignalLevel(&level))
                            {
                                track["level"] = level;
                            }
                        }

                        Json::Value tracks;
                        tracks[media_track->id()] = track;
                        std::string stream_label = local_stream->stream_ids()[0];
                        streams[stream_label] = tracks;
                    }
                }
            }

            content["streams"] = streams;
        }

        Json::Value pc;
        pc[it.first] = content;
        value.append(pc);
    }
    return value;
}

//------------------------------------------------------------------------------
/**
 * @brief Get the list of Stream
 *
 * @return list of streams
 */
const Json::Value PeerConnectionManager::getStreamList(void)
{
    std::lock_guard<std::mutex> mlock(stream_map_mutex);
    Json::Value value(Json::arrayValue);
    for (const auto &it : stream_map)
    {
        value.append(it.first);
    }
    return value;
}

//------------------------------------------------------------------------------
/**
 * @brief Check if factory is initialized
 *
 * @return true if initialized, false otherwhise
 */
bool PeerConnectionManager::InitializePeerConnection(void)
{
    return (peer_connection_factory.get() != nullptr);
}

//------------------------------------------------------------------------------
/**
 * @brief Create a new PeerConnection
 *
 * @param peer_id
 * @param role: user's role
 * @param token: call id
 * @return the associated peer connection observer
 */
std::shared_ptr<PeerConnectionManager::PeerConnectionObserver>
PeerConnectionManager::CreatePeerConnection(const std::string &peer_id,
                                            const role_type &role,
                                            const std::string &token)
{
    webrtc::PeerConnectionInterface::RTCConfiguration config;
    config.sdp_semantics = webrtc::SdpSemantics::kUnifiedPlan;
    for (const auto &ice_server : ice_server_list)
    {
        webrtc::PeerConnectionInterface::IceServer server;
        util_webrtc::IceServer srv = util_webrtc::get_ice_server_from_url(ice_server);
        server.uri = srv.url;
        server.username = srv.user;
        server.password = srv.pass;
        config.servers.push_back(server);
    }

    NRPI_LOG(DEBUG) << "CreatePeerConnection peer id:" << peer_id;
    PeerConnectionObserver *obs = new PeerConnectionObserver(this, peer_id, config, role, token);
    if (!obs)
    {
        NRPI_LOG(ERROR) << "CreatePeerConnection failed";
    }
    return std::shared_ptr<PeerConnectionObserver>(obs);
}

//------------------------------------------------------------------------------
/**
 * @brief Character to remove from URL to make web RTC label
 *
 * @param c: the char to test
 * @return true if should be removed, false otherwhise
 */
static bool ignore_in_label(char c)
{
    return c == ' ' || c == ':' || c == '.' || c == '/' || c == '&';
}

//------------------------------------------------------------------------------
/**
 * @brief transform the label to make it compliant with the contraints
 *
 * @param label: the label to sanitize
 * @return the sanitized label
 */
const std::string PeerConnectionManager::sanitizeLabel(const std::string &label)
{
    std::string out(label);
    out.erase(std::remove_if(out.begin(), out.end(), ignore_in_label), out.end());
    return out;
}

//------------------------------------------------------------------------------
/**
 * @brief Wrapper to create the audio source
 *
 * @param audio_url: the audio URL
 * @return the audio source
 */
rtc::scoped_refptr<webrtc::AudioSourceInterface> PeerConnectionManager::CreateAudioSource(
    const std::string &audio_url)
{
    NRPI_LOG(DEBUG) << "audio_url:" << audio_url;
    if (audio_sources.find(audio_url) == audio_sources.end())
    {
        rtc::scoped_refptr<webrtc::AudioSourceInterface> audio_source = worker_thread->BlockingCall(
            [this, audio_url]
            {
                return CapturerFactory::create_audio_source(
                    audio_url, peer_connection_factory, audio_device_module);
            });

        audio_sources[audio_url] = audio_source;
    }

    return audio_sources[audio_url];
}

//------------------------------------------------------------------------------
/**
 * @brief Add a stream to a PeerConnection
 *
 * @param peer_connection: the peer connection
 * @param options: the options
 * @return true in case of success, false otherwhise
 */
bool PeerConnectionManager::AddStreams(webrtc::PeerConnectionInterface *peer_connection,
                                       const std::string &options)
{
    bool ret = false;
    std::string video_url;
    std::string audio_url;

    std::istringstream is(options);
    std::map<std::string, std::string> opts;
    std::string key, value;

    while (std::getline(std::getline(is, key, '='), value, ';')) opts[key] = value;
    std::string username = "";
    if (opts.find("username") != opts.end())
        username = opts.at("username");
    std::string password = "";
    if (opts.find("password") != opts.end())
        password = opts.at("password");
    role_type role;

    try
    {
        role = util::login(username, password);
    }
    catch (const std::string &err)
    {
        NRPI_LOG(ERROR) << err;
        return false;
    }

    short dev_id = 0;
    if (opts.find("device_id") != opts.end())
        dev_id = std::stoi(opts.at("device_id"));

    std::shared_ptr<Camera> camera;
    try
    {
        camera = Camera::get_instance(dev_id);
        video_url = camera->get_name();
    }
    catch (const std::string &err)
    {
        NRPI_LOG(ERROR) << err;
        return false;
    }

    try
    {
        audio_url = util::get_cfg_value(CONFIG_AUDIO_DEV).toString();
    }
    catch (const std::string &err)
    {
        audio_url = "";
    }

    bool ret_tmp = true;

    std::list<std::string> video_device_list = V4l2::get_video_capture_device_list();
    bool found = (std::find(video_device_list.begin(), video_device_list.end(), video_url) !=
                  video_device_list.end());

    if (!found && video_url != "")
    {
        NRPI_LOG(ERROR) << "Video device not found! " << video_url << " List: ";
        for (const auto &video_device : video_device_list) NRPI_LOG(ERROR) << video_device;
        ret_tmp = false;
    }

    std::list<std::string> audio_device_list =
        CapturerFactory::get_audio_capture_device_list(audio_device_module);
    found = (std::find(audio_device_list.begin(), audio_device_list.end(), audio_url) !=
             audio_device_list.end());
    if (!found && audio_url != "")
    {
        NRPI_LOG(ERROR) << "Audio device not found! " << audio_url << " List: ";
        for (const auto &audio_device : audio_device_list) NRPI_LOG(ERROR) << audio_device;
    }
    if (!ret_tmp)
        return false;

    // compute stream label removing space because SDP use label
    std::string stream_label = this->sanitizeLabel(video_url + "|" + audio_url + "|" + options);

    bool existing_stream = false;
    {
        std::lock_guard<std::mutex> mlock(stream_map_mutex);
        existing_stream = (stream_map.find(stream_label) != stream_map.end());
    }

    bool disable_audio = false, disable_video = false;
    if (audio_url == "")
    {
        disable_audio = true;
        NRPI_LOG(INFO) << "No audio url provided";
    }
    if (!existing_stream)
    {
        // set bandwidth
        if (opts.find("bitrate") != opts.end())
        {
            try
            {
                int bitrate = std::stoi(opts.at("bitrate"));
                webrtc::BitrateSettings bitrate_param;
                bitrate_param.min_bitrate_bps = std::optional<int>(bitrate / 2);
                bitrate_param.start_bitrate_bps = std::optional<int>(bitrate);
                bitrate_param.max_bitrate_bps = std::optional<int>(bitrate * 2);

                peer_connection->SetBitrate(bitrate_param);
                NRPI_LOG(INFO) << "set bitrate:" << bitrate;
            }
            catch (const std::out_of_range &oor)
            {
                NRPI_LOG(ERROR) << "Bitrate too high" << std::endl;
            }
        }

        if (opts.find("disable_video") != opts.end())
        {
            disable_video = std::stoi(opts.at("disable_video"));
        }

        if (opts.find("disable_audio") != opts.end())
        {
            disable_audio = std::stoi(opts.at("disable_audio"));
        }

        // need to create the stream
        rtc::scoped_refptr<webrtc::VideoTrackSourceInterface> video_source;
        rtc::scoped_refptr<webrtc::AudioSourceInterface> audio_source;

        role_type role_minimal_audio = FULL, role_minimal_video = FULL;
        try
        {
            role_minimal_audio =
                util::str_to_role(util::get_cfg_value(CONFIG_ROLE_MINIMAL_AUDIO).toString());
            role_minimal_video =
                util::str_to_role(util::get_cfg_value(CONFIG_ROLE_MINIMAL_VIDEO).toString());
        }
        catch (const std::string &err)
        {
        }

        if (!disable_video && role < role_minimal_video)
            disable_video = true;

        if (!disable_audio && role < role_minimal_audio)
        {
            NRPI_LOG(INFO) << "The user does not have the permission to get the audio.";
            disable_audio = true;
        }

        if (!disable_video)
        {
            video_source = camera->get_video_source();
            if (!video_source)
            {
                NRPI_LOG(ERROR) << "Cannot create capturer video:" << video_url;
            }
            else
            {
                camera->start();
            }
        }

        if (!disable_audio)
        {
            audio_source = this->CreateAudioSource(audio_url);
            if (!audio_source)
                NRPI_LOG(ERROR) << "Cannot create capturer audio:" << audio_url;
        }

        NRPI_LOG(DEBUG) << "Adding Stream to map";
        std::lock_guard<std::mutex> mlock(stream_map_mutex);
        stream_map[stream_label] = std::make_pair(camera, audio_source);
    }
    std::map<std::string,
             std::pair<std::shared_ptr<Camera>,
                       rtc::scoped_refptr<webrtc::AudioSourceInterface>>>::iterator it;

    {
        std::lock_guard<std::mutex> mlock(stream_map_mutex);
        it = stream_map.find(stream_label);
    }
    if (it != stream_map.end())
    {
        std::pair<std::shared_ptr<Camera>, rtc::scoped_refptr<webrtc::AudioSourceInterface>> pair =
            it->second;
        rtc::scoped_refptr<webrtc::VideoTrackSourceInterface> video_source(
            pair.first->get_video_source());
        rtc::scoped_refptr<webrtc::VideoTrackInterface> video_track;

        if (video_source)
        {
            video_track =
                peer_connection_factory->CreateVideoTrack(video_source, stream_label + "_video");
        }

        if (!disable_video && video_track &&
            (!peer_connection->AddTrack(video_track, {stream_label}).ok()))
        {
            NRPI_LOG(ERROR) << "Adding VideoTrack to MediaStream failed";
        }
        else
        {
            NRPI_LOG(INFO) << "stream added to PeerConnection";
            ret = true;
        }

        rtc::scoped_refptr<webrtc::AudioSourceInterface> audio_source(pair.second);
        rtc::scoped_refptr<webrtc::AudioTrackInterface> audio_track;

        if (audio_source)
        {
            audio_track = peer_connection_factory->CreateAudioTrack(stream_label + "_audio",
                                                                    audio_source.get());
        }

        if (!disable_audio && audio_track &&
            (!peer_connection->AddTrack(audio_track, {stream_label}).ok()))
        {
            NRPI_LOG(ERROR) << "Adding AudioTrack to MediaStream failed";
        }
        else
        {
            NRPI_LOG(DEBUG) << "stream added to PeerConnection";
            ret = true;
        }
    }
    else
    {
        NRPI_LOG(ERROR) << "Cannot find stream";
    }

    return ret;
}

//------------------------------------------------------------------------------
/**
 * @brief ICE callback (event)
 *
 * @param candidate: the new ICE candidate to process
 */
// cppcheck-suppress unusedFunction
void PeerConnectionManager::PeerConnectionObserver::OnIceCandidate(
    const webrtc::IceCandidateInterface *candidate)
{
    NRPI_LOG(DEBUG) << candidate->sdp_mline_index();

    std::string sdp;
    if (!candidate->ToString(&sdp))
    {
        NRPI_LOG(ERROR) << "Failed to serialize candidate";
    }
    else
    {
        NRPI_LOG(DEBUG) << sdp;
        Json::Value jmessage;
        jmessage[CANDIDATE_SDP_MID_NAME] = candidate->sdp_mid();
        jmessage[CANDIDATE_SDP_MLINE_INDEX_NAME] = candidate->sdp_mline_index();
        jmessage[CANDIDATE_SDP_NAME] = sdp;
        ice_candidate_list.append(jmessage);
    }
}

//------------------------------------------------------------------------------
/**
 * @brief Returns the list of tasks controller
 * @return list of tasks controller
 */
tasks_controller_t PeerConnectionManager::getTasksController(void) { return tasks_controller; }
//------------------------------------------------------------------------------
/**
 * @brief Returns the list of tasks automation
 * @return list of tasks automation
 */
tasks_automation_t PeerConnectionManager::getTasksAutomation(void) { return tasks_automation; }
