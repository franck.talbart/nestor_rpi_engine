#!/bin/bash
#************************************************************************
# NestoRPi Engine
# Copyright (C) 2015-2025 Franck Talbart
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#************************************************************************

# Author: Franck Talbart
# This script is used to generate version.hpp

set -euo pipefail

export NRPI_VERSION_MAJOR="1"
export NRPI_VERSION_MINOR="20"
export NRPI_VERSION_HOT_FIXES="0"

export NRPI_VERSION_BUILD_NUMBER=`date --rfc-2822 | sed 's/ /_/g'`
export NRPI_VERSION_CMT_ID=`git rev-parse --short HEAD`

#----------------------------------
# Make version include file
NRPI_VERSION_H=$1
cp ${NRPI_VERSION_H}.template ${NRPI_VERSION_H}
echo "#define NRPI_VERSION_MAJOR \"${NRPI_VERSION_MAJOR}\"" >> ${NRPI_VERSION_H}
echo "#define NRPI_VERSION_MINOR \"${NRPI_VERSION_MINOR}\"" >> ${NRPI_VERSION_H}
echo "#define NRPI_VERSION_HOT_FIXES \"${NRPI_VERSION_HOT_FIXES}\"" >> ${NRPI_VERSION_H}
echo "#define NRPI_VERSION_BUILD_NUMBER \"${NRPI_VERSION_BUILD_NUMBER}\"" >> ${NRPI_VERSION_H}
echo "#define NRPI_VERSION_CMT_ID \"${NRPI_VERSION_CMT_ID}\"" >> ${NRPI_VERSION_H}

