#!/bin/bash
#************************************************************************
# Nestorpi Engine
# Copyright (C) 2015-2025 Franck Talbart
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#************************************************************************

# Author: Franck Talbart
# This script is used to deploy webrtc
set -uo pipefail

BASE_DIR="$( cd "$(dirname "$0")" ; pwd -P )"
DEPOT_TOOLS_DIR=${BASE_DIR}/../../tools/depot_tools
LOG_FILE="/tmp/nestorpi_build_update.log"

# ------------------------------------------------------------------------------
# Functions
function check_error()
{ # Show logs in case of error and exit
    if [ "$?" != "0" ] ; then
        echo -e "\\033[1;31m" "Error:" "\\033[0;39m"
        cat "${LOG_FILE}"
        printf "\nFull log file in ${LOG_FILE}\n"
        exit 1
    fi
}

# ------------------------------------------------------------------------------
export PATH="${PATH}":"${DEPOT_TOOLS_DIR}"

pushd "${BASE_DIR}" > /dev/null
rm -rf src/out

echo "-- Updating ..."
pushd src

git config --global --add safe.directory '*'
git checkout main
git stash
check_error
git pull origin main
check_error
popd
./sync.sh
rm -f "${LOG_FILE}"
