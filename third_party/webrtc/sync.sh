#!/bin/bash
#************************************************************************
# Nestorpi Engine
# Copyright (C) 2015-2025 Franck Talbart
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#************************************************************************

# Author: Franck Talbart
# This script is used to deploy webrtc
set uo pipefail

BASE_DIR="$( cd "$(dirname "$0")" ; pwd -P )"
DEPOT_TOOLS_DIR="${BASE_DIR}/../../tools/depot_tools"
LOG_FILE="/tmp/nestorpi_build_sync.log"

# ------------------------------------------------------------------------------
# Functions
function check_error()
{ # Show logs in case of error and exit
    if [ "$?" != "0" ] ; then
        echo -e "\\033[1;31m" "Error:" "\\033[0;39m"
        cat "${LOG_FILE}"
        printf "\nFull log file in ${LOG_FILE}\n"
        exit 1
    fi
}

# ------------------------------------------------------------------------------
export PATH="${PATH}":"${DEPOT_TOOLS_DIR}"
echo "-- Syncing WebRTC ... It may take some time."
git config --global --add safe.directory '*'

pushd "${BASE_DIR}/src" > /dev/null
CURRENT_COMMIT=$(git rev-parse HEAD 2>/dev/null || echo "none")
popd > /dev/null

# Check if the src directory exists
if [ ! -f "${BASE_DIR}/.gclient_entries" ]; then
  SYNC_REQUIRED=true
elif [ ! -f "${BASE_DIR}/.commit" ]; then
  SYNC_REQUIRED=true
else
  RECORDED_COMMIT=$(cat "${BASE_DIR}/.commit")
  if [ "${CURRENT_COMMIT}" != "${RECORDED_COMMIT}" ]; then
    echo "The current commit (${CURRENT_COMMIT}) differs from the previous commit (${RECORDED_COMMIT})."
    SYNC_REQUIRED=true
  fi
fi

pushd "${BASE_DIR}" > /dev/null

#SYNC_REQUIRED=true
if [ "${SYNC_REQUIRED}" = true ]; then
  echo "Synchronization required."

  echo "Cleaning patches..."
  ./clean_patch.sh > "${LOG_FILE}" 2>&1
  check_error

  echo "Syncing with gclient..."
  gclient sync --no-history --with_branch_heads > "${LOG_FILE}" 2>&1
  check_error

  touch "${BASE_DIR}/.must_compile"
  echo "${CURRENT_COMMIT}" > "${BASE_DIR}/.commit"

else
  echo "WebRTC repository is already in sync."
fi

echo "Applying patches..."
./clean_patch.sh > "${LOG_FILE}" 2>&1
check_error

./patch.sh > "${LOG_FILE}" 2>&1
check_error

echo "Cleaning up temporary logs..."
rm -f "${LOG_FILE}"
