#!/bin/bash
#************************************************************************
# NestoRPi Engine
# Copyright (C) 2015-2025 Franck Talbart
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#************************************************************************

# Author: Franck Talbart
# This script is used to patch WebRTC and ensure proper compilation.
set -euo pipefail

WEBRTC_ROOT="$( cd "$(dirname "$0")" ; pwd -P )"

# Every change to this script should also be applied to clean_patch.sh

# 0/
MEDIA_CHANNEL_FILE="${WEBRTC_ROOT}/src/media/base/media_channel.h"
if [ -f "${MEDIA_CHANNEL_FILE}" ]; then
  filecontent=$(<"${MEDIA_CHANNEL_FILE}")
  filecontent=$(echo "${filecontent}" | sed -E 's|ost << ToStringIfSet[^;]*;||g')
  echo "${filecontent}" > "${MEDIA_CHANNEL_FILE}"
else
  echo "File ${MEDIA_CHANNEL_FILE} does not exist!"
fi

# 1/
FILE_PATH="${WEBRTC_ROOT}/src/call/rtp_config.cc"
FILE_CONTENT=$(<"${FILE_PATH}")
UPDATED_CONTENT=$(echo "${FILE_CONTENT}" | sed 's/stream_config.rtx.emplace()/stream_config.rtx.emplace(RtpStreamConfig::Rtx())/g')
echo "${UPDATED_CONTENT}" > "${FILE_PATH}"
