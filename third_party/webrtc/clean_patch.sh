#!/bin/bash
#************************************************************************
# NestoRPi Engine
# Copyright (C) 2015-2025 Franck Talbart
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#************************************************************************

# Author: Franck Talbart
# This script is used to clean the manual patch previously applied to WebRTC
set -euo pipefail

# Every change to this script should also be applied to patch.sh

WEBRTC_ROOT="$( cd "$(dirname "$0")" ; pwd -P )"

if [ ! -d "${WEBRTC_ROOT}/src" ]; then
    exit 0
fi

# 0/ root
cd ${WEBRTC_ROOT}/src
git config --global --add safe.directory '*'
git checkout .

# 1/ third parties
for dir in "${WEBRTC_ROOT}/src/third_party"/*; do
    if [ -d "${dir}/.git" ]; then
        (
            cd "${dir}" || exit
            git checkout .
        )
    fi
done
