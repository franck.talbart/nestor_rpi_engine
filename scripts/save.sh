#!/bin/bash
#************************************************************************
# NestoRPi Engine
# Copyright (C) 2015-2025 Franck Talbart
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#************************************************************************

# Author: Franck Talbart
# This script creates a backup of the robot and send an email once done.
HOME_USER=/home/cyrus
EMAIL=franck@talbart.fr

cat > /tmp/exclure << EOF
/home
/cdrom
/dev
/lost+found
/media
/mnt
/proc
/sys
/tmp
EOF

mkdir /tmp/save
mkdir /tmp/save-user
sudo rm -r /home/controle-user/* /home/controle-user/.*
sudo rsync -av --del --backup --backup-dir=/home/controle/ --exclude-from=/tmp/exclure / /tmp/save
sudo tar cvjf /tmp/save.tar.bz  /tmp/save
sudo rm -rf /tmp/save
sudo rsync -av --del --backup --backup-dir=/home/controle-user/ ${HOME_USER} /tmp/save-user
sudo tar cvjf /tmp/save-user.tar.bz  /tmp/save-user
sudo rm -rf /tmp/save-user
sudo rm -r /home/controle-user/* /home/controle-user/.*
rm /tmp/exclure

echo "" | mail -s "Nestor: The backup is ready!" ${EMAIL}
