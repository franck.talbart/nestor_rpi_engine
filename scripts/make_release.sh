#!/bin/bash
#************************************************************************
# NestoRPi Engine
# Copyright (C) 2015-2020 Franck Talbart
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#************************************************************************

# Author: Franck Talbart
# Script used to send the binaries link to the release if tagged
name="NestoRPi Engine"
baseurl="https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}"
linkurl="https://gitlab.com/franck.talbart/nestor_rpi_engine/-/jobs/"

[[ -z "${CI_COMMIT_TAG}" ]] && exit 0
linkurl="${linkurl}/${CI_JOB_ID}/artifacts/download"

# Create release
name_link="Debian packages ${name} version ${CI_COMMIT_TAG}"
curl --request POST --header "JOB-TOKEN: ${CI_JOB_TOKEN}" --data "{ \"name\": \"${CI_COMMIT_TAG}\", \"tag_name\": \"${CI_COMMIT_TAG}\", \"ref\": \"master\", \"description\": \"Release\", \"assets\": { \"links\": [{ \"name\": \"${name_link}\", \"url\": \"${linkurl}\", \"link_type\":\"package\"}]}}" --header 'Content-Type: application/json'  ${baseurl}/releases
