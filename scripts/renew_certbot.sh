#!/bin/bash
#************************************************************************
# NestoRPi Engine
# Copyright (C) 2015-2025 Franck Talbart
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#************************************************************************

# Author: Franck Talbart
# This script is used to update the HTTPS certificate
# It assumes that the overlay FS is enabled on the robot
# the ssh public key should be sent to the robot and the head node for automation purposes
# Example of automation in a cron: echo "head-merignac\nnestor\nPASSWORD\n" | ./renew_certbot.sh

set -euo pipefail
clear; clear

BASE_DIR=$(dirname "$0")
USERNAME="cyrus"

echo "This script will renew the HTTPS certificate. Please give the server where cerbot is run (hint: head-orsay / head-merignac):"
read SERVER_IP
echo "Please give the targeted robot's name (hint: freda / nestor):"
read ROBOT
echo "Please give the root password:"
read -s PASSWORD

echo "Disabling overlay FS ..."
# reboot < /dev/null is a trick to skip molly-guard
ssh -t ${USERNAME}@${ROBOT} "echo ${PASSWORD} | sudo -S bash -c \"raspi-config nonint disable_overlayfs && reboot </dev/null\""

echo "Waiting for ${ROBOT} to be back ..."
success_count=0
set +e
while [ $success_count -lt 10 ]; do
    if ping -c 1 ${ROBOT} &> /dev/null; then
        ((success_count++))
    else
        success_count=0
    fi
    sleep 1
done

ssh -t ${USERNAME}@${SERVER_IP} "echo ${PASSWORD} | sudo -S bash -c \"service apache2 stop\""
set -e

echo "Connecting to the server ..."
CERT_FILE=${BASE_DIR}/../bin/cfg/${ROBOT}/nestorpi.pem
ssh -t ${USERNAME}@${SERVER_IP} "echo ${PASSWORD} | sudo -S bash -c \"certbot renew && cat /etc/letsencrypt/live/*/fullchain.pem > /tmp/cert_tmp && cat /etc/letsencrypt/live/*/privkey.pem >> /tmp/cert_tmp\""

set +e
ssh -t ${USERNAME}@${SERVER_IP} "echo ${PASSWORD} | sudo -S bash -c \"service apache2 start\""
set -e

ssh -t ${USERNAME}@${SERVER_IP} "bash -c \"cat /tmp/cert_tmp\"" > ${CERT_FILE}
${BASE_DIR}/../nrpi.sh deploy ${ROBOT}

echo "Enabling overlay FS ..."
# reboot < /dev/null is a trick to skip molly-guard
ssh -t ${USERNAME}@${ROBOT} "echo ${PASSWORD} | sudo -S bash -c \"raspi-config nonint enable_overlayfs && reboot </dev/null\""

echo "${ROBOT} is restarting. Success!"
