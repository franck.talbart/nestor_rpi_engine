#!/bin/bash
#************************************************************************
# NestoRPi Engine
# Copyright (C) 2015-2025 Franck Talbart
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#************************************************************************

# Author: Franck Talbart
# This script is used to update nestoRpi on the robot
# It assumes that the overlay FS is enabled on the robot
# the ssh public key should be sent to the robot for automation purposes

set -euo pipefail
clear; clear

BASE_DIR=$(dirname "$0")
USERNAME="cyrus"

echo "This script will update NestorRpi Engine on the robot."
echo "Please give the targeted robot's name (hint: freda / nestor):"
read ROBOT
echo "Please give the root password:"
read -s PASSWORD

echo "Disabling overlay FS ..."
# reboot < /dev/null is a trick to skip molly-guard
ssh -t ${USERNAME}@${ROBOT} "echo ${PASSWORD} | sudo -S bash -c \"raspi-config nonint disable_overlayfs && reboot </dev/null\""

echo "Waiting for ${ROBOT} to be back ..."
success_count=0
set +e
while [ $success_count -lt 10 ]; do
    if ping -c 1 ${ROBOT} &> /dev/null; then
        ((success_count++))
    else
        success_count=0
    fi
    sleep 1
done

${BASE_DIR}/../nrpi.sh deploy ${ROBOT}

echo "Enabling overlay FS ..."
# reboot < /dev/null is a trick to skip molly-guard
ssh -t ${USERNAME}@${ROBOT} "echo ${PASSWORD} | sudo -S bash -c \"raspi-config nonint enable_overlayfs && reboot </dev/null\""

echo "${ROBOT} is restarting. Success!"
