var actions = {
    "backward":
    {
        "parameters":
        {
            "motor_left": "backward",
            "motor_right": "backward"
        },
        "name": "relay"
    },
    "battery":
    {
        "parameters":
        {
            "battery": "on"
        },
        "name": "relay"
    },
    "camera":
    {
        "parameters":
        {
            "motor_camera": "move"
        },
        "name": "relay"
    },
    "down":
    {
        "parameters":
        {
            "motor_arm": "down"
        },
        "name": "relay"
    },
    "forward":
    {
        "parameters":
        {
            "motor_left": "forward",
            "motor_right": "forward"
        },
        "name": "relay"
    },
    "klaxon":
    {
        "parameters":
        {
            "sound": 0
        },
        "name": "audio_video"
    },
    "left":
    {
        "parameters":
        {
            "motor_left": "backward",
            "motor_right": "forward"
        },
        "name": "relay"
    },
    "light":
    {
        "parameters":
        {
            "light": "on"
        },
        "name": "relay"
    },
    "parking":
    {
        "parameters":
        {
            "parking": "on"
        },
        "name": "relay"
    },
    "random_sounds":
    {
        "parameters":
        {
            "sound": -1
        },
        "name": "audio_video"
    },
    "right":
    {
        "parameters":
        {
            "motor_left": "forward",
            "motor_right": "backward"
        },
        "name": "relay"
    },
    "up":
    {
        "parameters":
        {
            "motor_arm": "up"
        },
        "name": "relay"
    }
};