var gamepad = {
    "threshold_sensibility": 0.20,
    "axis":
    {
        0:
        {
            "actions": [
            {
                "name": "left",
                "operator": "<",
                "threshold": -0.2
            },
            {
                "name": "right",
                "operator": ">",
                "threshold": 0.2
            }],
            "group": "direction"
        },
        3:
        {
            "actions": [
            {
                "name": "camera",
                "operator": "<",
                "threshold": -0.2
            }]
        }
    },
    "buttons":
    {
        0:
        {
            "actions": [
            {
                "name": "battery"
            }],
            "permanent": true
        },
        1:
        {
            "actions": [
            {
                "name": "parking"
            }],
            "permanent": true
        },
        3:
        {
            "actions": [
            {
                "name": "light"
            }],
            "permanent": true
        },
        6:
        {
            "actions": [
            {
                "name": "backward",
                "threshold": 0.2
            }],
            "group": "direction"
        },
        7:
        {
            "actions": [
            {
                "name": "forward",
                "threshold": 0.2
            }],
            "group": "direction"
        },
        8:
        {
            "actions": [
            {
                "name": "random_sounds"
            }],
            "permanent": true
        },
        9:
        {
            "actions": [
            {
                "name": "klaxon"
            }],
            "permanent": true
        },
        12:
        {
            "actions": [
            {
                "name": "up"
            },
            {
                "name": "down",
                "value": 0
            }],
            "permanent": true
        },
        13:
        {
            "actions": [
            {
                "name": "down"
            },
            {
                "name": "up",
                "value": 0
            }],
            "permanent": true
        }
    }
};