var keyboard = {
    "ArrowUp":
    {
        "actions": [
        {
            "name": "forward"
        }],
        "group": "direction"
    },
    "ArrowDown":
    {
        "actions": [
        {
            "name": "backward"
        }],
        "group": "direction"
    },
    "ArrowRight":
    {
        "actions": [
        {
            "name": "right"
        }],
        "group": "direction"
    },
    "ArrowLeft":
    {
        "actions": [
        {
            "name": "left"
        }],
        "group": "direction"
    },
    "Control":
    {
        "actions": [
        {
            "name": "light"
        }],
        "permanent": true
    },
    "End":
    {
        "actions": [
        {
            "name": "camera"
        }]
    },
    "PageUp":
    {
        "actions": [
        {
            "name": "up"
        },
        {
            "name": "down",
            "value": 0
        }],
        "permanent": true
    },
    "PageDown":
    {
        "actions": [
        {
            "name": "down"
        },
        {
            "name": "up",
            "value": 0
        }],
        "permanent": true
    },
    "p":
    {
        "actions": [
        {
            "name": "parking"
        }],
        "permanent": true
    },
    "b":
    {
        "actions": [
        {
            "name": "battery"
        }],
        "permanent": true
    },
    "k":
    {
        "actions": [
        {
            "name": "klaxon"
        }],
        "permanent": true
    },
    "s":
    {
        "actions": [
        {
            "name": "random_sounds"
        }],
        "permanent": true
    }
};