var view_dashboard = {
    "voltage_station":
    {
        "min": 0,
        "max": 15,
        "progress_bar": true,
        "name": "Station (V)"
    },
    "voltage_battery":
    {
        "min": 12.50,
        "max": 13.50,
        "progress_bar": true,
        "name": "Battery (V)"
    },
    "temp_battery":
    {
        "max": 31,
        "name": "Temp. Batt. (C)"
    },
    "temp_cpu":
    {
        "max": 55,
        "name": "Temp. CPU (C)"
    },
    "wifi_signal":
    {
        "min": 15,
        "max": 70,
        "progress_bar": true,
        "name": "Wifi (/70)"
    },
    "ultrasound_right":
    {
        "name": "Dist. Right (cm)",
        "min": 5
    },
    "ultrasound_left":
    {
        "name": "Dist. Left (cm)",
        "min": 5
    },
    "photoresistor":
    {
        "name": "Brightness"
    },
    "temp_outside":
    {
        "name": "Temp. Outside (C)"
    },
    "wheel_left":
    {
        "name": "Total Dist. (cm)"
    }
};

var view_l298n = {};

var view_position = {};

var view_relay = {
    "motor_arm":
    {
        "up": "relay_arm_up",
        "down": "relay_arm_down"
    },
    "light":
    {
        "on": "relay_light"
    },
    "motor_camera":
    {
        "move": "relay_motor_camera_move"
    },
    "motor_left":
    {
        "backward": "relay_motor_left_backward",
        "forward": "relay_motor_left_forward"
    },
    "motor_right":
    {
        "backward": "relay_motor_right_backward",
        "forward": "relay_motor_right_forward"
    },
    "parking":
    {
        "on": "relay_parking"
    },
    "battery":
    {
        "on": "relay_battery"
    }
};

var view_servo = {};


var video = {
    "bitrate": 2000000,
    "codec": "H264hw"
};

var tasks_map = {
    "automation_log": "log",
    "automation_monitoring": "item",
    "automation_odometry": "odometry",
    "controller_audio_video": "camera",
    "controller_relay": "relay"
};

// Message to display on the webpage
var motd = "";

// Graphs
var url_monitoring = "http://192.168.1.215:8123/lovelace/nestor";