var buttons = {
    "forward":
    {
        "actions": [
        {
            "name": "forward"
        }],
        "group": "direction"
    },
    "backward":
    {
        "actions": [
        {
            "name": "backward"
        }],
        "group": "direction"
    },
    "right":
    {
        "actions": [
        {
            "name": "right"
        }],
        "group": "direction"
    },
    "left":
    {
        "actions": [
        {
            "name": "left"
        }],
        "group": "direction"
    },
    "light":
    {
        "actions": [
        {
            "name": "light"
        }],
        "permanent": true
    },
    "camera":
    {
        "actions": [
        {
            "name": "camera"
        }]
    },
    "neck_up":
    {
        "actions": [
        {
            "name": "up"
        },
        {
            "name": "down",
            "value": 0
        }],
        "permanent": true
    },
    "neck_down":
    {
        "actions": [
        {
            "name": "down"
        },
        {
            "name": "up",
            "value": 0
        }],
        "permanent": true
    },
    "parking":
    {
        "actions": [
        {
            "name": "parking"
        }],
        "permanent": true
    },
    "battery":
    {
        "actions": [
        {
            "name": "battery"
        }],
        "permanent": true
    },
    "klaxon":
    {
        "actions": [
        {
            "name": "klaxon"
        }],
        "permanent": true
    },
    "sound":
    {
        "actions": [
        {
            "name": "random_sounds"
        }],
        "permanent": true
    }
};