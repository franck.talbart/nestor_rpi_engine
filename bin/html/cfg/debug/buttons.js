var buttons = {
    "forward":
    {
        "actions": [
        {
            "name": "forward"
        }]
    },
    "backward":
    {
        "actions": [
        {
            "name": "backward"
        }]
    },
    "right":
    {
        "actions": [
        {
            "name": "right"
        }]
    },
    "left":
    {
        "actions": [
        {
            "name": "left"
        }]
    },
    "light":
    {
        "actions": [
        {
            "name": "light"
        }],
        "permanent": true
    },
    "camera":
    {
        "actions": [
        {
            "name": "camera"
        }]
    },
    "neck_up":
    {
        "actions": [
        {
            "name": "up"
        }],
        "permanent": true
    },
    "neck_down":
    {
        "actions": [
        {
            "name": "down"
        }],
        "permanent": true
    },
    "parking":
    {
        "actions": [
        {
            "name": "parking"
        }],
        "permanent": true
    },
    "battery":
    {
        "actions": [
        {
            "name": "battery"
        }],
        "permanent": true
    },
    "klaxon":
    {
        "actions": [
        {
            "name": "klaxon"
        }],
        "permanent": true
    },
    "sound":
    {
        "actions": [
        {
            "name": "random_sounds"
        }],
        "permanent": true
    },
    "camera_right":
    {
        "actions": [
        {
            "name": "camera_right"
        }]
    },
    "camera_left":
    {
        "actions": [
        {
            "name": "camera_left"
        }]
    }
};