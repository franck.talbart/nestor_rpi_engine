var gamepad = {
    "threshold_sensibility": 0.20,
    "axis":
    {
        1:
        {
            "actions": [
            {
                "name": "left"
            }]
        },
        3:
        {
            "actions": [
            {
                "name": "right"
            }]
        }
    },
    "buttons":
    {
        0:
        {
            "actions": [
            {
                "name": "battery"
            }],
            "permanent": true
        },
        1:
        {
            "actions": [
            {
                "name": "parking"
            }],
            "permanent": true
        },
        2:
        {
            "actions": [
            {
                "name": "gamepad_arm.js"
            }]
        },
        3:
        {
            "actions": [
            {
                "name": "light"
            }],
            "permanent": true
        },
        4:
        {
            "actions": [
            {
                "name": "camera_left"
            }]
        },
        5:
        {
            "actions": [
            {
                "name": "camera_right"
            }]
        },
        6:
        {
            "actions": [
            {
                "name": "servo_pliers_turn"
            }]
        },
        7:
        {
            "actions": [
            {
                "name": "servo_pliers"
            }]
        },
        8:
        {
            "actions": [
            {
                "name": "random_sounds"
            }],
            "permanent": true
        },
        9:
        {
            "actions": [
            {
                "name": "klaxon"
            }],
            "permanent": true
        },
        12:
        {
            "actions": [
            {
                "name": "down"
            }],
            "permanent": true
        },
        13:
        {
            "actions": [
            {
                "name": "up"
            }],
            "permanent": true
        }
    }
};