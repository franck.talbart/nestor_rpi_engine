var view_dashboard = {
    "voltage_station":
    {
        "min": 15.30,
        "name": "Station (V)"
    },
    "voltage_battery":
    {
        "min": 12.50,
        "max": 14,
        "progress_bar": true,
        "name": "Battery (V)"
    },
    "temp_battery":
    {
        "max": 31,
        "name": "Temp. Batt. (C)"
    },
    "temp_cpu":
    {
        "max": 55,
        "name": "Temp. CPU (C)"
    },
    "wifi_signal":
    {
        "min": 15,
        "max": 70,
        "progress_bar": true,
        "name": "Wifi (/70)"
    }
};

var view_l298n = {};

var view_position = {};

var view_relay = {
    "motor_arm":
    {
        "up": "relay_arm_up",
        "down": "relay_arm_down"
    },
    "light":
    {
        "on": "relay_light"
    },
    "motor_camera":
    {
        "left": "relay_motor_camera_left",
        "right": "relay_motor_camera_right"
    },
    "parking":
    {
        "on": "relay_parking"
    },
    "battery":
    {
        "on": "relay_battery"
    }
};

var view_servo = {};

var video = {
    "bitrate": 2000000,
    "codec": "VP9"
};

var tasks_map = {
    "automation_log": "log",
    "automation_monitoring": "item",
    "automation_odometry": "odometry",
    "controller_audio_video": "camera",
    "controller_L298N": "l298n",
    "controller_PID": "l298n",
    "controller_relay": "relay",
    "controller_servo": "servo",
};

// Message to display on the webpage
var motd = "<video  style='width:400px;border: 1px solid #fff;' autoplay='true' muted onended='show_hide(\"\")' src='instructions.mp4'>Sorry, your browser doesn't support embedded videos.</video>";

var url_monitoring = "http://google.fr";