var keyboard = {
    "Shift":
    {
        "speed": 100
    },
    "ArrowUp":
    {
        "actions": [
        {
            "name": "forward"
        }]
    },
    "ArrowDown":
    {
        "actions": [
        {
            "name": "backward"
        }]
    },
    "ArrowRight":
    {
        "actions": [
        {
            "name": "right"
        }]
    },
    "ArrowLeft":
    {
        "actions": [
        {
            "name": "left"
        }]
    },
    "Control":
    {
        "actions": [
        {
            "name": "light"
        }],
        "permanent": true
    },
    "End":
    {
        "actions": [
        {
            "name": "camera"
        }]
    },
    "PageUp":
    {
        "actions": [
        {
            "name": "up"
        }],
        "permanent": true
    },
    "PageDown":
    {
        "actions": [
        {
            "name": "down"
        }],
        "permanent": true
    },
    "p":
    {
        "actions": [
        {
            "name": "parking"
        }],
        "permanent": true
    },
    "b":
    {
        "actions": [
        {
            "name": "battery"
        }],
        "permanent": true
    },
    "k":
    {
        "actions": [
        {
            "name": "klaxon"
        }],
        "permanent": true
    },
    "s":
    {
        "actions": [
        {
            "name": "random_sounds"
        }],
        "permanent": true
    },
    "l":
    {
        "actions": [
        {
            "name": "camera_right"
        }]
    },
    "m":
    {
        "actions": [
        {
            "name": "camera_left"
        }]
    }
};