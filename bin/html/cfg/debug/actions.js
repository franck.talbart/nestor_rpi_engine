var actions = {
    "battery":
    {
        "parameters":
        {
            "battery": "on"
        },
        "name": "relay"
    },
    "camera_left":
    {
        "parameters":
        {
            "motor_camera": "left"
        },
        "name": "relay"
    },
    "camera_right":
    {
        "parameters":
        {
            "motor_camera": "right"
        },
        "name": "relay"
    },
    "down":
    {
        "parameters":
        {
            "motor_arm": "down"
        },
        "name": "relay"
    },
    "klaxon":
    {
        "parameters":
        {
            "sound": 0
        },
        "name": "audio_video"
    },
    "left":
    {
        "parameters":
        {
            "front": "left"
        },
        "name": "PID"
    },
    "light":
    {
        "parameters":
        {
            "light": "on"
        },
        "name": "relay"
    },
    "parking":
    {
        "parameters":
        {
            "parking": "on"
        },
        "name": "relay"
    },
    "random_sounds":
    {
        "parameters":
        {
            "sound": -1
        },
        "name": "audio_video"
    },
    "right":
    {
        "parameters":
        {
            "front": "right"
        },
        "name": "PID"
    },
    "servo_arm_forward":
    {
        "parameters":
        {
            "pca9685": "servo_arm_forward"
        },
        "name": "servo"
    },
    "servo_arm_turn":
    {
        "parameters":
        {
            "pca9685": "servo_arm_turn"
        },
        "name": "servo"
    },
    "servo_arm_up":
    {
        "parameters":
        {
            "pca9685": "servo_arm_up"
        },
        "name": "servo"
    },
    "servo_camera":
    {
        "parameters":
        {
            "pca9685": "servo_camera"
        },
        "name": "servo"
    },
    "servo_pliers":
    {
        "parameters":
        {
            "pca9685": "servo_pliers"
        },
        "name": "servo"
    },
    "servo_pliers_turn":
    {
        "parameters":
        {
            "pca9685": "servo_pliers_turn"
        },
        "name": "servo"
    },
    "up":
    {
        "parameters":
        {
            "motor_arm": "up"
        },
        "name": "relay"
    }
};