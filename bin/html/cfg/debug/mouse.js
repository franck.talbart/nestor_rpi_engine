var mouse = {
    "0":
    {
        "y":
        {
            "actions": [
            {
                "name": "servo_camera_vertical",
                "reverse": true
            }]
        },
        "x":
        {
            "actions": [
            {
                "name": "servo_camera_horizontal"
            }]
        }
    },
    "1":
    {
        "y":
        {
            "actions": [
            {
                "name": "servo_arm_forward"
            }]
        },
        "x":
        {
            "actions": [
            {
                "name": "servo_arm_turn",
                "reverse": true
            }]
        }
    },
    "3":
    {
        "y":
        {
            "actions": [
            {
                "name": "servo_arm_up"
            }]
        },
        "x":
        {
            "actions": [
            {
                "name": "servo_arm_turn",
                "reverse": true
            }]
        }
    }
};