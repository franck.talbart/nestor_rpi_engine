var mode = "Arm";

var gamepad = {
    "threshold_sensibility": 0.20,
    "axis":
    {
        0:
        {
            "actions": [
            {
                "name": "servo_arm_turn",
                "reverse": true,
                "relative_sensibility": 30
            }],
            "repeat": true
        },
        1:
        {
            "actions": [
            {
                "name": "servo_arm_up",
                "relative_sensibility": 20
            }],
            "repeat": true
        },
        2:
        {
            "actions": [
            {
                "name": "servo_arm_forward",
                "reverse": true,
                "relative_sensibility": 30
            }],
            "repeat": true
        },
        3:
        {
            "actions": [
            {
                "name": "servo_camera_vertical",
                "relative_sensibility": 50
            }],
            "repeat": true
        }
    },
    "buttons":
    {
        0:
        {
            "actions": [
            {
                "name": "battery"
            }],
            "permanent": true
        },
        1:
        {
            "actions": [
            {
                "name": "parking"
            }],
            "permanent": true
        },
        2:
        {
            "actions": [
            {
                "name": "gamepad.js"
            }]
        },
        3:
        {
            "actions": [
            {
                "name": "light"
            }],
            "permanent": true
        },
        4:
        {
            "actions": [
            {
                "name": "servo_camera_horizontal",
                "value": -1,
                "relative_sensibility": 30
            }],
            "repeat": true
        },
        5:
        {
            "actions": [
            {
                "name": "servo_camera_horizontal",
                "value": 1,
                "relative_sensibility": 30
            }],
            "repeat": true
        },

        6:
        {
            "actions": [
            {
                "name": "servo_pliers_turn",
                "reverse_way": true
            }]
        },
        7:
        {
            "actions": [
            {
                "name": "servo_pliers",
                "reverse_way": true
            }]
        },
        8:
        {
            "actions": [
            {
                "name": "random_sounds"
            }],
            "permanent": true
        },
        9:
        {
            "actions": [
            {
                "name": "klaxon"
            }],
            "permanent": true
        },
        12:
        {
            "actions": [
            {
                "name": "up"
            },
            {
                "name": "down",
                "value": 0
            }],
            "permanent": true
        },
        13:
        {
            "actions": [
            {
                "name": "down"
            },
            {
                "name": "up",
                "value": 0
            }],
            "permanent": true
        }
    }
};