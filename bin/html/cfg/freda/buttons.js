var buttons = {
    "fan":
    {
        "actions": [
        {
            "name": "fan"
        }],
        "permanent": true
    },
    "light":
    {
        "actions": [
        {
            "name": "light"
        }],
        "permanent": true
    },
    "neck_up":
    {
        "actions": [
        {
            "name": "up"
        },
        {
            "name": "down",
            "value": 0
        }],
        "permanent": true
    },
    "neck_down":
    {
        "actions": [
        {
            "name": "down"
        },
        {
            "name": "up",
            "value": 0
        }],
        "permanent": true
    },
    "parking":
    {
        "actions": [
        {
            "name": "parking"
        }],
        "permanent": true
    },
    "battery":
    {
        "actions": [
        {
            "name": "battery"
        }],
        "permanent": true
    },
    "klaxon":
    {
        "actions": [
        {
            "name": "klaxon"
        }],
        "permanent": true
    },
    "sound":
    {
        "actions": [
        {
            "name": "random_sounds"
        }],
        "permanent": true
    },
    "forward":
    {
        "actions": [
        {
            "name": "left_front",
            "value": "-{val_speed}"
        },
        {
            "name": "right_front",
            "value": "-{val_speed}"
        },
        {
            "name": "left_back",
            "value": "-{val_speed}"
        },
        {
            "name": "right_back",
            "value": "-{val_speed}"
        }],
        "group": "direction"
    },
    "backward":
    {
        "actions": [
        {
            "name": "left_front",
            "value": "{val_speed}"
        },
        {
            "name": "right_front",
            "value": "{val_speed}"
        },
        {
            "name": "left_back",
            "value": "{val_speed}"
        },
        {
            "name": "right_back",
            "value": "{val_speed}"
        }],
        "group": "direction"
    },
    "left":
    {
        "actions": [
        {
            "name": "left_front",
            "value": "{val_speed}"
        },
        {
            "name": "left_back",
            "value": "{val_speed}"
        },
        {
            "name": "right_front",
            "value": "-{val_speed}"
        },
        {
            "name": "right_back",
            "value": "-{val_speed}"
        }],
        "group": "direction"
    },
    "right":
    {
        "actions": [
        {
            "name": "left_front",
            "value": "-{val_speed}"
        },
        {
            "name": "left_back",
            "value": "-{val_speed}"
        },
        {
            "name": "right_front",
            "value": "{val_speed}"
        },
        {
            "name": "right_back",
            "value": "{val_speed}"
        }],
        "group": "direction"
    },
    "arm_forward":
    {
        "actions": [
        {
            "name": "servo_arm_forward",
            "value": -0.5,
            "relative_sensibility": 30
        }],
        "repeat": true
    },
    "arm_backward":
    {
        "actions": [
        {
            "name": "servo_arm_forward",
            "value": 0.5,
            "relative_sensibility": 30
        }],
        "repeat": true
    },
    "arm_left":
    {
        "actions": [
        {
            "name": "servo_arm_turn",
            "value": 0.5,
            "relative_sensibility": 30
        }],
        "repeat": true
    },
    "arm_right":
    {
        "actions": [
        {
            "name": "servo_arm_turn",
            "value": -0.5,
            "relative_sensibility": 30
        }],
        "repeat": true
    },
    "arm_up":
    {
        "actions": [
        {
            "name": "servo_arm_up",
            "value": -0.5,
            "relative_sensibility": 20
        }],
        "repeat": true
    },
    "arm_down":
    {
        "actions": [
        {
            "name": "servo_arm_up",
            "value": 0.5,
            "relative_sensibility": 20
        }],
        "repeat": true
    },
    "camera_left":
    {
        "actions": [
        {
            "name": "servo_camera_horizontal",
            "value": -1,
            "relative_sensibility": 30
        }],
        "repeat": true
    },
    "camera_right":
    {
        "actions": [
        {
            "name": "servo_camera_horizontal",
            "value": 1,
            "relative_sensibility": 30
        }],
        "repeat": true
    },
    "camera_up":
    {
        "actions": [
        {
            "name": "servo_camera_vertical",
            "value": -1,
            "relative_sensibility": 30
        }],
        "repeat": true
    },
    "camera_down":
    {
        "actions": [
        {
            "name": "servo_camera_vertical",
            "value": 1,
            "relative_sensibility": 30
        }],
        "repeat": true
    },
    "pliers":
    {
        "actions": [
        {
            "name": "servo_pliers"
        }],
        "permanent": true
    },
    "pliers_turn":
    {
        "actions": [
        {
            "name": "servo_pliers_turn"
        }],
        "permanent": true
    }
};