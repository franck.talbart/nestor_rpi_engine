var view_dashboard = {
    "voltage_station":
    {
        "min": 0,
        "max": 16,
        "progress_bar": true,
        "name": "Station (V)"
    },
    "voltage_battery":
    {
        "min": 12.50,
        "max": 13.50,
        "progress_bar": true,
        "name": "Battery (V)"
    },
    "temp_battery":
    {
        "max": 31,
        "name": "Temp. Bat. (C)"
    },
    "temp_cpu":
    {
        "max": 65,
        "name": "Temp. CPU (C)"
    },
    "wifi_signal":
    {
        "min": 15,
        "max": 70,
        "progress_bar": true,
        "name": "Wifi (/70)"
    },
    "photoresistor":
    {
        "name": "Brightness"
    },
    "temp_outside":
    {
        "name": "Temp. Outside (C)"
    },
    "voltage_regulator":
    {
        "name": "Regulator (V)"
    },
    "ultrasound_right":
    {
        "name": "Dist. Right (cm)",
        "min": 5
    },
    "ultrasound_left":
    {
        "name": "Dist. Left (cm)",
        "min": 5
    },
    "encoding_front_right":
    {
        "name": "Total Dist. (cm)"
    }
};

var view_l298n = {
    "front_left": "l298n_front_left",
    "front_right": "l298n_front_right",
    "back_left": "l298n_back_left",
    "back_right": "l298n_right_left",
};

var view_position = {
    "servo_camera_horizontal": "position_x",
    "servo_camera_vertical": "position_y"
};

var view_relay = {
    "motor_arm":
    {
        "up": "relay_arm_up",
        "down": "relay_arm_down"
    },
    "light":
    {
        "on": "relay_light"
    },
    "fan":
    {
        "on": "relay_fan"
    },
    "parking":
    {
        "on": "relay_parking"
    },
    "battery":
    {
        "on": "relay_battery"
    }
};

var view_servo = {
    "servo_arm_forward": ["servo_arm_backward", "servo_arm_forward"],
    "servo_arm_turn": ["servo_arm_right", "servo_arm_left"],
    "servo_arm_up": ["servo_arm_up", "servo_arm_down"],
    "servo_camera_horizontal": ["servo_camera_right", "servo_camera_left"],
    "servo_camera_vertical": ["servo_camera_up", "servo_camera_down"],
    "servo_pliers": ["servo_pliers"],
    "servo_pliers_turn": ["servo_pliers_turn"]
};

var video = {
    "bitrate": 2000000,
    "codec": "H264hw"
};

var tasks_map = {
    "automation_log": "log",
    "automation_monitoring": "item",
    "automation_odometry": "odometry",
    "controller_audio_video": "camera",
    "controller_L298N": "l298n",
    "controller_relay": "relay",
    "controller_servo": "servo",
    "controller_PID": "l298n",
};

// Message to display on the webpage
var motd = "<video  style='width:400px;border: 1px solid #fff;' autoplay='true' muted onended='show_hide(\"\")' src='instructions.mp4'>Sorry, your browser doesn't support embedded videos.</video>";

// Graphs
var url_monitoring = "http://192.168.1.215:8123/lovelace/freda";