var mode = "Move";
var gamepad = {
    "threshold_sensibility": 0.20,
    "axis":
    {
        0:
        {
            "actions": [
            {
                "name": "left_front"
            },
            {
                "name": "left_back",
                "reverse": true
            }],
            "threshold_sensibility": 0.80,
        },
        1:
        {
            "actions": [
            {
                "name": "left_front"
            },
            {
                "name": "left_back"
            }]
        },
        2:
        {
            "actions": [
            {
                "name": "right_front",
                "reverse": true
            },
            {
                "name": "right_back"
            }],
            "threshold_sensibility": 0.80,
        },
        3:
        {
            "actions": [
            {
                "name": "right_front"
            },
            {
                "name": "right_back"
            }]
        },
    },
    "buttons":
    {
        0:
        {
            "actions": [
            {
                "name": "battery"
            }],
            "permanent": true
        },
        1:
        {
            "actions": [
            {
                "name": "parking"
            }],
            "permanent": true
        },
        2:
        {
            "actions": [
            {
                "name": "gamepad_arm.js"
            },
            {
                "name": "left_front",
                "value": 0
            },
            {
                "name": "right_front",
                "value": 0
            },
            {
                "name": "left_back",
                "value": 0
            },
            {
                "name": "right_back",
                "value": 0
            }]
        },
        3:
        {
            "actions": [
            {
                "name": "light"
            }],
            "permanent": true
        },
        4:
        {
            "actions": [
            {
                "name": "servo_camera_horizontal",
                "value": -1,
                "relative_sensibility": 30
            }],
            "repeat": true
        },
        5:
        {
            "actions": [
            {
                "name": "servo_camera_horizontal",
                "value": 1,
                "relative_sensibility": 30
            }],
            "repeat": true
        },
        6:
        {
            "actions": [
            {
                "name": "servo_pliers_turn",
                "reverse_way": true
            }]
        },
        7:
        {
            "actions": [
            {
                "name": "servo_pliers",
                "reverse_way": true
            }]
        },
        8:
        {
            "actions": [
            {
                "name": "random_sounds"
            }],
            "permanent": true
        },
        9:
        {
            "actions": [
            {
                "name": "klaxon"
            }],
            "permanent": true
        },
        12:
        {
            "actions": [
            {
                "name": "up"
            },
            {
                "name": "down",
                "value": 0
            }],
            "permanent": true
        },
        13:
        {
            "actions": [
            {
                "name": "down"
            },
            {
                "name": "up",
                "value": 0
            }],
            "permanent": true
        }
    }
};