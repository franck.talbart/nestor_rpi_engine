var keyboard = {
    "Shift":
    {
        "speed": 100,
    },
    "Control":
    {
        "actions": [
        {
            "name": "light"
        }],
        "permanent": true
    },
    "PageUp":
    {
        "actions": [
        {
            "name": "up"
        },
        {
            "name": "down",
            "value": 0
        }],
        "permanent": true
    },
    "PageDown":
    {
        "actions": [
        {
            "name": "down"
        },
        {
            "name": "up",
            "value": 0
        }],
        "permanent": true
    },
    "p":
    {
        "actions": [
        {
            "name": "parking"
        }],
        "permanent": true
    },
    "b":
    {
        "actions": [
        {
            "name": "battery"
        }],
        "permanent": true
    },
    "k":
    {
        "actions": [
        {
            "name": "klaxon"
        }],
        "permanent": true
    },
    "s":
    {
        "actions": [
        {
            "name": "random_sounds"
        }],
        "permanent": true
    },
    "ArrowUp":
    {
        "actions": [
        {
            "name": "left_front",
            "value": "-{val_speed}"
        },
        {
            "name": "right_front",
            "value": "-{val_speed}"
        },
        {
            "name": "left_back",
            "value": "-{val_speed}"
        },
        {
            "name": "right_back",
            "value": "-{val_speed}"
        }],
        "group": "direction"
    },
    "ArrowDown":
    {
        "actions": [
        {
            "name": "left_front",
            "value": "{val_speed}"
        },
        {
            "name": "right_front",
            "value": "{val_speed}"
        },
        {
            "name": "left_back",
            "value": "{val_speed}"
        },
        {
            "name": "right_back",
            "value": "{val_speed}"
        }],
        "group": "direction"
    },
    "ArrowLeft":
    {
        "actions": [
        {
            "name": "left_front",
            "value": "{val_speed}"
        },
        {
            "name": "left_back",
            "value": "{val_speed}"
        },
        {
            "name": "right_front",
            "value": "-{val_speed}"
        },
        {
            "name": "right_back",
            "value": "-{val_speed}"
        }],
        "group": "direction"
    },
    "ArrowRight":
    {
        "actions": [
        {
            "name": "left_front",
            "value": "-{val_speed}"
        },
        {
            "name": "left_back",
            "value": "-{val_speed}"
        },
        {
            "name": "right_front",
            "value": "{val_speed}"
        },
        {
            "name": "right_back",
            "value": "{val_speed}"
        }],
        "group": "direction"
    },
    "r":
    {
        "actions": [
        {
            "name": "servo_arm_forward",
            "value": "-{val_speed}",
            "relative_sensibility": 30
        }],
        "repeat": true
    },
    "f":
    {
        "actions": [
        {
            "name": "servo_arm_forward",
            "value": "{val_speed}",
            "relative_sensibility": 30
        }],
        "repeat": true
    },
    "d":
    {
        "actions": [
        {
            "name": "servo_arm_turn",
            "value": "{val_speed}",
            "relative_sensibility": 30
        }],
        "repeat": true
    },
    "g":
    {
        "actions": [
        {
            "name": "servo_arm_turn",
            "value": "-{val_speed}",
            "relative_sensibility": 30
        }],
        "repeat": true
    },
    "y":
    {
        "actions": [
        {
            "name": "servo_arm_up",
            "value": "-{val_speed}",
            "relative_sensibility": 20
        }],
        "repeat": true
    },
    "h":
    {
        "actions": [
        {
            "name": "servo_arm_up",
            "value": "{val_speed}",
            "relative_sensibility": 20
        }],
        "repeat": true
    },
    "x":
    {
        "actions": [
        {
            "name": "servo_camera_horizontal",
            "value": -1,
            "relative_sensibility": 30
        }],
        "repeat": true
    },
    "w":
    {
        "actions": [
        {
            "name": "servo_camera_horizontal",
            "value": 1,
            "relative_sensibility": 30
        }],
        "repeat": true
    },
    "u":
    {
        "actions": [
        {
            "name": "servo_camera_vertical",
            "value": -1,
            "relative_sensibility": 30
        }],
        "repeat": true
    },
    "j":
    {
        "actions": [
        {
            "name": "servo_camera_vertical",
            "value": 1,
            "relative_sensibility": 30
        }],
        "repeat": true
    },
    "Enter":
    {
        "actions": [
        {
            "name": "servo_pliers"
        }],
        "permanent": true
    },
    "*":
    {
        "actions": [
        {
            "name": "servo_pliers_turn",
        }],
        "permanent": true
    }
};