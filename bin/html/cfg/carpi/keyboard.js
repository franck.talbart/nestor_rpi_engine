var keyboard = {
    "k":
    {
        "actions": [
        {
            "name": "klaxon"
        }],
        "permanent": true
    },
    "s":
    {
        "actions": [
        {
            "name": "random_sounds"
        }],
        "permanent": true
    },
    "ArrowUp":
    {
        "actions": [
        {
            "name": "propulsion",
            "value": "{val_speed}"
        }]
    },
    "ArrowDown":
    {
        "actions": [
        {
            "name": "propulsion",
            "value": "-{val_speed}"
        }]
    },
    "ArrowLeft":
    {
        "actions": [
        {
            "name": "wheel"
        }]
    },
    "ArrowRight":
    {
        "actions": [
        {
            "name": "wheel",
            "value": -1
        }]
    }
};