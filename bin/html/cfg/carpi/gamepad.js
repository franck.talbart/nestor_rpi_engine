var gamepad = {
    "threshold_sensibility": 0.20,
    "axis":
    {
        0:
        {
            "actions": [
            {
                "name": "wheel",
                "reverse": true
            }]
        }
    },
    "buttons":
    {
        1:
        {
            "actions": [
            {
                "name": "random_sounds"
            }],
            "permanent": true
        },
        3:
        {
            "actions": [
            {
                "name": "klaxon"
            }],
            "permanent": true
        },
        6:
        {
            "actions": [
            {
                "name": "propulsion",
                "reverse": true
            }]
        },
        7:
        {
            "actions": [
            {
                "name": "propulsion"
            }]
        }
    }
};