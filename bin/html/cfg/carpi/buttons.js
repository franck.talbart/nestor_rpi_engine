var buttons = {
    "klaxon":
    {
        "actions": [
        {
            "name": "klaxon"
        }],
        "permanent": true
    },
    "sound":
    {
        "actions": [
        {
            "name": "random_sounds"
        }],
        "permanent": true
    },
    "forward":
    {
        "actions": [
        {
            "name": "propulsion"
        }]
    },
    "backward":
    {
        "actions": [
        {
            "name": "propulsion",
            "value": -1
        }]
    },
    "left":
    {
        "actions": [
        {
            "name": "wheel"
        }]
    },
    "right":
    {
        "actions": [
        {
            "name": "wheel",
            "value": -1
        }]
    }
};