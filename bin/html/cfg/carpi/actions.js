var actions = {
    "klaxon":
    {
        "parameters":
        {
            "sound": 0
        },
        "name": "audio_video"
    },
    "propulsion":
    {
        "parameters":
        {
            "motor": "right"
        },
        "name": "L298N"
    },
    "random_sounds":
    {
        "parameters":
        {
            "sound": -1
        },
        "name": "audio_video"
    },
    "wheel":
    {
        "parameters":
        {
            "motor": "left"
        },
        "name": "L298N"
    }
};