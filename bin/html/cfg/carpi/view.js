var view_dashboard = {
    "temp_cpu":
    {
        "max": 55,
        "name": "Temp. CPU (C)"
    },
    "wifi_signal":
    {
        "min": 15,
        "max": 70,
        "progress_bar": true,
        "name": "Wifi (/70)"
    }
};

var view_l298n = {
    "left": "l298n_left",
    "right": "l298n_right",
};

var view_position = {};

var view_relay = {};

var view_servo = {};

var video = {
    "bitrate": 1000000,
    "codec": "H264hw"
};

var tasks_map = {
    "automation_log": "log",
    "automation_monitoring": "item",
    "controller_audio_video": "camera",
    "controller_L298N": "l298n"
};

// Message to display on the webpage
var motd = "";