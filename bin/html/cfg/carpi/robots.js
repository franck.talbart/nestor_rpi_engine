// List of robots
var robots = {
    "Freda (Paris)": "https://serveur.talbart.fr:8000",
    "Nestor (Bordeaux)": "https://serveur2.talbart.fr:8000",
    "Carpi (Paris)": "https://serveur.talbart.fr:9000"
};

// Default robot
if (typeof window.robot === 'undefined')
    window.robot = robots["Carpi (Paris)"];