/************************************************************************
 NestoRPi Engine
 Copyright (C) 2015-2025 Franck Talbart

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

/* Author: Franck Talbart
   webrtc-streamer written by Michel Promonet
   and updated for the NestoRPi Engine */

//------------------------------------------------------------------------------
/**
 * Create a WebRTCserver instance
 *
 * @return the webrtc object
 */
function load_webrtc(options)
{
    var webRtcServer = new WebRtc("video");
    webRtcServer.connect(options);
    return webRtcServer;
}

//------------------------------------------------------------------------------
/**
 * Interface with WebRTC API - constructor
 *
 * @param video_element: id of the video element tag
 * @return nothing
 */
function WebRtc(video_element)
{
    if (typeof video_element === "string")
    {
        this.video_element = $("#" + video_element)[0];
    }
    else
    {
        this.video_element = video_element;
    }
    this.video_element.style.backgroundImage = "url(css/img/rings.svg)";

    this.pc = null;
    this.token = Math.random().toString(36).substr(2); // remove `0.`

    this.pcOptions = {
        "optional": [
        {
            "DtlsSrtpKeyAgreement": true
        }]
    };

    this.mediaConstraints = {
        offerToReceiveAudio: true,
        offerToReceiveVideo: true
    };

    this.ice_servers = null;
    this.earlyCandidates = [];
}

//------------------------------------------------------------------------------
/**
 * Connect a WebRTC Stream to video_element
 *
 * @param options:  options of WebRTC call
 */
WebRtc.prototype.connect = function(options)
{
    var bind = this;
    // getICeServers is not already received
    if (!this.ice_servers)
    {
        console.log("Get ice_servers");

        $.get(window.robot + "/api/getIceServers", function(response)
        {
            bind.on_receive_get_ice_servers.call(bind,
                JSON.parse(response));
            bind.set_options(options);
        });
    }
    else
    {
        this.on_receive_get_ice_servers(this.ice_servers);
        this.set_options(options);
    } // create Offer

    this.timeout = setTimeout(function()
    {
        bind.timeout_disconnect();
    }, TIMEOUT);
}

//------------------------------------------------------------------------------
/**
 * Disconnect a WebRTC Stream and clear video_element source
 *
 * @return nothing
 */
WebRtc.prototype.disconnect = function(callback)
{
    clearInterval(this.interval_ping);
    clearTimeout(this.timeout);
    if (this.video_element)
    {
        this.video_element.src = "";
    }
    var bind = this;
    if (this.pc)
    {
        var username = this.general_options["username"];
        var password = this.general_options["password"];
        $.get(window.robot + "/api/hangup?peer_id=" + this.pc.peer_id + "&user=" + username + "&passwd=" + password + "&token=" + this.token, function(response)
        {
            try
            {
                bind.pc.close();
            }
            catch (e)
            {
                console.log("Failure close peer connection:" + e);
            }
            bind.pc = null;
            if (callback !== undefined) callback();
        });
    }
    else if (callback !== undefined) callback();
}

//------------------------------------------------------------------------------
/**
 * Disconnect a WebRTC Stream if no action was done after a spcified amount of time
 *
 * @return nothing
 */
WebRtc.prototype.timeout_disconnect = function()
{
    this.disconnect(function()
    {
        alert("You have been disconnected because of your inactivity. Please refresh this page to run again.");
    });
}

//------------------------------------------------------------------------------
/**
 * Change the webrtc options
 *
 * @param dict_options: the dictionary of options
 * @return nothing
 */
WebRtc.prototype.set_options = function(dict_options = {})
{
    if (typeof this.general_options == 'undefined')
        this.general_options = {
            "token": this.token
        };

    for (var key in dict_options)
        this.general_options[key] = dict_options[key];

    var options = "&options=";
    for (var key in this.general_options)
    {
        if (this.general_options[key] !== undefined)
        {
            options += unescape(encodeURIComponent(key + "=" + this.general_options[key] + ";"));
        }
    }

    this.create_offer("call", options);
}

//------------------------------------------------------------------------------
/**
 * Get the webrtc options
 *
 * @return the dict
 */
WebRtc.prototype.get_options = function()
{
    return this.general_options;
}

//------------------------------------------------------------------------------
/**
 * Add the local microphone to the peer connection
 *
 * @param audio_input: the audio device name
 * @return nothing
 */
WebRtc.prototype.add_microphone = function(audio_input)
{
    const constraints = {
        audio:
        {
            deviceId: audio_input ?
            {
                exact: audio_input
            } : undefined
        }
    };
    var bind = this;
    navigator.mediaDevices.getUserMedia(constraints).then(
        function(stream)
        {
            const audio_tracks = stream.getAudioTracks();
            if (audio_tracks.length > 0)
                console.log(`Using Audio device: ${audio_tracks[0].label}`);
            bind.disconnect(function()
            {
                bind.on_receive_get_ice_servers(bind.ice_servers);
                stream.getTracks().forEach(track => bind.pc.addTrack(track));
                console.log('Adding Local Stream to peer connection');
                bind.set_options(
                {});
            });

        }

    ).catch(handle_error);
}

//------------------------------------------------------------------------------
/**
 * Create an offer
 *
 * @param api_url: the offer name used to generate the URL to call
 * @param append_url: append a string to the URL
 * return nothing
 */
WebRtc.prototype.create_offer = function(api_url, append_url = "")
{
    var bind = this;

    codec = $('#codec').val();
    this.pc.createOffer(this.mediaConstraints).then(function(session_description)
    {
        switch (codec)
        {
            case 'H264hw':
                session_description.sdp = remove_codec(session_description.sdp, 'VP8');
                session_description.sdp = remove_codec(session_description.sdp, 'VP9');
                break;
            case 'VP8':
                session_description.sdp = remove_codec(session_description.sdp, 'H264');
                session_description.sdp = remove_codec(session_description.sdp, 'VP9');
                break;
            case 'VP9':
                session_description.sdp = remove_codec(session_description.sdp, 'H264');
                session_description.sdp = remove_codec(session_description.sdp, 'VP8');
                break;
        }
        console.log("Create offer:" + JSON.stringify(session_description));
        bind.pc.setLocalDescription(session_description, function()
        {
            $.post(window.robot + "/api/" + api_url + bind.callurl + append_url,
                    JSON.stringify(session_description)
                )
                .done(function(response)
                {
                    bind.on_receive_call.call(bind, JSON.parse(response));
                });
        }, function(error)
        {
            console.log("setLocalDescription error:" + JSON.stringify(error));
        });
    }, function(error)
    {
        alert("Create offer error:" + JSON.stringify(error));
    });
}

//------------------------------------------------------------------------------
/*
 * Getice_servers callback
 *
 * @return nothing
 */
WebRtc.prototype.on_receive_get_ice_servers = function(ice_servers)
{
    this.ice_servers = ice_servers;
    this.pcConfig = ice_servers ||
    {
        "ice_servers": []
    };
    try
    {
        this.pc = this.create_peer_connection();
        this.callurl = "?peer_id=" + this.pc.peer_id;
        // clear early candidates
        this.earlyCandidates.length = 0;

    }
    catch (e)
    {
        this.disconnect();
        alert("connect error: " + e);
    }
}

//------------------------------------------------------------------------------
/*
 * Call the servers to get the ice candidate
 *
 * return nothing
 */
WebRtc.prototype.get_ice_candidate = function()
{
    var bind = this;
    $.get(window.robot + "/api/getIceCandidate?peer_id=" + this.pc.peer_id,
        function(response)
        {
            bind.on_receive_candidate.call(bind, JSON.parse(response));
        });
}

//------------------------------------------------------------------------------
/*
 * Start recording the RTC video
 *
 * return nothing
 */
WebRtc.prototype.start_record = function()
{
    this.recorder = RecordRTC(this.stream_to_be_recorded,
    {
        type: 'video',
        recorderType: MediaStreamRecorder,
        audioBitsPerSecond: 512 * 8 * 1024,
        videoBitsPerSecond: 512 * 8 * 1024,
        bitsPerSecond: 256 * 8 * 1024,
        frameRate:
        {
            exact: 60
        }
    });
    this.recorder.startRecording();
}

//------------------------------------------------------------------------------
/*
 * Record the web RTC video locally
 *
 * return nothing
 */
WebRtc.prototype.stop_record = function()
{
    var bind = this;
    bind.recorder.stopRecording(function()
    {
        blob = bind.recorder.getBlob();
        var fileExtension = "webm";
        var mimeType = 'video/webm';
        var fileName = 'NestoRPi-' + (new Date).toISOString().replace(/:|\./g, '-') + '.' + fileExtension;
        var file = new File([blob], fileName,
        {
            type: mimeType
        });
        invokeSaveAsDialog(file, file.name);
    });
}

//------------------------------------------------------------------------------
/*
 * Create RTCPeerConnection
 *
 * @return the RTCPeerConnection
 */
WebRtc.prototype.create_peer_connection = function()
{
    console.log("create_peer_connection  config: " + JSON.stringify(this.pcConfig) + " option:" + JSON.stringify(this.pcOptions));
    var pc = new RTCPeerConnection(this.pcConfig, this.pcOptions);
    pc.peer_id = Math.random();

    var bind = this;

    pc.onicecandidate = function(evt)
    {
        bind.on_ice_candidate.call(bind, evt);
    };

    pc.ontrack = function(evt)
    {
        bind.stream_to_be_recorded = evt.streams[0];
        bind.on_add_stream.call(bind, evt);
    };

    pc.oniceconnectionstatechange = function(evt)
    {
        console.log("oniceconnectionstatechange  state: " + pc.iceConnectionState);

        if (bind.video_element)
        {
            if (pc.iceConnectionState === "connected")
            {
                bind.video_element.style.opacity = "1.0";
                setTimeout(
                    function()
                    {
                        bind.video_element.style.backgroundImage = "url(css/img/no_video.png)";
                    }, 3000);
            }
            else if (pc.iceConnectionState === "disconnected")
            {
                bind.video_element.style.opacity = "0.25";
                bind.connect();
            }
            else if ((pc.iceConnectionState === "failed") || (pc.iceConnectionState === "closed"))
            {
                bind.video_element.style.opacity = "0.5";
            }
            else if (pc.iceConnectionState === "new")
            {
                bind.get_ice_candidate.call(bind, pc.peer_id)
            }
        }
    }

    try
    {
        bind.data_channel = pc.createDataChannel("ClientDataChannel_" + pc.peer_id);
        bind.data_channel.onopen = function()
        {
            console.log("local datachannel open");
            bind.interval_ping = setInterval(function()
            {
                bind.send_ping();
            }, 2000);
        }
        bind.data_channel.onmessage = function(evt)
        {
            var msg = trim(JSON.stringify(evt.data), '"');
            console.log("local datachannel recv:" + msg);
            msg = msg.split(COMM_DELIMITER);
            if (msg[0] == MsgType.PING)
            {
                ping = performance.now() - bind.time_ping;
                $("#ping").text(Math.round(ping));
            }
            else if (msg[0] == MsgType.PERMISSION)
            {
                alert("You don't have the permission to perform this action (" + msg[1] + "). Please sign in to get the permissions.");
            }
            else // Tasks
            {
                process_message(msg);
            }
        }
    }
    catch (e)
    {
        console.log("Cannot create datachannel error: " + e);
    }

    console.log("Created RTCPeerConnnection with config: " + JSON.stringify(this.pcConfig) + "option:" + JSON.stringify(this.pcOptions));
    return pc;
}

//------------------------------------------------------------------------------
/*
 * Send a ping message to the server
 *
 * @return nothing
 */
WebRtc.prototype.send_ping = function()
{
    console.log("local datachannel send: ping");
    this.time_ping = performance.now();
    this.data_channel.send(MsgType.PING);
}


//------------------------------------------------------------------------------
/*
 * Send a message to the server
 *
 * @param msg_type: MsgType
 * @param task: task name to invoke
 * @param msg_list: the array of messages
 * @return nothing
 */
WebRtc.prototype.send_msg_server = function(msg_type, task, msg_list)
{
    var bind = this;
    clearTimeout(this.timeout);
    this.timeout = setTimeout(function()
    {
        bind.timeout_disconnect();
    }, TIMEOUT);

    var msg = msg_type.toString() + COMM_DELIMITER + task + COMM_DELIMITER;
    msg += msg_list.join(COMM_DELIMITER);
    console.log("local datachannel send:" + msg);

    try
    {
        this.data_channel.send(msg);
    }
    catch (error)
    {
        if (error instanceof DOMException && error.name === 'InvalidStateError')
        {
            console.log("RTCDataChannel not opened");
        }
        else
        {
            console.log("Error while sending onto datachannel: ", error);
        }
    }

    if (typeof this.first == 'undefined')
        this.first = true;
    // This can be done only if the user interact with the webpage first
    // (Chrome design)
    if (this.first)
    {
        force_unmute();
        this.first = false;
    }
}

//------------------------------------------------------------------------------
/*
 * RTCPeerConnection IceCandidate callback
 *
 * @return nothing
 */
WebRtc.prototype.on_ice_candidate = function(event)
{
    if (event.candidate)
    {
        if (this.pc.currentRemoteDescription)
        {
            this.add_ice_candidate(this.pc.peer_id, event.candidate);
        }
        else
        {
            this.earlyCandidates.push(event.candidate);
        }
    }
    else
    {
        console.log("End of candidates.");
    }
}

//------------------------------------------------------------------------------
/*
 * Add ice candidate by calling the server
 *
 * @param peer_id: the peer ID
 * @param candidate: the candidate to add
 * @return nothing
 */
WebRtc.prototype.add_ice_candidate = function(peer_id, candidate)
{
    var bind = this;
    $.post(window.robot + "/api/addIceCandidate?peer_id=" + peer_id,
            JSON.stringify(candidate)
        )
        .done(function(response)
        {
            console.log("add_ice_candidate ok:" + response);
        });
}

//------------------------------------------------------------------------------
/*
 * RTCPeerConnection AddStream callback
 *
 * @return nothing
 */
WebRtc.prototype.on_add_stream = function(event)
{
    console.log("Remote track added:" + JSON.stringify(event));
    bind = this;
    bind.video_element.srcObject = event.streams[0];
    var promise = bind.video_element.play();
    if (promise !== undefined)
    {
        promise.catch(function(error)
        {
            console.warn("error:" + error);
            bind.video_element.setAttribute("controls", true);
        });
    }
}

//------------------------------------------------------------------------------
/*
 * AJAX /call callback
 *
 * @param data_json: the offer
 * @return nothing
 */
WebRtc.prototype.on_receive_call = function(data_json)
{
    var bind = this;
    console.log("offer: " + JSON.stringify(data_json));
    var descr = new RTCSessionDescription(data_json);
    this.pc.setRemoteDescription(descr, function()
    {
        console.log("setRemoteDescription ok");
        while (bind.earlyCandidates.length)
        {
            var candidate = bind.earlyCandidates.shift();
            bind.add_ice_candidate.call(bind, bind.pc.peer_id, candidate);
        }
        bind.get_ice_candidate.call(bind, bind.pc.peer_id)
    }, function(error)
    {
        console.log("setRemoteDescription error:" + error.toString());
    });
}

//------------------------------------------------------------------------------
/*
 * AJAX /getIceCandidate callback
 *
 * @param data_json: candidate
 * @return nothing
 */
WebRtc.prototype.on_receive_candidate = function(data_json)
{
    console.log("candidate: " + JSON.stringify(data_json));
    if (data_json)
    {
        for (var i = 0; i < data_json.length; ++i)
        {
            var candidate = new RTCIceCandidate(data_json[i]);

            console.log("Adding ICE candidate :" + JSON.stringify(candidate));
            this.pc.addIceCandidate(candidate, function()
            {
                console.log("addIceCandidate OK");
            }, function(error)
            {
                console.log("addIceCandidate error:" + JSON.stringify(error));
            });
        }
    }
}

//------------------------------------------------------------------------------
/*
 * AJAX callback for Error
 *
 * @return nothing
 */
WebRtc.prototype.on_error = function(status)
{
    console.log("onError:" + status);
}

//------------------------------------------------------------------------------
/*
 * Remove a codec from a session description
 *
 * @param orgdsp: session description
 * @param codec: the codec to remove
 * @return result
 */
function remove_codec(orgsdp, codec)
{
    const internalFunc = (sdp) =>
    {
        const codecre = new RegExp('(a=rtpmap:(\\d*) ' + codec + '\/90000\\r\\n)');
        const rtpmaps = sdp.match(codecre);
        if (rtpmaps == null || rtpmaps.length <= 2)
        {
            return sdp;
        }
        const rtpmap = rtpmaps[2];
        let modsdp = sdp.replace(codecre, "");

        const rtcpre = new RegExp('(a=rtcp-fb:' + rtpmap + '.*\r\n)', 'g');
        modsdp = modsdp.replace(rtcpre, "");

        const fmtpre = new RegExp('(a=fmtp:' + rtpmap + '.*\r\n)', 'g');
        modsdp = modsdp.replace(fmtpre, "");

        const aptpre = new RegExp('(a=fmtp:(\\d*) apt=' + rtpmap + '\\r\\n)');
        const aptmaps = modsdp.match(aptpre);
        let fmtpmap = "";
        if (aptmaps !== null && aptmaps.length >= 3)
        {
            fmtpmap = aptmaps[2];
            modsdp = modsdp.replace(aptpre, "");

            const rtppre = new RegExp('(a=rtpmap:' + fmtpmap + '.*\r\n)', 'g');
            modsdp = modsdp.replace(rtppre, "");
        }

        let videore = /(m=video.*\r\n)/;
        const videolines = modsdp.match(videore);
        if (videolines !== null)
        {
            //If many m=video are found in SDP, this program doesn't work.
            let videoline = videolines[0].substring(0, videolines[0].length - 2);
            const videoelems = videoline.split(" ");
            let modvideoline = videoelems[0];
            videoelems.forEach((videoelem, index) =>
            {
                if (index === 0) return;
                if (videoelem == rtpmap || videoelem == fmtpmap)
                {
                    return;
                }
                modvideoline += " " + videoelem;
            })
            modvideoline += "\r\n";
            modsdp = modsdp.replace(videore, modvideoline);
        }
        return internalFunc(modsdp);
    }
    return internalFunc(orgsdp);
}