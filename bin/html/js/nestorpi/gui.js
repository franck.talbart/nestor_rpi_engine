/************************************************************************
 NestoRPi Engine
 Copyright (C) 2015-2025 Franck Talbart

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ************************************************************************/

/* Author: Franck Talbart */

//------------------------------------------------------------------------------
/* Update the number of users
 *
 * @return nothing
 **/
function update_nb_users()
{
    $.get(window.robot + "/api/getPeerConnectionList", function(response)
    {
        $("#users").html(JSON.parse(response).length);
    });
}

//------------------------------------------------------------------------------
/* Update the camera links
 *
 * @param nb_devices: the number of devices
 * @return nothing
 **/
function update_gui_camera(nb_devices)
{
    $("#camera").html("");
    if (nb_devices > 1)
    {
        for (let i = 0; i < nb_devices; i++)
        {
            $("#camera").append(" <button onclick=\"window.webRtcServer.disconnect(function(){window.webRtcServer.connect({'device_id': " + i + "});})\">Camera " + (i + 1) + "</button> ");
        }
    }
    $("#camera").append(" <button onclick=\"window.webRtcServer.disconnect(function(){init_robot();})\">Re-connect</button> ");
}

//------------------------------------------------------------------------------
/* Update an item in the dashboard
 *
 * @param id: the item id
 * @param val: the value
 * @return nothing
 **/
function update_gui_item(id, val)
{
    if (!$("#" + id + "_container").length)
    {
        var name = id;
        if (id in view_dashboard && "name" in view_dashboard[id])
            name = view_dashboard[id]["name"];
        var html = [
            "<div id='" + id + "_container" + "' class='dashboard_sensors_item'>",
            "<h3>",
            name,
            "</h3>",
            "<div id='" + id + "_val'>",
            "</div>"
        ].join("\n");
        $("#dashboard_sensors").append(html);
    }
    $("#" + id + "_val").text(val);
    if (id in view_dashboard)
    {
        class_name = ("min" in view_dashboard[id] && val < view_dashboard[id]["min"] ||
            "max" in view_dashboard[id] && val > view_dashboard[id]["max"]) ? "value_error" : "value_ok";
        $("#" + id + "_val").attr("class", class_name);
        if ("progress_bar" in view_dashboard[id] && view_dashboard[id]["progress_bar"])
        {
            if (!$("#" + id + "_progress").length)
                var html = [
                    "<div class='container_progress_bar'>",
                    "<div id='" + id + "_progress' class='progress_bar' percent='0' ></div>",
                    "</div>"
                ].join("\n");
            $("#" + id + "_container").append(html);
            val = (val > view_dashboard[id]["max"]) ? view_dashboard[id]["max"] : val;
            val = (val - view_dashboard[id]["min"]) / (view_dashboard[id]["max"] - view_dashboard[id]["min"]) * 100;
            $("#" + id + "_progress").attr("percent", Math.round(val));
        }
    }
}

//------------------------------------------------------------------------------
/* Update the log
 *
 * @param val: the value
 * @return nothing
 **/
function update_gui_log(val)
{
    val = unescape_slashes(val);
    var ansi_up = new AnsiUp;
    var html = ansi_up.ansi_to_html(val);
    $("#log").append(html + "<br/>");
    $("#log").scrollTop($("#log")[0].scrollHeight);
}

//------------------------------------------------------------------------------
/* Update the gui relay
 *
 * @param action_name: the action
 * @param parameter: the list of actions where the status relay must be updated
 * @param val: the value
 * @return nothing
 **/
function update_gui_relay(action_name, parameter, val)
{
    if (action_name in view_relay && parameter in view_relay[action_name])
    {
        if (val)
        {
            $("#" + view_relay[action_name][parameter]).css(
            {
                display: "block",
                opacity: 1
            });
        }
        else
        {
            $("#" + view_relay[action_name][parameter]).css(
            {
                display: "block",
                opacity: 0.3
            });
        }
    }
}

//------------------------------------------------------------------------------
/* Update the gui servo
 *
 * @param action_name: the action
 * @param value: the value
 * @return nothing
 **/
function update_gui_servo(action_name, value)
{
    value = Math.round(value);
    // View
    if (action_name in view_servo)
    {
        for (servo of view_servo[action_name])
        {
            $("#" + servo).css(
            {
                display: "block",
                opacity: 0.3
            });
            $("#" + servo).text(value);
        }
    }

    // Mouse
    if (typeof mouse_position.x == 'undefined')
    {
        mouse_position.x = [-2, -2, -2, -2];
        mouse_position.y = [-2, -2, -2, -2];
    }

    for (i = 0; i < 3; i++)
    {
        if (i.toString() in mouse)
        {
            for (action of mouse[i.toString()]["x"]["actions"])
            {
                if (action["name"] == action_name && mouse_position.x[i] == -2)
                {
                    mouse_position.x[i] = value / 100;
                    if ("reverse" in action && action["reverse"])
                        mouse_position.x[i] = -mouse_position.x[i];
                }
            }
            for (action of mouse[i.toString()]["y"]["actions"])
            {
                if (action["name"] == action_name && mouse_position.y[i] == -2)
                {
                    mouse_position.y[i] = value / 100;
                    if ("reverse" in action && action["reverse"])
                        mouse_position.y[i] = -mouse_position.y[i];
                }
            }
        }
    }
    update_gui_position(action_name, value);
}

//------------------------------------------------------------------------------
/* Update the gui position
 *
 * @param action_name: the action
 * @param value: the value
 * @return nothing
 **/
function update_gui_position(action_name, value)
{
    value = Math.round((value + 100) / 2) / 100;
    // View
    if (action_name in view_position)
    {
        $("#" + view_position[action_name]).css(
        {
            display: "block"
        });
        if (view_position[action_name] == "position_x")
        {
            value = value * $('#video').width() - $("#" + view_position[action_name]).width();
            if (value < 0) value = 0;
            $("#" + view_position[action_name]).css("left", value + "px");
        }
        else if (view_position[action_name] == "position_y")
        {
            value = value * $('#video').height() - $("#" + view_position[action_name]).height();
            if (value < 0) value = 0;
            $("#" + view_position[action_name]).css("bottom", value + "px");
        }
    }
}

//------------------------------------------------------------------------------
/* Update the gui L298N
 *
 * @param action_name: the action
 * @param value: the value
 * @return nothing
 **/
function update_gui_l298n(action_name, parameter, value)
{
    full_action_name = action_name + "_" + parameter;
    if (full_action_name in view_l298n)
    {
        $("#" + view_l298n[full_action_name]).css(
        {
            display: "block",
            opacity: 0.3
        });
        $("#" + view_l298n[full_action_name]).text(Math.round(value));
    }
}

//------------------------------------------------------------------------------
/* Update the gui gamepad
 *
 * @param enable: display the icon if true, hide it otherwhise
 * @return nothing
 **/
function update_gui_gamepad(enable)
{
    if (enable)
    {
        $("#gamepad_detected").css(
        {
            opacity: 1
        });
    }
    else
    {
        $("#gamepad_detected").css(
        {
            opacity: 0.3
        });
    }
}

//------------------------------------------------------------------------------
/* Update the gui progress bars
 *
 * @return nothing
 **/
function update_gui_progress_bar()
{
    $(".progress_bar").each(function()
    {
        var percent = parseInt($(this).attr("percent"));

        if (percent < 35)
        {
            $(this).animate(
            {
                width: percent + "%"
            }, 500);
        }
        else if (percent < 100)
        {
            $(this).animate(
            {
                width: percent + "%",
                backgroundColor: "#E9AF3E"
            }, 300);
        }
        else if (percent == 100)
        {
            $(this).animate(
            {
                width: "100%",
                backgroundColor: "#7dc323"
            }, 300);
        }
    });
    $(".percent").each(function()
    {
        var percent = parseInt($(this).attr("percent"));
        var id = $(this).attr("id");
        var increase = setInterval(function()
        {
            var value = parseInt($("#" + id).html());

            if (value == percent)
            {
                clearInterval(increase);
            }
            else
            {

                $("#" + id).html(value + 1);
                if (value == 34)
                {
                    $("#" + id).css("color", "#E9AF3E");
                    $("#f" + id).css("color", "#E9AF3E");
                }
                else if (value == 99)
                {
                    $("#" + id).css("color", "#7dc323");
                    $("#f" + id).css("color", "#7dc323");
                }
            }
        }, 10);
    });
}

//------------------------------------------------------------------------------
/* Update the odometry grid
 *
 * @param x
 * @param y
 * @return nothing
 **/
function update_gui_odometry(x, y)
{
    if (typeof update_gui_odometry.grid === 'undefined')
    {
        update_gui_odometry.grid = new OdometryGrid("canvas_odometry", "grid_canvas_odometry");
    }
    update_gui_odometry.grid.update_position(x, y);
}

//------------------------------------------------------------------------------
/* Load the hotlinks buttons
 *
 * @return nothing
 **/
function load_hotlinks_buttons()
{
    hotlinks = {
        "admin": window.robot + "/admin.html",
        "help": window.robot + "/api/help"
    };
    if (typeof url_monitoring !== "undefined")
        hotlinks["monitoring"] = url_monitoring;
    $("#hotlinks_buttons").html("");
    for (var key in hotlinks)
    {
        {
            $("#hotlinks_buttons").append(
                '<a href="' + hotlinks[key] + '" target="_blank"><div id="' + key + '" title="' + key + '" class="button active"></div></a>'
            );
        }
    }
}

//------------------------------------------------------------------------------
/* Load the controller settings
 *
 * @return nothing
 **/
function load_controller(name)
{
    result = "<table>";
    for (var button in window[name])
    {
        if (typeof window[name][button]["actions"] !== "undefined")
        {
            value = "";
            for (action in window[name][button]["actions"])
            {
                if (value !== "") value += " / ";
                if (typeof window[name][button]["actions"][action]["value"] !== "undefined" &&
                    window[name][button]["actions"][action]["value"] == 0) continue;
                value += window[name][button]["actions"][action]["name"];
            }
            result += "<tr><td>" + button + "</td><td>" + value + "</td></tr>";
        }
        else
        {
            for (id in window[name][button])
            {
                value = "";
                for (action in window[name][button][id]["actions"])
                {
                    if (value !== "") value += " / ";
                    local_value = window[name][button][id]["actions"][action]["name"];
                    if (local_value.indexOf("js") !== -1)
                        local_value = "Change mode: " + local_value;
                    if (typeof window[name][button][id]["actions"][action]["value"] !== "undefined" &&
                        window[name][button][id]["actions"][action]["value"] == 0) continue;
                    value += local_value;
                }
                result += "<tr><td>" + button + " : " + id + "</td><td>" + value + "</td></tr>";
            }
        }
    }
    result += "</table>";
    $("#" + name + "_settings").html(result);
}


//------------------------------------------------------------------------------
/* Show or hide a jquery element
 *
 * @return nothing
 **/
function show_hide(obj)
{
    $("#button_close").hide();
    if (typeof this.previous == 'undefined')
    {
        this.previous = $("#motd");
    }

    if (typeof this.previous !== 'undefined' && this.previous !== obj)
    {
        $(this.previous).hide();
        var video = $(this.previous).find("video")[0];
        if (typeof video !== 'undefined') video.pause();
    }
    if ($(obj).is(":visible"))
    {
        $(obj).hide();
        var video = $(obj).find("video")[0];
        if (typeof video !== 'undefined')
            video.pause();
    }
    else
    {
        $(obj).show();
    }
    this.previous = obj;
}

//------------------------------------------------------------------------------
/* Odometry GUI
 **/
class OdometryGrid
{
    constructor(container_id, canvas_id, grid_size = 10)
    {
        this.container = document.getElementById(container_id);
        this.canvas = document.getElementById(canvas_id);
        // show the grid
        this.container.style.display = 'block';
        this.ctx = this.canvas.getContext("2d");
        this.grid_size = grid_size;
        this.x = this.canvas.width / 2; // Center
        this.y = this.canvas.height / 2;

        this.draw_grid();
        this.draw_point();

        this.center_view();

    }

    draw_grid()
    {
        this.ctx.strokeStyle = "#ddd";
        for (let i = 0; i < this.canvas.width; i += this.grid_size)
        {
            this.ctx.beginPath();
            this.ctx.moveTo(i, 0);
            this.ctx.lineTo(i, this.canvas.height);
            this.ctx.stroke();
            this.ctx.beginPath();
            this.ctx.moveTo(0, i);
            this.ctx.lineTo(this.canvas.width, i);
            this.ctx.stroke();
        }
    }

    draw_point()
    {
        this.ctx.fillStyle = "blue";
        this.ctx.fillRect(this.x, this.y, this.grid_size - 2, this.grid_size - 2);
    }

    update_position(x, y)
    {
        this.x = this.canvas.width / 2 + x;
        this.y = this.canvas.height / 2 + y;

        if (this.x < 0) this.x = 0;
        if (this.x > this.canvas.width - this.grid_size) this.x = this.canvas.width - this.grid_size;
        if (y < 0) y = 0;
        if (y > this.canvas.height - this.grid_size) y = this.canvas.height - this.grid_size;

        this.draw_point();

        this.center_view();
    }

    center_view()
    {
        this.container.scrollLeft = this.x - this.container.clientWidth / 2;
        this.container.scrollTop = this.y - this.container.clientHeight / 2;
    }
}