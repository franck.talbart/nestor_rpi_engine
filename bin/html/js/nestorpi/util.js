/************************************************************************
 NestoRPi Engine
 Copyright (C) 2015-2025 Franck Talbart

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ************************************************************************/

/* Author: Franck Talbart */

//------------------------------------------------------------------------------
/* Trim a string by removing the specified char
 *
 * @param s: the string to trim
 * @param c: the char to remove
 * @return the trimmed string
 **/
function trim(s, c)
{
    if (c === "]") c = "\\]";
    if (c === "\\") c = "\\\\";
    return s.replace(new RegExp(
        "^[" + c + "]+|[" + c + "]+$", "g"
    ), "");
}

//------------------------------------------------------------------------------
/*
 * Error handler
 *
 * @return nothing
 **/
function handle_error(error)
{
    console.log(error.message, error.name);
}

//------------------------------------------------------------------------------
/* MD5 */
var MD5 = function(d)
{
    var r = M(V(Y(X(d), 8 * d.length)));
    return r.toLowerCase()
};

//------------------------------------------------------------------------------
/* MD5 */
function M(d)
{
    for (var _, m = "0123456789ABCDEF", f = "", r = 0; r < d.length; r++) _ = d.charCodeAt(r), f += m.charAt(_ >>> 4 & 15) + m.charAt(15 & _);
    return f
}

//------------------------------------------------------------------------------
/* MD5 */
function X(d)
{
    for (var _ = Array(d.length >> 2), m = 0; m < _.length; m++) _[m] = 0;
    for (m = 0; m < 8 * d.length; m += 8) _[m >> 5] |= (255 & d.charCodeAt(m / 8)) << m % 32;
    return _
}

//------------------------------------------------------------------------------
/* MD5 */
function V(d)
{
    for (var _ = "", m = 0; m < 32 * d.length; m += 8) _ += String.fromCharCode(d[m >> 5] >>> m % 32 & 255);
    return _
}

//------------------------------------------------------------------------------
/* MD5 */
function Y(d, _)
{
    d[_ >> 5] |= 128 << _ % 32, d[14 + (_ + 64 >>> 9 << 4)] = _;
    for (var m = 1732584193, f = -271733879, r = -1732584194, i = 271733878, n = 0; n < d.length; n += 16)
    {
        var h = m,
            t = f,
            g = r,
            e = i;
        f = md5_ii(f = md5_ii(f = md5_ii(f = md5_ii(f = md5_hh(f = md5_hh(f = md5_hh(f = md5_hh(f = md5_gg(f = md5_gg(f = md5_gg(f = md5_gg(f = md5_ff(f = md5_ff(f = md5_ff(f = md5_ff(f, r = md5_ff(r, i = md5_ff(i, m = md5_ff(m, f, r, i, d[n + 0], 7, -680876936), f, r, d[n + 1], 12, -389564586), m, f, d[n + 2], 17, 606105819), i, m, d[n + 3], 22, -1044525330), r = md5_ff(r, i = md5_ff(i, m = md5_ff(m, f, r, i, d[n + 4], 7, -176418897), f, r, d[n + 5], 12, 1200080426), m, f, d[n + 6], 17, -1473231341), i, m, d[n + 7], 22, -45705983), r = md5_ff(r, i = md5_ff(i, m = md5_ff(m, f, r, i, d[n + 8], 7, 1770035416), f, r, d[n + 9], 12, -1958414417), m, f, d[n + 10], 17, -42063), i, m, d[n + 11], 22, -1990404162), r = md5_ff(r, i = md5_ff(i, m = md5_ff(m, f, r, i, d[n + 12], 7, 1804603682), f, r, d[n + 13], 12, -40341101), m, f, d[n + 14], 17, -1502002290), i, m, d[n + 15], 22, 1236535329), r = md5_gg(r, i = md5_gg(i, m = md5_gg(m, f, r, i, d[n + 1], 5, -165796510), f, r, d[n + 6], 9, -1069501632), m, f, d[n + 11], 14, 643717713), i, m, d[n + 0], 20, -373897302), r = md5_gg(r, i = md5_gg(i, m = md5_gg(m, f, r, i, d[n + 5], 5, -701558691), f, r, d[n + 10], 9, 38016083), m, f, d[n + 15], 14, -660478335), i, m, d[n + 4], 20, -405537848), r = md5_gg(r, i = md5_gg(i, m = md5_gg(m, f, r, i, d[n + 9], 5, 568446438), f, r, d[n + 14], 9, -1019803690), m, f, d[n + 3], 14, -187363961), i, m, d[n + 8], 20, 1163531501), r = md5_gg(r, i = md5_gg(i, m = md5_gg(m, f, r, i, d[n + 13], 5, -1444681467), f, r, d[n + 2], 9, -51403784), m, f, d[n + 7], 14, 1735328473), i, m, d[n + 12], 20, -1926607734), r = md5_hh(r, i = md5_hh(i, m = md5_hh(m, f, r, i, d[n + 5], 4, -378558), f, r, d[n + 8], 11, -2022574463), m, f, d[n + 11], 16, 1839030562), i, m, d[n + 14], 23, -35309556), r = md5_hh(r, i = md5_hh(i, m = md5_hh(m, f, r, i, d[n + 1], 4, -1530992060), f, r, d[n + 4], 11, 1272893353), m, f, d[n + 7], 16, -155497632), i, m, d[n + 10], 23, -1094730640), r = md5_hh(r, i = md5_hh(i, m = md5_hh(m, f, r, i, d[n + 13], 4, 681279174), f, r, d[n + 0], 11, -358537222), m, f, d[n + 3], 16, -722521979), i, m, d[n + 6], 23, 76029189), r = md5_hh(r, i = md5_hh(i, m = md5_hh(m, f, r, i, d[n + 9], 4, -640364487), f, r, d[n + 12], 11, -421815835), m, f, d[n + 15], 16, 530742520), i, m, d[n + 2], 23, -995338651), r = md5_ii(r, i = md5_ii(i, m = md5_ii(m, f, r, i, d[n + 0], 6, -198630844), f, r, d[n + 7], 10, 1126891415), m, f, d[n + 14], 15, -1416354905), i, m, d[n + 5], 21, -57434055), r = md5_ii(r, i = md5_ii(i, m = md5_ii(m, f, r, i, d[n + 12], 6, 1700485571), f, r, d[n + 3], 10, -1894986606), m, f, d[n + 10], 15, -1051523), i, m, d[n + 1], 21, -2054922799), r = md5_ii(r, i = md5_ii(i, m = md5_ii(m, f, r, i, d[n + 8], 6, 1873313359), f, r, d[n + 15], 10, -30611744), m, f, d[n + 6], 15, -1560198380), i, m, d[n + 13], 21, 1309151649), r = md5_ii(r, i = md5_ii(i, m = md5_ii(m, f, r, i, d[n + 4], 6, -145523070), f, r, d[n + 11], 10, -1120210379), m, f, d[n + 2], 15, 718787259), i, m, d[n + 9], 21, -343485551), m = safe_add(m, h), f = safe_add(f, t), r = safe_add(r, g), i = safe_add(i, e)
    }
    return Array(m, f, r, i)
}

//------------------------------------------------------------------------------
/* MD5 */
function md5_cmn(d, _, m, f, r, i)
{
    return safe_add(bit_rol(safe_add(safe_add(_, d), safe_add(f, i)), r), m)
}

//------------------------------------------------------------------------------
/* MD5 */
function md5_ff(d, _, m, f, r, i, n)
{
    return md5_cmn(_ & m | ~_ & f, d, _, r, i, n)
}

//------------------------------------------------------------------------------
/* MD5 */
function md5_gg(d, _, m, f, r, i, n)
{
    return md5_cmn(_ & f | m & ~f, d, _, r, i, n)
}

//------------------------------------------------------------------------------
/* MD5 */
function md5_hh(d, _, m, f, r, i, n)
{
    return md5_cmn(_ ^ m ^ f, d, _, r, i, n)
}

//------------------------------------------------------------------------------
/* MD5 */
function md5_ii(d, _, m, f, r, i, n)
{
    return md5_cmn(m ^ (_ | ~f), d, _, r, i, n)
}

//------------------------------------------------------------------------------
/* MD5 */
function safe_add(d, _)
{
    var m = (65535 & d) + (65535 & _);
    return (d >> 16) + (_ >> 16) + (m >> 16) << 16 | 65535 & m
}

//------------------------------------------------------------------------------
/* MD5 */
function bit_rol(d, _)
{
    return d << _ | d >>> 32 - _
}

//------------------------------------------------------------------------------
/*
 * Login (create cookies and send credentials)
 *
 * @param username: the username
 * @param password: :)
 * @return nothing
 **/
function login(username, password)
{
    password = MD5(password);
    create_cookie("username", username, 100);
    create_cookie("password", password, 100);

    if (typeof window.webRtcServer !== "undefined")
        window.webRtcServer.disconnect(function()
        {
            init_robot();
        });
}

//------------------------------------------------------------------------------
/* On load event */
window.onload = function()
{
    init_form("#talk", "#button_talk");
    init_form("#password", "#button_login");

    if (typeof navigator.mediaDevices !== "undefined")
    {
        navigator.mediaDevices.enumerateDevices().then(function(device_infos)
        {
            const audio_input_select = document.querySelector('#audio_source');
            for (let i = 0; i !== device_infos.length; ++i)
            {
                const device_info = device_infos[i];
                const option = document.createElement('option');
                option.value = device_info.deviceId;
                if (device_info.kind === 'audioinput')
                {
                    option.text = device_info.label || `microphone ${audio_input_select.length + 1}`;
                    audio_input_select.appendChild(option);
                }
            }
        }).catch(handle_error);
    }

    setInterval(update_gui_progress_bar, 1000);
    setInterval(update_nb_users, 2000);
    update_gui_gamepad(false);

    window.addEventListener("gamepadconnected", (event) =>
    {
        var gp = navigator.getGamepads()[0];
        var cpt = 0;
        while (gp.axes.length == 0 && ++cpt < navigator.getGamepads().length)
            gp = navigator.getGamepads()[cpt];
        manage_gamepad(cpt);
        update_gui_gamepad(true);
    });

    window.addEventListener("gamepaddisconnected", (event) =>
    {
        console.log("disconnection event");
        update_gui_gamepad(false);
    });

    window.keypressed = {};
    document.addEventListener('keydown', (event) =>
    {
        repeat = false;
        if (event.key in keyboard && "repeat" in keyboard[event.key])
            repeat = keyboard[event.key]["repeat"];

        // Stop key from repeating on hold
        if (!repeat)
        {
            if (window.keypressed[event.which])
            {
                event.preventDefault();
            }
            else
            {
                window.keypressed[event.which] = true;
                process_key_event(event, 1);
            }
        }
        else
        {
            process_key_event(event, 1);
        }
    }, false);

    document.addEventListener('keyup', (event) =>
    {
        process_key_event(event, 0);
        window.keypressed[event.which] = false;
    }, false);

    $("#video").on('touchstart mousedown', function(evt)
    {
        evt.preventDefault();
        $("#video")[0].play(); // It can be on pause
        $("#video").on("mousemove touchmove", mouse_position);
    });

    $("#video").on("touchend mouseup mouseout", function(evt)
    {
        $("#video").off("mousemove touchmove", mouse_position);
        mouse_position.old_x = null;
        mouse_position.old_y = null;
    });

    // Despite the check below, we still need this
    $("#video").on('pause', function(e)
    {
        $("#video")[0].play();
    });

    init_robot();
}

//------------------------------------------------------------------------------
/**
 * Test a connection to a robot and add the link to the list of robot (header)
 *
 * @return nothing
 */
function test_connection(my_robot)
{
    $.ajax(
    {
        type: "GET",
        url: robots[my_robot] + "/api/name",
        cache: false,
        success: function(output)
        {
            output = JSON.parse(output);
            class_name = "online";
            if (robot == robots[my_robot])
                class_name += " " + "selected";
            html = "<li><a class='" + class_name + "' onclick='if (typeof window.webRtcServer !== \"undefined\") window.webRtcServer.disconnect(function(){window.robot=\"" + robots[my_robot] + "\";init_robot();});'>" + my_robot + "</a></li>";
            if (!$('#robots').text().indexOf(html) > -1) // If not already included
                $("#robots").append(html);
        },
        error: function(output, stat, err)
        {
            html = "<li><span class='offline'>" + my_robot + " (offline)</span></li>";
            if (!$('#robots').text().indexOf(html) > -1) // If not already included
                $("#robots").append(html);
        },
    });
}

//------------------------------------------------------------------------------
/**
 * Prepare the webpage to control the selected robot
 *
 * @return nothing
 */
function init_robot()
{
    $("#video")[0].style.backgroundImage = "url('css/img/offline.png')";

    username = read_cookie("username")
    password = read_cookie("password")

    if (username !== null)
    {
        $("#username").val(username);
        $("#password").val(password);
    }
    else
    {
        username = "default";
        password = "";
    }

    $("#dashboard_sensors").html("");
    $("#log").html("");
    $(".hidden").css("display", "none");
    load_cfg(["actions.js", "keyboard.js", "gamepad.js", "buttons.js", "mouse.js", "view.js", "robots.js"],
        function()
        {
            // We wait for the vars to be available (the scripts are just
            // loaded, not executed yet)
            wait_vars(["actions", "keyboard", "gamepad", "buttons", "mouse", "motd", "robots"],
                function()
                {
                    for (var element in buttons_map)
                        button_handler(element, buttons_map[element]);

                    $("#bitrate").val(video["bitrate"]);
                    $("#codec").val(video["codec"]);

                    for (const controller of ["keyboard", "gamepad", "mouse"])
                        load_controller(controller);

                    window.webRtcServer = load_webrtc(
                    {
                        "bitrate": $("#bitrate").val(),
                        "device_id": 0,
                        "disable_video": 0,
                        "username": username,
                        "password": password
                    });
                    load_version();
                    $("#motd").html(motd);
                    if (motd !== "")
                    {
                        $("#button_close").show();
                    }
                    $("#robots").html("");
                    for (var my_robot in robots)
                        test_connection(my_robot);

                    load_hotlinks_buttons();
                });
        });
}

//------------------------------------------------------------------------------
/* On unload event*/
window.onbeforeunload = function()
{
    if (typeof window.webRtcServer !== "undefined")
        window.webRtcServer.disconnect();
}

//------------------------------------------------------------------------------
/**
 * Get the name of the controlled RPi and execute a callback
 *
 * @param callback: callback to execute; the parameter is the name
 * @return nothing
 */
function get_name(callback)
{
    var url = "api/name";

    if (typeof window.robot !== "undefined")
        url = window.robot + "/" + url;

    $.get(url, function(response)
        {
            // Web page on local robot
            name = JSON.parse(response);
            callback(name);
        })
        .fail(function()
        {
            // Web page on external host
            jQuery.getScript("cfg/external/robots.js")
                .fail(function()
                {
                    alert("Can't load the robots list.");
                })
                .done(function()
                {
                    $.get(window.robot + "/api/name", function(response)
                        {
                            name = JSON.parse(response);
                            callback(name);
                        })
                        .fail(function()
                        {
                            // Fail safe:
                            // if the main robot is offline: load the list
                            // of robots from the "external" config file
                            window.webRtcServer = load_webrtc(
                            {});
                            $("#robot_name").text("Please select the robot");
                            $("#robots").html("");
                            for (var my_robot in robots)
                                test_connection(my_robot);
                        });
                });
        });
}

//------------------------------------------------------------------------------
/**
 * Load the client config files
 *
 * @param list_files: the list of config files to load
 * @param callback: executed after the load
 * @return nothing
 */
function load_cfg(list_files, callback)
{
    get_name(function(name)
    {
        txt_name = name.charAt(0).toUpperCase() + name.substring(1);
        $("#robot_name").text(txt_name);

        list_files.forEach(function(cfg_file, index)
        {
            console.log("Loading " + "cfg/" + name + "/" + cfg_file);
            jQuery.getScript("cfg/" + name + "/" + cfg_file)
                .fail(function()
                {
                    alert("Can't load the config files for " + name);
                })
                .done(function()
                {
                    if (index + 1 == list_files.length)
                        callback()
                });
        });
    });
}

//------------------------------------------------------------------------------
/**
 * Wait for a list of variables to be loaded
 *
 * @param list_vars: the list of variables to check
 * @param callback: executed after the check
 * @return nothing
 */
function wait_vars(list_vars, callback)
{
    var interval = setInterval(function()
    {
        cpt = 0;
        list_vars.forEach(function(var_, index)
        {
            console.log("Checking " + var_);
            if (window[var_] !== undefined)
                cpt++;
            if (cpt == list_vars.length)
            {
                clearInterval(interval);
                callback();
            }
        });
    }, 100);
}

//------------------------------------------------------------------------------
/**
 * Capitalize the first letter of a string
 *
 * @param s: the string
 * @return capitalized string
 */
function capitalize(s)
{
    if (typeof s !== 'string') return '';
    return s.charAt(0).toUpperCase() + s.slice(1);
}

//------------------------------------------------------------------------------
/**
 * Save the picture to the disk
 *
 * @param filename
 * @param file size
 * @param content
 */
function save_picture(filename, content)
{
    var binaryArray = new Uint8Array(content.length);
    for (var i = 0; i < content.length; i++)
    {
        binaryArray[i] = content.charCodeAt(i);
    }
    var blob = new Blob([binaryArray],
    {
        type: "image/jpeg"
    });
    saveAs(blob, filename);
}

//------------------------------------------------------------------------------
/* Process the received message from the server
 *
 * @param message: the message to process
 * @return nothing
 **/
function process_message(message)
{
    task_type = message[0];
    task_name = message[1];
    task = task_type + "_" + task_name;
    parameter_name = message[2];
    parameter_value = message[3];
    value = message[4];
    type = tasks_map[task];

    if (type === undefined)
    {
        alert("Unknown task " + task);
    }

    if (typeof process_message.pic_filename == 'undefined')
    {
        process_message.pic_filename = "";
        process_message.pic_content = "";
    }
    switch (type)
    {
        case "item":
            update_gui_item(parameter_name, parameter_value);
            break;
        case "log":
            update_gui_log(parameter_name);
            break;
        case "odometry":
            update_gui_odometry(parseInt(parameter_value), -parseInt(parameter_name));
        case "camera":
            if (parameter_name == "nb_dev")
            {
                update_gui_camera(parameter_value);
            }
            else if (parameter_name == "pic_file")
            {
                if (parameter_value == process_message.pic_filename)
                {
                    if (value == "end")
                    {
                        console.log("Saving the picture");
                        save_picture(parameter_value, atob(process_message.pic_content));
                        process_message.pic_filename = "";
                        process_message.pic_content = "";
                    }
                    else
                    {
                        process_message.pic_content += value;
                    }
                }
                else
                {
                    process_message.pic_filename = parameter_value;
                    process_message.pic_content = value;
                }
            }
            break;
        default:
            for (var action_name in actions)
            {
                if (!("values" in actions[action_name]))
                    actions[action_name]["values"] = {};
                if (task == "controller_" + actions[action_name]["name"] && parameter_name in actions[action_name]["parameters"] &&
                    actions[action_name]["parameters"][parameter_name] == parameter_value)
                    actions[action_name]["values"][parameter_name] = parseFloat(value);
                // If every params has the same value, then the action must have this value
                var same_val = null;
                for (var parameter in actions[action_name]["parameters"])
                {
                    if (!(parameter in actions[action_name]["values"]))
                    {
                        same_val = null;
                        break;
                    }
                    if (same_val == null)
                    {
                        same_val = actions[action_name]["values"][parameter];
                    }
                    else
                    {
                        if (same_val !== actions[action_name]["values"][parameter])
                        {
                            same_val = 0;
                            break;
                        }
                    }
                }
                if (same_val !== null)
                    actions[action_name]["value"] = same_val;
            }
            if (type == "relay")
                update_gui_relay(parameter_name, parameter_value, parseFloat(value) == 100);
            else if (type == "servo")
                update_gui_servo(parameter_value, parseFloat(value));
            else if (type == "l298n")
                update_gui_l298n(parameter_name, parameter_value, parseFloat(value));
            else
            {
                alert("Type: " + type + "unknown !!");
            }
            break;
    }
}

//------------------------------------------------------------------------------
/* Unescape slashes from a string
 *
 * @param str: the string to format
 * @return the formatted string
 **/
function unescape_slashes(str)
{
    // add another escaped slash if the string ends with an odd
    // number of escaped slashes which will crash JSON.parse
    let parsedStr = str.replace(/(^|[^\\])(\\\\)*\\$/, "$&\\");

    try
    {
        parsedStr = JSON.parse(`"${parsedStr}"`);
    }
    catch (e)
    {
        return str;
    }
    return parsedStr;
}

//------------------------------------------------------------------------------
/* Load the version on the webpage
 *
 * @return nothing
 **/
function load_version()
{
    $.get(window.robot + "/api/version", function(response)
    {
        $('#version').html(JSON.parse(response));
    });
}

//------------------------------------------------------------------------------
/* Auto click the button when pressing enter
 *
 * @param id_input: id of the input field
 * @param id_button: id of the button to click
 * @return nothing
 **/
function init_form(id_input, id_button)
{
    var input = $(id_input);
    // Execute a function when the user releases a key on the keyboard
    input.on("keyup", function(event)
    {
        // Number 13 is the "Enter" key on the keyboard
        if (event.keyCode === 13)
        {
            // Cancel the default action, if needed
            event.preventDefault();
            // Trigger the button element with a click
            $(id_button).click();
        }
    });
}

//------------------------------------------------------------------------------
/* Update the mute button
 *
 * @return nothing
 **/
function update_audio_status()
{
    if ($("#video").prop("muted"))
    {
        $("#mute").attr("class", "button mute");
    }
    else
    {
        $("#mute").attr("class", "button mute2");
    }
}

//------------------------------------------------------------------------------
/* Unmute the video
 *
 * @return nothing
 **/
function force_unmute()
{
    $("#video").prop("muted", false);
    update_audio_status();
}

//------------------------------------------------------------------------------
/* Mute handler for the video
 *
 * @return nothing
 **/
function mute()
{
    $("#video").prop("muted", !$("#video").prop("muted"));
    update_audio_status();
}

//------------------------------------------------------------------------------
/* Create a cookie
 *
 * @param name: the name
 * @param value: the value
 * @param days: expiration
 * @return nothing
 **/
function create_cookie(name, value, days)
{
    if (days)
    {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        var expires = "; expires=" + date.toGMTString();
    }
    else var expires = "";
    document.cookie = name + "=" + value + expires + "; path=/";
}

//------------------------------------------------------------------------------
/* Read a cookie
 *
 * @param name: the name
 * @return the value or null
 **/
function read_cookie(name)
{
    var name_eq = name + "=";
    var ca = document.cookie.split(';');

    for (var i = 0; i < ca.length; i++)
    {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1, c.length);
        if (c.indexOf(name_eq) == 0) return c.substring(name_eq.length, c.length);
    }
    return null;
}