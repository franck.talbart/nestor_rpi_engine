/************************************************************************
 NestoRPi Engine
 Copyright (C) 2015-2025 Franck Talbart

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

/* Author: Franck Talbart */

var buttons_map = {
    "relay_fan": "fan",
    "relay_arm_up": "neck_up",
    "relay_arm_down": "neck_down",
    "relay_light": "light",
    "relay_motor_camera_move": "camera",
    "relay_parking": "parking",
    "relay_battery": "battery",
    "klaxon": "klaxon",
    "sound": "sound",
    "servo_camera_left": "camera_left",
    "servo_camera_right": "camera_right",
    "servo_camera_up": "camera_up",
    "servo_camera_down": "camera_down",
    "robot_left": "left",
    "robot_right": "right",
    "robot_forward": "forward",
    "robot_backward": "backward",
    "servo_arm_right": "arm_right",
    "servo_arm_left": "arm_left",
    "servo_arm_backward": "arm_backward",
    "servo_arm_forward": "arm_forward",
    "servo_arm_up": "arm_up",
    "servo_arm_down": "arm_down",
    "servo_pliers": "pliers",
    "servo_pliers_turn": "pliers_turn"
};

//------------------------------------------------------------------------------
/**
 * Process Manage the click & touch screen
 *
 * @param element: the button
 * @param action: the action to execute
 */
function button_handler(element, action)
{
    if (action in buttons)
    {
        $("#" + element).css(
        {
            display: "block"
        });
    }
    $("#" + element).off('touchstart mousedown');
    $("#" + element).on('touchstart mousedown', function(e)
    {
        e.preventDefault();
        if (action in buttons)
        {
            process_action(buttons[action], 1);
        }
    });
    $("#" + element).off('touchend mouseup');
    $("#" + element).on('touchend mouseup', function(e)
    {
        e.preventDefault();
        if (action in buttons)
            process_action(buttons[action], 0);
    });
}

//------------------------------------------------------------------------------
/**
 * Process Manage the mouse event
 *
 * @param evt: the event
 */
function mouse_position(evt)
{
    touch = undefined
    which = evt.buttons;
    // Touchscreen
    if (evt.originalEvent.touches)
    {
        touch = evt.originalEvent.touches[0];
        which = evt.originalEvent.touches.length;
    }
    which = which - 1; // Starts at 1

    if (!(which in mouse)) return;

    if (typeof mouse_position.x == 'undefined')
    {
        mouse_position.x = [0, 0, 0, 0];
        mouse_position.y = [0, 0, 0, 0];
    }

    pos_x = evt.pageX || touch.pageX;
    pos_y = evt.pageY || touch.pageY;

    var x = (pos_x - $('#video').offset().left) / $('#video').width() * 2 - 1;
    var y = (pos_y - $('#video').offset().top) / $('#video').height() * 2 - 1;

    if (typeof mouse_position.old_x == 'undefined' || mouse_position.old_x == null)
    {
        mouse_position.old_x = [-2, -2, -2, -2];
        mouse_position.old_y = [-2, -2, -2, -2];
    }

    if (mouse_position.old_x[which] == -2)
    {
        mouse_position.old_x[which] = x;
        mouse_position.old_y[which] = y;
    }

    mouse_position.x[which] += (x - mouse_position.old_x[which]);
    mouse_position.y[which] += (y - mouse_position.old_y[which]);

    mouse_position.x[which] = (mouse_position.x[which] > 1) ? 1 : mouse_position.x[which];
    mouse_position.x[which] = (mouse_position.x[which] < -1) ? -1 : mouse_position.x[which];

    mouse_position.y[which] = (mouse_position.y[which] > 1) ? 1 : mouse_position.y[which];
    mouse_position.y[which] = (mouse_position.y[which] < -1) ? -1 : mouse_position.y[which];

    if ('x' in mouse[which])
        process_action(mouse[which]['x'], mouse_position.x[which]);

    if ('y' in mouse[which])
        process_action(mouse[which]['y'], mouse_position.y[which]);

    mouse_position.old_x[which] = x;
    mouse_position.old_y[which] = y;
}

//------------------------------------------------------------------------------
/**
 * Process the actions parameters before running them
 *
 * @param input_actions: the input actions ordered by the input device (keyboard, gamepad ...)
 * @param value: the value returned by the input device
 */
function process_action(input_actions, value)
{
    // Internal
    if ("speed" in input_actions)
    {
        if (value)
        {
            $("#speed").slider("value", input_actions["speed"]);
        }
        else
        {
            $("#speed").slider("value", 50);
        }
        return;
    }

    // Tasks
    var input_actions_list = input_actions["actions"];
    var permanent = (("permanent" in input_actions) ? input_actions["permanent"] : false);
    var group = (("group" in input_actions) ? input_actions["group"] : null);

    if (typeof this.last_action == "undefined")
        this.last_action = {};

    if (group !== null && !(group in this.last_action))
        this.last_action[group] = null;

    // Permanent
    if (permanent && !value)
        return;

    if (group !== null && value)
    {
        if (this.last_action[group] !== null)
        {
            // diff_actions is used to avoid useless switch of the relay
            run_action(this.last_action[group], diff_actions(this.last_action[group], input_actions_list), 0, permanent);
        }
        else
        {
            this.last_action[group] = input_actions_list;
        }
    }
    if (group !== null && !value)
    {
        // if the last action was the same we currently have, it means no other actions
        // was asked, so we can just ignore
        if (this.last_action[group] !== null && JSON.stringify(this.last_action[group]) == JSON.stringify(input_actions_list))
        {
            this.last_action[group] = null;
        }
        run_action(input_actions_list, diff_actions(input_actions_list, this.last_action[group]), value, permanent);
    }
    else
    {
        run_action(input_actions_list, actions, value, permanent);
    }

    if (group !== null && !value)
    {
        if (this.last_action[group] !== null)
            run_action(this.last_action[group], actions, 1, permanent);
    }
}

//------------------------------------------------------------------------------
/**
 * Remove from input_action_a the parameters included into input_action_b and return the resulting actions themselves
 */
function diff_actions(input_action_a, input_action_b)
{
    var final_actions = {};

    for (input_action of input_action_a)
    {
        // Deep copy
        final_actions[input_action["name"]] = JSON.parse(JSON.stringify(actions[input_action["name"]]));
    }

    if (input_action_b !== null)
    {
        for (input_action of input_action_b)
        {
            for (parameter_name_b in actions[input_action["name"]]["parameters"])
            {
                for (action_name_a in final_actions)
                {
                    for (parameter_name_a in final_actions[action_name_a]["parameters"])
                    {
                        if (parameter_name_a == parameter_name_b &&
                            final_actions[action_name_a]["parameters"][parameter_name_a] == actions[input_action["name"]]["parameters"][parameter_name_b])
                        {
                            delete final_actions[action_name_a]["parameters"][parameter_name_a];
                        }
                    }
                }
            }
        }
    }
    return final_actions;
}

//------------------------------------------------------------------------------
/**
 * Called after a key event
 */
function process_key_event(event, value)
{
    // Prevent key from being capturer if the focus is in an input form
    switch (event.target.tagName.toLowerCase())
    {
        case "input":
        case "textarea":
        case "select":
        case "button":
            return;
            break;
        default:
            if (event.key in keyboard)
            {
                process_action(keyboard[event.key], value);
            }
            break;
    }
}

//------------------------------------------------------------------------------
/**
 * Called in a loop to get gamepad status and run actions
 */
function manage_gamepad(id_gp)
{

    var gp = navigator.getGamepads()[id_gp];
    if (typeof this.old_values_btn == "undefined")
    {
        this.old_values_btn = [];
        this.old_values_axis = [];
        for (var num_button = 0; num_button < gp.buttons.length; ++num_button)
            this.old_values_btn.push(0);
        for (var num_axis = 0; num_axis < gp.axes.length; ++num_axis)
            this.old_values_axis.push(0);
    }

    // Buttons
    for (var num_button = 0; num_button < gp.buttons.length; ++num_button)
    {
        if (num_button in gamepad["buttons"])
        {
            var value = gp.buttons[num_button].value;
            repeat = false;
            if ("repeat" in gamepad["buttons"][num_button])
                repeat = gamepad["buttons"][num_button]["repeat"];
            if (!repeat)
            {
                if (this.old_values_btn[num_button] !== value)
                {
                    this.old_values_btn[num_button] = value;
                    process_action(gamepad["buttons"][num_button], value)
                }
            }
            else
            {
                process_action(gamepad["buttons"][num_button], value)
            }
        }
    }

    // Axis
    for (var num_axis = 0; num_axis < gp.axes.length; ++num_axis)
    {
        if (num_axis in gamepad["axis"])
        {
            var value = gp.axes[num_axis];
            var reverse = (value > 0) ? 1 : -1;
            value = Math.abs(value);
            var threshold_str = "threshold_sensibility";
            if (Math.abs(value) <= gamepad[threshold_str])
                new_value = 0;
            else
                new_value = ((value - gamepad[threshold_str]) * (1 / (1 - gamepad[threshold_str])));
            repeat = false;
            if ("repeat" in gamepad["axis"][num_axis])
                repeat = gamepad["axis"][num_axis]["repeat"];
            if (threshold_str in gamepad["axis"][num_axis])
            {
                threshold = gamepad["axis"][num_axis][threshold_str];
                if (value <= threshold)
                    new_value = 0;
                else
                    new_value = ((value - threshold) * (1 / (1 - threshold)));
            }
            value = reverse * new_value;

            if (!repeat)
            {
                if (this.old_values_axis[num_axis] !== value)
                {
                    this.old_values_axis[num_axis] = value;
                    process_action(gamepad["axis"][num_axis], value);
                }
            }
            else
            {
                process_action(gamepad["axis"][num_axis], value);
            }
        }
    }
    // We call it at the end to be sure it's not executed multiple times in //
    setTimeout(function()
    {
        manage_gamepad(id_gp);
    }, 50);
}

//------------------------------------------------------------------------------
/**
 * Run an action
 *
 * @param action: the action to execute
 * @param value: the value of the action
 * @param permanent: true if permanent flag, false otherwise
 */
function run_action(input_actions_list, compacted_actions, value, permanent)
{
    for (const input_action of input_actions_list)
    {
        final_value = value; // value may be changed from one iteration to another
        if (input_action["name"].endsWith(".js"))
        {
            if (final_value)
            {
                // We clean gamepad to prevent the pending actions to be executed
                // while the new config file is being loaded (async)
                gamepad["axis"] = {};
                gamepad["buttons"] = {};
                load_cfg([input_action["name"]], function()
                {
                    if (typeof mode !== 'undefined')
                    {
                        $("#mode").text(mode);
                    }
                    load_controller("gamepad");
                });
            }
        }
        else if (input_action["name"] in compacted_actions)
        {
            // Threshold
            if ("threshold" in input_action)
            {
                var operator_gt = ("operator" in input_action) ? (input_action["operator"] == ">") : true;
                if (operator_gt)
                {
                    final_value = (final_value > input_action["threshold"]) ? 1 : 0;
                }
                else
                {
                    final_value = (final_value < input_action["threshold"]) ? 1 : 0;
                }
            }

            // Permanent
            if (permanent && final_value && "value" in compacted_actions[input_action["name"]])
            {
                final_value = (compacted_actions[input_action["name"]]["value"]) ? 0 : final_value;
            }

            // Forced value
            final_value = (final_value && "value" in input_action) ? input_action["value"] : final_value;

            if (typeof final_value == "string" && final_value.includes(VAL_SPEED_PLACE_HOLDER))
            {
                final_value = parseFloat(final_value.replace(VAL_SPEED_PLACE_HOLDER, $('#speed').slider('value') / 100));
            }

            // Reverse
            var reverse = ("reverse" in input_action) ? input_action["reverse"] : false;
            if (reverse) final_value *= -1;
            var reverse_way = ("reverse_way" in input_action) ? input_action["reverse_way"] : false;
            if (reverse_way) final_value = 1 - final_value;

            if ("relative_sensibility" in input_action)
            {
                if (!("relative_value" in compacted_actions[input_action["name"]]))
                    compacted_actions[input_action["name"]]["relative_value"] = compacted_actions[input_action["name"]]["value"] / 100;
                // We don't directly use input_action["name"]["value"] anymore as it
                // won't be smooth with high latencies
                final_value = compacted_actions[input_action["name"]]["relative_value"] + final_value / input_action["relative_sensibility"];
                final_value = (final_value > 1) ? 1 : final_value;
                final_value = (final_value < -1) ? -1 : final_value;
                compacted_actions[input_action["name"]]["relative_value"] = final_value;
            }

            final_value = final_value * 100;
            final_value = final_value.toFixed(2);

            var task = compacted_actions[input_action["name"]]["name"];
            var parameters = compacted_actions[input_action["name"]]["parameters"];

            for (parameter in parameters)
            {
                window.webRtcServer.send_msg_server(MsgType.TASK_CONTROLLER,
                    task, [parameter, parameters[parameter], final_value]);
            }
        }
        else
        {
            alert("Action " + input_action["name"] + " not found.");
        }
    }
}