/************************************************************************
 NestoRPi Engine
 Copyright (C) 2015-2025 Franck Talbart

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

/* Author: Franck Talbart */

//------------------------------------------------------------------------------
// ACTIONS TRIGGERS
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
/* Send a talk action to the server
 *
 * @param txt: the message to say
 * @return nothing
 **/
function talk(txt)
{
    window.webRtcServer.send_msg_server(MsgType.TASK_CONTROLLER, "voice", ["mbrola", txt]);
}

//------------------------------------------------------------------------------
/* Send a picture action to the server
 *
 * @return nothing
 **/
function picture()
{
    camera_id = window.webRtcServer.get_options()["device_id"];
    window.webRtcServer.send_msg_server(MsgType.TASK_CONTROLLER, "audio_video", ["pic", camera_id]);
}

//------------------------------------------------------------------------------
/* Start / stop the video recording
 *
 * @return nothing
 **/
function record()
{
    if (typeof this.status == "undefined")
        this.status_record = false;
    if (!this.status_record)
    {
        window.webRtcServer.start_record();
        $("#record").css("filter", "saturate(20) hue-rotate(170deg)");
        this.blink = setInterval(function()
        {
            $("#record").fadeTo(900, 0).delay(300).fadeTo(800, 1);
        }, 2200);
    }
    else
    {
        window.webRtcServer.stop_record();
        clearInterval(this.blink);
        $("#record").css("filter", "");
    }
    this.status_record = !this.status_record;
}