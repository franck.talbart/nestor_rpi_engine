#!/bin/bash
#************************************************************************
# NestoRPi Engine
# Copyright (C) 2015-2025 Franck Talbart
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#************************************************************************

# Author: Franck Talbart
# This script is used to format the JS scripts

set -euo pipefail
BASE_DIR=$(cd `dirname "$0"` && pwd)

find "${BASE_DIR}"/js/nestorpi -name "*.js" -exec js-beautify --brace-style=expand -r {} \;
find "${BASE_DIR}"/cfg -name "*.js" -exec js-beautify --brace-style=expand -r {} \;
