#!/bin/bash
#************************************************************************
# NestoRPi Engine
# Copyright (C) 2015-2025 Franck Talbart
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#************************************************************************

# Author: Franck Talbart
# This script is used to run the NestoRPi Engine

set -eo pipefail

BASE_DIR=$(cd `dirname "$0"` && pwd)

export NRPI_ROOT_DIR=${BASE_DIR}
if [ -z "${NRPI_CFG_DIR}" ]; then
    export NRPI_CFG_DIR="${NRPI_ROOT_DIR}/cfg/${HOSTNAME}"
    [ ! -d "${NRPI_CFG_DIR}" ] && export NRPI_CFG_DIR="${NRPI_ROOT_DIR}/cfg/freda"
fi

export LD_LIBRARY_PATH="${NRPI_ROOT_DIR}"
# Libcamera
export LIBCAMERA_IPA_CONFIG_PATH="${NRPI_ROOT_DIR}/config_ipa"
export LIBCAMERA_IPA_MODULE_PATH="${NRPI_ROOT_DIR}/ipa"
export LIBCAMERA_IPA_PROXY_PATH="${NRPI_ROOT_DIR}/"

BIN="${NRPI_ROOT_DIR}/nestorpi_engine"
[ ! -f "${BIN}" ] && echo "Please compile the project first." && exit
ulimit -c unlimited # core dump

# "nice" is required for "realtime" threads
nice -n -19 "${BIN}" $@
