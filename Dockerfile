# NestoRPi Engine in a container
# This Dockerfile is NOT used to make a clean & small docker container.
# Its goal is to prepare a proper environment to build and test the engine.
# As third parties (in particular libcamera) require specific libc / compilers
# which are not necesseraly available on every system, it is much simpler
# to use Docker as a building environment

# Common build ---------------------------------------
FROM debian:bookworm AS builder
WORKDIR /

ENV DEBIAN_FRONTEND=noninteractive
ENV TERM=xterm

RUN (apt update && apt upgrade -y -q && apt dist-upgrade -y -q && apt -y -q autoclean && apt -y -q autoremove)
# NRPi deps
RUN apt install -y -q git build-essential cmake pkg-config python3 wget
# Libcamera deps
RUN apt install -y -q meson ninja-build python3-jinja2 python3-ply python3-yaml
RUN git config --global --add safe.directory '*'

# Debug build -----------------------------------------
FROM builder AS build_debug
WORKDIR /nestorpi_engine
ENV TERM=xterm
RUN apt install -y -q libao-dev libasound2-dev libmpg123-dev libpulse-dev doxygen

# Test ------------------------------------------------
FROM build_debug AS test
WORKDIR /nestorpi_engine
ENV TERM=xterm
RUN apt install -y -q clang-format cppcheck curl jq jsbeautifier
