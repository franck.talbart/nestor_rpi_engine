#!/usr/bin/env python3
# ************************************************************************
# NestoRPi Engine
# Copyright (C) 2015-2025 Franck Talbart
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# ************************************************************************
import logging
import re
import subprocess
import sys

from pathlib import Path

# -------------------------------------------------------------------------------
# Consts
# Dirs to sync
RSYNC_INCLUDE = ["etc/", "lib/", "usr/"]

# Rsync options
RSYNC_CMD = "/usr/bin/rsync"
RSYNC_OPTIONS = ["-rRlvu", "--stats", "--delete-after"]

# Target
TARGET_DIR = Path("fs").resolve()


# -------------------------------------------------------------------------------
def rsync_err_msg(retcode):
    """
    Error code from rsync manual page
    """
    errorcode_to_msg = {
        1: "Syntax or usage error",
        2: "Protocol incompatibility",
        3: "Errors selecting input/output files, dirs",
        4: "Requested  action not supported: an attempt was made to manipulate 64-bit files on a platform\
that cannot support them; or an option was specified that is supported by the client and  not\
by the server.",
        5: "Error starting client-server protocol",
        6: "Daemon unable to append to log-file",
        10: "Error in socket I/O",
        11: "Error in file I/O",
        12: "Error in rsync protocol data stream",
        13: "Errors with program diagnostics",
        14: "Error in IPC code",
        20: "Received SIGUSR1 or SIGINT",
        21: "Some error returned by waitpid()",
        22: "Error allocating core memory buffers",
        23: "Partial transfer due to error",
        24: "Partial transfer due to vanished source files",
        25: "The --max-delete limit stopped deletions",
        30: "Timeout in data send/receive",
        35: "Timeout waiting for daemon connection",
    }
    return errorcode_to_msg.get(retcode, "error code not found")


# -------------------------------------------------------------------------------
def process_rsync_rootfs(user):
    # Building rsync command line
    rsync_full_command = (
        [RSYNC_CMD]
        + RSYNC_OPTIONS
        + ["--include-from=rsync_include_list.txt"]
        + ["--exclude-from=rsync_exclude_list.txt"]
    )

    for dir_rsync in RSYNC_INCLUDE:
        cmd = rsync_full_command + [user + ":/" + dir_rsync] + [str(TARGET_DIR)]
        logging.debug(cmd)
        ret = subprocess.call(cmd, shell=False)
        if ret != 0:
            logging.error("Rsync error : %s", rsync_err_msg(ret))
            return ret

    return 0


# -------------------------------------------------------------------------------
def relative(target: Path, origin: Path):
    """return path of target relative to origin"""
    try:
        return Path(target).resolve().relative_to(Path(origin).resolve())
    except ValueError as e:  # target does not start with origin
        # recursion with origin (eventually origin is root so try will succeed)
        return Path("..").joinpath(relative(target, Path(origin).parent))


# -------------------------------------------------------------------------------
def relativelinks_handlelink(filep, subdir):
    """
    Convert absolute link to relative link
    """
    link = filep.readlink()

    # Already relative
    if str(link)[0] != "/":
        return
    if str(link).startswith(str(TARGET_DIR)):
        return

    # Remove the first /
    link = Path(str(link)[1:])

    logging.debug("File Starting %s link %s", filep, link)
    filep.unlink()
    filep.symlink_to(relative(Path(TARGET_DIR / link), subdir))


# -------------------------------------------------------------------------------
def process_relativelinks():
    for _file in TARGET_DIR.rglob("*"):
        if _file.is_symlink():
            relativelinks_handlelink(_file, _file.parent)


# -------------------------------------------------------------------------------
def symlink_force(target, link_name):
    """
    Making pkg-config links
    """
    try:
        link_name.symlink_to(target)
    except FileExistsError:
        link_name.unlink()
        link_name.symlink_to(target)
    except Exception as e:
        logging.error('%s -- target:"%s", link_name:"%s"', e, target, link_name)


# -------------------------------------------------------------------------------
def inplace_change(filename, old_string, new_string):
    """
    GNU linker script fixing
    This function will search the entire rootfs path with 'grep' command.
    """
    with open(filename, mode="r", encoding="utf-8") as f:
        s = f.read()
        if old_string not in s:
            logging.info('"%s" not found in %s.', old_string, s)
            return

    try:
        with open(filename, mode="w", encoding="utf-8") as f:
            logging.info(
                'Changing "%s" to "%s" in %s', old_string, new_string, filename
            )
            s = s.replace(old_string, new_string)
            f.write(s)
    except (OSError, IOError) as e:
        logging.error('%s -- target:"%s"', e, filename)
        return


# -------------------------------------------------------------------------------
def fix_process_ld_scripts(filename):
    file_contents = []
    if not filename.exists():
        logging.info("linker script file does not exist: %s", filename)

    with open(filename, mode="r", encoding="utf-8") as fstream:
        file_contents = fstream.readlines()

    # Check whether this file is GNU linker script
    if not "/* GNU ld script" in file_contents[0]:
        # stop fixing link script
        logging.info("the file is not the linker script: %s", filename)
        return

    # Search 'GROUP' keyword in file content
    for line_content in file_contents:
        if "GROUP" in line_content:
            token_list = [x for x in re.split("[(), ]", line_content) if x]
            for item in token_list:
                if item[0] == "/":
                    # This means that this item has absolute path, it need to be fixed
                    # Remove the first /
                    item_concat = Path(str(item)[1:])
                    if Path(TARGET_DIR / item_concat).exists():
                        split_head = filename.parent
                        relpath = relative(TARGET_DIR / item_concat, split_head)
                        inplace_change(filename, str(item), str(relpath))


# -------------------------------------------------------------------------------
def process_ld_scripts():
    process_ld_scripts_command = [
        "/bin/grep",
        "-rl",
        "--exclude=*",
        "--include=*.so",
        '"GNU ld script"',
        str(TARGET_DIR),
    ]
    grep_command = " ".join(process_ld_scripts_command)
    proc = subprocess.Popen(grep_command, stdout=subprocess.PIPE, shell=True)
    for line in proc.stdout:
        # the real code does filtering here
        fix_process_ld_scripts(Path(line.strip().decode("utf-8")))


# -------------------------------------------------------------------------------
def main(argv):
    logging.basicConfig(level=logging.INFO)
    if len(argv) not in [2, 3]:
        logging.info("Usage: %s <user@hostname> [target dir]", argv[0])
        return 1

    if not sys.platform.startswith("linux"):
        logging.error("RPi RootFS does not support this platform: %r", sys.platform)
        return 1

    global TARGET_DIR
    TARGET_DIR = Path("fs64/")
    if len(argv) == 3:
        TARGET_DIR = Path(argv[2]).resolve()
    logging.info("* Rootfs syncing from %s", argv[1])
    ret = process_rsync_rootfs(argv[1])
    if ret != 0:
        return ret

    logging.info("* Fixing absolute links")
    process_relativelinks()

    logging.info("* Fixing ld scripts absolute path to relative path")
    process_ld_scripts()

    logging.info("Done!")
    return 0


# -------------------------------------------------------------------------------
if __name__ == "__main__":
    sys.exit(main(sys.argv))
