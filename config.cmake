#************************************************************************
# NestoRPi Engine
# Copyright (C) 2015-2025 Franck Talbart
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#************************************************************************

# Author: Franck Talbart

# Title
set(NRPI_BUILD "nestorpi_engine")

# Paths
set(BIN_DIR "${CMAKE_CURRENT_SOURCE_DIR}/bin/")
set(SRC_DIR "${CMAKE_CURRENT_SOURCE_DIR}/src/")
set(CORE_DIR "${SRC_DIR}/core/")
set(DOC_DIR "${CMAKE_CURRENT_SOURCE_DIR}/doc/")
set(HTML_DIR "${BIN_DIR}/html/")
set(THIRD_PARTY_DIR "${CMAKE_CURRENT_SOURCE_DIR}/third_party/")
set(THIRD_PARTY_SRC_DIR "${THIRD_PARTY_DIR}/src/")
set(CIVETWEB_INC_DIR "${THIRD_PARTY_DIR}/civetweb/include/")
set(CIVETWEB_SRC_DIR "${THIRD_PARTY_DIR}/civetweb/src/")
set(LIBCAMERA_DIR "${THIRD_PARTY_DIR}/libcamera/")
set(RPICAM_APPS_DIR "${THIRD_PARTY_DIR}/rpicam-apps/")
set(MISC_DIR "${CMAKE_CURRENT_SOURCE_DIR}/misc/")
set(TOOLS_DIR "${CMAKE_CURRENT_SOURCE_DIR}/tools/")
set(DEPOT_TOOLS_DIR "${TOOLS_DIR}/depot_tools/")
set(SYSTEMD_SCRIPT_DIR "${MISC_DIR}/systemd/")
set(WEBRTC_ROOT ${THIRD_PARTY_DIR}/webrtc)
set(WEBRTC_INCLUDE ${WEBRTC_ROOT}/src ${WEBRTC_ROOT}/src/third_party/abseil-cpp ${WEBRTC_ROOT}/src/third_party/jsoncpp/source/include ${WEBRTC_ROOT}/src/third_party/jsoncpp/generated ${WEBRTC_ROOT}/src/third_party/libyuv/include)
set(WEBRTC_OBJS ${WEBRTC_ROOT}/src/out/obj)

# IPs
set(IP_RPI_CARPI "carpi")
set(IP_RPI_FREDA "freda")
set(IP_RPI_NESTOR "nestor")

# Tools
set(CLANG_FORMAT_BIN "clang-format")
set(CPPCHECK_BIN "cppcheck")

# WebRTC parameters
set(WEBRTC_ARGS rtc_include_tests=false\nrtc_enable_protobuf=false\nuse_custom_libcxx=false\nrtc_build_examples=false\nrtc_build_tools=false\nrtc_include_pulse_audio=false\ntreat_warnings_as_errors=false\nrtc_use_pipewire=false\nrtc_use_x11=false\nenable_js_protobuf=false\nlibyuv_use_sme=false\n)

set(NINJA_TARGET webrtc rtc_json builtin_video_decoder_factory builtin_video_encoder_factory peer_connection p2p_server_utils)

# Flags
set(CMAKE_CXX_FLAGS "-O3 -W -Wall -Wextra -pedantic -Wno-psabi -Wno-variadic-macros ${CMAKE_CXX_FLAGS}") # default, concatenated to Release / Debug
set(CMAKE_C_FLAGS "${CMAKE_CXX_FLAGS}")
# in debug mode please disable strip (CMakeLists.txt) if sent to nestor / freda
set(CMAKE_CXX_FLAGS_DEBUG "-O0 -pg -g -DDEBUG_MODE -fsanitize=address -fsanitize=undefined") # Debug
set(CMAKE_C_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG}") # Debug
set(CMAKE_LINKER_FLAGS_DEBUG "-fsanitize=address -fsanitize=undefined") # Debug
