#!/bin/bash
#************************************************************************
# NestoRPi Engine
# Copyright (C) 2015-2025 Franck Talbart
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#************************************************************************

# Author: Franck Talbart
# Unified script for building, deploying, and testing the NestoRPi Engine.

set -euo pipefail

BASE_DIR=$(cd "$(dirname "$0")" && pwd)
BIN_DIR="${BASE_DIR}"/bin
LOG_FILE="/tmp/nestorpi_docker.log"
ARCH="64"
DEBUG_IMAGE_NAME="nrpi_build_debug"
TEST_IMAGE_NAME="nrpi_build_test"
RELEASE_IMAGE_NAME="nrpi_build_arm${ARCH}"
DEBUG_BUILD_DIR="Debug"
RELEASE_BUILD_DIR="Release_arm${ARCH}"
NRPI_CFG_DIR=${NRPI_CFG_DIR:-"bin/cfg/debug"}

usage() {
    echo "Usage: $0 <command> [args...]"
    echo "Commands:"
    echo "  debug            Build and run the debug binary."
    echo "  cross-compile    Cross-compile the project for ARM${ARCH}."
    echo "  deploy <robot>   Cross-compile and deploy to the specified robot."
    echo "  test             Run tests in the Docker container."
    echo "  style            Fix code style issues using 'make style'."
    echo "  doc              Generate the documentation."
    echo "  mrproper         Clean the project ('make mrproper')."
    echo "  purge-docker     Remove the docker images"
    exit 1
}

check_debug_dir() {
  if [ ! -d "${BASE_DIR}/${DEBUG_BUILD_DIR}" ]; then
    echo "Please launch $0 debug first."
    exit 1
  fi
}

# Generalized Docker build function
build_docker_image() {
    local image_name=$1
    local target=$2
    echo "Building Docker container ${image_name} for ${target}..."
    docker build --target "$target" -t "$image_name" "${BASE_DIR}" > "${LOG_FILE}" 2>&1 || {
        echo -e "\\033[1;31mError building Docker image ${image_name} for ${target}:\\033[0;39m"
        cat "${LOG_FILE}"
        exit 1
    }
}

# Generalized Docker run function
run_docker_container() {
    local image_name=$1
    local command=$2
    echo "Running Docker container ${image_name} with command: ${command}"
    docker run --network host --user $(id -u):$(id -g) -e HOME=/tmp/ -t --rm -v "${BASE_DIR}:/tmp/nrpi" -w /tmp/nrpi "$image_name" bash -c "$command"
}

# Compile function with Docker
compile_debug() {
    build_docker_image "${DEBUG_IMAGE_NAME}" "build_debug"
    echo "Compiling ${DEBUG_BUILD_DIR} binary..."
    run_docker_container "${DEBUG_IMAGE_NAME}" "
        mkdir -p ${DEBUG_BUILD_DIR} &&
        cd ${DEBUG_BUILD_DIR} &&
        cmake -DCMAKE_BUILD_TYPE=${DEBUG_BUILD_DIR} -DCMAKE_TOOLCHAIN_FILE='toolchain-x86.cmake' .. &&
        make -j$(nproc) &&
        cp compile_commands.json ../"
    # compile_commands.json for neovim coc"
}

# Debug specific logic
debug() {
    compile_debug
    echo "Running debug binary..."
    run_docker_container "${DEBUG_IMAGE_NAME}" "
        ASAN_OPTIONS=alloc_dealloc_mismatch=0 \
        LSAN_OPTIONS=suppressions=lsan_suppr.txt \
        NRPI_ROOT_DIR=/tmp/nrpi/bin \
        NRPI_CFG_DIR=${NRPI_CFG_DIR} \
        bin/nestorpi_engine $@"
}

# Cross-compilation logic
cross_compile() {
    build_docker_image "${RELEASE_IMAGE_NAME}" "builder"
    echo "Cross-compiling project..."
    run_docker_container "${RELEASE_IMAGE_NAME}" "
        mkdir -p ${RELEASE_BUILD_DIR} &&
        cd ${RELEASE_BUILD_DIR} &&
        cmake -DCMAKE_BUILD_TYPE=Release -Wno-dev -DCMAKE_TOOLCHAIN_FILE='toolchain-rpi-arm${ARCH}.cmake' '..' &&
        make package -j$(nproc)"
}

# Deployment logic
deploy() {
    ROBOT=$1
    cross_compile
    echo "Deploying to robot ${ROBOT}..."
    run_docker_container "${RELEASE_IMAGE_NAME}" "
        cd ${RELEASE_BUILD_DIR} &&
        make strip"
    rsync --exclude 'log' --exclude 'records' --recursive --times --delete --progress ${BIN_DIR}/. ${ROBOT}:/usr/nestorpi_engine
}

# Testing logic
test() {
    compile_debug
    build_docker_image "${TEST_IMAGE_NAME}" "test"
    echo "Running tests..."
    run_docker_container "${TEST_IMAGE_NAME}" "
        ASAN_OPTIONS=alloc_dealloc_mismatch=0 \
        LSAN_OPTIONS=suppressions=lsan_suppr.txt \
        NRPI_ROOT_DIR=/tmp/nrpi \
        NRPI_CFG_DIR=${NRPI_CFG_DIR} \
        bin/nestorpi_engine $@ & pid=\$! &&
        sleep 10 &&
        curl http://127.0.0.1:8000/api/name | grep debug &&
        kill \$pid && wait \$pid"

    echo "Running code review, tests, and documentation generation..."
    run_docker_container "${TEST_IMAGE_NAME}" "
        cd ${DEBUG_BUILD_DIR} &&
        make code_review test"
}

# Code style check and fix
style() {
    check_debug_dir
    build_docker_image "${TEST_IMAGE_NAME}" "test"
    echo "Checking and fixing code style..."
    run_docker_container "${TEST_IMAGE_NAME}" "
        cd ${DEBUG_BUILD_DIR} &&
        make style"
}

# Generate the doc
doc() {
    check_debug_dir
    build_docker_image "${TEST_IMAGE_NAME}" "test"
    echo "Generating the doc..."
    run_docker_container "${TEST_IMAGE_NAME}" "
        cd ${DEBUG_BUILD_DIR} &&
        make doc"
}

# Clean the project
mrproper() {
    check_debug_dir
    build_docker_image "${DEBUG_IMAGE_NAME}" "build_debug"
    echo "Running mrproper ..."
    run_docker_container "${DEBUG_IMAGE_NAME}" "
        cd ${DEBUG_BUILD_DIR} &&
        make mrproper"
}

# Clean docker
purge_docker() {
    docker rmi -f "${DEBUG_IMAGE_NAME}"
    docker rmi -f "${TEST_IMAGE_NAME}"
    docker rmi -f "${RELEASE_IMAGE_NAME}"
}



main() {
    if [ "$#" -lt 1 ]; then
        usage
    fi

    if [ "$(id -u)" -eq 0 ]; then
        echo "Please do not execute this script as root." >&2
        exit 1
    fi

    case "$1" in
        debug) shift; debug "$@" ;;
        cross-compile) cross_compile ;;
        deploy) shift; deploy "$@" ;;
        test) shift; test "$@" ;;
        style) style ;;
        doc) doc ;;
        mrproper) mrproper ;;
        purge-docker)purge_docker ;;
        *) usage ;;
    esac
}

main "$@"
