# ARMv64
set(MULTIARCH "aarch64-linux-gnu")
set(RPI_ROOT_FS_FILE "rpirootfs64.tar.gz")
set(TOOLS_DIR "${CMAKE_CURRENT_LIST_DIR}/tools/")
set(RPI_ROOT_FS_DIR "${TOOLS_DIR}/rpirootfs/fs64/")

set(INCLUDE_CPP_DIR ${RPI_ROOT_FS_DIR}/usr/include/c++/12/)

set(CMAKE_SYSTEM_NAME Linux)
set(CMAKE_SYSTEM_PROCESSOR arm64)

# Root FS
set(CMAKE_SYSROOT ${RPI_ROOT_FS_DIR})
set(RPI_ROOT_FS_URL "http://franck.talbart.free.fr/${RPI_ROOT_FS_FILE}")
if(NOT EXISTS "${RPI_ROOT_FS_DIR}/lib")
  message(STATUS "Downloading the RPi root FS")
  file(MAKE_DIRECTORY ${RPI_ROOT_FS_DIR}/)
  execute_process(
    WORKING_DIRECTORY ${RPI_ROOT_FS_DIR}
    COMMAND ${SHELLCOMMAND} wget -q ${RPI_ROOT_FS_URL}
    RESULT_VARIABLE STATUS)
  if(STATUS AND NOT STATUS EQUAL 0)
    message(FATAL_ERROR "FAILED: ${STATUS}")
  endif()
  execute_process(
    WORKING_DIRECTORY ${RPI_ROOT_FS_DIR}
    COMMAND ${SHELLCOMMAND} tar xzf ${RPI_ROOT_FS_FILE}
    RESULT_VARIABLE STATUS)
  if(STATUS AND NOT STATUS EQUAL 0)
    message(FATAL_ERROR "FAILED: ${STATUS}")
  endif()
  execute_process(
    WORKING_DIRECTORY ${RPI_ROOT_FS_DIR}
    COMMAND ${SHELLCOMMAND} rm ${RPI_ROOT_FS_FILE}
    RESULT_VARIABLE STATUS)
  if(STATUS AND NOT STATUS EQUAL 0)
    message(FATAL_ERROR "FAILED: ${STATUS}")
  endif()
endif()

set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY BOTH)
set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_PACKAGE ONLY)

set(ENV{PKG_CONFIG_LIBDIR} "${CMAKE_SYSROOT}/usr/lib/${MULTIARCH}/pkgconfig/")
set(ENV{PKG_CONFIG_SYSROOT_DIR} ${CMAKE_SYSROOT})

# While cross-compiling, the linkage part may hit issues as follow: crt1.o not
# found, crti.o not found etc ... The sysroot parameter works right. The root
# cause of all this is since Wheezy, Debian adds "multiarch". That means that LD
# scripts, headers and actual libs can be in the sysroot for different
# achitectures at the same time. Old Raspbian had all libs/includes/LD scripts
# under "standard" locations like /usr/lib, /lib, /usr/include... but now, some
# of them are under routes like /usr/lib/arm-linux-gnueabihf,
# /lib/arm-linux-gnueabihf, /usr/include/arm-linux-gnueabihf... The only option
# I am left with is manually putting whatever is needed from the
# arm-linux-gnueabihf subdirs to "standard" non-multiarch locations.
execute_process(
  WORKING_DIRECTORY ${RPI_ROOT_FS_DIR}/usr/lib/${MULTIARCH}
  COMMAND sh -c "cp -nrf * ../"
  RESULT_VARIABLE STATUS)
if(STATUS AND NOT STATUS EQUAL 0)
  message(FATAL_ERROR "FAILED: ${STATUS}")
endif()
execute_process(
  WORKING_DIRECTORY ${RPI_ROOT_FS_DIR}/lib/${MULTIARCH}
  COMMAND sh -c "cp -nrf * ../"
  RESULT_VARIABLE STATUS)
if(STATUS AND NOT STATUS EQUAL 0)
  message(FATAL_ERROR "FAILED: ${STATUS}")
endif()
execute_process(
  WORKING_DIRECTORY ${RPI_ROOT_FS_DIR}/usr/include/${MULTIARCH}
  COMMAND sh -c "cp -nrf * ../"
  RESULT_VARIABLE STATUS)
if(STATUS AND NOT STATUS EQUAL 0)
  message(FATAL_ERROR "FAILED: ${STATUS}")
endif()

set(THIRD_PARTY_DIR "${CMAKE_CURRENT_SOURCE_DIR}/third_party/")
set(WEBRTC_ROOT ${THIRD_PARTY_DIR}/webrtc)
set(CIVETWEB_SRC_DIR "${THIRD_PARTY_DIR}/civetweb/src/")
if(NOT EXISTS "${CIVETWEB_SRC_DIR}")
  message(STATUS "Submodule update init")
  execute_process(
    COMMAND git submodule update --init --recursive
    WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
    RESULT_VARIABLE STATUS)
  if(STATUS AND NOT STATUS EQUAL 0)
    message(FATAL_ERROR "FAILED: ${STATUS}")
  endif()
endif()
# Download the compiler
execute_process(
  WORKING_DIRECTORY ${WEBRTC_ROOT}
  COMMAND bash sync.sh)

# The compilers must be defined here (not in CMakeLists.txt) otherwhise some
# variables are not properly set by project() Use the same compiler everywhere
# to avoid ABI compatibility issues Here, we use clang provided by webrtc
set(COMPILER_DIR ${WEBRTC_ROOT}/src/third_party/llvm-build/Release+Asserts/bin/)
set(CMAKE_C_COMPILER ${COMPILER_DIR}/clang)
set(CMAKE_CXX_COMPILER ${COMPILER_DIR}/clang++)
set(STRIP_BIN ${COMPILER_DIR}/llvm-strip)
set(AR_BIN ${COMPILER_DIR}/llvm-ar)

set(CMAKE_CXX_FLAGS "--target=aarch64-linux-gnu --sysroot=${RPI_ROOT_FS_DIR}")
set(CMAKE_C_FLAGS "${CMAKE_CXX_FLAGS}")

set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} -fuse-ld=lld")
set(CMAKE_SHARED_LINKER_FLAGS "${CMAKE_SHARED_LINKER_FLAGS} -fuse-ld=lld")
