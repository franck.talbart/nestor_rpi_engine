# x86
set(CMAKE_SYSTEM_NAME Linux)
set(CMAKE_SYSTEM_PROCESSOR x86_64)

set(THIRD_PARTY_DIR "${CMAKE_CURRENT_SOURCE_DIR}/third_party/")
set(WEBRTC_ROOT ${THIRD_PARTY_DIR}/webrtc)
set(CIVETWEB_SRC_DIR "${THIRD_PARTY_DIR}/civetweb/src/")
if(NOT EXISTS "${CIVETWEB_SRC_DIR}")
  message(STATUS "Submodule update init")
  execute_process(
    COMMAND git submodule update --init --recursive
    WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
    RESULT_VARIABLE STATUS)
  if(STATUS AND NOT STATUS EQUAL 0)
    message(FATAL_ERROR "FAILED: ${STATUS}")
  endif()
endif()
# Download the compiler
execute_process(
  WORKING_DIRECTORY ${WEBRTC_ROOT}
  COMMAND bash sync.sh)

# The compilers must be defined here (not in CMakeLists.txt) otherwhise some
# variables are not properly set by project() Use the same compiler everywhere
# to avoid ABI compatibility issues Here, we use clang provided by webrtc
set(COMPILER_DIR ${WEBRTC_ROOT}/src/third_party/llvm-build/Release+Asserts/bin/)
set(CMAKE_C_COMPILER ${COMPILER_DIR}/clang)
set(CMAKE_CXX_COMPILER ${COMPILER_DIR}/clang++)
set(STRIP_BIN ${COMPILER_DIR}/llvm-strip)
set(AR_BIN ${COMPILER_DIR}/llvm-ar)

set(CMAKE_CXX_FLAGS "--target=x86_64-linux-gnu")
set(CMAKE_C_FLAGS "${CMAKE_CXX_FLAGS}")

set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} -fuse-ld=lld")
set(CMAKE_SHARED_LINKER_FLAGS "${CMAKE_SHARED_LINKER_FLAGS} -fuse-ld=lld")
