#!/bin/sh
#************************************************************************
# NestoRPi Engine
# Copyright (C) 2015-2025 Franck Talbart
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#************************************************************************

# Author: Franck Talbart

# This sample script is used to record sensor's values into the munin monitoring
# tool.
set -euo pipefail

dev="nestor_temp_battery"
case $1 in
   config)
        cat <<'EOM'
graph_title nestor_temp_battery
graph_vlabel load
load.label load
EOM
        exit 0;;
esac

printf "load.value "
cat /dev/shm/${dev}
