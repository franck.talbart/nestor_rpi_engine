[![pipeline status](https://gitlab.com/franck.talbart/nestor_rpi_engine/badges/master/pipeline.svg)](https://gitlab.com/franck.talbart/nestor_rpi_engine/commits/master)  
[![License: GPL v3](https://img.shields.io/badge/License-GPL%20v3-blue.svg)](./LICENSE)

# NestoRPi Engine

## Introduction

The NestoRPi engine is a tool used to remotely control a Raspberry Pi (RPi). It provides the user with a very low-latency audio/video stream, as well as remote controls to manage multiple devices connected to the GPIO pins of the RPi.  Initially, it was mainly used for designing robots.

The engine supports the RPi GPU hardware encoder (using libcamera, previously MMAL) and WebRTC to provide very low-latency video (below 200 ms) in high quality (H264 1080p 30 fps).

I wrote this software in a generic way, making it easy to adapt to many use cases on the RPi (not just robots!).  
It includes a Web User Interface to control the RPi from multiple devices (PCs, phones, tablets, etc.).  
If you're interested in the robot itself, the necessary files for 3D printing are located in the `doc` directory.

You can control my robot in Paris, France, via the [Demo page](https://nestorpi.talbart.fr/).

For more details, please check out the  [Wiki](https://gitlab.com/franck.talbart/nestor_rpi_engine/-/wikis/).

## Support

Happy with the project? Buy me a coffee!  
[https://ko-fi.com/franckarts](https://ko-fi.com/franckarts)  
Thanks!

## How it works

The tool provides various facilities to remotely interact with external devices using GPIOs.  
All devices (motors, lights, ultrasound sensors, etc.) are described in a JSON config file (in the `cfg` directory) and a C++ class.

Essentially, small codes called "Tasks" are used to control these devices (either by the user or autonomously).  
For instance, one task manages relays, while another retrieves data from the MCP3008 component (analog-to-digital converter) for parameters like temperature, voltage, etc.

Adding a device and/or a task is straightforward: the user only needs to write the corresponding class (if not already available) in the appropriate directory and recompile the project.

The NestoRPi Engine runs as a service on the RPi, and users can interact with it via a web user interface.

## Installation

Debian packages are available on the website (note: they may be outdated):  
* [Releases](https://gitlab.com/franck.talbart/nestor_rpi_engine/-/releases)

To access the latest features, you can compile the project as described below.

## Directories

* `bin`: The binary file along with the required config files and web interface  
* `doc`: Electronic schematics of the robot, 3D objects, and Doxygen documentation  
* `misc`: Various utilities for tasks such as communicating with Munin or Prometheus (monitoring), creating backups, etc.  
* `src`: C++ source code  
* `third_party`: External libraries required by the engine  
* `tools`: Tools for development purposes  

## Compilation

### Prerequisites
Dependencies:
* `Docker` 

Install Docker with:

```bash
sudo apt install docker.io
sudo usermod -aG docker $USER
newgrp docker
```

Clone this repository by using git.

```bash
git clone --depth=1 https://gitlab.com/franck.talbart/nestor_rpi_engine.git
cd nestor_rpi_engine
```

### Cross-compilation from a regular computer

The project cannot be compiled directly on the RPi due to the time required.  
The code must be cross-compiled on a regular computer.

The user can either provide his own RPi root FS (see the chapter dedicated to development) or use the one included in this project (default behaviour). Building his own root FS can be useful if a specific version of glibc, or new libraries, are required to run the engine.

To compile the engine for ARM64 (RPi3, RPi4, RPi Zero 2), use:

```bash
./nrpi.sh cross-compile
```

Once done, the Debian packages are located in Release_arm64.
If for some reasons those packages can't be used, every required bin / config file are generated in the "bin" directory 
and must be sent to the RPi.


## Execution

### Prerequisites
On the RPi (for execution):
* `espeak`
* `libao4`
* `libgnutls30` (required by libcamera)  
* `libi2c0`
* `libmpg123-0`
* `libssl1.1`
* `libssl-dev` (if omitted, the user may encounter a signal 11 error)  
* `libstdc++6`
* `mbrola-fr*` (for French; use `en*` for English, etc.)

Install these with:

```bash
sudo apt install espeak libao4 libgnutls30 libi2c0 libmpg123-0 libssl1.1 libssl-dev libstdc++6 mbrola-fr*
```

### Let's go !

If installed using the Debian packages, the NestoRPi Engine should already be running (managed by systemd).  
If not, start it manually on the RPi using the following script:

```bash
sudo <nestorpi dir>/run.sh
```

The `.deb` package installs the binary in `/usr/nestorpi_engine`.

Once running, the engine can be accessed via the web user interface at:  
`http://<RPi IP>:8000`

or

`https://<RPi IP>:8000`

depending on your configuration.

### Generating a Certificate

To enable HTTPS (especially for external network access), the user must generate a certificate.  
Use the following commands:

```bash
openssl req -new -x509 -days 365 -nodes -out nestorpi.pem -keyout nestorpi.key
cat nestorpi.key >> nestorpi.pem  # Move this file to the appropriate config directory
sudo apt install libssl1.0-dev
```

### Renewing an HTTPS Certificate Using Certbot

To renew an HTTPS certificate, run the following commands:

```bash
cd scripts
./renew_certbot.sh
```

## [Optional] For development only

Install the git hooks:

```bash
scripts/set_git_hooks.sh
```

Compile in debug mode and run locally on a regular computer:

```bash
./nrpi.sh debug
```

[Optional] To build an RPi root filesystem, the following dependencies are required and must be installed on the RPi itself:  
* `gcc-arm-linux-gnueabihf` (required by libcamera)
* `libao-dev`
* `libboost-program-options-dev` (required by rpicam-apps)
* `libexif-dev` (required by rpicam-apps)
* `libgnutls28-dev` (required by libcamera)
* `libgpiod-dev`
* `libi2c-dev`
* `libjpeg-dev` (required by rpicam-apps)
* `libmpg123-dev`
* `libpng-dev`
* `libssl-dev` (required by libcamera)
* `libtiff5-dev` (required by rpicam-apps)
* `libyaml-dev` (required by libcamera)


Install them with:

```bash
sudo apt install gcc-arm-linux-gnueabihf libao-dev libboost-program-options-dev libexif-dev libgnutls28-dev libgpiod-dev libi2c-dev libjpeg-dev libmpg123-dev libpng-dev libssl-dev libtiff5-dev libyaml-dev
```

To build a home made RPi root FS from an already installed RPi:

```bash
cd tools/rpirootfs
./rpirootfs.py <user@yourpi> fs64
```

This script will run multiple rsync commands to grab the file system from the RPi target and build the root FS on the host.

NOTICE: it is important to keep in mind that the version of glibc used in the root FS must be as old as possible. Doing so will allow the generated binary to be compatible with most of the systems. If the binary is compiled with a recent glibc, the user may hit the following error message while running the binary on the target:

```bash
./nestorpi_engine: /lib/arm-linux-gnueabihf/libc.so.6: version `GLIBC_X.XX' not found (required by ./nestorpi_engine)
```


Deploy binary and config files:

```bash
./nrpi.sh deploy <robot_name>
```

For further technical information, please check out README-dev.md.

## Known Issues

*TBD*

## Warning

Although the code includes some security features (e.g., turning off GPIO pins if the connection is interrupted), it is **highly recommended** to install a watchdog on the RPi to ensure system reliability.

## Documentation

* [Doxygen Documentation](https://nestorpi.talbart.fr/doc/)
* [Wiki](https://gitlab.com/franck.talbart/nestor_rpi_engine/-/wikis/)

## Tests & Deployment

GitLab CI is used to automate testing, cross-compilation, documentation generation, and binary deployment.  
Helpful links:  
* [Demo Page](https://nestorpi.talbart.fr/)  
* [Build History](https://gitlab.com/franck.talbart/nestor_rpi_engine/pipelines)

## Contact

For any questions or assistance regarding the engine, please join the Discord channel:  
[https://discord.gg/2Zhxkfdtyq](https://discord.gg/2Zhxkfdtyq)

## License

The NestoRPi Engine is released under the GNU General Public License, version 3.  
Refer to the [LICENSE](./LICENSE) file for more details.

## Authors

The list of contributors is documented in the [AUTHORS](./AUTHORS) file.

