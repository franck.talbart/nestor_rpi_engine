## How to Make a Release

1/ Update the release numbers in `VERSION.sh`.

2/ Create a tag:

```bash
git tag vX.Y.Z
```

3/ Push the tag:

```bash
git push origin vX.Y.Z
```

4/ The CI will updload the release to the repository. That's it!

## How to Cross-Compile Without Deploying

Run the following command:

```bash
./nrpi.sh cross-compile
```

## How to Deploy (Cross-Compile and Send to the Robot)

Use the deploy command, replacing `<robot>` with your robot's name:

```bash
./nrpi.sh deploy <robot>
```

## How to Debug

1/ Run `nestorpi_engine` locally:

```bash
./nrpi.sh debug
```

2/ To debug on the robot with `gdb`, modify the following in the `CMakeLists.txt` file:
   - Comment out the `strip` command.
   - Replace `-O3` with `-O0` and add `-g` in the `CMAKE_CXX_FLAGS` (in `config.cmake`).

## How to Run Code Reviews and Tests Locally

Run the following command:

```bash
./nrpi.sh test
```

## How to Launch Doxygen

Generate the documentation using:

```bash
./nrpi.sh doc
```

## How to Fix Code Style Issues

Fix code style issues by running:

```bash
./nrpi.sh style
```

## Purge Docker

Clear unused Docker containers, images, and data with:

```bash
./nrpi.sh purge-docker
```

## Renew Certbot

Renew the HTTPS certificate using:

```bash
scripts/renew_certbot.sh
```

